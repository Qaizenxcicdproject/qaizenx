import React, { useState } from 'react';
import Navbar from './components/Navbar';
import Sidebar from './components/Sidebar';
import useSidebarHooks from './hooks/SidebarNavbarHook';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ToastContainer, toast, Zoom } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import './assets/scss/variable.scss';
import './App.scss';
import './assets/font/NunitoSans/NunitoSans.css';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { useTranslation } from 'react-i18next';
import AppContext from './store/AppContext';
import LANGUAGES from "constants/languages";
import InnerRoutes from 'router/InnerRoutes';
import { appAccess } from 'FeatureAccess';
import EziLoader from 'components/EziLoader';
import { connect } from "react-redux";
const Login = React.lazy(() => import('pages/Login/Login'));
const ForgotPassword = React.lazy(() => import('pages/ForgetPassword/ForgotPassword'));
const CreatePassword = React.lazy(() => import('pages/ForgetPassword/CreatePassword'));
const FeedbackSurvey = React.lazy(() => import('pages/FeedbackSurvey/FeedbackSurvey'));
const ParticipantLogin = React.lazy(() => import('pages/ParticipantLogin/ParticipantLogin'));
const CompanyLogin = React.lazy(() => import('pages/CompanyLogin'));


function App({ appState }) {
	/**
	 * Initial App state for login, ACL Matrix
	 */
	const [translate, setTranslate] = useTranslation();
	const [isLoading, setIsLoading] = useState(false);
	const sidebarData = useSidebarHooks();

	/**
	 * Global Loader
	 */
	const globalLoader = () => {
		return {
			show: () => {
				setIsLoading(true)
			},
			hide: () => {
				setIsLoading(false)
			}
		}
	}

	/**
	 * Notify Access warning
	 */
	const handleUnAuthWarn = () => {
		toast.warn("You are not authorized to access this feature!")
	}

	/**
	 * Initial Language Data.
	 */
	const languageObj = {
		curLang: localStorage.getItem("i18nextLng"),
		languages: LANGUAGES,
		changeLang: (langCode = "En") => setTranslate.changeLanguage(langCode),
		translate: translate
	}

	return (
		<AppContext.Provider value={{ appState, languageObj, handleUnAuthWarn, accesFeature: appAccess(appState.access_matrix), EziLoader: globalLoader() }}>
			<Router>
				<Switch>
					<Route exact path="/login" render={props => <Login {...props} />} />
					<Route exact path="/company-login/:login_token/:express_survey_id?/:redirect_to?" render={props => <CompanyLogin {...props} />} />
					<Route exact path="/reset-password-link" component={ForgotPassword} />
					<Route exact path="/reset-password/:user_id/:token" component={CreatePassword} />
					<Route exact path="/feedback/:survey_id/:company_id?/:respondant_id?" component={FeedbackSurvey} />
					<Route exact path="/participant-login" component={ParticipantLogin} />
					<React.Fragment>
						<div className="main-wrapper">
							<Sidebar sidebarPara={sidebarData} />
							<Navbar navbarPara={sidebarData} />
							<div className="inner-wrapper">
								<InnerRoutes />
							</div>
							<div className={`overlay ${sidebarData.sidebar ? "active" : ""}`} onClick={sidebarData.overlaySidebar} ></div>
						</div>
					</React.Fragment>
				</Switch>
			</Router>
			<ToastContainer transition={Zoom} position={toast.POSITION.TOP_RIGHT} closeButton={false} hideProgressBar={true} />
			{isLoading && <EziLoader />}
		</AppContext.Provider>
	);
}

const mapStateToProps = state => {
	return {
		appState: state.app.appState,
		language: state.app.language,
	}
}

export default connect(mapStateToProps)(App);