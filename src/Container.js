import React, { Fragment, useState, useEffect } from 'react';
import App from "./App"
import { connect } from "react-redux";
import * as AppActions from "store/actions";
import Http from "axios";
import PageNotFound from 'components/PageNotFound';
import AppLoading from 'components/AppLoading';
import configURL from 'config/config';
import FormData from 'utility/AppFormData';

const Container = ({ dispatchVenderInfo }) => {
    const [loading, setLoading] = useState(true)
    const [isVerified, setIsVerified] = useState(false)

    /**
     * Update Document title and favicon
     * @param {String} title 
     * @param {String} logo
     */
    const updateAppMeta = (title = null, logo = null) => {
        if (logo && logo !== "") {
            let link = document.querySelector("link[rel*='icon']")
            link.rel = 'icon';
            link.href = logo
        }
        if (title && title !== "") {
            document.title = title
        }
    }

    /**
     * Vendor verification
     */
    const verifyVendorDetails = () => {
        let vendorName;
        if (window.location.host || window.location.hostname) {
            vendorName = window.location.hostname
            if (!vendorName) vendorName = window.location.host
            let formData = new FormData()
            formData.append('type', 'company')
            formData.append('name', 'fristcompany.app.teamqaizenx.com')
            Http.post(configURL.verify_vendor, formData).then(response => {
                if (response.data.success && response.data.success === true) {
                    setLoading(false)
                    setIsVerified(true)
                    updateAppMeta(response.data.vendorName, response.data.logoUrl)
                    dispatchVenderInfo({ name: response.data.vendorName, logo: response.data.logoUrl, webaddress: vendorName })
                } else {
                    setLoading(false)
                    setIsVerified(false)
                }
            }).catch(err => {
                console.log(err);
                setLoading(false)
                setIsVerified(false)
            })
        }
    }

    useEffect(verifyVendorDetails, []);

    return (
        <Fragment>
            {(!loading && !isVerified) && <PageNotFound message='Page not found' />}
            {isVerified && <App name="Qaizenx" />}
            {loading && <AppLoading message='Loading' />}
        </Fragment>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        dispatchVenderInfo: (info) => dispatch(AppActions.initVendor(info))
    }
}
export default connect(null, mapDispatchToProps)(Container);