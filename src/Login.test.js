import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import Login from './pages/Login/Login';
import { Provider } from "react-redux";
import store from 'store/store';
import { waitFor } from '@testing-library/react';


configure({ adapter: new Adapter() });

describe('Login Component', () => {
	it('Username Check', async () => {

		let wrapper;
		wrapper = mount(<Provider store={store}><Login /></Provider>);
		const event = { target: { name: 'userId', value: 'usstest111@gmail.com' } };
		wrapper.find('input[type="text"]').simulate("change", event);
		await waitFor(() => {
			expect(wrapper.find('input[type="text"]').props("value").value).toBe('usstest111@gmail.com')

		});
	});
	it('Password check', async () => {
		let wrapper;
		wrapper = mount(<Provider store={store}><Login /></Provider>);
		const event = { target: { name: 'password', value: 'Gpit@1234' } };
		wrapper.find('input[name="password"]').simulate("change", event);
		await waitFor(() => {
			expect(wrapper.find('input[name="password"]').props("value").value).toBe('Gpit@1234')
		});
	});
	it('On submit button ', async () => {
		let wrapper;
		const mockCallBack = jest.fn();
		wrapper = mount(<Provider store={store}><Login onSubmit={mockCallBack} /></Provider>);
		wrapper.find('form').simulate('submit');
		await waitFor(() => {
			expect(wrapper.find('form').simulate('submit').length).toEqual(1);
		});
	});
});