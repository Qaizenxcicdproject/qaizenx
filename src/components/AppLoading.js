import React from 'react';

const AppLoading = (props) => {
    return (
        <p className="app_loading">{props.message || 'App is Loading'}<span>.</span><span>.</span><span>.</span></p>
    )
}
export default AppLoading