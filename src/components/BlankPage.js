import React from 'react';

const BlankPage = (props) => {
    return (
        <div className="page-not-found-component">
            <div className="no-result-svg-wrap">
                <img src = {require("../assets/images/no-result-found.svg")}/>
                <span className="unauthorised-result-text">{props.title}</span>
            </div>
        </div>
    )
}
export default BlankPage;