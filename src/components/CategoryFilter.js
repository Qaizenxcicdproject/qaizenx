import React, { useState, useRef } from 'react';
import useOutsideClick from '../hooks/useOutsideClick';
import { useTranslation } from 'react-i18next';
import { useEffect } from 'react';
import InnerLoader from './InnerLoader';
import "../assets/scss/_category_filter.scss";

const CategoryFilter = (props) => {
    const [loading, setLoading] = useState(false);
    const [serching, setSerching] = useState(false);
    const [data, setData] = useState([...props.data]);
    const [filteredData, setFilteredData] = useState(props.data);
    const [searchTerm, setSearchTerm] = useState("");
    const [translate] = useTranslation();
    const ref = useRef();
    let searchTimer = null;

    /**
     * Search Catrgories.
     * 
     * @param {object} target Javascript Event target. 
     */
    const searchCategory = ({ target }) => {
        setLoading(true)
        let inputValue = target.value;
        clearTimeout(searchTimer);
        setSearchTerm(inputValue);
        searchTimer = setTimeout(() => {
            if (inputValue.length > 0) {
                const filtedData = data.filter(item => item.name.toLowerCase().includes(inputValue.toLowerCase()))
                setFilteredData(filtedData);
            } else {
                setFilteredData(data);
            }
            setLoading(false);
        }, 1000);
    }

    const handleSelect = (data = {}) => {
        props.handleSelect(data);
        setSearchTerm("");
        setSerching(false)
    }

    useOutsideClick(ref, () => {
        serching && setSerching(false);
    });

    useEffect(() => {
        let filters = [...props.data];
        setData(filters);
        setFilteredData(filters);
    }, [props.data])

    return (
        <div className="category-search" >
            <button type="button" onClick={() => setSerching(!serching)} className={`category-dropdown-btn ${props.defaultSelected ? "selected" : ""}`}> {props.defaultSelected} </button>
            <div className={`category-inner-dropdown ${serching ? 'active' : ""}`} ref={ref}>
                <div className="category-dropdown-header">
                    <input type="text" value={searchTerm} className="category-dropdown-search" onChange={(e) => searchCategory(e)} placeholder={translate('Search.1')} />
                    <label>{props.defaultSelected}</label>
                </div>
                <ul>
                    {
                        filteredData.map(item =>
                            <li onClick={() => handleSelect(item)} key={item.id}>
                                {item.name}
                            </li>
                        )
                    }
                </ul>
                {loading && <InnerLoader />}
            </div>
        </div>
    )

}

export default CategoryFilter;