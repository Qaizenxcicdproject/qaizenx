import React , { useRef } from 'react';
import { Popover , OverlayTrigger } from 'react-bootstrap';

const EziPopover = (props) =>{

    const closePopover = useRef(null);

    return(
      <OverlayTrigger trigger="click" placement="right" overlay={<Popover className="ezi-popover-box"><Popover.Content>{props.message}</Popover.Content><button className="popover-close-btn" type="button" onClick={() => closePopover.current.click()}>close</button></Popover>} > 
        <button type="button" className="ezi-popover-btn" ref={closePopover}>Popover</button> 
      </OverlayTrigger>
    )
  }

export default EziPopover;