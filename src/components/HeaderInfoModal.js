import React from 'react';
import { Modal } from 'react-bootstrap';

const InfoModal = (props) => (
    <React.Fragment>
        <Modal {...props} size="md" aria-labelledby="ezi-modal-for" centered className="ezi-modal-wrapper info-alert_c widgetinfo" >
            <Modal.Header className="ezi-modal-header" closeButton>
                <Modal.Title id="ezi-modal-for" className="ezi-modal-title-wrap" >
                    <span className="ezi-modal-header-title">{props.title}</span>
                    <span className="ezi-modal-close" onClick={props.onHide}></span>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p className="header-info-description">{props.description}</p>
            </Modal.Body>
        </Modal>
    </React.Fragment>
)
export default InfoModal;