import React from 'react';
import { Modal } from 'react-bootstrap';
import './../assets/scss/iframeinfomodal.scss';

const IframeInfoModel = (props) => {
    return (
        <Modal show={props.show} onHide={props.onHide} className="iframeModel" >
            <Modal.Header closeButton>
            </Modal.Header>
            <Modal.Body>
                <iframe title={props.title} src={props.src} width={props.width} height={props.height} frameBorder="0"></iframe>
            </Modal.Body>
        </Modal>
    );
}

export default IframeInfoModel;
