import React, { useEffect, useState } from 'react';

const MultiCheckbox = (props) => {
    const [checkedCheckbox, setCheckedCheckbox] = useState(null);
    useEffect(() => {
        let checkedArrayNewVal = props.checked;
        if (checkedArrayNewVal.length > 0) {
            let selectedIndex = checkedArrayNewVal.findIndex(item => item.ques_comment_id === props.value);
            if (selectedIndex !== -1 && selectedIndex <= 19) {
                setCheckedCheckbox(true);
            }
            else if (checkedArrayNewVal.length >= 20) {
                setCheckedCheckbox(false);
            }
            else {
                setCheckedCheckbox(null);
            }
        }
        else {
            setCheckedCheckbox(null);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.checked, props.onChange])
    return (
        <React.Fragment>
            <input
                type={props.type}
                name={props.name}
                checked={checkedCheckbox}
                onChange={props.onChange}
                value={props.value}
                className={props.class}
            />
        </React.Fragment>
    )
}

export default MultiCheckbox;