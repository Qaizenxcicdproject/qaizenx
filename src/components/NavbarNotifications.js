import React, { useState, useRef, useEffect } from 'react';
import useOutsideClick from 'hooks/useOutsideClick';
import { useHistory } from "react-router-dom";
import Axios from "utility/Axios";
import configURL from "config/config";

const NavbarNotifications = () => {
    const [notificationOpen, setNotificationOpen] = useState(false)
    const [notifications, setNotifications] = useState([])
    const notificationRef = useRef()
    const history = useHistory();

    useOutsideClick(notificationRef, () => {
        notificationOpen && setNotificationOpen(false)
    })

    const getAllNotifications = () => {
        let formData = new FormData()
        formData.append("per_page", 5)
        formData.append("status", 'unread')
        Axios.post(configURL.get_notifications, formData).then(response => {
            if (response.data.success === true && response.data.results.length > 0) {
                setNotifications(response.data.results)
            }
        }).catch(err => {
            console.log(err);
        })
    }

    const handleReadAll = () => {
        // const notificationIds = notifications.map(el => el.id)
        // let formData = new FormData()
        // formData.append("notification_ids", JSON.stringify(notificationIds))
        // Axios.post(configURL.update_notifications, formData).catch(err => {
        Axios.post(configURL.update_notifications).catch(err => {
            console.log(err);
        })
        setNotificationOpen(false)
    }

    useEffect(() => {
        if (!notificationOpen) {
            setNotifications([])
        }
    }, [notificationOpen])

    useEffect(() => {
        getAllNotifications()
    }, [])

    return (
        <div className={`notification ${(notificationOpen) ? 'notifications' : ''}`} ref={notificationRef}>
            <span className={`noti-icon ${notifications.length > 0 ? 'has-noti' : ''}`} onClick={() => setNotificationOpen(true)}></span>
            <div className={`notifications-wrapper`}>
                <ul className="notifications-lists">
                    {notifications.map((el, i) => (
                        <li key={i}>
                            <div className="messages-wrapper">
                                <div className="user-icon"></div>
                                <div className="user-response">
                                    <p className="user-name">{el.user_name || 'Anonymous'}</p>
                                    <span className="user-msg">{el.message}</span>
                                </div>
                            </div>
                        </li>
                    ))}
                    {notifications.length === 0 && <p className="no-result">No new notifications.</p>}
                    <div className="load-more-container">
                        {notifications.length > 0 && <button className="mark-read" type="button" onClick={handleReadAll}>Mark all read</button>}
                        <button type="button" className="view-all" onClick={() => {
                            setNotificationOpen(false)
                            history.push('/notifications')
                        }}>View all</button>
                    </div>
                </ul>

            </div>
        </div >
    )
}

export default NavbarNotifications