import React from "react";

const NoResult = () => (
    <div className="no-result-svg-wrap">
        <img alt="No result found" src={require("../assets/images/no-result-found.svg")} />
        <span className="common-no-result-text">Not Found</span>
    </div>
)

export default NoResult;