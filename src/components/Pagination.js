import React from 'react';

const Pagination = ({ perPage, total, paginate, currentPage }) => {	
	const pageNumbers = [];
	let lastPage = 0;
	let maxPages = 4;
	let totalPages = Math.ceil(total / perPage);
	let maxLeft = (currentPage - Math.floor(maxPages / 2))
	let maxRight = (currentPage + Math.floor(maxPages / 2))
	console.log(maxLeft, maxRight, currentPage);
	if (maxLeft < 1) {
		maxLeft = 1
		maxRight = maxPages
	}
	if (maxRight > totalPages) {
		maxLeft = totalPages - (maxPages - 1)
		if (maxLeft < 1) {
			maxLeft = 1
		}
		maxRight = totalPages
	}
	console.log(maxLeft, maxRight, currentPage);
	for (let i = maxLeft; i <= maxRight; i++) {
		lastPage = i;
		pageNumbers.push(i);
	}
	let nextNum = currentPage + 1;
	let prevNum = currentPage - 1;
	return (total !== undefined && total > perPage) ? (
		<div className="pagination-wrap">
			<ul className='pagination'>
				<li className="prev-item">
					<button disabled = { currentPage === 1 } onClick={() => paginate(prevNum)} className='page-link'>Prev</button>
				</li>
				{pageNumbers.map(number => (
					<li key={number} className={`page-item ${(currentPage == number) ? "active" : ""}`}>
						<div onClick={() => paginate(number)} className='page-link'>
							{number}
						</div>
					</li>
				))}
				<li className="next-item">
					<button onClick={() => paginate(nextNum)} className='page-link' disabled = { currentPage === lastPage }>Next</button>
				</li>
			</ul>
		</div>
	) : null;
};

export default Pagination;