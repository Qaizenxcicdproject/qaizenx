import React, { Fragment } from "react";

const SearchInput = (props) => (
    <Fragment>
        <div className="dt-table-search-wrap">
            <input type="text" className="dt-table-search" onChange={(e)=>props.change(e)} placeholder ={props.placeholder}/>
        </div>
    </Fragment>
);
export default SearchInput;
