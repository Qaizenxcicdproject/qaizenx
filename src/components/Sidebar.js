import React, { useContext } from 'react';
import '../assets/scss/sidebar.scss';
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import AppContext from 'store/AppContext';

const Sidebar = ({ sidebarPara, appState }) => {

    const { languageObj = {} } = useContext(AppContext);
    const appAccesses = appState.access_matrix || {};
    const rootPages = Object.keys(appAccesses);
    const collection = ["templates", "themes", "peoples", "question_bank"];
    const settings = ["company_profile", "account_management"];
    /*eslint-disable */
    return (
        <React.Fragment>
            <nav id="sidebar" className={`sidebar ${sidebarPara.sidebar ? "active" : ""}`}>
                <div className="sidebar-navigation">
                    <button type="button" className="humburger-button" onClick={sidebarPara.overlaySidebar}>
                        <img alt="" src={require(`../assets/images/sidebar/humburger.svg`)} className="humburger-logo" />
                    </button>
                    <div className="navigation-text">{languageObj.translate('Navigation.1')}</div>
                </div>
                <ul className="sidebar-list">

                    <li className="sidebar-item">
                        <NavLink activeClassName='link-active' to="/" onClick={sidebarPara.overlaySidebar} className="side-icon dashboard">
                            {languageObj.translate('Dashboard.1')}
                        </NavLink>
                    </li>
                    {rootPages.includes("survey") && appAccesses["survey"] !== undefined && appAccesses["survey"].includes("READ") &&
                        <li className="sidebar-item">
                            <NavLink activeClassName='link-active' to="/survey-dashboard" onClick={sidebarPara.overlaySidebar} className="side-icon surveys">
                                {languageObj.translate('Surveys.1')}
                            </NavLink>
                        </li>}
                    {rootPages.includes("report_dashboard") && appAccesses["report_dashboard"] !== undefined && appAccesses["report_dashboard"].includes("READ") &&
                        <li className={`sidebar-item`}>
                            <NavLink activeClassName='link-active' to="/reports" onClick={sidebarPara.overlaySidebar} className="side-icon reports">
                                {languageObj.translate('Reports.1')}
                            </NavLink>
                        </li>}
                    {collection.some(item => rootPages.includes(item)) &&
                        <><li className={`sidebar-item submenu ${(sidebarPara.menuOpen === 'collection') ? "active" : ""}`}>
                            <a onClick={() => sidebarPara.openMenu('collection')} className="side-icon collection">
                                {languageObj.translate('Collection.1')}
                            </a>
                        </li>
                            <div className={`sidebar-submenu-item ${(sidebarPara.menuOpen === 'collection') ? "collection-active" : ""}`}>
                                {rootPages.includes("peoples") && appAccesses["peoples"] !== undefined && appAccesses["peoples"].includes("READ") && <li className="sub-sidebar-item">
                                    <NavLink activeClassName='link-active' to="/people-directory" onClick={sidebarPara.overlaySidebar} className="side-icon people">
                                        {languageObj.translate('People.1')}
                                    </NavLink>
                                </li>}
                                {rootPages.includes("templates") && appAccesses["templates"] !== undefined && appAccesses["templates"].includes("READ") && <li className="sub-sidebar-item">
                                    <NavLink activeClassName='link-active' to="/template-dashboard" onClick={sidebarPara.overlaySidebar} className="side-icon templates">
                                        {languageObj.translate('Templates.1')}
                                    </NavLink>
                                </li>}

                                {rootPages.includes("themes") && appAccesses["themes"] !== undefined && appAccesses["themes"].includes("READ") && <li className="sub-sidebar-item">
                                    <NavLink activeClassName='link-active' to="/themes" onClick={sidebarPara.overlaySidebar} className="side-icon themes">
                                        {languageObj.translate('Themes.1')}
                                    </NavLink>
                                </li>}
                            </div></>}
                    {settings.some(item => rootPages.includes(item)) &&
                        <><li className={`sidebar-item submenu ${(sidebarPara.menuOpen === 'setting') ? "active" : ""}`}>
                            <a onClick={() => sidebarPara.openMenu('setting')} className="side-icon settings">
                                {languageObj.translate('Settings.1')}
                            </a>
                        </li>
                            <div className={`sidebar-submenu-item ${(sidebarPara.menuOpen === 'setting') ? "setting-active" : ""}`}>
                                {rootPages.includes("company_profile") && appAccesses["company_profile"] !== undefined && appAccesses["company_profile"].includes("READ") && <li className="sub-sidebar-item">
                                    <NavLink activeClassName='link-active' to="/company-profile" onClick={sidebarPara.overlaySidebar} className="side-icon company-profile">
                                        {languageObj.translate('CompanyProfile.1')}
                                    </NavLink>
                                </li>}
                                {rootPages.includes("account_management") && appAccesses["account_management"] !== undefined && appAccesses["account_management"].includes("READ") && <li className="sub-sidebar-item">
                                    <NavLink activeClassName='link-active' to="/users-roles" onClick={sidebarPara.overlaySidebar} className="side-icon user-roles">
                                        {languageObj.translate('UserManagement.1')}
                                    </NavLink>
                                </li>}
                                <li className="sub-sidebar-item">
                                    <NavLink activeClassName='link-active' to="/survey-workflow" onClick={sidebarPara.overlaySidebar} className="side-icon survey-workflow">
                                        Survey Workflows
                                    </NavLink>
                                </li>
                                <li className="sub-sidebar-item">
                                    <NavLink activeClassName='link-active' to="/kpi-settings" onClick={sidebarPara.overlaySidebar} className="side-icon kpi-setting">
                                        KPI Setting
                                    </NavLink>
                                </li>
                                <li className="sub-sidebar-item">
                                    <NavLink activeClassName='link-active' to="/recurring-setting" onClick={sidebarPara.overlaySidebar} className="side-icon recurring">
                                        Recurring Setting
                                    </NavLink>
                                </li>
                                <li className="sub-sidebar-item">
                                    <NavLink activeClassName='link-active' to="/smtp-settings" onClick={sidebarPara.overlaySidebar} className="side-icon smtp-setting">
                                        SMTP Setting
                                    </NavLink>
                                </li>

                            </div></>}

                    {rootPages.includes("action_planning") && appAccesses["action_planning"] !== undefined && appAccesses["action_planning"].includes("READ") &&
                        <li className={`sidebar-item`}>
                            <NavLink activeClassName='link-active' to="/action-plan" onClick={sidebarPara.overlaySidebar} className="side-icon action">
                                {languageObj.translate('ActionPlan.1')}
                            </NavLink>
                        </li>}
                    <li className="sidebar-item">
                        <a className="side-icon support" href="https://support.qaizenx.com" target="_blank" rel="noopener noreferrer">{languageObj.translate('Support.1')}</a>
                    </li>

                </ul>
                <div className="copyright-text">
                    © {new Date().getFullYear()} QaizenX &reg;. <br />All Rights Reserved.
                </div>
            </nav>
        </React.Fragment>

    )
}

const mapStateToProps = state => {
    return {
        appState: state.app.appState,
    }
}
/*eslint-enable */
export default connect(mapStateToProps)(Sidebar);

