import React, { useState , useEffect} from "react";

export default function Toggle(props) {
  const [toggleState, setToggleState] = useState("off");
  const [toggleFlag, setToggleFlag] = useState(false);

  function handleToggle() {
    setToggleState(toggleState === "off" ? "on" : "off");
    setToggleFlag(props.toggleFlag)
  }



  return <div className={`ezi-switch-toggle ${toggleState}`} onClick={props.handleToggle} />;
}
