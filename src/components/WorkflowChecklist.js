import React, { Fragment } from "react";
import { Modal } from 'react-bootstrap';

const WorkflowChecklist = ({ show = false, onHide, onSubmit, workFlowData = {} }) => {
    const { selected_survey = {}, selected_identifier = {}, trigger_data = {}, ...restProps } = workFlowData

    const timeConvert = (time) => {
        let ts = time;
        let H = +ts.substr(0, 2);
        let h = (H % 12) || 12;
        h = (h < 10) ? ("0" + h) : h;  // leading 0 at the left for 1 digit hours
        let ampm = H < 12 ? " AM" : " PM";
        ts = h + ts.substr(2, 3) + ampm;
        return ts;
    }

    return (
        <Modal show={show} size="md" centered className="theme-modal-wrapper checklist-modal" onHide={onHide} >
            <Modal.Header className="ezi-modal-header">
                <Modal.Title className="theme-modal-title ezi-modal-header-title" >
                    <span className="theme-modal-title-text">Workflow Checklist For {selected_survey.label || ''}</span>
                    <span className="ezi-modal-close" onClick={onHide}></span>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Fragment>
                    <div className="checklist-label-wrap">
                        <label className="checklist-label">Workflow Name</label>
                        <span className="checklist-info survey-name">{restProps.workflow_name || ''}</span>
                    </div>
                    <div className="checklist-label-wrap">
                        <label className="checklist-label">Starting Date</label>
                        <span className="checklist-info">{restProps.start_date.toDateString() || ''}</span>
                    </div>
                    <div className="checklist-label-wrap">
                        <label className="checklist-label">Ending On</label>
                        <span className="checklist-info">{restProps.end_date.toDateString() || ''}</span>
                    </div>
                    <div className="checklist-label-wrap">
                        <label className="checklist-label">Selected Identifier</label>
                        <span className="checklist-info">{(selected_identifier && selected_identifier.label) ? selected_identifier.label.toUpperCase() : ''}</span>
                    </div>
                    <div className="checklist-label-wrap">
                        <label className="checklist-label">Trigger Action</label>
                        <span className="checklist-info">
                            {(trigger_data && trigger_data.action_type) ? trigger_data.action_type.toUpperCase() : ''}
                            {(trigger_data && trigger_data.action_value) ? trigger_data.action_value : ''}
                            {(selected_identifier.type === 'datetime' && trigger_data && trigger_data.action_value) && ' Days'}
                        </span>
                    </div>

                    <div className="checklist-label-wrap">
                        <label className="checklist-label">Trigger Time</label>
                        <span className="checklist-info">{(trigger_data && trigger_data.trigger_time) ? timeConvert(trigger_data.trigger_time) : ''}</span>
                    </div>
                </Fragment>
            </Modal.Body>
            <Fragment>
                <Modal.Footer>
                    <button type="button" className="checklist-modal-close" onClick={onHide}> Cancel </button>
                    <button type="button" className="btn-ripple ezi-pink-btn checklist-modal-submit" onClick={onSubmit}> Confirm</button>
                </Modal.Footer>
            </Fragment>
        </Modal>
    );
}

export default WorkflowChecklist