import React, { useEffect, useState } from "react";
import StepsIndicator from './StepsIndicator'

const MultiStepContent = ({ steps = [], defaultData: existingData = {} }) => {
    const [currentStep, setCurrentStep] = useState(0)
    const [defaultData, setDefaultData] = useState(existingData)
    const stepsIterator = steps.map(el => el.label)
    const CurrentStep = steps[currentStep].component || null

    /**
     * Save Form Global state Data
     * @param {Object} data State Data
     */
    const saveStateData = (data) => {
        setDefaultData({ ...defaultData, ...data })
    }

    /**
     * Handle next page navigation on parent cantainer
     */
    const handleNext = () => {
        let totalStep = steps.length
        let calcStep = currentStep + 1
        if (calcStep < totalStep) {
            setCurrentStep(currentStep + 1)
        }
    }

    /**
     * Handle previous page navigation on parent cantainer
     */
    const handlePrev = () => {
        setCurrentStep((currentStep > 0) ? currentStep - 1 : 0)
    }

    useEffect(() => {
        setDefaultData(existingData)
    }, [existingData])

    return (
        <div className="step-form-wrapper">
            <StepsIndicator currentStep={currentStep} allSteps={stepsIterator} />
            <div className="step-form-content">
                <CurrentStep saveState={saveStateData} stateData={defaultData} nextStep={handleNext} prevStep={handlePrev} />
            </div>
        </div>
    )
}

export default MultiStepContent;