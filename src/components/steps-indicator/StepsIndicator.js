import React from "react";

/**
 * 
 * @param {Object} param Steps data indicator
 */
const StepsIndicator = ({ currentStep = 0, allSteps = [] }) => (
    <div className="step-indicator">
        <ul className="progressbar">
            {allSteps.map((el, i) => <li key={i + 1} className={`${(currentStep === i || i < currentStep) ? 'active' : ''}`}>{el}</li>)}
        </ul>
    </div>
)

export default StepsIndicator