import React, { useContext, useEffect, useState } from 'react';
import AppContext from 'store/AppContext';
import { useHistory } from 'react-router-dom';
import configURL from 'config/config';
import Axios from "utility/Axios";
import { confirmAlert } from 'react-confirm-alert';
import { toast } from "react-toastify";
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import AddNewParticipantModal from 'pages/Survey/CategorySurveyDashboard/AddNewParticipantModal';
import FormData from 'utility/AppFormData';
import InnerLoader from 'components/InnerLoader';
import "./RecentCards.scss";

const RecentSurveyCard = () => {
    const { languageObj = {}, handleUnAuthWarn = {}, accesFeature = {} } = useContext(AppContext)
    const history = useHistory()
    const [addParticipantMeta, setAddParticipantMeta] = useState({ survey_id: null, visible: false });
    const [recentSurveys, setRecentSurveys] = useState([]);
    const [loading, setLoading] = useState(false);

    const handleSurveyAnalyze = (surveyId) => {
        let notificationId = toast.info('Loading Survey information..', { position: "bottom-right" })
        let formData = new FormData()
        formData.append('survey_id', surveyId)
        Axios.post(configURL.CheckSurveyData, formData).then(response => {
            toast.dismiss(notificationId)
            if (response.data.success !== undefined && response.data.success === true) {
                history.push(`/customer-experience/${surveyId}`, { survey_id: surveyId });
            } else {
                confirmAlert({
                    title: 'Analyse Survey',
                    message: 'Data is not pulled for reporting, please try again after some time',
                    buttons: [{ label: 'I Understood' }, { label: 'Close' }]
                });
                return;
            }
        })
    }

    useEffect(() => {
        setLoading(true)
        Axios.post(configURL.get_dashboard_survey_highlights, {}).then(response => {
            setLoading(false)
            if (response.data.success !== undefined && response.data.success === true) {
                setRecentSurveys(response.data.results.recent_surveys || [])
            }
        }).catch((error) => {
            setLoading(false)
            console.log(error);
        })
    }, [])

    return (
        <section className="recent-highlight-main-wrap">
            <AddNewParticipantModal
                show={addParticipantMeta.visible}
                onHide={() => setAddParticipantMeta({ survey_id: null, visible: false })}
                survey_id={addParticipantMeta.survey_id}
            />
            <div className="recent-surveys-card">
                <div className="recent-surveys-card-header">{languageObj.translate('RecentSurveys.1')}</div>
                <div className="recent-surveys-card-body">
                    <ul>
                        {
                            recentSurveys && recentSurveys.map((item, index) =>
                                <li key={`recent-surveys${index}`} className={(item.status === 'close') ? 'closed' : item.status.toLowerCase() || 'draft'}>
                                    <span className="recent-surveys-index">{index + 1}</span>
                                    <span className={`recent-surveys-name ${(item.status === 'close') ? 'closed' : item.status.toLowerCase() || 'draft'}`}>{item.name}</span>
                                    <span className={`recent-surveys-status ${(item.status === 'close') ? 'closed' : item.status.toLowerCase() || 'draft'}`}>
                                        {(item.status === 'close') ? 'closed' : item.status || 'draft'}
                                    </span>
                                    <button type="button" className={`recent-surveys-report ${accesFeature.analyze_survey || " access_lock"}`} onClick={() => {
                                        if (accesFeature.create_report || accesFeature.edit_report || accesFeature.analyse_report) {
                                            handleSurveyAnalyze(item.id)
                                        } else {
                                            handleUnAuthWarn()
                                        }
                                    }}>Analyse</button>
                                    {item.surveyType &&
                                        <OverlayTrigger overlay={<Tooltip>Add Participants</Tooltip>}>
                                            <button type="button" className="recent-surveys-report_add" disabled={(item.surveyType.toLowerCase() === 'opensurvey' || item.status.toLowerCase() === 'close')} onClick={() => {
                                                if (accesFeature.edit_survey || accesFeature.create_survey) {
                                                    setAddParticipantMeta({ survey_id: item.id, visible: true })
                                                } else {
                                                    handleUnAuthWarn()
                                                }
                                            }
                                            }>
                                                <span className="add_participant">Add Participants </span>
                                            </button>
                                        </OverlayTrigger>
                                    }
                                    {!item.surveyType && <button type="button" className="recent-surveys-report_add" disabled>
                                        <span className="add_participant">Add Participants </span>
                                    </button>}
                                </li>
                            )
                        }
                    </ul>
                    {recentSurveys && recentSurveys.length === 0 && <span className="no-survey">{languageObj.translate('NoRecentSurveysAvailable.1')}</span>}
                </div>
                {loading && <InnerLoader />}
            </div>
        </section>
    )
}

export default RecentSurveyCard;