import React, { useContext, useEffect, useState } from 'react';
import AppContext from 'store/AppContext';
import configURL from 'config/config';
import Axios from "utility/Axios";
import { toast } from "react-toastify";
import InnerLoader from 'components/InnerLoader';
import { useHistory } from 'react-router-dom';
import "./RecentCards.scss";

const RecentTemplateCard = () => {
	const { languageObj = {}, handleUnAuthWarn = {}, accesFeature = {} } = useContext(AppContext)
	const [recentTemplates, setRecentTemplates] = useState([]);
	const [loading, setLoading] = useState(false);
	const history = useHistory()

	/**
	 * Get Survey widget counts
	 */
	const getRecentTemplates = () => {
		setLoading(true)
		Axios.post(configURL.template_dashboard_initials, {}).then(response => {
			setLoading(false)
			if (response.data.success !== undefined && response.data.success) {
				setRecentTemplates(response.data.results.recent_templates ? response.data.results.recent_templates : []);
			} else {
				toast.warn(response.data.message)
			}
		}).catch(err => {
			console.log(err);
			setLoading(false)
		})
	}

	const handleCreateSurveyCat = (template_id = null) => {
		if (accesFeature.create_survey && template_id) {
			let formData = new FormData();
			formData.append("template_id", template_id)
			Axios.post(configURL.create_survey_cat_by_template, formData).then(response => {
				if (response.data.success !== undefined && response.data.success) {
					let result = response.data.result
					history.push(`/survey-dashboard/add-survey/${result.category_id}`, {
						template_id: template_id,
						action: "add-survey",
						category_name: result.category_name,
						survey_type: 'EXPRESS'
					});
				} else {
					toast.warn(response.data.message)
				}
			})
		} else {
			handleUnAuthWarn()
		}
	}

	useEffect(() => {
		getRecentTemplates();
	}, [])

	return (
		<section className="recent-highlight-main-wrap recent-highlight-template" >
			<div className="recent-surveys-card">
				<div className="recent-surveys-card-header">{languageObj.translate('Recenttemplates.1')}</div>
				<div className="recent-surveys-card-body">
					<ul>
						{
							recentTemplates && recentTemplates.map((item, index) =>
								<li key={`recent-surveys${index}`} className={(item.status === 'close') ? 'closed' : item.status.toLowerCase() || 'draft'}>
									<span className="recent-surveys-index">{index + 1}</span>
									<span className={`recent-surveys-name ${(item.status === 'close') ? 'closed' : item.status.toLowerCase() || 'draft'}`}>{item.name}</span>
									<span className={`recent-surveys-status ${((item.ispublished === true) ? "Published" : "Unpublished").toLowerCase()}`}>{(item.ispublished === true) ? "Published" : "Unpublished"}</span>
									<button type="button" className={`recent-surveys-report ${accesFeature.create_survey || " access_lock"}`} onClick={() => handleCreateSurveyCat(item.id)}>{languageObj.translate('QuickLaunch.1')}</button>
								</li>
							)
						}
					</ul>
					{recentTemplates && recentTemplates.length === 0 && <span className="no-survey">No Recent Template Available</span>}
				</div>
				{loading && <InnerLoader />}
			</div>
		</section>
	)
}

export default RecentTemplateCard;