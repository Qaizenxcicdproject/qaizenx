export default [
    { id: 1, lang_code: "En", image: "india-flag", lang: "English" },
    { id: 2, lang_code: "Arb", image: "arab-flag", lang: "Arabic" },
    { id: 3, lang_code: "Jp", image: "japan-flag", lang: "Japanese" },
];