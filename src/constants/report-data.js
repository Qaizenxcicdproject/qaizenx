export const reportFilter = [
    {
        name: "mood",
        label: "Mood",
        active: true,
        options: [
            "Loving it",
            "Joyful",
            "At Ease",
            "Upset",
            "Bored"
        ]
    },
    {
        name: "sentiments",
        label: "Sentiment",
        active: true,
        options: [
            "Positive",
            "Neutral",
            "Negative"
        ]
    }
];