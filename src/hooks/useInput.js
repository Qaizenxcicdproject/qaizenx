import { useState } from "react";

export const useInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  const reset = () =>{
    setValue (initialValue);
  }

  const bind = {
    value,
    onChange: e => {
      setValue(e.target.value);
    }
  }

  return [ value , bind , reset] ;
}

export default useInput;



// const [ firstName , bindFirstName , resetFirstName ] = useInput('');
// const [ lastName , bindLastName , resetLastName ] = useInput('');

// const handleTest = (evt) => {
//     evt.preventDefault();
//     alert(`Submitting Name ${firstName} ${lastName}`);
//     resetFirstName();
//     resetLastName();
// }

// <form onSubmit={handleTest}>
// <label>
//   First Name:
//   <input type="text" {...bindFirstName} />
// </label>
// <label>
//   Last Name:
//   <input type="text" {...bindLastName} />
// </label>
// <input type="submit" value="Submit" />
// </form>