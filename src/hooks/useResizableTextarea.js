import {useState} from 'react';

const useResizableTextarea = () => {
    
    const [ resizableTextarea, setResizableTextarea ] = useState({
        value: '',
        rows: 5,
        minRows: 5,
        maxRows: 50,
    });

    const handleResizableTextChange = (event) => {
		const textareaLineHeight = 24;
		const { minRows, maxRows } = resizableTextarea;
		
		const previousRows = event.target.rows;
        event.target.rows = minRows; // reset number of rows in textarea 
            
        const currentRows = ~~(event.target.scrollHeight / textareaLineHeight);
        
        if (currentRows === previousRows) {
            event.target.rows = currentRows;
        }
            
        if (currentRows >= maxRows) {
            event.target.rows = maxRows;
            event.target.scrollTop = event.target.scrollHeight;
        }
        
        setResizableTextarea({...resizableTextarea, 
            value: event.target.value,
            rows: currentRows < maxRows ? currentRows : maxRows
        });
    };
    
    return {
        handleResizableTextChange,
        resizableTextarea
    };
}
export default useResizableTextarea;
