import * as Survey from "survey-knockout";

Survey.ComponentCollection.Instance.add({
    name: 'rating_csat',
    title: 'CSAT',
    iconName: "",
    questionJSON: {
        type: 'rating',
        name: 'rating_csat',
        csatScores: []
    },
    onInit() {
        Survey.Serializer.addClass(
            "csatscores",
            [{ name: "value", visible: true }, { name: "text", visible: true }],
            function () {
                return new Survey.ItemValue(null, null, "csatscores");
            }, "itemvalue"
        )
        Survey.Serializer.addProperty("rating_csat", {
            name: "csatScores:csatscores[]",
            category: "general",
        });
    },
    onLoaded(question) {
        this.setProperties(question);
    },
    onPropertyChanged(question, propertyName, newValue) {
        this.setProperties(question);
    },
    setProperties(question) {
        question.contentQuestion.rateValues = question.csatScores;
    }
});