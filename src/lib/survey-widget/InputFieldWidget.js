import * as Survey from "survey-knockout";
import "jquery";
var widget = {
	name: "aspects",
	title: "Personal Details",
	iconName: "",
	questionValue: {},
	widgetIsLoaded: function () {
		return true;
	},
	isFit: function (question) {
		return question.getType() === 'aspects';
	},
	activatedByChanged: function () {
		Survey.JsonObject.metaData.addClass("aspects", [], null, "text");
	},
	isDefaultRender: false,
	htmlTemplate: "<div class='survey-personal-form-main'> <fieldset> <div class='survey-personal-form-wrap'> <input type='text' name='fname'  placeholder = 'First Name'/> <input type='text' name='lname' placeholder = 'Last Name'/> <input type='email' name='email' placeholder = 'Email Id'/> <input type='number' name='mobile_number' placeholder = 'Phone Number'/> <input type='text' name='country' placeholder = 'Country'/> <input type='text' name='state' placeholder = 'State'/> <input type='text' name='city' placeholder = 'City'/> < type='number' name='pincode' placeholder = 'Pincode'/> </div> <fieldset/> </div>",
	afterRender: function (question, el) {
		const elements = el.getElementsByTagName("input");
		const text = el.getElementsByTagName("fieldset")[0];
		text.inputType = question.inputType;
		text.onchange = function () {
			let obj = {};
			for (let i = 0, l = elements.length; i < l; i++) {
				if (elements[i].value !== null) {
					obj[elements[i].name] = elements[i].value;
				}
			}
			window.personal_details = obj;
		}
	},
	willUnmount: function (question, el) {
		question.value = window.personal_details;
		window.personal_details = null;
	}
}

Survey.CustomWidgetCollection.Instance.addCustomWidget(widget, "customtype");
export default widget;
