import * as Survey from "survey-react";
import './Widget.scss';
var widget = {
    name: "smily",
    title: "Smily Rating Question",
    iconName: "",
    widgetIsLoaded: function () {
        return true;
    },
    isFit: function (question) {
        return question.getType() === 'smily';
    },
    activatedByChanged: function (activatedBy) {
        Survey.JsonObject.metaData.addClass("smily", [], null, "radiogroup");
    },
    isDefaultRender: false,
    htmlTemplate: "<div class='smily-wrapper'> <fieldset> <div class='smily-inner-wrapper'> <div class='smily-inner'> <div class='smily-icon-wrapper'> <input class='smily-radio' type='radio' name='smilyradio' value='verypoor' /> <span class='smily-icon very-poor'></span> </div> <div class='smily-name'>Very Poor</div> </div> <div class='smily-inner'> <div class='smily-icon-wrapper'> <input class='smily-radio' type='radio' name='smilyradio' value='poor' /> <span class='smily-icon poor'></span> </div> <div class='smily-name'>Poor</div> </div> <div class='smily-inner'> <div class='smily-icon-wrapper'> <input class='smily-radio' type='radio' name='smilyradio' value='average' /> <span class='smily-icon average'></span> </div> <div class='smily-name'>Average</div> </div> <div class='smily-inner'> <div class='smily-icon-wrapper'> <input class='smily-radio' type='radio' name='smilyradio' value='good' /> <span class='smily-icon good'></span> </div> <div class='smily-name'>Good</div> </div> <div class='smily-inner'> <div class='smily-icon-wrapper'> <input class='smily-radio' type='radio' name='smilyradio' value='excellent' /> <span class='smily-icon excellent'></span> </div> <div class='smily-name'>Excellent</div> </div> </div> <fieldset/> </div>",
    afterRender: function (question, el) {
        var elements = el.getElementsByTagName("input");
        var text = el.getElementsByTagName("fieldset")[0];
        text.inputType = question.inputType;
        question.value = "";
        text.onchange = function () {
            for (var i = 0, l = elements.length; i < l; i++) {
                if (elements[i].checked) {
                    question.value = elements[i].value;
                }
            }
        }
        var onValueChangedCallback = function () {
            for (var i = 0, l = elements.length; i < l; i++) {
                if (elements[i].checked) {
                    question.value = elements[i].value;
                }
            }
        }
        var onReadOnlyChangedCallback = function () {
            if (question.isReadOnly) {
                text.setAttribute('disabled', 'disabled');
            } else {
                text.removeAttribute("disabled");
            }
        };
        question.readOnlyChangedCallback = onReadOnlyChangedCallback;
        question.valueChangedCallback = onValueChangedCallback;
        onValueChangedCallback();
        onReadOnlyChangedCallback();

    },
    willUnmount: function (question, el) {
    }
}
Survey.CustomWidgetCollection.Instance.addCustomWidget(widget, "customtype");
export default widget;
