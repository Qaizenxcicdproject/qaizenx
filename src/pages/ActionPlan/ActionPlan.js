import React, { useState, useEffect, useContext } from "react";
import "./ActionPlan.scss";
import ActionPlanCard from "pages/ActionPlan/ActionPlanCard";
import ActionPlanModal from "pages/ActionPlan/ActionPlanModal";
import ActionPlanCreateModal from "pages/ActionPlan/ActionPlanCreateModal";
import RealtimeBoard from 'react-trello';
import AppContext from 'store/AppContext';
import Axios from "utility/Axios";
import configURL from 'config/config';
import DatePicker from 'react-date-picker';
import { toast } from 'react-toastify';
import { useParams } from "react-router-dom";
import SelectSearch from "react-select-search";
import { Breadcrumb } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';

const ActionPlan = (props) => {
	const [showModel, setShowModel] = useState(false);
	const [showActionModal, setShowActionModal] = useState(false);
	const handleModalClose = () => { setShowActionModal(false) };
	const handleClose = () => { setShowModel(false) };
	const [taskListData, settaskListData] = useState([]);
	const urlParams = useParams();
	const [taskData, settaskData] = useState([]);
	const [taskComment, settaskComment] = useState([]);
	const [taskHistory, settaskHistory] = useState([]);
	const { EziLoader, languageObj = {} } = useContext(AppContext);
	// const [prioritiesOption, setPrioritiesOption] = useState([]);
	const [createdByOption, setCreatedByOption] = useState([]);
	const [assigneeOption, setAssigneeOption] = useState([]);
	// const [selectedPriority, setSelectedPriority] = useState(null);
	const [selectedCreated, setSelectedCreated] = useState(0);
	const [selectedAssignee, setSelectedAssignee] = useState(0);
	const appStore = JSON.parse(localStorage.getItem("appState"));
	let timezone = appStore.user.timezone;
	var todayDate = new Date();
	let time = todayDate.getHours() + ":" + todayDate.getMinutes() + ":" + todayDate.getSeconds();
	let utcTime = new Date().getTimezoneOffset("en-US", { timeZone: timezone });
	let timeZoneString = utcTime.toString();
	const { survey_id = null } = props.match.params;
	const [forWhichListOption, setForWhichListOption] = useState([]);
	const [selectedForWhich, setSelectedForWhich] = useState(0);
	let task_id = localStorage.getItem("task_id");
	let task_status_id = localStorage.getItem("task_status_id");

	//convert timezone into utc time difference
	function time_convert(num, sign) {
		var hours = Math.floor(num / 60);
		var minutes = num % 60;
		return sign + hours + ":" + minutes;
	}
	let UtcDiff = time_convert(timeZoneString.substring(1), timeZoneString[0]);
	const [filterByDate, setFilterByDate] = useState({
		start_date: new Date(),
		end_date: new Date(
			todayDate.getFullYear(),
			todayDate.getMonth(),
			todayDate.getDate() + 30 * 1,
		),
		time: time,
		utc: UtcDiff
	});

	/**
	 * Tickets listing Filtered Data
	 */
	const laneData = {
		lanes: [
			{
				id: '1',
				title: 'OPEN',
				label: taskListData.filter((data) => {
					return data.task_status_id === 1;
				}).length,
				cards: taskListData.filter((data) => {
					return data.task_status_id === 1;
				})
			},
			{
				id: '2',
				title: 'INPROGRESS',
				label: taskListData.filter((data) => {
					return data.task_status_id === 2;
				}).length,
				cards: taskListData.filter((data) => {
					return data.task_status_id === 2;
				})
			},
			{
				id: '3',
				title: 'COMPLETE',
				label: taskListData.filter((data) => {
					return data.task_status_id === 3;
				}).length,
				cards: taskListData.filter((data) => {
					return data.task_status_id === 3;
				})
			},
			{
				id: '4',
				title: 'CANCELLED',
				label: taskListData.filter((data) => {
					return data.task_status_id === 4;
				}).length,
				cards: taskListData.filter((data) => {
					return data.task_status_id === 4;
				})
			}
		]

	}

	/**
	 * Status change when Tickets drag from one Lane to Other
	 * @param {*} fromLaneId 
	 * @param {*} toLaneId 
	 * @param {*} cardId 
	 * @param {*} index 
	 */
	const onCardMoveAcrossLanes = (fromLaneId, toLaneId, cardId, index) => {

		let formData = new FormData()
		formData.append("task_id", cardId)
		formData.append("task_status_id", toLaneId)
		formData.append("profile_name", 'action_planning')
		Axios.post(configURL.create_action_task, formData).then(res => {

			if (res.data.success === true) {
				getActionPlanListingOnDrag();
				toast.success(res.data.message || `Task Status Changed Successfully`)

			} else {
				toast.warn(res.data.message || "Something went wrong.")
			}
		}).catch(err => {

			console.log(err)
			toast.warn("Something went wrong.")
		})
	}

	/**
	 * On Ticket Click get Comments, History and task data
	 * @param {*} cardId 
	 * @param {*} metadata 
	 * @param {*} laneId 
	 */
	const handleOnCardClick = (cardId, metadata, laneId) => {
		EziLoader.show();
		let formData = new FormData()
		formData.append("survey_id", urlParams.survey_id)
		Axios.post(configURL.action_plan_task_details + cardId, formData).then(res => {
			settaskData(res?.data?.results?.task);
			settaskComment(res?.data?.results?.task_comments);
			settaskHistory(res?.data?.results?.task_histories);
			setShowModel(true);
			EziLoader.hide();
		}).catch(err => {
			console.log(err)
			toast.warn("Something went wrong.")
		})
	}

	/**
	 * Get All Tickets Data on page load
	 */
	const getActionPlanListing = () => {
		EziLoader.show();
		let formData = new FormData()
		selectedAssignee && formData.append("assigned_to_user_id", selectedAssignee)
		selectedCreated && formData.append("created_by_user_id", selectedCreated)
		selectedForWhich && formData.append("for_which", selectedForWhich)
		// selectedPriority && formData.append("task_priority_level_id", selectedPriority)
		filterByDate && formData.append("date", JSON.stringify(filterByDate))
		timezone && formData.append("timezone", timezone)
		survey_id && formData.append("survey_id", survey_id)
		formData.append("per_page", 1000)
		Axios.post(configURL.action_plan_task_list, formData).then(res => {
			settaskListData(res?.data?.results);
			EziLoader.hide();
		}).catch(err => {
			console.log(err)
			toast.warn("Something went wrong.")
		})
	}

	/**
	 * Get All Tickets Data when Ticket drag from one Lane to other lane
	 */
	const getActionPlanListingOnDrag = () => {
		let formData = new FormData()
		if (selectedAssignee) formData.append("assigned_to_user_id", selectedAssignee)
		if (selectedCreated) formData.append("created_by_user_id", selectedCreated)
		if (selectedForWhich) formData.append("for_which", selectedForWhich)
		// if (selectedPriority) formData.append("task_priority_level_id", selectedPriority)
		if (filterByDate) formData.append("date", JSON.stringify(filterByDate))
		if (timezone) formData.append("timezone", timezone)
		survey_id && formData.append("survey_id", survey_id)
		Axios.post(configURL.action_plan_task_list, formData).then(res => {
			settaskListData(res?.data?.results);
		}).catch(err => {
			console.log(err)
			toast.warn("Something went wrong.")
		})
	}

	/**
	 * Get Filters Option
	 */
	const getDefaultOptions = () => {
		let formData = new FormData()
		formData.append("profile_name", 'action_planning')
		formData.append("filtered_users", true)
		formData.append("allUserFilter", 1)
		Axios.post(configURL.task_master_list, formData).then(res => {
			// setPrioritiesOption(res.data.result.task_priority_level.map((priority) => {
			//     return {
			//         name: priority.name, value: priority.id
			//     }
			// }));
			setCreatedByOption(res.data.result.task_creators.map((creators) => {
				return {
					name: creators.name, value: creators.id
				}
			}));
			setAssigneeOption(res.data.result.task_assignee.map((assignee) => {
				return {
					name: assignee.name, value: assignee.id
				}
			}));
			setForWhichListOption(res.data.result.for_which.map((actionPlanType) => {
				return {
					name: actionPlanType.name, value: actionPlanType.id
				}
			}))
		}).catch(err => {
			console.log(err)
			toast.warn("Something went wrong.")
		})
	}

	/**
	 * Handle Filter functionality
	 */
	const onhandleFilter = () => {
		getActionPlanListing();
	}
	const handleAddAction = () => {
		setShowActionModal(true)
	}
	useEffect(() => {
		window.scrollTop = 0;
		getActionPlanListing();
		getDefaultOptions();
		if (task_status_id && task_id) {
			handleOnCardClick(task_id, null, task_status_id);
			localStorage.removeItem('task_status_id');
			localStorage.removeItem('task_id');
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [showModel, showActionModal]);
	return (
		<section className="Page-ActionPlan">
			<div className="breadcrumb_ezi">
				<Breadcrumb>
					<Breadcrumb.Item onClick={() => props.history.goBack()}>{languageObj.translate('ActionPlan.1')}</Breadcrumb.Item>
					<Breadcrumb.Item >{props.location.state.surveyName && props.location.state.surveyName}</Breadcrumb.Item>
				</Breadcrumb>
				<div class="st_heading">{props.location.state.surveyName && props.location.state.surveyName} - {languageObj.translate('ActionPlan.1')} </div>
			</div>

			<div class="actionPlanCreation">
				<button type="button" class="actionPlanBtn" onClick={() => handleAddAction()}>Add New Action</button>
				<ActionPlanCreateModal show={showActionModal} onHide={handleModalClose} />
			</div>

			<div className="actionplan-header">
				<div className="date-search">
					{/* <div className="date-filter header-start-item"> */}
					<div className="date-selection date-selection-start header-item">
						<label className="form-heading date-start">Start Date </label>
						<DatePicker
							format={"dd/MM/yyyy"}
							selected={filterByDate.start_date}
							value={filterByDate.start_date}
							disabled={false}
							//minDate={new Date()}
							onChange={(d) => setFilterByDate({ ...filterByDate, start_date: d })}
							clearIcon={null}
							className="sweet-datepicker-custom"
						/>
					</div>
					<div className="date-selection header-item">
						<label className="form-heading date-end">End Date</label>
						<DatePicker
							format={"dd/MM/yyyy"}
							selected={filterByDate.end_date}
							value={filterByDate.end_date}
							disabled={false}
							minDate={filterByDate.start_date}
							clearIcon={null}
							className="sweet-datepicker-custom"
							onChange={(value) => {
								setFilterByDate({ ...filterByDate, end_date: value })
							}}
						/>
					</div>
					{/* </div> */}

					{/* <div className="priority-search  header-item">
                            <label>Priority</label>
                            <SelectSearch
                                key='1'
                                options={prioritiesOption}
                                search
                                value={selectedPriority || null}
                                placeholder="Select"
                                onChange={(priority) => { setSelectedPriority(priority) }}
                                emptyMessage="Not Found"
                                name="priority"

                            />

                        </div> */}
					<div className="due-week  header-item">
						<label>Created For</label>
						<SelectSearch
							options={forWhichListOption}
							search
							value={selectedForWhich}
							placeholder="Select"
							onChange={(actionPlanType) => { setSelectedForWhich(actionPlanType) }}
							emptyMessage="Not Found"
							name="actionPlanType"
						/>
					</div>

					<div className="due-week  header-item">
						<label>Created By</label>
						<SelectSearch
							options={createdByOption}
							search
							value={selectedCreated}
							placeholder="Select"
							onChange={(createdby) => { setSelectedCreated(createdby) }}
							emptyMessage="Not Found"
							name="createdby"
						/>
					</div>
					<div className="due-week  header-item">
						<label>Assignee</label>
						<SelectSearch
							options={assigneeOption}
							search
							value={selectedAssignee}
							placeholder="Select"
							onChange={(assignee) => { setSelectedAssignee(assignee) }}
							emptyMessage="Not Found"
							name="assignee"
						/>
					</div>

					<div className="submit-filter ">
						<button className="filter-action-plan" onClick={onhandleFilter}>Filter</button>
					</div>

				</div>
			</div>
			<div className="actionplan-modal">
				{showModel && <ActionPlanModal taskDetails={taskData} taskComment={taskComment} taskHistory={taskHistory} show={showModel} onHide={handleClose} />}
			</div>
			<div className="actionplan-cards-wrapper">
				<RealtimeBoard cardDraggable={true} components={{ Card: ActionPlanCard }} data={laneData} onCardClick={handleOnCardClick} onCardMoveAcrossLanes={onCardMoveAcrossLanes} />
			</div>
		</section>

	);
};

export default withRouter(ActionPlan);
