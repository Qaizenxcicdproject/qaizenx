import React from "react";
import { Fragment } from "react";

const ActionPlanCard = ({
	onClick,
	comment,
	assigned_to_user_name,
	due_date_hf,
	task_status_name,
	description
}) => {
	return (
		<Fragment>
			<div className={`cards ${task_status_name && task_status_name}`} onClick={onClick}>
				<p>{comment ? comment : description}</p>
				<div className="assigned-user"><label>Owner</label><br />{assigned_to_user_name && assigned_to_user_name}</div>
				<div className="date-section">
					<div className="due-date"><span>Due Date: </span>{due_date_hf ? due_date_hf : '---'}</div>
				</div>
			</div>
		</Fragment>
	);
};

export default ActionPlanCard;
