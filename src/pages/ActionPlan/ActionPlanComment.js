import React, { Fragment, useEffect, useState } from 'react';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';


const ActionPlanComment = ({ taskDetails }) => {

	const [taskComment, settaskComment] = useState([]);
	const [commentAdded, setcommentAdded] = useState(false);
	const [inputValue, setInputValue] = useState("")
	const appStore = JSON.parse(localStorage.getItem("appState"));
	const [emptyComment, setEmptyComment] = useState(true);
	let userName = appStore.user.name;

	/**
	 * Handle Comment Submit event
	 */
	const onhandleCommentSave = () => {

		let formData = new FormData()
		formData.append("survey_id", taskDetails?.survey_id)
		formData.append("task_id", taskDetails?.id)
		formData.append("comment_text", inputValue)
		formData.append("user_name", userName)
		Axios.post(configURL.action_task_comment_create, formData).then(res => {
			if (res.data.success === true) {
				setcommentAdded(true);
				setInputValue("");
				setEmptyComment(true)
				toast.success(res.data.message || `Comment Added Successfully`)

			} else {
				toast.warn(res.data.message || "Something went wrong.")
			}
		}).catch(err => {

			console.log(err)
			toast.warn("Something went wrong.")
		})


	};

	/**
	 * Handle on Change comment and set comment input value
	 * @param {*} event 
	 */
	const onHandleChangeComment = (event) => {
		event.target.value !== "" ? setEmptyComment(false) : setEmptyComment(true);
		setInputValue(event.target.value);
	}

	useEffect(() => {
		let formData = new FormData()
		formData.append("survey_id", taskDetails?.survey_id)
		Axios.post(configURL.action_plan_task_details + taskDetails?.id, formData).then(res => {
			settaskComment(res?.data?.results?.task_comments);
		}).catch(err => {
			console.log(err)
			toast.warn("Something went wrong.")
		})
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [commentAdded]);
	return (
		<Fragment>
			<div className="comment-create">
				<label className="comment-heading">{userName ? userName.split(/\s/).reduce((response, word) => response += word.slice(0, 1), '') : 'AN'}</label>
				<div className="comment-section">
					<textarea className="add-comment" type="text" value={inputValue} onChange={onHandleChangeComment} placeholder="Add a comment..." />
					{emptyComment ? <button className="save-btn disabled" >Submit</button> : <button className="save-btn" onClick={onhandleCommentSave}>Submit</button>}
					<button className="cancel-btn" onClick={() => { setInputValue(""); setEmptyComment(true); }}>Clear</button>
				</div>
			</div>

			<div className="comment-list">
				{
					taskComment.length > 0 && taskComment.map((item, index) =>
						<div className="comment-item" key={item.id ? item.id : index}>
							<span className="comment-user">{item?.created_by_user_name ? item?.created_by_user_name.split(/\s/).reduce((response, word) => response += word.slice(0, 1), '') : 'AN'}</span>
							<div className="comment-data">
								<label>{item?.created_by_user_name ? item?.created_by_user_name : 'Anonymous'} <span className="comment-created-date">{item?.created_at_hf && item?.created_at_hf}</span></label>
								<div className="content"> {item?.comment_text && item?.comment_text}</div>
							</div>
						</div>
					)
				}

			</div>
		</Fragment>
	);
}

export default ActionPlanComment;