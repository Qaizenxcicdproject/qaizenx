import React, { useState, useEffect } from 'react'
import { Modal, Spinner } from 'react-bootstrap'
import "./ActionPlanCreateModal.scss";
import SelectSearch from "react-select-search";
import DatePicker from 'react-date-picker';
import { useParams } from "react-router-dom";
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';
import useForm from 'react-hook-form';

const ActionPlanCreateModal = ({ show, onHide }) => {

	const { register, handleSubmit, errors, setValue, clearError } = useForm();
	const [saving, setSaving] = useState(false)
	const [options, setOptions] = useState([]);
	const urlParams = useParams();
	const [selectedValue, setSelectedValue] = useState(0);
	const appStore = JSON.parse(localStorage.getItem("appState"));
	let timezone = appStore.user.timezone;
	let userName = appStore.user.name;
	let userEmail = appStore.user.email;
	let userCellNumber = appStore.user.cell_number;
	var todayDate = new Date();
	let time = todayDate.getHours() + ":" + todayDate.getMinutes() + ":" + todayDate.getSeconds();
	let utcTime = new Date().getTimezoneOffset("en-US", { timeZone: timezone });
	let timeZoneString = utcTime.toString();
	//convert timezone into utc time difference
	function time_convert(num, sign) {
		var hours = Math.floor(num / 60);
		var minutes = num % 60;
		return sign + hours + ":" + minutes;
	}
	let UtcDiff = time_convert(timeZoneString.substring(1), timeZoneString[0]);
	const [reportDate, setReportDate] = useState({
		start_date: new Date(),
		due_date: null,
		time: time,
		utc: UtcDiff
	});

	const handleTaskCreate = data => {
		setSaving(true)
		let formData = new FormData()
		let startDate = new Date(reportDate.start_date.getTime() - (reportDate.start_date.getTimezoneOffset() * 60000)).toJSON();
		let dueDate = new Date(reportDate.due_date.getTime() - (reportDate.due_date.getTimezoneOffset() * 60000)).toJSON();
		let selectedUserName = options.filter((item) => {
			return item.value === selectedValue && item.name;
		})
		localStorage.getItem("lastname");
		formData.append("survey_id", urlParams.survey_id)
		formData.append("question_id", '')
		formData.append("task_type_id", 1)
		formData.append("assigned_to_user_id", selectedValue)
		formData.append("assigned_to_user_name", selectedUserName[0]?.name)
		formData.append("user_name", userName)
		formData.append("user_email", userEmail)
		formData.append("user_contact", userCellNumber)
		formData.append("task_priority_level_id", 1)
		formData.append("start_date", startDate)
		formData.append("due_date", dueDate)
		formData.append("task_repeat_enum_id", 1)
		formData.append("task_status_id", 1)
		formData.append("description", data.description)
		formData.append("comment", data.summary)
		formData.append("profile_name", "action_planning")
		formData.append("for_which", 'SURVEY')

		Axios.post(configURL.create_action_task, formData).then(res => {
			setSaving(false)
			if (res.data.success === true) {
				toast.success(res.data.message || `Task Created Successfully`)
				onHide()
			} else {
				toast.warn(res.data.message || "Something went wrong.")
			}
		}).catch(err => {
			setSaving(false)
			console.log(err)
			toast.warn("Something went wrong.")
		})
	};

	const getDefaultOptions = () => {
		let formData = new FormData()
		formData.append("survey_id", urlParams.survey_id)
		formData.append("profile_name", 'action_planning')
		Axios.post(configURL.task_master_list, formData).then(res => {
			setOptions(res.data.result.task_assignee.map((user) => {
				return {
					name: user.name, value: user.id
				}
			}));
		}).catch(err => {
			console.log(err)
			toast.warn("Something went wrong.")
		})
	}

	const handleChange = value => {
		setSelectedValue(value);
	}

	useEffect(() => {
		register({ name: "task_assignee" }, { required: true });
		register({ name: "due_date" }, { required: true });
		getDefaultOptions();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])

	useEffect(() => {
		clearError('task_assignee')
		clearError('due_date')
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [show, onHide])

	return (
		<div className="Page-ActionPlanModal">
			<Modal show={show} onHide={onHide} className="actionPlanCreateModalCustom" >
				<Modal.Header closeButton>
					<div className="modal-title">
						<h4>Create Action Plan</h4>
					</div>
				</Modal.Header>
				<Modal.Body>
					<div className="create-action-modal-section">
						<form className="add-theme-form" onSubmit={handleSubmit(handleTaskCreate)}>

							<div className="theme-field-wrapper">
								<div className="theme-field">
									{/* <label className="form-heading">Summary :</label> */}
									<textarea className="theme-field-control-create" name="summary" rows={2} placeholder="Summary" ref={register({ required: true })}></textarea>
									{errors.summary && <span className="theme-error_cu assigne-error error">* Summary field is required.</span>}
								</div>
							</div>

							<div className="theme-field-wrapper">
								<div className="theme-field">
									{/* <label className="form-heading">Description :</label> */}
									<textarea className="theme-field-control-create" name="description" rows={3} placeholder="Description" ref={register({ required: true })}></textarea>
									{errors.description && <span className="theme-error_cu assigne-error error">* Description field is required.</span>}
								</div>
							</div>

							<div className="owner-action-create">
								<div className="owner-action-section form-grp">
									<label className="form-heading">Owner of Action Plan :</label>
									<div>
										<SelectSearch
											options={options}
											search
											value={selectedValue}
											placeholder="Please choose task assignee"
											ref={register({ required: true })}
											emptyMessage="Not Found"
											onChange={(value) => {
												setValue('task_assignee', value)
												handleChange(value)
												clearError('task_assignee')
											}}
										/>
										{errors.task_assignee && <span className="theme-error_cu assigne-error error">* Owner of Action Plan field is required.</span>}
									</div>
								</div>

							</div>

							<div className="form-grp date">
								<div className="d-flex">
									<label className="form-heading align-self-center">Start Date :</label>
									<DatePicker
										format={"dd/MM/yyyy"}
										selected={reportDate.start_date}
										value={reportDate.start_date}
										disabled={false}
										minDate={new Date()}
										onChange={(d) => setReportDate({ ...reportDate, start_date: d })}
										clearIcon={null}
										className="sweet-datepicker-custom start"
									/>
								</div>
								<div>
									<div className="d-flex">
										<label className="form-heading align-self-center">Due Date :</label>
										<DatePicker
											format={"dd/MM/yyyy"}
											selected={reportDate.due_date}
											value={reportDate.due_date}
											disabled={false}
											minDate={reportDate.start_date}
											clearIcon={null}
											className="sweet-datepicker-custom start"
											onChange={(value) => {
												setValue('due_date', value)
												setReportDate({ ...reportDate, due_date: value })
												clearError('due_date')
											}}
										/>
										{errors.due_date && <span className="theme-error_cu error">* Due date field is required.</span>}
									</div>

								</div>
							</div>

							<div className="theme-modal-footer">
								{/* <button type="button" className="close-theme-btn" onClick={onHide}>Close</button> */}
								<button type="submit" className="btn-ripple ezi-pink-btn add-theme-btn" disabled={saving}>
									Save {saving && <Spinner animation="border" size="sm" />}
								</button>
							</div>
						</form>
					</div>
				</Modal.Body>
			</Modal>

		</div>
	)
}

export default ActionPlanCreateModal;