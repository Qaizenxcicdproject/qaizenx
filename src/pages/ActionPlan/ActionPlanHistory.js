import React, { Fragment, useState, useEffect } from 'react';
import parse from 'html-react-parser';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';

const ActionPlanHistory = ({ taskDetails, taskHistoryPrev }) => {

	const [taskHistory, settaskHistory] = useState(taskHistoryPrev);

	useEffect(() => {
		let formData = new FormData()
		formData.append("survey_id", taskDetails?.survey_id)
		Axios.post(configURL.action_plan_task_details + taskDetails?.id, formData).then(res => {
			settaskHistory(res?.data?.results?.task_histories);
		}).catch(err => {
			console.log(err)
			toast.warn("Something went wrong.")
		})
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	return (<Fragment>

		<div className="history-list">

			{
				taskHistory?.length > 0 && taskHistory.map((item, index) =>
					<div className="comment-item" key={item.id ? item.id : index}>
						<span className="comment-user">{item?.created_by_user_name ? item?.created_by_user_name.split(/\s/).reduce((response, word) => response += word.slice(0, 1), '') : 'AN'}</span>
						<div className="comment-data">
							<label>{item?.created_by_user_name ? item?.created_by_user_name : 'Anonymous'} <span className="comment-created-date">{item?.created_at_hf && item?.created_at_hf}</span></label>
							<div className="content"> {item?.name && item?.name}</div>
							<div>
								{item?.details?.length > 0 && item?.details.map((detail, indexd) =>

									<div className="history-content" key={indexd}>
										<span>{detail?.task_status_name && parse(detail?.task_status_name)}</span>
										<span>{detail?.description && parse(detail?.description)}</span>
									</div>
								)}
							</div>
						</div>
					</div>
				)
			}
			{
				taskHistory.length === 0 && <div className="history-not-found">History Not Found</div>
			}


		</div>
	</Fragment>);
}

export default ActionPlanHistory;