/* eslint-disable array-callback-return */
import React, { useEffect, useState } from 'react'
import { Modal, Tab, Nav, Dropdown } from 'react-bootstrap'
import "./ActionPlanModal.scss";
import Select from 'react-select';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';
import ActionPlanComment from './ActionPlanComment';
import ActionPlanHistory from './ActionPlanHistory';
import RespondentResponse from './../Report/ShareComponent/RespondentResponse';
import makeAnimated from 'react-select/animated';

const ActionModal = ({ show, onHide, taskDetails, taskHistory }) => {

	const [editDescription, setEditDescription] = useState(false);
	const [statusOptions, setStatusOptions] = useState([]);
	const [description, setDescription] = useState(null);
	const [selectedStatusIndex, setSelectedStatusIndex] = useState(null);
	const [tabKey, setTabKey] = useState('action_plan_comment');
	const [reasons, setReasons] = useState([]);
	const appStore = JSON.parse(localStorage.getItem("appState"));
	let userName = appStore.user.name;
	const [emptyDescription, setEmptyDescription] = useState(false);
	const [watchList, setWatchList] = useState([]);
	let watchingList = taskDetails.watch_list;
	const [selectedWatchList, setSelectedWatchList] = useState(watchingList ? watchingList : []);
	const [watchListCount, setWatchListCount] = useState(watchingList ? watchingList.length : 0);
	const animatedComponents = makeAnimated();
	const [editAssignee, setEditAssignee] = useState(false);
	const [taskAssignee, setTaskAssignee] = useState([]);
	const [selectedAssignee, setSelectedAssignee] = useState(null);
	const [currentAssignedToUser, setCurrentAssignedToUser] = useState(taskDetails ? taskDetails.assigned_to_user_id : null);

	const onhandleSave = () => {
		let formData = new FormData()
		formData.append("survey_id", taskDetails?.survey_id)
		formData.append("task_id", taskDetails?.id)
		formData.append("description", description)
		formData.append("user_name", userName)
		formData.append("profile_name", 'action_planning')
		Axios.post(configURL.create_action_task, formData).then(res => {

			if (res.data.success === true) {
				toast.success(res.data.message || `Task Updated Successfully`)
				setEditDescription(false);
			} else {
				toast.warn(res.data.message || "Something went wrong.")
			}
		}).catch(err => {

			console.log(err)
			toast.warn("Something went wrong.")
		})
	};

	const onStatusChange = data => {
		setSelectedStatusIndex(data);
		let formData = new FormData()
		formData.append("survey_id", taskDetails?.survey_id)
		formData.append("task_id", taskDetails?.id)
		formData.append("task_status_id", data?.value)
		formData.append("user_name", userName)
		formData.append("profile_name", 'action_planning')
		Axios.post(configURL.create_action_task, formData).then(res => {

			if (res.data.success === true) {
				toast.success(res.data.message || `Task Status Changed Successfully`)

			} else {
				toast.warn(res.data.message || "Something went wrong.")
			}
		}).catch(err => {

			console.log(err)
			toast.warn("Something went wrong.")
		})
	};

	const getDefaultOptions = () => {
		let formData = new FormData()

		formData.append("profile_name", 'action_planning')
		formData.append("survey_id", taskDetails?.survey_id)
		// formData.append("filtered_users", true)
		// formData.append("task_id", taskDetails?.id)
		Axios.post(configURL.task_master_list, formData).then(res => {
			if (res.data.success === true) {
				setStatusOptions(res.data.result.task_status.map((status) => {
					return {
						label: status.name, value: status.id
					}
				}));
				setSelectedStatusIndex(res.data.result.task_status.map((status) => {
					return status.id === taskDetails?.task_status_id && { label: status.name, value: status.id };
				}));
				setWatchList(res.data.result.task_watches);
				setTaskAssignee(res.data.result.task_assignee.map((assignee) => {
					return {
						label: assignee.name, value: assignee.id
					}
				}));
				setSelectedAssignee(res.data.result.task_assignee.map((assignee) => {
					return assignee.id === taskDetails?.assigned_to_user_id && { label: assignee.name, value: assignee.id };
				}));
			}
		}).catch(err => {
			console.log(err)
			toast.warn("Something went wrong.")
		})

	}

	const overallReasonListing = () => {
		if (taskDetails.respondent_id) {
			let formData = new FormData();
			formData.append("survey_id", taskDetails?.survey_id)
			formData.append("respondent_id", taskDetails?.respondent_id);

			Axios.post(configURL.ReportIndividualResponse, formData).then(res => {
				if (res.data.success !== undefined && res.data.success) {
					setReasons(res.data.result);
				} else {
					toast.warn(res.data.message || "Something went wrong.")
				}
			}).catch(err => {
				console.log(err)
			})
		}
	}
	const onCancel = () => {
		setEditDescription(false);
		setDescription(taskDetails?.description && taskDetails?.description);
	}

	const onHandleChangeDescription = (event) => {
		event.target.value !== "" ? setEmptyDescription(false) : setEmptyDescription(true);
		setDescription(event.target.value);
	}

	/**
	 * To Add Watch Users in Ticket
	 * @param {*} data 
	 */
	const onWatchUserSelect = data => {
		let filteredWatchUser = [];
		let count = 0;
		if (data) {
			filteredWatchUser = watchList.filter((user) => {
				return data.some((selectedUser) => {
					if (user.id === selectedUser.value) {
						user.is_watch = true;
						return 1;
					}

				});
			});
			// filteredWatchUser = [...selectedWatchList, ...filteredWatchUser];
			count = filteredWatchUser.length;
		}

		let filteredWatchUserJSON = filteredWatchUser ? JSON.stringify(filteredWatchUser) : filteredWatchUser;
		let formData = new FormData()
		formData.append("survey_id", taskDetails?.survey_id)
		formData.append("task_id", taskDetails?.id)
		if (filteredWatchUserJSON) formData.append("watch_list", filteredWatchUserJSON)
		formData.append("user_name", userName)
		formData.append("profile_name", 'action_planning')

		Axios.post(configURL.create_action_task, formData).then(res => {
			if (res.data.success === true) {
				setWatchListCount(count)
				setSelectedWatchList(filteredWatchUser);
				toast.success(`Watch List Updated Successfully`)

			} else {
				toast.warn(res.data.message || "Something went wrong.")
			}
		}).catch(err => {

			console.log(err)
			toast.warn("Something went wrong.")
		})

	};

	/**
	 * To change Assignee From Ticket & also Assign previous user in watch List
	 * @param {*} data 
	 */
	const onAsigneeChange = data => {
		let filteredWatchUser = [];
		let count = 0;
		if (data) {
			filteredWatchUser = watchList.filter((user) => {
				if (user.id === currentAssignedToUser && selectedWatchList) {
					user.is_watch = true;
					return 1;
				}
			});

			let filteredResult = selectedWatchList.filter(e => !e.id.includes(currentAssignedToUser))
			filteredWatchUser = [...filteredResult, ...filteredWatchUser];
			// filteredWatchUser = [...new Set(filteredWatchUser)];
			count = filteredWatchUser.length;
		}

		let filteredWatchUserJSON = filteredWatchUser ? JSON.stringify(filteredWatchUser) : filteredWatchUser;
		let listUpdate = filteredWatchUser;
		let formData = new FormData()
		formData.append("survey_id", taskDetails?.survey_id)
		formData.append("task_id", taskDetails?.id)
		if (filteredWatchUserJSON) formData.append("watch_list", filteredWatchUserJSON)
		formData.append("assigned_to_user_id", data?.value)
		formData.append("assigned_to_user_name", data?.label)
		formData.append("user_name", userName)
		formData.append("profile_name", 'action_planning')
		Axios.post(configURL.create_action_task, formData).then(res => {

			if (res.data.success === true) {
				setSelectedAssignee(data);
				setEditAssignee(false);
				setSelectedWatchList(listUpdate);
				setWatchListCount(count)
				setCurrentAssignedToUser(data?.value);
				toast.success(`Task Assignee Changed Successfully`)
			} else {
				toast.warn(res.data.message || "Something went wrong.")
			}
		}).catch(err => {
			console.log(err)
			toast.warn("Something went wrong.")
		})
	}


	useEffect(() => {
		onCancel();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [show]);

	useEffect(() => {
		getDefaultOptions();
		overallReasonListing();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	return (
		<div className="Page-ActionPlanModal">
			<Modal show={show} onHide={onHide} className="actionPlanModalCustom" >
				<Modal.Header closeButton>
					<div className="eye-dropdown">
						<Dropdown>
							<Dropdown.Toggle id="dropdown-basic">
								<div className="user-avtar">
									<span className="test"><img alt="" className="eye-btn" src={require(`../../assets/images/view.png`)} />{watchListCount}</span>
								</div>
							</Dropdown.Toggle>
							<Dropdown.Menu className="dropdown-menu-right">
								{/* <div className="watchflex" onClick={onWatchStartHandle}>
									<div className="watchimg">
										<img alt="" className="eye-ico" src={require(`../../assets/images/eye.png`)} />
										<p className="watchHeading">Start Watching</p>
									</div>
									<div>
										<div className="divW">W</div>
									</div>
								</div> */}
								<div className="watchIssue">
									<p>WATCHING THIS ISSUE</p>

								</div>
								{/* <div className="dropMenu">
									{
										selectedWatchList && selectedWatchList.map((user) =>
											<div key={user.id}>
												<Dropdown.Item>
													<div className="dropMenudiv">
														<div className="divInitial">{user?.name ? user?.name.split(/\s/).reduce((response, word) => response += word.slice(0, 1), '') : 'AN'}</div>
														<p className="name">{user.name}</p>
													</div>
												</Dropdown.Item>
											</div>
										)
									}

								</div> */}
								<div>
									{/* <div className="watchflex watch_hide_show" onClick={() => setShowSelectWatch(!showSelectWatch)}>{showSelectWatch ? "- Close watch" : "+ Add Watch"}</div> */}
									<Select

										// closeMenuOnSelect={false}
										components={animatedComponents}
										isMulti
										onChange={onWatchUserSelect}
										options={watchList && watchList.map((user) => {
											return {
												label: user.name, value: user.id
											}
										})}
										value={selectedWatchList && selectedWatchList.map((user) => {
											return {
												label: user.name, value: user.id
											}
										})}
										className="customActModal remove_border"
									/>
								</div>

							</Dropdown.Menu>
						</Dropdown>
					</div>
				</Modal.Header>
				<Modal.Body>
					<div className="action-modal-section">
						<div className="assining-survey-section">
							<h4 className="heading mb-3">{taskDetails?.comment && taskDetails?.comment}</h4>
							<div className="modal-form">
								<div className="description-section-wrapper">
									<div className="description-section">
										{editDescription && <div className="action-modal-description-edit">
											<div className="form-grp">
												<label className="modal-form-heading"><b>Description :</b></label>
												<textarea className="descp-textarea" name="description" rows="6" cols="70" onChange={onHandleChangeDescription}>{description}</textarea>
											</div>
											<div className="form-btn">
												{emptyDescription ? <button className="save-btn disabled" >Save</button> : <button className="save-btn" onClick={onhandleSave}>Save</button>}
												<button className="cancel-btn" onClick={onCancel}>Cancel</button>
											</div>
										</div>}

										{!editDescription && <div className="action-modal-description-preview" onClick={() => setEditDescription(true)}>
											<div className="form-grp">
												<label className="modal-form-heading"><b>Description :</b></label>
												<p className="preview-description">{description}</p>
											</div>

										</div>}
									</div>
									<div className="owner-data-section">
										<div className="owner-data">
											<span className="data-text-heading">Participant Name: </span>
											<span className="data-text">{taskDetails?.respondent_name ? taskDetails?.respondent_name : "---"}</span>
										</div>
										<div className="owner-data">
											<span className="data-text-heading">Email Id: </span>
											<span className="data-text">{taskDetails?.respondent_email ? taskDetails?.respondent_email : "---"}</span>
										</div>
										<div className="owner-data">
											<span className="data-text-heading">Contact: </span>
											<span className="data-text">{taskDetails?.respondent_contact ? taskDetails?.respondent_contact : "---"}</span>
										</div>
									</div>
								</div>
								<div className="action-plan-activity">
									<div className="title">Activity:</div>
									<Tab.Container activeKey={tabKey} onSelect={k => setTabKey(k)}>
										<div className="action-plan-inner-tab-header-wrap">
											<div className="tab-left-header">
												<Nav variant="pills" >
													<Nav.Item>
														<Nav.Link eventKey="action_plan_comment">Comment</Nav.Link>
													</Nav.Item>
													<Nav.Item>
														<Nav.Link eventKey="action_plan_history">History</Nav.Link>
													</Nav.Item>
													{
														taskDetails.respondent_id && <Nav.Item>
															<Nav.Link eventKey="action_plan_respondant_response">Respondent Report</Nav.Link>
														</Nav.Item>
													}

												</Nav>
											</div>
										</div>
										<Tab.Content className="action-plan-inner-tab-content">
											<Tab.Pane eventKey="action_plan_comment" mountOnEnter>
												<ActionPlanComment taskDetails={taskDetails} />
											</Tab.Pane>
											<Tab.Pane eventKey="action_plan_history" mountOnEnter unmountOnExit>
												<ActionPlanHistory taskHistoryPrev={taskHistory} taskDetails={taskDetails} />
											</Tab.Pane>
											<Tab.Pane eventKey="action_plan_respondant_response" mountOnEnter unmountOnExit>
												{taskDetails.respondent_id && <RespondentResponse reasonList={reasons} />}

											</Tab.Pane>
										</Tab.Content>
									</Tab.Container>

								</div>
							</div>
						</div>

						<div className="gathering-section">
							<div className="status-field">
								<span>Status: </span>
								<div className="status-slect-box">
									<Select
										value={selectedStatusIndex}
										onChange={onStatusChange}
										options={statusOptions}
										defaultValue={selectedStatusIndex}
										className="customActModal"
									/></div>
							</div>


							<div className="details-table">
								<div className="table-heading">
									<span className="table-head">Details</span>
								</div>
								<div className="table-data Assignee-table-data">
									<p className="data">Assignee: </p>
									<div className="editflex">
										{editAssignee ?
											<div className="select-edit-flex">
												<Select
													value={selectedAssignee}
													onChange={onAsigneeChange}
													options={taskAssignee}
													defaultValue={selectedAssignee}
													className="assigneeActModal"
												/>
												<button type="button" className="theme-delete" onClick={() => setEditAssignee(!editAssignee)}>X</button>
											</div>
											:
											<div className="select-edit-flex">
												<p className="data">{(selectedAssignee && selectedAssignee.label) ? selectedAssignee.label : taskDetails?.assigned_to_user_name}</p>
												<button type="button" className="theme-edit" onClick={() => setEditAssignee(!editAssignee)}>Edit</button>
											</div>
										}


									</div>
								</div>
								<div className="table-data">
									<p className="data">Created By: </p>
									<p className="data">{taskDetails?.created_by_user_name && taskDetails?.created_by_user_name}</p>
								</div>
								<div className="table-data">
									<p className="data">Created Date: </p>
									<p className="data">{taskDetails?.created_at_hf && taskDetails?.created_at_hf}</p>
								</div>
								<div className="table-data">
									<p className="data">Due Date: </p>
									<p className="data">{taskDetails?.due_date_hf && taskDetails?.due_date_hf}</p>
								</div>
							</div>

						</div>
					</div>
				</Modal.Body>
			</Modal>

		</div>
	)
}

export default ActionModal;