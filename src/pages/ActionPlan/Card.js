import React from 'react'

const CustomCard = props => {
    const handleEditActionPlan = (props) => {
        alert(`Title:- ${props.title} , Description: ${props.description} , Date : ${props.created_at}, Status: ${props.status}`);
    }
    return (
        <div className="cards" onClick={() => handleEditActionPlan(props)}>
            <header>
                <div className="title">{props.title}</div>
                <div className="date">{props.created_at}</div>
            </header>
            <div>
                <div>{props.status}</div>
                <div><i>{props.description}</i></div>
            </div>
            <footer>
                <div>@Qaiznex</div>
            </footer>
        </div>
    )
}

export default CustomCard;
