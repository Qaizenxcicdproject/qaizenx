import React, { useState, useEffect, useContext } from "react";
import './AddSingle.scss';
import { useHistory } from 'react-router-dom';
import { Tab, Breadcrumb, Nav } from 'react-bootstrap';
import AddSingleFields from './AddSingleFields';
import useForm from 'react-hook-form';
import configURL from 'config/config';
import { toast } from 'react-toastify';
import Axios from "utility/Axios";
import AppContext from 'store/AppContext';
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import FormData from 'utility/AppFormData';

const OtherContactAdd = () => {

    const { languageObj = {} } = useContext(AppContext)
    const [key, setKey] = useState('personal');
    const [demographicData, setDemographicData] = useState();
    const { register, handleSubmit, errors, clearError, setValue } = useForm();
    const [formData, setFormData] = useState({});
    const [phoneNumber, setPhoneNumber] = useState("");
    const history = useHistory();

    const makeEmptyFormObject = (fieldJson) => {
        let tempFormData = {};
        for (var field in fieldJson) {
            if (fieldJson.hasOwnProperty(field)) {
                let tempFormFields = {};
                let formFields = fieldJson[field];
                formFields.forEach((formField, index) => {
                    tempFormFields[formField.name] = '';
                });
                tempFormData[field] = tempFormFields;
            }
        }

        let personalObj = {
            'unique_id': '',
            'fname': '',
            'lname': '',
            'email': '',
            'contact_number': ''
        }

        tempFormData['Personal'] = personalObj;
        setFormData(tempFormData);
    };

    useEffect(() => {
        clearError(["both", "contact_number"])
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [phoneNumber])

    const handleChange = (e, tab) => {
        const { value, type } = e.target;
        const name = e.target.getAttribute('name');
        setValue(name, value.trim(), { shouldValidate: true })
        if (name === 'email') {
            clearError("both")
        }
        if (type === 'radio' || type === 'select-one' || type === 'text') {
            setFormData({
                ...formData,
                [tab]: {
                    ...formData[tab],
                    [name]: value.trim()
                }
            });
        }
        if (type === 'checkbox') {
            // no checkbox in the form
        }
    }

    const handleDateChange = (date, activeTab, field_name = 'dob') => {
        setFormData({ ...formData, [activeTab]: { ...formData[activeTab], [field_name]: new Date(date.getTime() - (date.getTimezoneOffset() * 60000)).toJSON() } });
    }

    const submitFormData = (data = {}) => {
        var sendData = new FormData();
        let phone = (phoneNumber.length <= 4) ? "" : phoneNumber
        sendData.append("type", 'other');
        sendData.append("unique_id", data.unique_id);
        sendData.append("fname", data.fname || "");
        sendData.append("lname", data.lname || "");
        if (data.email !== "") {
            sendData.append("email", data.email);
        }
        if (phone !== "") {
            sendData.append("contact_number", phone);
        }
        delete formData.Personal;
        sendData.append("demographics", JSON.stringify(formData));
        Axios.post(configURL.addSinglePeople, sendData).then(res => {
            if (res.data.success) {
                toast.success(res.data.message);
                history.push("/people-directory", {
                    page_key: "other_contact"
                })
            } else {
                toast.warn(res.data.message || 'Something went wrong.');
                setKey('personal')
            }
        }).catch(err => {
            console.log(err)
        })
    }

    useEffect(() => {
        var formData = new FormData();
        formData.append("type", 'other');
        Axios.post(configURL.demographicURL, formData).then(res => {
            if (res.data.success !== undefined && res.data.success === true) {
                setDemographicData(res.data.data[languageObj.curLang]);
                makeEmptyFormObject(res.data.data[languageObj.curLang]);
            } else {
                toast.warn(res.data.message)
            }
        }).catch(err => {
            console.log(err)
        })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <React.Fragment>

            <section className="Page-AddSingle">

                {/* Breadcrumb Start  */}
                <div className="breadcrumb_ezi">
                    <Breadcrumb>
                        <Breadcrumb>
                            <Breadcrumb.Item onClick={() => history.push("/people-directory")}>
                                {languageObj.translate('PeopleDirectory.1')}
                            </Breadcrumb.Item>
                            <Breadcrumb.Item onClick={() => history.push("/people-directory", {
                                page_key: "other_contact"
                            })}>{languageObj.translate('OtherCustomer.1')}</Breadcrumb.Item>
                            <Breadcrumb.Item>{languageObj.translate('NewUser.1')}</Breadcrumb.Item>
                        </Breadcrumb>
                    </Breadcrumb>
                </div>
                {/* // Breadcrumb End */}

                <div className="add-single-card">
                    <form onSubmit={handleSubmit(submitFormData)} className="personal-form">
                        <div className="single-card-header">
                            <div className="card-user-info">
                                <h4 className="name">{languageObj.translate('UserName.1')}</h4>
                                <p className="role">{languageObj.translate('Role.1')}</p>
                            </div>
                            <div className="single-card-btn">
                                <button type="reset" className="single-clear">{languageObj.translate('ClearAll.1')}</button>
                                <button type="submit" className="btn-ripple single-save" onClickCapture={() => setKey('personal')}>{languageObj.translate('Save.1')}</button>
                            </div>
                        </div>
                        <div className="tablist_ezi">
                            <Tab.Container activeKey={key} onSelect={k => setKey(k)}>
                                <div className="tab-header-wrap">
                                    <div className="tab-left-header">
                                        <Nav variant="pills" >
                                            <Nav.Item>
                                                <Nav.Link eventKey="personal">{languageObj.translate('Personal.1')}</Nav.Link>
                                            </Nav.Item>
                                            <Nav.Item>
                                                <Nav.Link eventKey="demographic">{languageObj.translate('Identifiers.1')}</Nav.Link>
                                            </Nav.Item>
                                        </Nav>
                                    </div>
                                </div>
                                <Tab.Content>
                                    <Tab.Pane eventKey="personal">
                                        <div className="add-single-field-wrapper">
                                            <label>
                                                <input type="text" placeholder="Enter Id" name="unique_id" onChange={(e) => handleChange(e, 'Personal')} ref={register({ required: true })} />
                                                {errors.unique_id && errors.unique_id.type === "required" && <span className="error_cu">* Unique Id required.</span>}
                                                {errors.unique_id && errors.unique_id.type === "backend" && <span className="error_cu">{errors.unique_id.message}</span>}
                                            </label>
                                            <label>
                                                <input type="text" placeholder="Enter First Name" name="fname" onChange={(e) => handleChange(e, 'Personal')} ref={register} />
                                            </label>

                                            <label>
                                                <input type="text" placeholder="Enter  Last Name" name="lname" onChange={(e) => handleChange(e, 'Personal')} ref={register} />
                                            </label>

                                            <label>
                                                <input
                                                    type="text"
                                                    placeholder="Enter  Email"
                                                    name="email"
                                                    onChange={(e) => handleChange(e, 'Personal')}
                                                    ref={register({
                                                        // eslint-disable-next-line
                                                        pattern: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/i
                                                    })}
                                                />
                                                {errors.email && errors.email.type === 'pattern' && <span className="error_cu">Please Enter Valid Email id</span>}
                                                {errors.email && errors.email.type === "backend" && <span className="error_cu">{errors.email.message}</span>}
                                                {errors.both && errors.both.type === "backend" && <span className="error_cu">{errors.both.message}</span>}
                                            </label>
                                            <div className="phone-input-wrap">
                                                <PhoneInput
                                                    placeholder="Enter phone number"
                                                    country={'in'}
                                                    value={phoneNumber}
                                                    onChange={setPhoneNumber} />
                                                {errors.contact_number && errors.contact_number.type === "backend" && <span className="error_cu">{errors.contact_number.message}</span>}
                                            </div>
                                        </div>
                                    </Tab.Pane>
                                    <Tab.Pane eventKey="demographic">
                                        <div className="demographic_data add-single-field-wrapper">
                                            <AddSingleFields demographicData={demographicData} activeTab='identifier' selectChangeHandler={handleChange} register={register} dateChange={handleDateChange} />
                                        </div>
                                    </Tab.Pane>

                                </Tab.Content>

                            </Tab.Container>
                        </div>
                    </form>
                </div>
                {/* { data.loading && <EziLoader />} */}
            </section>

        </React.Fragment>
    )

}

export default OtherContactAdd;