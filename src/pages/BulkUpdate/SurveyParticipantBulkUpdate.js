import React, { useCallback, useEffect, useState, useContext } from 'react';
import './BulkUpdate.scss';
import Dropzone from "hooks/Dropzone";
import configURL from 'config/config';
import Axios from "utility/Axios";
import { toast } from 'react-toastify';
import AppContext from 'store/AppContext';
import FormData from 'utility/AppFormData';
import { useParams } from "react-router-dom";

var file;
const SurveyParticipantBulkUpdate = (props) => {
    const { languageObj = {}, EziLoader } = useContext(AppContext)
    const [upload, setUpload] = useState(false);
    const [excelDownloadLink, setExcelDownloadLink] = useState('');
    const onDrop = useCallback(acceptedFiles => {
        file = acceptedFiles[0];
    }, []);
    const urlParams = useParams();
    const downloadExcel = () => {
        if (excelDownloadLink !== '') {
            window.open(excelDownloadLink, '_blank').focus();
        } else {
            toast.error(`Invalid Download Link !`);
        }
    }

    const getExcelDownloadLink = () => {
        EziLoader.show();
        const formdata = new FormData();
        formdata.append("survey_id", urlParams.survey_id);
        formdata.append("selected_people", JSON.stringify(props.selectedParticipants || []));
        formdata.append("selected_column", JSON.stringify(props.selectedColumns || []));
        formdata.append("selected_column_name", JSON.stringify(props.selectedColumnsName || []));
        Axios.post(configURL.survey_participant, formdata).then(res => {
            if (res.data.success !== undefined && res.data.success) {
                setExcelDownloadLink(res.data.excel_link);
            } else {
                toast.warn(res.data.message);
            }
            EziLoader.hide();
        })
    }

    useEffect(() => {
        if (upload) {
            EziLoader.show();
            if (typeof (file) === "undefined") {
                EziLoader.hide();
                toast.warn("Please attach file in drop area before upload.");
            }
            else if (file.size >= 50000) {
                EziLoader.hide();
                toast.warn("Please do not upload file size greater than 50MB");
            }
            else if (file.name.split('.').pop() !== "xlsx" && "xls") {
                EziLoader.hide();
                toast.warn("Please upload file in xlsx or xls format");
            }
            else {
                const fileupload = new FormData();
                fileupload.append("language", languageObj.curLang);
                fileupload.append("file", file);
                fileupload.append("survey_id", urlParams.survey_id);
                Axios.post(configURL.survey_participant_bulk_update, fileupload).then(response => {
                    if (response.data.success) {
                        toast.success(response.data.message.toString(), {
                        });
                        EziLoader.hide();
                        props.hide();
                    }
                    else {
                        toast.warn(response.data.message.toString());
                        EziLoader.hide();
                    }
                })
            }

        }
        setUpload(false);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [upload]);

    useEffect(getExcelDownloadLink, [])

    return (
        <React.Fragment>
            <section className="Page-BulkUpdate">
                <div className="breadcrumb_ezi people-directory-breadcrumb">
                    <div className="column-header">
                        <button className="survey_rollback m-0" onClick={props.hide} title="Back"></button>
                        <h1 className="page-heading m-0">{languageObj.translate('BulkUpdate.1')}</h1>

                        <button className="btn-ripple  download-template" onClick={downloadExcel}>
                            Download Template of selected Participants
                        </button>
                    </div>
                </div>
                <div className="dropzone-wrap">
                    <Dropzone onDrop={onDrop} accept={"application/vnd.ms-excel/*"} />
                </div>
                <div className="dropzone-btn">
                    <input type="button" className="btn-ripple upload-btn" onClick={() => { setUpload(true) }} value="Upload Selected File" />
                </div>
                <span className="dropzone-supported-file"> {languageObj.translate('Filessupported.1')}, Max File Size is 50MB</span>
            </section>
        </React.Fragment>
    )
}
export default SurveyParticipantBulkUpdate;