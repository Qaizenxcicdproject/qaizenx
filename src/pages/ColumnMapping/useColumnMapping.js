import { useState, useEffect } from 'react';

const useColumnMapping = () => {
    const [mappingData, setMappingData] = useState();
    const handleChange = selectedOption => {
        setMappingData(selectedOption);
    };


    useEffect(() => {
        let data = localStorage.getItem("val").split(",");
        setMappingData(data);
    }, []);

    return {
        mappingData,
        handleChange
    };
}
export default useColumnMapping;
