import React, { useEffect } from 'react';
import { connect } from "react-redux";
import * as AppActions from "store/actions";
import { useParams } from "react-router-dom";
import configURL from 'config/config';
import Axios from 'axios';
import { toast } from 'react-toastify';
import FormData from 'utility/AppFormData';

const CompanyLogin = ({ loginUser, logOutUser, history, }) => {
    let { login_token = "", redirect_to = "", express_survey_id = "" } = useParams()
    useEffect(() => {
        if (!login_token || login_token === "") {
            history.replace('/login')
        }
        logOutUser()
        handleVerifyToken()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const handleRedirection = () => {
        let redirectTo = "/"
        switch (redirect_to) {
            case 'express-survey':
                redirectTo = `/express-survey/${express_survey_id}`
                break;

            default:
                redirectTo = "/"
                break;
        }
        history.replace(redirectTo);
    }

    /**
     * Login With Token
     * 
     * @param {Object} data 
     */
    const handleVerifyToken = () => {
        let formData = new FormData();
        formData.append("token", login_token);
        Axios.post(configURL.verify_partner_token, formData).then(response => {
            if (response.data.success !== undefined && response.data.success === true) {
                let userData = {
                    name: response.data.user.first_name,
                    first_name: response.data.user.first_name,
                    last_name: response.data.user.last_name,
                    cell_number: response.data.user.cell_number,
                    id: response.data.user.id,
                    email: response.data.user.email,
                    auth_token: response.data.access_token,
                    profile_pic_url: response.data.user.profile_pic_url,
                    assigned_roles: response.data.user.assigned_roles || [],
                    company_id: response.data.current_account.id,
                    partner_id: response.data.current_partner_account.id || null,
                    timestamp: new Date().toString(),
                    timezone: response.data.current_account.timezone || "Asia/Kolkata"
                };
                localStorage.setItem("appState", JSON.stringify({
                    isLoggedIn: true,
                    user: userData,
                    access_matrix: response.data.access_matrix || [],
                    allowed_apps: response.data.applications || [],
                    current_app_detail: response.data.current_account || null,
                    current_partner_account: response.data.current_partner_account || null,
                }));
                localStorage.setItem('isLogin', true);
                loginUser()
                handleRedirection()
            } else {
                toast.warn(response.data.message);
                history.replace("/login");
            }
        }).catch(error => {
            if (!error.response) {
                toast.warn("Server is not responding please try after some time.");
            }
            if (error.response && error.response.status) {
                const { data = {} } = error.response
                toast.warn(data.message || "Something went wrong Please try later!!")
            }
            history.replace("/login");
        })
    }

    return (
        <p className="app_loading">Verifying Details<span>.</span><span>.</span><span>.</span></p>
    )
}


const mapDispatchToProps = dispatch => {
    return {
        loginUser: () => dispatch(AppActions.loginUser()),
        logOutUser: () => dispatch(AppActions.logOutUser())
    }
}

export default connect(null, mapDispatchToProps)(CompanyLogin);