import React, { useState, useEffect, useContext, useRef } from "react";
import './AddCompany.scss';
import { Breadcrumb, Tab, Nav } from 'react-bootstrap';
import useForm from 'react-hook-form';
import configURL from 'config/config';
import { TIME_ZONES } from 'constants/constants';
import Axios from "utility/Axios";
import { toast } from 'react-toastify';
import Select from 'react-select';
import { outlineRemove } from 'utility/helper';
import AppContext from 'store/AppContext';
import FormData from 'utility/AppFormData';

const CompanyProfile = () => {

    const { EziLoader, appState, accesFeature, languageObj, handleUnAuthWarn } = useContext(AppContext)
    const [companyTab, setCompanyTab] = useState('general');
    const { register, handleSubmit, errors, reset } = useForm();
    const [submitClicked, setSubmitClicked] = useState(false);
    const [companyData, setCompanyData] = useState({});
    const [timeZone, setTimeZone] = useState({ label: "Asia/Kolkata", value: "Asia/Kolkata" });
    const company_id = appState.current_app_detail.id || null
    const imageRef = useRef(null)

    /**
     * Save Company
     * @param {Object} data 
     */
    const submitFormData = (data) => {
        if (!accesFeature.update_company) {
            handleUnAuthWarn()
            return;
        }
        EziLoader.show()
        const sendData = new FormData();
        let logo = data.company_logo.length > 0 ? data.company_logo[0] : null
        if (logo) {
            sendData.append("company_logo", logo);
        }
        delete data.company_logo
        data.timezone = timeZone.value
        data.start_date = companyData.start_date
        data.end_date = companyData.end_date
        sendData.append("insertdata", JSON.stringify({ 'data': data }));
        sendData.append("account_id", company_id)
        Axios.post(configURL.save_compnay_details, sendData).then(response => {
            EziLoader.hide()
            if (response.data.success === true) {
                toast.success(response.data.message || "Company Saved");
            } else {
                toast.warn(response.data.message || "Something went wrong");
            }
        }).catch(err => {
            console.log(err.toString())
            EziLoader.hide()
        })
    }


    /**
     * Handle Form Error Tab
     */
    useEffect(() => {
        let errorsKey = Object.keys(errors)
        if (submitClicked && errorsKey.length > 0) {
            let general = ["companyname", "companywebsite", "companyaddress", "city", "zipcode", "webaddress"]
            let additional = ["first_name", "password", "phone", "email"]
            let errorTab = "";
            if (errorsKey.length > 0) {
                if (general.some(item => errorsKey.includes(item)) && errorTab === "") {
                    errorTab = "general";
                }
                if (additional.some(item => errorsKey.includes(item)) && errorTab === "") {
                    errorTab = "additional";
                }
                if (errorTab !== "") {
                    setCompanyTab(errorTab)
                }
            }
            setSubmitClicked(false)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [submitClicked, errors]);

    /**
     * Get Company Details
     */
    const getCompanyDetails = () => {
        if (company_id) {
            EziLoader.show()
            let formData = new FormData()
            formData.append("account_id", company_id)
            Axios.post(configURL.get_compnay_details, formData).then(response => {
                EziLoader.hide()
                if (response.data.success) {
                    setCompanyData(response.data.result)
                    if (response.data.result.timezone) {
                        setTimeZone({ label: response.data.result.timezone, value: response.data.result.timezone })
                    }
                }
            }).catch(err => {
                EziLoader.hide()
            })
        }
    }

    useEffect(getCompanyDetails, []);

    /**
     * Handle Form Reset
     */
    const clearForm = () => {
        reset({
            companyname: "",
            companywebsite: "",
            companyaddress: "",
            city: "",
            zipcode: "",
            first_name: "",
            password: "",
            phone: "",
            email: "",
            billingemailaddress: "",
            subscriptionplan: "",
            subscriptionstartdate: "",
            subscriptionenddate: "",
            webaddress: "",
            paymentmode: ""
        })
    }

    /**
     * Handle Image Upload
     * @param {Event} e 
     */
    const previewFile = () => {
        const file = document.querySelector('input[type=file]').files[0];
        const reader = new FileReader();
        reader.addEventListener("load", function () {
            imageRef.current.src = reader.result
        }, false);
        reader.readAsDataURL(file);
    }

    return (
        <React.Fragment>
            <section className="Page-AddCompany" >
                <form onSubmit={handleSubmit(submitFormData)} className="company-form">
                    <div className="breadcrumb_ezi">
                        <Breadcrumb>
                            <Breadcrumb.Item>{languageObj.translate('Settings.1')}</Breadcrumb.Item>
                            <Breadcrumb.Item>{languageObj.translate('companyprofile.1')}</Breadcrumb.Item>
                        </Breadcrumb>
                        <div className="column-header">
                            <h1 className="page-heading">{languageObj.translate('companydetails.1')}</h1>
                            <div className="column-header-btn">
                                <button type="button" className="btn-ripple clear_all" disabled={!accesFeature.update_company} title={!accesFeature.update_company ? 'Unauthorized Operation' : ''} onClick={clearForm} >{languageObj.translate("ClearAll.1")}</button>
                                <button type="submit" className="btn-ripple add-new" onClickCapture={() => setSubmitClicked(true)}>{languageObj.translate("Save.1")}</button>
                            </div>
                        </div>
                    </div>
                    <div className="add-company-form-wrap">
                        <div className="company-info-header">
                            <div className="conpany-name-id-wrap">
                                <span className="title">{companyData.companyname || 'Company name'}</span>
                                {/* <span className="subtitle">Account ID</span> */}
                            </div>
                            <div className="company-add-logo-wrap">
                                <img ref={imageRef} alt="" src={companyData.company_logo || require(`../../assets/images/partner-default-logo.svg`)} className="company-default-logo" />
                                <div className="company-logo-upload-position">
                                    <div className="company-logo-upload-wrapper">
                                        <span className="company-logo-upload-btn"></span>
                                        <input type="file" name="company_logo" ref={register} className="company-logo-upload-input" onChange={previewFile} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tablist_ezi">
                            <Tab.Container activeKey={companyTab} onSelect={k => setCompanyTab(k)}>
                                <div className="tab-header-wrap">
                                    <div className="tab-left-header">
                                        <Nav variant="pills" >
                                            <Nav.Item>
                                                <Nav.Link eventKey="general">{languageObj.translate('general.1')}</Nav.Link>
                                            </Nav.Item>
                                            <Nav.Item>
                                                <Nav.Link eventKey="additional">{languageObj.translate('additional.1')}</Nav.Link>
                                            </Nav.Item>
                                        </Nav>
                                    </div>
                                </div>
                                <Tab.Content>
                                    <Tab.Pane eventKey="general">
                                        <div className="add-company-field-wrapper">
                                            <label>
                                                <input type="text"
                                                    placeholder={languageObj.translate('companyname.1')}
                                                    name="companyname"
                                                    ref={register({ required: true })}
                                                    defaultValue={companyData.companyname || ""} />
                                                {errors.companyname && errors.companyname.type === 'required' &&
                                                    <span className="error_cu">{languageObj.translate('CompanyNameisrequired.1')}</span>}
                                            </label>
                                            <label>
                                                <input type="text"
                                                    placeholder={languageObj.translate('CompanyWebsite.1')}
                                                    name="companywebsite"
                                                    ref={register({ required: true })}
                                                    defaultValue={companyData.companywebsite || ""} />
                                                {errors.companywebsite && errors.companywebsite.type === 'required' &&
                                                    <span className="error_cu">{languageObj.translate('CompanyWebsiteNameisrequired.1')}</span>}
                                            </label>
                                            <label>
                                                <input type="text"
                                                    placeholder={languageObj.translate('CompanyAddress.1')}
                                                    name="companyaddress"
                                                    ref={register({ required: true })}
                                                    defaultValue={companyData.companyaddress || ""} />
                                                {errors.companyaddress && errors.companyaddress.type === 'required' &&
                                                    <span className="error_cu">{languageObj.translate('CompanyAddressisrequired.1')}</span>}
                                            </label>
                                            <label>
                                                <select name="industry" value={companyData.industry || ""} ref={register}
                                                    onChange={({ target }) => {
                                                        setCompanyData({ ...companyData, industry: target.value })
                                                    }}>
                                                    <option >Select Industry Vertical</option>
                                                    <option value="IT">IT</option>
                                                    <option value="Software">Software</option>
                                                </select>
                                            </label>

                                            <label>
                                                <input type="text"
                                                    placeholder={languageObj.translate('City.1')}
                                                    name="city"
                                                    ref={register({ required: true })}
                                                    defaultValue={companyData.city || ""} />
                                                {errors.city && errors.city.type === 'required' &&
                                                    <span className="error_cu">{languageObj.translate('Cityisrequired.1')}</span>}
                                            </label>
                                            <label>
                                                <input type="text"
                                                    placeholder={languageObj.translate('Zipcode.1')}
                                                    name="zipcode"
                                                    ref={register({ maxLength: 7, required: true, pattern: /^[0-9]*$/ })}
                                                    defaultValue={companyData.zipcode || ""} />
                                                {errors.zipcode && errors.zipcode.type === 'required' &&
                                                    <span className="error_cu">{languageObj.translate('Zipcodeisrequired.1')}</span>}
                                                {errors.zipcode && errors.zipcode.type === 'pattern' &&
                                                    <span className="error_cu">{languageObj.translate('Zipcodeisnotvalid.1')}</span>}
                                                {errors.zipcode && errors.zipcode.type === 'maxLength' &&
                                                    <span className="error_cu">{languageObj.translate('Maxzipcodelengthis7.1')}</span>}
                                            </label>
                                            <label>
                                                <Select
                                                    ref={register}
                                                    options={TIME_ZONES}
                                                    value={timeZone}
                                                    styles={outlineRemove}
                                                    placeholder="Time Zone"
                                                    onChange={(selectedOption) => {
                                                        setTimeZone(selectedOption)
                                                    }}
                                                />
                                            </label>
                                        </div>
                                    </Tab.Pane>
                                    <Tab.Pane eventKey="additional">
                                        <div className="add-company-field-wrapper">
                                            <label>
                                                <input type="text"
                                                    placeholder={languageObj.translate('Name.1')}
                                                    name="first_name"
                                                    ref={register({ required: true })}
                                                    defaultValue={companyData.first_name || ""} />
                                                {errors.first_name && errors.first_name.type === 'required' &&
                                                    <span className="error_cu">{languageObj.translate('Nameisrequired.1')}</span>}
                                            </label>
                                            <label>
                                                <input type="text"
                                                    placeholder={languageObj.translate('ContactNumber.1')}
                                                    name="phone" ref={register({ required: true })}
                                                    defaultValue={companyData.phone || ""} />
                                                {errors.phone && errors.phone.type === 'required' &&
                                                    <span className="error_cu">{languageObj.translate('PhoneNumberisrequired.1')}</span>}
                                            </label>

                                            <label>

                                                <input type="text"
                                                    placeholder="Login email id"
                                                    name="email"
                                                    ref={register({
                                                        required: true,
                                                        // eslint-disable-next-line
                                                        pattern: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/i
                                                    })} defaultValue={companyData.email || ""}
                                                    disabled={(company_id)}
                                                />
                                                {errors.email && errors.email.type === 'required' &&
                                                    <span className="error_cu">{languageObj.translate('Emailisrequired.1')}</span>}
                                                {errors.email && errors.email.type === 'pattern' &&
                                                    <span className="error_cu">{languageObj.translate('PleaseEnterValidEmailid.1')}</span>}
                                            </label>
                                        </div>
                                    </Tab.Pane>
                                </Tab.Content>
                            </Tab.Container>
                        </div>
                    </div>
                </form>
            </section>
        </React.Fragment>
    )
}

export default CompanyProfile