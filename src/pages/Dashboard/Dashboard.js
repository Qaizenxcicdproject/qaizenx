import React, { Fragment, useState, useContext, useEffect } from 'react';
import './Dashboard.scss';
import RecentSurveyCard from "components/widgets/RecentSurveyCard"
import DashboardHead from './DashboardHead';
import KpiWidgets from './KpiWidgets';
import Axios from 'utility/Axios';
import configURL from 'config/config';
import GreetUser from './GreetUser';
import AppContext from 'store/AppContext';
import IframeInfoModel from 'components/IframeInfoModel';
import { FaAngleDoubleRight } from 'react-icons/fa';


const Dashboard = () => {
    const { languageObj } = useContext(AppContext);
    const [headerData, setHeaderData] = useState(null);
    const [showIframeModel, setShowIframeModel] = useState(false);
    const appStore = JSON.parse(localStorage.getItem("appState"));
    let partnerId = appStore.user.partner_id;
    let showReportButton = false;
    if (partnerId === "947f38b7-3ecf-4f41-8bd6-6d07d9f6c4e5") {
        showReportButton = true;
    }
    let appUrl = "";
    if (window.location.href) {
        appUrl = window.location.href
    }

    const setIframeModel = () => {
        setShowIframeModel(true)
    }

    const resetIframeModel = () => {
        setShowIframeModel(false)
    }
    useEffect(() => {
        Axios.post(configURL.get_dashboard_count_graphs, {}).then(response => {
            if (response.data.success !== undefined && response.data.success === true) {
                setHeaderData(response.data.results)
            }
        }).catch(error => {
            console.log(error);
        })
    }, [])

    return (
        <Fragment>
            <section className="Page-Dashboard">
                <div className="column-header">
                    <div>
                        <GreetUser />
                        <p className="dashboard-subtitle">{languageObj.translate('Dashboard.1')}</p>
                    </div>
                    <div className="column-header-btn">
                        {showReportButton && <button className="iframe-model-check" onClick={setIframeModel}>View Sample Report <span className="arrow-span"><FaAngleDoubleRight /></span></button>}
                        {showIframeModel &&
                            <IframeInfoModel
                                src={`${appUrl}customer-experience/4c10a64a-b989-4197-a7bb-277eea2249de/?header_hide=true`}
                                width="100%"
                                height="500px"
                                show={showIframeModel}
                                onHide={resetIframeModel}
                                title="Sample Report"
                            />
                        }
                    </div>
                </div>
                <KpiWidgets />
                <div className="dashboard-col-wrap">
                    <RecentSurveyCard />
                    <DashboardHead headerData={headerData} />
                </div>
            </section>
        </Fragment>
    )
}

export default Dashboard;