import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import RecentTemplateCard from "components/widgets/RecentTemplateCard"
import AppContext from 'store/AppContext';
import RecentActivity from 'components/RecentActivity';

const DashboardHead = props => {
    const { headerData } = props;
    const history = useHistory();
    const { languageObj = {}, handleUnAuthWarn = {}, accesFeature = {} } = useContext(AppContext)

    return (
        <div className="graph-card-main-wrap">
            <section className="graph-card-left-section">
                <div className="survey-info-card-wrap">
                    <div className="survey-info-card--common active-surveys" onClick={() => {
                        history.push('survey-dashboard/category-survey-dashboard', {
                            category_id: "",
                            category_name: "All Categories",
                            survey_status: "active",
                        });
                    }}>
                        <div className="surveys-icon-wrap--common">
                            <span className="surveys-icon--common active-surveys_ic"></span>
                        </div>
                        <div className="survey-info-text-wrap">
                            <span className="title_c">{languageObj.translate('ActiveSurveys.1')}</span>
                            <span className="subtitle_c">{headerData ? headerData.counts.active : 0}</span>
                        </div>
                    </div>

                    <div className={`survey-info-card--common create-surveys ${accesFeature.create_survey || "access_lock"}`} onClick={() => {
                        if (accesFeature.create_survey) {
                            history.push("/survey-dashboard/categories", { action: "add-new" })
                        } else {
                            handleUnAuthWarn();
                        }
                    }
                    }>
                        <div className="surveys-icon-wrap--common">
                            <span className="surveys-icon--common create-surveys_ic"></span>
                        </div>
                        <div className="survey-info-text-wrap" >
                            <span className="title_c">{languageObj.translate('Create.1')}</span>
                            <span className="subtitle_c">{languageObj.translate('NewSurvey.1')}</span>
                        </div>
                    </div>
                </div>
                <RecentTemplateCard />
                <RecentActivity />
            </section>
        </div>
    )
}

export default DashboardHead;