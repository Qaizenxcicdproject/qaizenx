import React, { useState, useEffect, useContext } from 'react';
import Axios from 'utility/Axios';
import configURL from 'config/config';
import InfoModal from 'components/HeaderInfoModal';
import { useHistory } from "react-router-dom";
import AppContext from "store/AppContext";

const KpiWidgets = () => {
    const [kpiScores, setKpiScores] = useState([])
    const [infoModal, setInfoModal] = useState({ show: false, content: '' })
    const history = useHistory()
    const {
        accesFeature = {},
        handleUnAuthWarn = {},
    } = useContext(AppContext);
    useEffect(() => {
        Axios.post(configURL.get_kpi_widgets, {}).then(response => {
            if (response.data.success !== undefined && response.data.success === true) {
                setKpiScores(response.data.result)
            }
        }).catch(error => {
            console.log(error);
        })
    }, [])

    return (
        <div className="column-header widgets-wrapper">
            {kpiScores.map((el, i) => (
                <div className="widget-wrapper" key={i} >
                    <div className="score-left" onClick={() => {
                        if (accesFeature.create_report || accesFeature.edit_report || accesFeature.analyse_report) {
                            history.push(`/customer-experience/${el.survey_id || 'dummy'}`)
                        } else {
                            handleUnAuthWarn()
                        }
                    }}>
                        <h3 className="score-label">{el.kpi_label || '---'}</h3>
                        <h3 className="score-count">{el.score || '0'}</h3>
                        <div className="score-indicators">
                            <span className={`${(el.diff_score > 0) ? 'arrow-up' : 'arrow-down'} arrow-indicator`}></span>
                            <span className={`since_number ${(el.diff_score > 0) ? 'positive' : 'negative'}`}>{el.diff_score}</span>
                            <span className={`since_text`}>since last {el.kpi_days || 0} Days</span>
                        </div>
                    </div>
                    <span className={`score-right ${(el.diff_score > 0) ? 'positive' : 'negative'}`}></span>
                    <div className={`score-info`} onClick={() => setInfoModal({ show: true, content: el.kpi_description || '' })}>
                    </div>
                </div>
            ))}
            <InfoModal
                show={infoModal.show}
                title='Widget Description'
                description={infoModal.content}
                onHide={() => setInfoModal({ show: false, content: '' })}
            />
        </div>
    )
}

export default KpiWidgets;