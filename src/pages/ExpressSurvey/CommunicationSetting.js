import React, { useState, useEffect, useRef, Fragment, useContext } from "react";
import Axios from "utility/Axios"
import configURL from 'config/config';
import { dateConvert } from 'utility/helper';
import DateTimePicker from 'react-datetime-picker';
import { EditorState, convertToRaw } from 'draft-js';
import { stateFromHTML } from 'draft-js-import-html';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import DropdownPlaceholder from '../Survey/ParticipantSetting/DropdownPlaceholder';
/** Editor Imports ends */
import { CHANNEL_PLACEHOLDERS_SUBJECT, COMMUNICATION_TOOLBAR_CONFIG } from "constants/constants";
import { toast } from "react-toastify";
import { Spinner } from 'react-bootstrap';
import AppContext from 'store/AppContext';

const CommunicationSetting = ({ survey_id, prev, next }) => {
    const { EziLoader } = useContext(AppContext)
    const inputSubjectRef = useRef(null);
    const [toggleEmail, setToggleEmail] = useState(false);
    const [startDateEmail, setStartDateEmail] = useState(new Date());
    const [channelTemplates, setChannelTemplates] = useState({})
    const [selectedTemplate, setSelectedTemplate] = useState(null)
    const [editorState, setEditorState] = useState(EditorState.createEmpty())
    const [loading, setLoading] = useState(false)

    /**
     * Change Editor Data
     * @param {Object} editorState 
     */
    const onEditorStateChange = (editorState) => {
        handleInputChange({
            target: {
                name: "message",
                value: draftToHtml(convertToRaw(editorState.getCurrentContent()))
            }
        }, 'webEmail')
        setEditorState(editorState)
    }

    /**
     * Handle Variable insert on input
     */
    const insertVariable = (e) => {
        let textToInsert = e.target.value;
        if (inputSubjectRef?.current?.selectionStart) {
            let cursorPosition = inputSubjectRef.current.selectionStart;
            let textBeforeCursorPosition = inputSubjectRef.current.value.substring(0, cursorPosition);
            let textAfterCursorPosition = inputSubjectRef.current.value.substring(cursorPosition, inputSubjectRef.current.value.length);
            let finalText = textBeforeCursorPosition + textToInsert + textAfterCursorPosition;
            let customEvent = { ...inputSubjectRef };
            customEvent.target = { 'value': finalText, 'name': inputSubjectRef.current.name };
            handleInputChange(customEvent, inputSubjectRef.current.getAttribute("data-channel"));
        }
    }

    /**
     * Handle Template change
     * @param {Synthetic Event} e Input event
     * @param {String} tab selected tab
     */
    const changeTemplate = (e, tab) => {
        setSelectedTemplate(e.target.value);
        if (tab === 'webEmail' && channelTemplates.webEmail[e.target.value]) {
            setEditorState(EditorState.createWithContent(stateFromHTML(channelTemplates.webEmail[e.target.value]['message'])))
        }
    }

    /**
     * Handle Template content change
     * @param {Event} e 
     * @param {String} tab 
     */
    const handleInputChange = (e, tab) => {
        setChannelTemplates({
            ...channelTemplates,
            [tab]: {
                ...channelTemplates[tab],
                [selectedTemplate]: {
                    ...channelTemplates[tab][selectedTemplate],
                    [e.target.name]: e.target.value
                }
            }
        });
    }

    /**
     * Get All Channel Templates
     */
    const getChannelTemplates = () => {
        EziLoader.show()
        let formData = new FormData();
        formData.append("survey_id", survey_id);
        formData.append("isedit", false);
        Axios.post(configURL.publishingSettingListing, formData).then(res => {
            EziLoader.hide()
            if (res.data.success !== undefined && res.data.success === true) {
                setChannelTemplates(res.data.data)
            } else {
                toast.warn(res.data.message || "Something went wrong.");
            }
        }).catch(err => {
            toast.warn("Something went wrong.");
            EziLoader.hide()
            console.log(err);
        })
    }

    /**
     * Get Communication Data
     */
    const saveCommunication = () => {
        if (!selectedTemplate || selectedTemplate === "") {
            toast.warn('Please select template')
            return;
        }
         
        setLoading(true)
        let tempSurveySetting = JSON.parse(JSON.stringify(channelTemplates));
        let communicationTemp = {}
        if (tempSurveySetting.webEmail && selectedTemplate && selectedTemplate !== "") {
            let templateNameEmail = (tempSurveySetting.webEmail[selectedTemplate] && tempSurveySetting.webEmail[selectedTemplate].name) ? tempSurveySetting.webEmail[selectedTemplate].name : "";
            communicationTemp.webEmail = {
                'templateId': selectedTemplate,
                'schedule_datetime': (toggleEmail === false) ? 'now' : dateConvert(startDateEmail),
                'name': templateNameEmail,
                ...channelTemplates.webEmail[selectedTemplate]
            }

        }
        let formData = new FormData();
        formData.append("survey_id", survey_id);
        formData.append("template_json", JSON.stringify(communicationTemp));
        formData.append("isedit", false);
        Axios.post(configURL.saveChannelTemplates, formData).then(res => {
            setLoading(false)
            if (res.data.success !== undefined && res.data.success) {
                toast.info(res.data.message || "Survey Setting added.");
                next()
            } else {
                toast.warn(res.data.message || "Something went wrong.");
            }
        }).catch(err => {
            setLoading(false)
            console.log(err);
        })
    }

    useEffect(getChannelTemplates, [])

    return (
        <Fragment>
            <div className="tab-content-header">
                <span className="tab-content-header-title">Communication Setting</span>
                <span className="tab-content-header-subtitle">Set email template to send survey link to participants</span>
            </div>
            <div className="tab-content-body">
                <div className="communication-wrapper">

                    <div className="template-selection">
                        <select className="channel-select-mail" onChange={(e) => changeTemplate(e, 'webEmail')} value={selectedTemplate || ''}>
                            <option value="" key="0">Select Email Template </option>
                            {
                                channelTemplates.webEmail && Object.keys(channelTemplates.webEmail).map(key =>
                                    <option value={key} key={key} data-name={channelTemplates.webEmail[key]['name']}>
                                        {channelTemplates.webEmail[key]['name']}
                                    </option>
                                )
                            }
                        </select>
                    </div>
                    {selectedTemplate &&
                        <React.Fragment>
                            <div className="email-preview-wrap">
                                <div className="email-preview-header">
                                    <span className="email-preview-heading">Email Preview</span>
                                </div>
                                <div className="email-preview-content">
                                    <div className="placeholder-btn-wrap">
                                        {
                                            CHANNEL_PLACEHOLDERS_SUBJECT.map((item) =>
                                                <div key={item.id} className="placeholder-btn-inner">
                                                    <button type="button" className="insert-placeholder-btn" value={item.value} onClick={insertVariable}>{item.label}</button>
                                                </div>
                                            )
                                        }
                                    </div>
                                    <div className="channel-form-wrap">
                                        <div className="channel-form-field">
                                            <label>From Name:</label>
                                            <input type="text" className={`email-preview-input`} name="from_name" value={selectedTemplate ? channelTemplates.webEmail[selectedTemplate]?.from_name : 'QaizenX'} onChange={(e) => handleInputChange(e, 'webEmail')} />
                                        </div>
                                        <div className="channel-form-field">
                                            <label>From Email:</label>
                                            <input readOnly type="text" className={`email-preview-input`} name="from" value={selectedTemplate ? channelTemplates.webEmail[selectedTemplate]?.from : ""} onChange={(e) => handleInputChange(e, 'webEmail')} />
                                        </div>
                                        <div className="channel-form-field">
                                            <label>Subject:</label>
                                            <input type="text" ref={inputSubjectRef} data-channel="webEmail" className={`email-preview-input email-preview-input-edit`} name="subject" value={selectedTemplate ? channelTemplates.webEmail[selectedTemplate]?.subject : ''} onChange={(e) => handleInputChange(e, 'webEmail')} />
                                        </div>
                                        <div className="channel-form-field">
                                            <label>Message:</label>
                                            <Editor
                                                wrapperClassName="ezi-editor-wrapper"
                                                editorClassName="ezi-editor-inner"
                                                toolbarClassName="ezi-editor-toolbar"
                                                editorState={editorState}
                                                toolbar={COMMUNICATION_TOOLBAR_CONFIG}
                                                onEditorStateChange={onEditorStateChange}
                                                toolbarCustomButtons={[<DropdownPlaceholder />]}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="schedule-main-wrap">
                                <div className="schedule-toggle-date">
                                    <label className="schedule-label">Email Schedule  :</label>
                                    <div className="schedule-toggle-text">Now
                                <div className={`ezi-switch-toggle ${toggleEmail ? "on" : "off"}`} onClick={() => setToggleEmail(!toggleEmail)} />Later
                                        {toggleEmail && <div className="schedule-date-wrap">
                                            <DateTimePicker
                                                onChange={(date) => setStartDateEmail(date)}
                                                value={startDateEmail}
                                                clearIcon={null}
                                                calendarIcon={null}
                                                minDate={new Date()}
                                            />
                                        </div>}
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>}
                </div>
                <div className="btn-wrapper">
                    <button type="button" className="prev-btn ezi-btn" onClick={prev}>Previous</button>
                    <button type="button" className="next-btn ezi-btn" onClick={saveCommunication}>
                        Save & Preview {loading && <Spinner className="burgundy-spinner" animation="border" size="sm" />}
                    </button>
                </div>
            </div>
        </Fragment>
    )
}
export default CommunicationSetting