import React, { useContext, useState, useEffect } from "react";
import './ExpressSurvey.scss';
import 'react-circular-progressbar/dist/styles.css';
import { Nav, Tab } from 'react-bootstrap';
import AppContext from 'store/AppContext';
import Axios from "utility/Axios";
import configURL from "config/config";
import { toast } from "react-toastify";
import UploadSurveyLogo from "./UploadSurveyLogo";
import UploadParticipants from "./UploadParticipants";
import CommunicationSetting from "./CommunicationSetting";
import PreviewSurvey from "./PreviewSurvey";
import { useParams } from "react-router-dom";

const ExpressSurvey = (props) => {
    const { EziLoader } = useContext(AppContext)
    const { survey_id = null } = props.match.params;
    const [activeTab, setActiveTab] = useState('upload_logo')
    const [surveyName, setSurveyName] = useState('')
    const [editSurveyName, setEditSurveyName] = useState(false)
    const [tabMeta, setTabMeta] = useState({ logo: false, participant: false, communicate: false, preview: false, surveyType: false })
    let urlParams = useParams();
    let nameChangeTimeOut = null;

    const handleTabSwitch = (tab) => {
        let metaData = { ...tabMeta }
        switch (tab) {
            case 'upload_participant':
                metaData.logo = true
                break;
            case 'communicate':
                metaData.participant = true
                break;
            case 'preview':
                metaData.communicate = true
                metaData.preview = true
                break;
            default:
                break;
        }
        setTabMeta(metaData)
        setActiveTab(tab)
    }

    useEffect(() => {
        if (survey_id && survey_id !== "") {
            EziLoader.show()
            let formData = new FormData();
            formData.append("created_survey_id", survey_id);
            Axios.post(configURL.getSurveyQustions, formData).then(res => {
                EziLoader.hide()
                if (res.data.success !== undefined && res.data.success === true) {
                    setSurveyName(res.data.survey_name || '')
                } else {
                    toast.warn(res.data.message || 'Something went wrong!')
                }
            }).catch(err => {
                EziLoader.hide()
                toast.warn('Something went wrong!')
            })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const handleSurveyNameChange = (event) => {
        clearTimeout(nameChangeTimeOut)
        nameChangeTimeOut = setTimeout(() => {
            let formData = new FormData();
            formData.append("display_name", surveyName);
            formData.append("survey_id", urlParams.survey_id);
            Axios.post(configURL.survey_name_update, formData).then(response => {
                if (response.data.success !== undefined && response.data.success === true) {
                    setEditSurveyName(false)
                } else {
                    toast.warn(response.data.message);
                }

            })
        }, 500);
    }

    return (
        <React.Fragment>
            <section className="Page-ExpressSurvey" >
                <header className="info-header box-content">
                    <h2>Quick Survey Launch</h2>
                    {editSurveyName ? <div><input type="text" className="survey-name" value={surveyName} onBlur={handleSurveyNameChange} onChange={(event) => setSurveyName(event.target.value)} /></div> : <div><h2 className="survey-icon">{surveyName || ''}</h2><span class="edit_data" onClick={() => setEditSurveyName(true)}></span></div>}
                </header>
                <main className="main-content box-content side-tab-wrapper">
                    <Tab.Container activeKey={activeTab} onSelect={handleTabSwitch} >
                        <div className="side-tab">
                            <Nav variant="pills" className="side-tab-nav" >
                                <Nav.Item>
                                    <Nav.Link eventKey="upload_logo" className={`step-indicator${tabMeta.logo ? ' done' : ''}`}>General setting</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey="upload_participant" disabled={!tabMeta.logo} className={`step-indicator${tabMeta.participant ? ' done' : ''}`}>Upload Participant's</Nav.Link>
                                </Nav.Item>

                                <Nav.Item>
                                    <Nav.Link eventKey="communicate" disabled={!tabMeta.participant} className={`step-indicator${tabMeta.communicate ? ' done' : ''}`}>Communication Setting</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey="preview" disabled={!tabMeta.communicate} className={`step-indicator${tabMeta.preview ? ' done' : ''}`}>Preview</Nav.Link>
                                </Nav.Item>
                            </Nav>
                        </div>
                        <Tab.Content className="tab-content">
                            <Tab.Pane eventKey="upload_logo">
                                <UploadSurveyLogo survey_id={survey_id} next={() => handleTabSwitch("upload_participant")} />
                            </Tab.Pane>
                            <Tab.Pane eventKey="upload_participant" >
                                <UploadParticipants survey_id={survey_id} next={() => handleTabSwitch("communicate")} prev={() => handleTabSwitch("upload_logo")} />
                            </Tab.Pane>

                            <Tab.Pane eventKey="communicate" mountOnEnter>
                                <CommunicationSetting survey_id={survey_id} prev={() => handleTabSwitch("upload_participant")} next={() => handleTabSwitch("preview")} />
                            </Tab.Pane>
                            <Tab.Pane eventKey="preview" mountOnEnter unmountOnExit>
                                <PreviewSurvey survey_id={survey_id} prev={() => handleTabSwitch("communicate")} />
                            </Tab.Pane>
                        </Tab.Content>
                    </Tab.Container>
                </main>
            </section>
        </React.Fragment>
    )
}

export default ExpressSurvey;