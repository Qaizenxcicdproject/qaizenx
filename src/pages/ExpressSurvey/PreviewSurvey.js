import React, { Fragment, useState, useEffect, useContext } from "react";
import Axios from "utility/Axios";
import configURL from "config/config";
import { toast } from "react-toastify";
import { Spinner } from 'react-bootstrap';
import { useHistory } from "react-router-dom";
import AppContext from 'store/AppContext';

const PreviewSurvey = ({ survey_id, prev }) => {
    const [previewData, setPreviewData] = useState({})
    const [loading, setLoading] = useState(false)
    const { EziLoader } = useContext(AppContext)
    const urlHistory = useHistory();
    const checklistLabel = {
        webEmail: "Web Email",
        webSms: "Web Sms",
        emailEmbed: "Embeded Email",
        webEmbed: "Web Embeded",
        qrCode: "Qr Code",
        webLink: "Web Link",
    }

    const handleLaunchSurvey = () => {
        setLoading(true)
        let formData = new FormData();
        formData.append("survey_id", survey_id);
        Axios.post(configURL.surveyLaunch, formData).then(res => {
            setLoading(false)
            if (res.data.success !== undefined && res.data.success) {
                urlHistory.replace("/");
                toast.success(res.data.message || "Survey Launched successfully.");
            } else {
                toast.warn(res.data.message || "Something went wrong.");
            }
        }).catch(err => {
            setLoading(false)
            toast.warn("Something went wrong.");
        })
    }

    useEffect(() => {
        if (survey_id && survey_id !== "") {
            EziLoader.show()
            let formData = new FormData();
            formData.append("survey_id", survey_id);
            Axios.post(configURL.surveyCheckList, formData).then(res => {
                EziLoader.hide()
                if (res.data.success !== undefined && res.data.success) {
                    setPreviewData(res.data.result)
                } else {
                    toast.warn(res.data.message || 'Something went wrong!');
                }
            }).catch(err => {
                EziLoader.hide()
                toast.warn('Something went wrong!')
            })
        }
        return () => toast.dismiss()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    return (
        <Fragment>
            <div className="tab-content-header">
                <span className="tab-content-header-title">Preview Survey Settings </span>
                <span className="tab-content-header-subtitle">Preview Survey Logo, Participants and Communication Settings</span>
            </div>
            <div className="tab-content-body">
                <div className="prview-wrapper checklist-modal">
                    <div className="checklist-label-wrap">
                        <label className="checklist-label">Survey Name</label>
                        <span className="checklist-info survey-name">{previewData.survey_name || "NA"}</span>
                    </div>
                    <div className="checklist-label-wrap">
                        <label className="checklist-label">No of Question</label>
                        <span className="checklist-info">{previewData.no_of_question || 0}</span>
                    </div>
                    <div className="checklist-label-wrap">
                        <label className="checklist-label">Survey Type</label>
                        <span className="checklist-info">{previewData.survey_type || "NA"}</span>
                    </div>
                    <div className="checklist-label-wrap">
                        <label className="checklist-label">Participant Source</label>
                        <span className="checklist-info">{previewData.participant_source ? previewData.participant_source.join(', ') : "NA"}</span>
                    </div>
                    <div className="checklist-label-wrap">
                        <label className="checklist-label">Total Count</label>
                        <span className="checklist-info">{previewData.participant_count || "NA"}</span>
                    </div>
                    <div className="checklist-label-wrap">
                        <label className="checklist-label">Channel Detail</label>
                        <div className="channel-name-wrap">
                            {(previewData.channel_detail && previewData.channel_detail.length > 0) ?
                                previewData.channel_detail.map((item, i) => (<span key={i} className="checklist-info channel-name">{checklistLabel[item.name]}<span className="checklist-info">{item.scheduled_time && `, Triggers On : ${item.scheduled_time}`}</span> </span>))
                                : <span>Communication Channels Not Added.</span>
                            }
                        </div>
                    </div>
                </div>
                <div className="btn-wrapper">
                    <button type="button" className="prev-btn ezi-btn" onClick={prev}>Previous</button>
                    <button type="button" className="next-btn ezi-btn" onClick={handleLaunchSurvey}>
                        Launch Survey {loading && <Spinner className="burgundy-spinner" animation="border" size="sm" />}
                    </button>
                </div>
            </div>
        </Fragment>
    )
}
export default PreviewSurvey