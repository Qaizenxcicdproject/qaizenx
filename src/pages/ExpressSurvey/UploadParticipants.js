import React, { Fragment, useState, useCallback } from "react";
import Dropzone from "hooks/Dropzone";
import Axios from "utility/Axios";
import configURL from "config/config";
import { toast } from "react-toastify";
import { Spinner } from 'react-bootstrap';

const UploadParticipants = ({ survey_id, next, prev }) => {
    const [uploadData, setUploadData] = useState(null)
    const [loading, setLoading] = useState(false)

    /**
     * Save Participant Data.
     */
    const saveParticipentData = () => {
        if (!uploadData || uploadData === "") {
            toast.warn('Please upload participants')
            return;
        }
        setLoading(true)
        let formData = new FormData();
        formData.append("survey_id", survey_id);
        formData.append("people_ids", "{ employee: [], customers: [], others: [] }");
        formData.append("survey_setting", "{}");
        formData.append("isedit", false);
        formData.append("participant_source", 'upload_participant');
        formData.append("upload_participant_source_type", 'upload');
        formData.append("upload_participant_data", uploadData);

        Axios.post(configURL.addParticipantsToSurvey, formData).then(res => {
            setLoading(false)
            if (res.data.success !== undefined && res.data.success) {
                toast.info(res.data.message || "Setting Saved");
                next()
            } else {
                toast.warn(res.data.message || "Something went wrong.");
            }
        }).catch(err => {
            setLoading(false)
            console.log(err);
        })
    }

    /**
     * Dynamic Source Sample Data link
     */
    const getDynamicSourceSampleLink = async () => {
        let formData = new FormData()
        formData.append("survey_id", survey_id);
        let dynamicSource = await Axios.post(configURL.dynamicPeopleSourceLink, formData)
        if (dynamicSource.data.success) {
            window.open(dynamicSource.data.url, "_blank")
        }
    }
    /**
     * Handle Dynamic source exel load
     */
    const handleDynamicSource = useCallback((acceptedFiles) => {
        if (acceptedFiles.length > 0) {
            setUploadData(acceptedFiles[0])
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <Fragment>
            <div className="tab-content-header">
                <span className="tab-content-header-title">Upload Participant's </span>
                <span className="tab-content-header-subtitle">Upload participants data in Excel Format</span>
            </div>
            <div className="tab-content-body">
                <div className="upload-participant-wrapper">
                    <div className="dropzone-wrap survey-dropzone-wrap">
                        <Dropzone sampleDownloadFunc={getDynamicSourceSampleLink} dropzoneText="Download the sample template. In the template either email or mobile is mandatory. Identifiers & their values are to be entered one per column" onDrop={handleDynamicSource} accept={".xlsx, .xls, .csv"} />
                    </div>
                </div>
                <div className="btn-wrapper">
                    <button type="button" className="prev-btn ezi-btn" onClick={prev}>Previous</button>
                    <button type="button" className="next-btn ezi-btn" onClick={saveParticipentData}>
                        Upload & Continue {loading && <Spinner className="burgundy-spinner" animation="border" size="sm" />}
                    </button>
                </div>
            </div>
        </Fragment>
    )
}
export default UploadParticipants