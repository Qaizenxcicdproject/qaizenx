import React, { Fragment, useRef, useState, useContext } from "react";
import { Spinner, Tab, Nav, OverlayTrigger, Tooltip } from 'react-bootstrap';
import DatePicker from 'react-date-picker';
import { confirmAlert } from 'react-confirm-alert';
import { toast } from 'react-toastify';
import configURL from 'config/config';
import Axios from "utility/Axios";
import AppContext from 'store/AppContext';
import { SketchPicker } from 'react-color';

const UploadSurveyLogo = ({ survey_id, next }) => {
    const logoRef = useRef(null)
    const [logoSrc, setLogoSrc] = useState(null)
    const [loading, setLoading] = useState(false)
    const [surveyType, setSurveyType] = useState("oneTimeSurvey");
    const { languageObj = {} } = useContext(AppContext);
    const [viewColorPicker, setViewColorPicker] = useState(false);
    const [colorCode, setColorCode] = useState({ hex: '#5A4E63' });
    const [finalColorCode, setfinalColorCode] = useState('#5A4E63');
    let colorpickerRef = useRef();
    const [themeColorCode, setThemeColorCode] = useState({ hex: '#5A4E63' });
    const [finalThemeColorCode, setFinalThemeColorCode] = useState('#5A4E63');
    let themecolorpickerRef = useRef(null);
    const [viewThemeColorPicker, setviewThemeColorPicker] = useState(false);
    const [surveySetting, setSurveySetting] = useState({
        survey_type: "",
        start_date: new Date(),
        end_date: new Date(),
        continues_fq: 'always'
    });

    const handleSelectFile = ({ target }) => {
        let file = target.files[0]
        logoRef.current = file
        setLogoSrc(URL.createObjectURL(file))
    }
    const handleRemoveLogo = () => {
        logoRef.current = null
        setLogoSrc(null)
    }

    const handleUploadFile = () => {
        if (surveySetting.start_date.getTime() > surveySetting.end_date.getTime()) {
            confirmAlert({ title: 'Invalid Date', message: 'Start date could not be greater than End Date', buttons: [{ label: 'Okay' }] });
            return;
        }
        setLoading(true)
        let formData = new FormData();
        formData.append("survey_id", survey_id);
        if (logoRef.current && logoRef.current !== '') {
            formData.append("company_logo", logoRef.current);
        }
        let setting_json = {
            surveyType: surveyType,
            startDate: new Date(surveySetting.start_date.getTime() - (surveySetting.start_date.getTimezoneOffset() * 60000)).toJSON(),
            endDate: new Date(surveySetting.end_date.getTime() - (surveySetting.end_date.getTimezoneOffset() * 60000)).toJSON(),
        };
        if (surveyType === 'continuous') {
            setting_json.frequency = surveySetting.continues_fq
        }
        formData.append("setting_json", JSON.stringify(setting_json));
        formData.append("view_partner_logo", "false");
        formData.append("header_background_color", finalColorCode);
        formData.append("survey_theme_color", finalThemeColorCode);
        Axios.post(configURL.storeSurveySetting, formData).then(res => {
            setLoading(false)
            if (res.data.success !== undefined && res.data.success) {
                toast.info(res.data.message || "Survey Setting added");
                next()
            } else {
                toast.warn(res.data.message || "Something went wrong");
            }
        }).catch(err => {
            setLoading(false)
        })
    }

    return (
        <Fragment>
            <div className="tab-content-header">
                <span className="tab-content-header-title">General settings</span>
                {/* <span className="tab-content-header-subtitle">Upload survey logo</span> */}
            </div>
            <div className="tab-content-body">
                <div className="upload-logo-wrapper">
                    <img src={logoSrc || require('../../assets/images/template-logo.png')} className="survey_logo" alt="" />
                    {logoSrc && <button type="button" className="remove-logo ezi-btn" onClick={handleRemoveLogo}>Remove</button>}
                    <input type="file" name="file" id="file" className="inputfile" accept="image/*" onChange={handleSelectFile} />
                    <label htmlFor="file" className="ezi-btn">Choose a file</label>
                </div>

                <Tab.Pane eventKey="general">
                    <div className="survey-type-wrap">
                        <div className="survey-type-container">
                            <Tab.Container activeKey={surveyType} onSelect={k => setSurveyType(k)}>
                                <div className="survey-button-tab-header">
                                    <div className="tab-left-header">
                                        <Nav variant="pills" >
                                            <Nav.Item>
                                                <Nav.Link eventKey="oneTimeSurvey" >
                                                    {languageObj.translate('OneTime.1')}
                                                    <OverlayTrigger overlay={<Tooltip>Participants can give/submit survey only once.</Tooltip>}>
                                                        <span className="tabpill-info-icon"></span>
                                                    </OverlayTrigger>
                                                </Nav.Link>
                                            </Nav.Item>
                                            <Nav.Item >
                                                <Nav.Link eventKey="continuous">
                                                    {languageObj.translate('Continuous.1')}
                                                    <OverlayTrigger overlay={<Tooltip>Participant can give/submit survey multiple times depend on the frequency set by survey administrator.</Tooltip>}>
                                                        <span className="tabpill-info-icon"></span>
                                                    </OverlayTrigger>
                                                </Nav.Link>
                                            </Nav.Item>
                                        </Nav>
                                    </div>
                                </div>
                                <div className="customize-setting-main">
                                    <div className="survey-dates">



                                        <div className="startend-date-wrap">
                                            <div className="start-date-wrap">
                                                <label className="startend-label">{languageObj.translate('Starts.1')} :</label>
                                                <DatePicker
                                                    format={"dd/MM/yyyy"}
                                                    selected={surveySetting.start_date}
                                                    value={surveySetting.start_date}
                                                    minDate={new Date()}
                                                    onChange={(date) => setSurveySetting({ ...surveySetting, start_date: date })}
                                                    className="sweet-datepicker-custom"
                                                />
                                            </div>
                                        </div>
                                        <div className="startend-date-wrap">
                                            <div className="end-date-wrap">
                                                <label className="startend-label">{languageObj.translate('Ends.1')} :</label>
                                                <DatePicker
                                                    selected={surveySetting.end_date}
                                                    format={"dd/MM/yyyy"}
                                                    value={surveySetting.end_date}
                                                    minDate={new Date()}
                                                    onChange={(date) => setSurveySetting({ ...surveySetting, end_date: date })}
                                                    className="sweet-datepicker-custom"
                                                />
                                            </div>
                                        </div>
                                        <div className="startend-date-wrap">
                                            {surveyType === 'continuous' &&
                                                <div className="frequency-select-wrap">
                                                    <label className="frequency-label">Response Frequency :</label>
                                                    <select className="frequency-select" value={surveySetting.continues_fq} onChange={({ target }) => setSurveySetting({ ...surveySetting, continues_fq: target.value })} >
                                                        <option value="always">Always On</option>
                                                        <option value="daily">Daily once</option>
                                                        <option value="weekly">Once a week</option>
                                                        <option value="monthly">Once a month</option>
                                                        <option value="quarterly">Once a quarter</option>
                                                    </select>
                                                </div>
                                            }
                                        </div>
                                    </div>
                                    <div className="survey-color-settings">
                                        <div className="company-logo-main-wrap">
                                            <h1 className="company-logo-heading">Header Background Color :</h1>
                                            <div className='color_picker_container'>
                                                <div className="preview_color" style={{ backgroundColor: finalColorCode }}></div>
                                                <button styles="background:red" onClick={() => setViewColorPicker(!viewColorPicker)} className={viewColorPicker ? `ezi-pink-btn` : 'btn_gray'}>
                                                    Select Color
                                                </button>

                                                {viewColorPicker && <div ref={colorpickerRef} className='color_picker_content'>
                                                    <SketchPicker color={colorCode} onChangeComplete={setColorCode} />
                                                    <button onClick={() => {
                                                        setfinalColorCode(colorCode.hex || '#5A4E63')
                                                        setViewColorPicker(false)
                                                    }} className='btn_gray confirm_color_pick'>
                                                        Confirm
                                                    </button>
                                                </div>}

                                            </div>
                                        </div>
                                        <div className="company-logo-main-wrap">
                                            <h1 className="company-logo-heading">Survey Theme Color :</h1>
                                            <div className='color_picker_container'>
                                                <div className="preview_color" style={{ backgroundColor: finalThemeColorCode }}></div>
                                                <button styles="background:red" onClick={() => setviewThemeColorPicker(!viewThemeColorPicker)} className={viewThemeColorPicker ? `ezi-pink-btn` : 'btn_gray'}>
                                                    Select Color
                                                </button>
                                                {viewThemeColorPicker && <div ref={themecolorpickerRef} className='color_picker_content'>
                                                    <SketchPicker color={themeColorCode} onChangeComplete={setThemeColorCode} />
                                                    <button onClick={() => {
                                                        setFinalThemeColorCode(themeColorCode.hex || '#5A4E63')
                                                        setviewThemeColorPicker(false)
                                                    }} className='btn_gray confirm_color_pick'>
                                                        Confirm
                                                    </button>
                                                </div>}

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Tab.Container>
                        </div>
                    </div>
                </Tab.Pane>

                <div className="btn-wrapper">
                    <button type="button" className="next-btn ezi-btn" onClick={handleUploadFile} disabled={loading}>
                        Save & Continue {loading && <Spinner className="burgundy-spinner" animation="border" size="sm" />}
                    </button>
                </div>
            </div>
        </Fragment>
    )
}
export default UploadSurveyLogo