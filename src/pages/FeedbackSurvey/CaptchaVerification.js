import React, { useEffect, useState } from 'react';

const CaptchaVerification = ({ varifyCaptcha }) => {
    let inputVal = null;
    const [inputErr, setInputErr] = useState(null)
    const [captchaCode, setCaptchaCode] = useState('')

    const generateCode = (length) => {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        setCaptchaCode(result)
    }

    useEffect(() => {
        generateCode(6);
    }, [])

    const handleContinue = () => {
        if (!inputVal || inputVal.value === "") {
            setInputErr('Please enter captcha code')
            return;
        }
        if (captchaCode === inputVal.value.trim()) {
            varifyCaptcha()
        }
        else {
            setInputErr('Captcha Does Not Match')
        }
    }
    return (
        <div className="captcha_verification">
            <div className="captcha_canvas">
                <span onCopy={(e) => e.preventDefault()}>{captchaCode}</span>
                <button className='reload_captcha' type="button" onClick={() => generateCode(6)}>Reload</button>
            </div>
            <div className="captcha_input_wrapper">
                <label>Type the characters above :</label>
                <div className="captcha_input_inner">
                    <input ref={(input) => inputVal = input} type="text" onPaste={(e) => e.preventDefault()} onChange={() => setInputErr(null)} className="captcha_input" />
                    <button type="button" className="captcha_submit_btn" onClick={handleContinue}>Continue</button>
                </div>
            </div>
            {inputErr && <span className="error-message">{inputErr}</span>}
        </div>
    )
}

export default CaptchaVerification;