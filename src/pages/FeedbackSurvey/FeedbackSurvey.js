import React from 'react';
import './FeedbackSurvey.scss';
import axios from "axios"
import configURL from 'config/config';
import AppContext from 'store/AppContext';
import SurveyFeedbackError from './SurveyFeedbackError';
import SurveyFeedback from './SurveyFeedback';
import CaptchaVerification from './CaptchaVerification';
import SurveyFeedbackFooter from './SurveyFeedbackFooter';
import SurveyFeedbackHeader from './SurveyFeedbackHeader';
import FormData from 'utility/AppFormData';

class FeedbackSurvey extends React.Component {
    state = {
        vendorInfo: {},
        showVendorInfo: true,
        participant_source: null,
        report_type: null,
        hasError: false,
        header_background_color: '#5A4E63',
        showVerification: false,
        enablesurveyGeolocation: false,
    }
    static contextType = AppContext;

    componentWillMount() {
        this.context.EziLoader.show()
    }

    /**
     * Load data on component mount
     */
    componentDidMount() {
        let formData = new FormData();
    
        formData.append("survey_id", this.props.match.params.survey_id);

        if (this.props.match.params.company_id !== undefined && this.props.match.params.company_id !== "") {
            formData.append("company_id", this.props.match.params.company_id);
        }
        if (this.props.match.params.respondant_id !== undefined && this.props.match.params.respondant_id !== "") {
            formData.append("respondant_id", this.props.match.params.respondant_id);
        }

        axios.post(configURL.getCompanydata, formData).then(response => {
            if (response.data.success && response.status === 200) {

                this.setState({
                    vendorInfo: response.data.user_data,
                    participant_source: response.data.participant_source || 'from_survey',
                    report_type: response.data.report_type,
                    header_background_color: response.data.header_background_color || '#5A4E63',
                    showVerification: (response.data.participant_source === 'from_survey' && response.data.captcha_enabled) ? true : false,
                    enablesurveyGeolocation: response.data.enable_survey_geolocation
                })
                document.documentElement.style.setProperty("--survey-theme-color", response.data.survey_theme_color || '#ce2e6c');

            } else {
                this.setState({
                    showVendorInfo: false,
                    userMsg: response.data.message,
                    hasError: true,
                    showVerification: false,
                    enablesurveyGeolocation:false
                })
            }
            this.context.EziLoader.hide()
        }).catch(() => {
            this.setState({ showVendorInfo: false, userMsg: "Oops something is wrong here..", hasError: true, showVerification: false })
            this.context.EziLoader.hide()
        })
    }

    render() {
        return (
            <React.Fragment>
                <section className={`Page-FeedbackSurvey ${this.state.report_type && this.state.report_type.toLowerCase().replace(/\s+/g, '_')}`}>
                    {this.state.showVendorInfo && <SurveyFeedbackHeader vendorInfo={this.state.vendorInfo} background_color={this.state.header_background_color} />}
                    {(!this.state.hasError && !this.state.showVerification) &&
                        <SurveyFeedback
                            enableSurveyGeolocation={this.state.enablesurveyGeolocation}
                            hideVendorInfo={() => this.setState({ showVendorInfo: false })}
                            showFinishLater={(this.state.participant_source !== "from_survey")}
                            showElements={this.state.showVendorInfo}
                        />}
                    {this.state.showVerification && <CaptchaVerification varifyCaptcha={() => this.setState({ showVerification: false })} />}
                    {this.state.hasError && <SurveyFeedbackError message={this.state.userMsg} />}
                    {this.state.showVendorInfo && <SurveyFeedbackFooter />}
                </section>
            </React.Fragment>
        );
    }
}

export default FeedbackSurvey;