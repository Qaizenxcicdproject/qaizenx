import React, { useContext, useState, useEffect, useRef } from 'react';
import './FeedbackSurvey.scss';
import axios from "axios"
import configURL from 'config/config';
import { RPT_URL_BASE } from 'config/config';
import plateformInfo from 'utility/PlatformInfo';
import * as Survey from "survey-knockout";
import "survey-creator/survey-creator.css";
import "jquery-ui/themes/base/all.css";
import "select2/dist/css/select2.css";
import "bootstrap-slider/dist/css/bootstrap-slider.css";
import "jquery-bar-rating/dist/themes/css-stars.css";
import "jquery-bar-rating/dist/themes/fontawesome-stars.css";
import $ from "jquery";
import "jquery-ui/ui/widgets/datepicker.js";
import "select2/dist/js/select2.js";
import "jquery-bar-rating";
import "pretty-checkbox/dist/pretty-checkbox.css";
import "react-toastify/dist/ReactToastify.css";
import * as widgets from "surveyjs-widgets";
import "lib/survey-widget/ThumbsWidget";
import "lib/survey-widget/SmilyWidget";
import 'lib/survey-widget/CsatRating'
import "lib/survey-widget/PersonalInfoText";
import "lib/survey-widget/NPSWidget";
import AppContext from 'store/AppContext';
import { useParams } from "react-router-dom"
import { toast } from 'react-toastify';
import { Dropdown } from 'react-bootstrap';
import { CUSTOM_LANGUAGES, LOCALES_CUSTOM_STRINGS } from "constants/SurveyConstants";
import '../../assets/scss/navbar.scss';
import FormData from 'utility/AppFormData';

widgets.prettycheckbox(Survey);
widgets.select2(Survey, $);
widgets.inputmask(Survey);
widgets.jquerybarrating(Survey, $);
widgets.jqueryuidatepicker(Survey, $);
widgets.select2tagbox(Survey, $);
widgets.sortablejs(Survey);
widgets.ckeditor(Survey);
widgets.autocomplete(Survey, $);
widgets.bootstrapslider(Survey);

var surveyFeedbackModel = null;
var timerId = null
var autoSaveTimer = null
var pageSaveTimer = null
const SurveyFeedback = (props, { hideVendorInfo, showElements, enableSurveyGeolocation }) => {
	const skipHighlight = ['html', 'checkbox']
	const { EziLoader } = useContext(AppContext)
	const [surveyMeta, setSurveyMeta] = useState({
		userMsg: "",
		response_user_id: null,
		hasError: false,
		isLoaded: false,
		answer_json: null,
		finishedLater: false,
		survey_locales: [],
		redirect_to_report: false
	})
	const surveyPageTime = {}
	let surveyStartTime = null
	const [surveyJson, setSurveyJson] = useState(null)
	const [surveyLocale, setSurveyLocale] = useState("English")
	const [surveyLang, setSurveyLang] = useState([])
	const urlParams = useParams()
	const ipData = useRef('')
	const identifier_data = useRef(null)
	let pageWarning = {}
	const [coordinates, setCoordinates] = useState({
		latitude: null,
		longitude: null
	})
	/**
	 *  Handle Survey Langauge
	 */
	const handleLanguage = (lang) => {
		surveyFeedbackModel.locale = lang.id
		document.documentElement.setAttribute('lang', (lang.id && lang.id !== "") ? lang.id : 'en');
		setSurveyLocale(lang.name)
	}


	/**
	 * Get Query Paramaters
	 */
	const getQueryParams = () => {
		let url = window.location.search;
		let qs = url.substring(url.indexOf('?') + 1)
		if (qs !== "" && qs.startsWith('identifiers=')) {
			let identifiers = qs.trim().replace('identifiers=', '')
			if (identifiers.length % 4 !== 0) {
				identifiers += ('===').slice(0, 4 - (identifiers.length % 4));
			}
			return identifiers
		}
		return "";
	}

	/**
	 * Load Survey Data
	 */
	const initSurveyData = () => {
		EziLoader.show()
		let formData = new FormData();
		formData.append("survey_id", urlParams.survey_id);
		formData.append("user_agent", window.navigator.userAgent);
		formData.append("user_platform", plateformInfo.os.name);
		formData.append("user_browser", plateformInfo.browser.name);
		formData.append("user_os", navigator.platform);
		if (urlParams.company_id !== undefined && urlParams.company_id !== "") {
			formData.append("company_id", urlParams.company_id);
		}
		if (urlParams.respondant_id !== undefined && urlParams.respondant_id !== "") {
			formData.append("respondant_id", urlParams.respondant_id);
		}
		axios.post(configURL.getSurveydata, formData).then(response => {
			if (response.data.success && response.status === 201) {
				setSurveyMeta({
					...surveyMeta,
					userMsg: response.data.message || "",
					response_user_id: response.data.response_user_id,
					isLoaded: true,
					answer_json: response.data.answer_json || null,
					survey_locales: response.data.survey_locales || [],
					redirect_to_report: response.data.redirect_to_report || false
				})
				setSurveyJson(response.data.survey_json || null);
			} else {
				setSurveyMeta({ ...surveyMeta, userMsg: response.data.message || "", hasError: true, isLoaded: true })
				props.hideVendorInfo(true)
			}
			EziLoader.hide()
		}).catch(() => {
			props.hideVendorInfo(true)
			setSurveyMeta({ ...surveyMeta, userMsg: "Please refresh again! something is wrong here..", hasError: true, isLoaded: true })
			EziLoader.hide()
		})
	}

	/**
	 * Render Survey Model
	 */
	const handleRenderSurvey = async () => {
		if (surveyJson && surveyJson !== null && surveyJson !== "" && surveyMeta.isLoaded) {
			/** -----Custom Languages--- */
			Object.keys(CUSTOM_LANGUAGES).forEach(key => {
				Survey.surveyLocalization.locales[key] = CUSTOM_LANGUAGES[key].localeStrings;
				Survey.surveyLocalization.localeNames[key] = CUSTOM_LANGUAGES[key].label;
			})
			Survey.JsonObject.metaData.addProperty("survey", 'highlightUnanswered');
			surveyFeedbackModel = new Survey.Model(surveyJson);
			Survey.surveyStrings.emptySurvey = surveyMeta.userMsg;
			surveyFeedbackModel.onComplete.add(onComplete);
			let usedLocales = []
			usedLocales = surveyMeta.survey_locales.filter(el => el !== '').map(el => {
				if (el !== "" || el !== null) {
					return {
						id: el,
						name: Survey.surveyLocalization.localeNames[el]
					}
				}
				return {};
			})
			usedLocales.push({ id: "", name: "English" })
			setSurveyLang(usedLocales)
			identifier_data.current = getQueryParams()
			if (identifier_data.current && identifier_data.current !== "") {
				try {
					let presetFormData = new FormData()
					presetFormData.append('preset_json', identifier_data.current)
					const presetAnswerData = await axios.post(configURL.decode_preset_data, presetFormData)
					if (presetAnswerData.data.success === true) surveyFeedbackModel.data = presetAnswerData.data.preset_json
				} catch (error) {
					console.log(error);
				}
				Survey.surveyStrings.completingSurvey = 'Thank you for your response, we are redirecting you to the results page.';
			}
			if (surveyMeta.answer_json && surveyMeta.answer_json !== "") {
				let surveyAnswer = surveyMeta.answer_json
				surveyFeedbackModel.currentPageNo = surveyAnswer.currentPageNo;
				surveyFeedbackModel.data = surveyAnswer.answer;
			}
			surveyFeedbackModel.onCurrentPageChanged.add(savePageData);
			surveyFeedbackModel.onStarted.add(() => {
				setTimeout(setTabIndexing, 300)
			});
			if (surveyFeedbackModel.highlightUnanswered) {
				surveyFeedbackModel.onCurrentPageChanging.add(handleHighlightUnanswered);
			}
			if (surveyFeedbackModel.locale && surveyFeedbackModel.locale !== "") {
				setSurveyLocale(CUSTOM_LANGUAGES[surveyFeedbackModel?.locale]?.label || 'default')
			}
			surveyStartTime = new Date().getTime()
			surveyFeedbackModel.canComplete = false
			surveyFeedbackModel.textUpdateMode = 'onTyping'
			surveyFeedbackModel.onValueChanged.add(handleCommentsSave)
			surveyFeedbackModel.onCompleting.add(verifySurveyResponse);
			surveyFeedbackModel.render("surveyPreview");
			timerId = setInterval(surveyTimer, 1000);
			setTabIndexing(0)
		}
		if (!surveyJson && !surveyMeta.isLoaded) {
			initSurveyData()
		}
	}

	const savePageData = () => {
		clearTimeout(pageSaveTimer)
		pageSaveTimer = setTimeout(() => {
			handleSavePageData()
			surveyTimer()
			setTabIndexing(0)
			clearTimeout(pageSaveTimer)
		});
	}

	/**
	 * Save Comments
	 */
	const handleCommentsSave = (sender, options) => {
		clearTimeout(autoSaveTimer)
		if (options.question.getType() === 'comment') {
			autoSaveTimer = setTimeout(handleSavePageData, 1000);
		}
	}

	/**
	 * Set Tab indexing
	 */
	const setTabIndexing = (tabIndex = 0) => {
		try {
			let elements = document.getElementsByClassName('tab_indexing')
			if (elements) {
				for (let index = 0; index < elements.length; index++) {
					elements[index].tabIndex = tabIndex
				}
			}
		} catch (error) {
			console.log(error);
		}
	}

	/**
	 * Code For Highlight Survey questions
	 * @param {Survey} sender 
	 * @param {Object} options 
	 */
	const handleHighlightUnanswered = (sender, options) => {
		if (!options.oldCurrentPage) {
			pageWarning = {}
			return;
		}
		let currentPageName = options.oldCurrentPage.name
		if (!options.isNextPage) {
			pageWarning[currentPageName] = false
			return;
		}
		if (pageWarning[currentPageName]) {
			pageWarning[currentPageName] = false
			options.allowChanging = true
			return
		}
		sender.currentPage.questions.forEach(item => {
			if (!item.isAnswered && item.isReadOnly === false && item.isVisible === true && !skipHighlight.includes(item.getType())) {
				pageWarning[currentPageName] = true
				let questionId = item.id
				$("div#" + questionId).addClass('survey_question_unanswered')
			}
		})
		if (pageWarning[currentPageName]) {
			let msg = (surveyFeedbackModel.locale && surveyFeedbackModel.locale !== "") ? surveyFeedbackModel.locale : 'en';
			toast.warn(LOCALES_CUSTOM_STRINGS.unanswered_question[msg], { position: toast.POSITION.BOTTOM_RIGHT });
			options.allowChanging = false
		} else {
			options.allowChanging = true
		}
	}

	/**
	 * Code For Verifying Survey Identifiers and Uniqueness for NoSource Survey
	 * @param {Survey} sender 
	 * @param {Object} options 
	 */
	const verifySurveyResponse = async (sender, options) => {
		if (surveyFeedbackModel.canComplete) return true;
		EziLoader.show()
		options.allowComplete = false;
		let formData = new FormData();
		formData.append("survey_id", urlParams.survey_id);
		formData.append("answer_json", JSON.stringify(sender.data));
		if (urlParams.company_id !== undefined && urlParams.company_id !== "") {
			formData.append("company_id", urlParams.company_id || null);
		}
		if (urlParams.respondant_id !== undefined && urlParams.respondant_id !== "") {
			formData.append("respondant_id", urlParams.respondant_id || null);
		}
		if (surveyMeta.response_user_id && surveyMeta.response_user_id !== "") {
			formData.append("response_user_id", surveyMeta.response_user_id);
		}
		axios.post(configURL.verifySurveyResponse, formData).then(response => {
			if (response.data.success === true) {
				surveyFeedbackModel.canComplete = true
				surveyFeedbackModel.doComplete();
			} else {
				EziLoader.hide()
				toast.warn(response.data.message || 'Something went wrong.')
			}
		}).catch(err => {
			console.log(err.response)
			toast.warn('Something went wrong.')
			EziLoader.hide()
		})
	}

	/**
	 * Save Survey Page Data.
	 */
	const handleSavePageData = () => {
		let timeSpent = getSurveySpentTime()
		let surveyAnswer = { currentPageNo: surveyFeedbackModel.currentPageNo, answer: surveyFeedbackModel.data }
		let formData = new FormData();
		formData.append("survey_id", urlParams.survey_id);
		formData.append("answer_json", JSON.stringify(surveyAnswer));
		formData.append("response_language", surveyLocale);
		if (urlParams.company_id !== undefined && urlParams.company_id !== "") {
			formData.append("company_id", urlParams.company_id || null);
		}
		if (urlParams.respondant_id !== undefined && urlParams.respondant_id !== "") {
			formData.append("respondant_id", urlParams.respondant_id || null);
		}
		if (surveyMeta.response_user_id && surveyMeta.response_user_id !== "") {
			formData.append("response_user_id", surveyMeta.response_user_id);
		}
		formData.append("user_agent", window.navigator.userAgent);
		formData.append("duration", timeSpent);
		formData.append("page_duration", JSON.stringify(surveyPageTime));
		formData.append("user_platform", plateformInfo.os.name);
		formData.append("user_browser", plateformInfo.browser.name);
		formData.append("user_os", navigator.platform);
		formData.append("public_ip", ipData.current || '');
		formData.append("geo_tag", JSON.stringify(coordinates));
		axios.post(configURL.surveySavePageResponse, formData)
	}

	/**
	 * get public ip
	 */
	const getPublicIp = async () => {
		try {
			const publicIpRes = await axios.get('https://api.ipify.org/?format=json')
			if (publicIpRes.status === 200) {
				ipData.current = publicIpRes.data.ip
			}
		} catch (error) {
			console.log('Unable to get ip data', error);
		}
	}

	const geoLocation = () => {
		let coordinates_all = {
			latitude: null,
			longitude: null
		};

		if (props.enableSurveyGeolocation) {

			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(successFunction);
			}
			//Get the latitude and the longitude;
			function successFunction(position) {
				let lat = position.coords.latitude;
				let lng = position.coords.longitude;
				coordinates_all = {
					latitude: lat,
					longitude: lng
				};
				setCoordinates(coordinates_all);
			}
		}

	}
	/**
	 * Map answer with questions
	 */
	const getMappedResponse = (result) => {
		const answersData = [];
		if (surveyJson && surveyJson.hasOwnProperty('pages') && surveyJson.pages.length > 0) {
			const Questions = surveyJson.pages.reduce((arr, el) => {
				let newEl = (el.hasOwnProperty('elements') && el['elements'].length > 0) ? el['elements'] : [];
				return arr.concat(newEl);
			}, []);
			let results = result.data || [];
			Questions.length > 0 && Questions.forEach(item => {
				let name = item.name
				let id = item.id
				let type = item.question_type
				let answer = {}
				if (results.hasOwnProperty(name)) {
					answer["value"] = results[name]
					answer["id"] = id
					answer["question_type"] = type
				}
				if (item.hasOwnProperty("hasComment") && item["hasComment"]) {
					answer["comments"] = results[`${name}-Comment`]
				}
				if (item.hasOwnProperty("hasOther") && item["hasOther"]) {
					answer["others"] = results[`${name}-Comment`]
				}
				if (!results.hasOwnProperty(name)) {
					answer["value"] = null
					answer["id"] = id
					answer["question_type"] = type
				}
				answersData.push(answer);
			});
			return answersData;
		} else {
			return [];
		}
	}

	/**
	 * Set Time in seconds for every page
	 */
	const surveyTimer = () => {
		let page = surveyFeedbackModel.currentPageNo;
		if (page === undefined) return;
		let valueName = "page" + (page + 1);
		let seconds = surveyPageTime[valueName];
		if (seconds == null) seconds = 0;
		else seconds++;
		surveyPageTime[valueName] = seconds
	}

	/**
	 * Get Survey Duration In Seconds
	 */
	const getSurveySpentTime = () => {
		let timeInMl = new Date().getTime() - surveyStartTime
		if (timeInMl > 0) {
			return Math.floor(timeInMl / 1000)
		} else {
			return 0
		}
	}

	/**
	 * Call coupon generate api.
	 * @param {String} respondant_id 
	 */
	const handleGenerateCoupon = (respondant_id = null) => {
		let formData = new FormData();
		formData.append("survey_id", urlParams.survey_id);
		formData.append("respondant_id", respondant_id);
		if (urlParams.company_id !== undefined && urlParams.company_id !== "") {
			formData.append("account_id", urlParams.company_id || null);
		}
		axios.post(configURL.generate_coupon, formData).then(response => {
			if (response.data.success === true) {
				toast.success(response.data.message || 'Coupon Details sent successfully.')
			} else {
				console.log("Coupon Generate Error:", response.data.message || 'Something went wrong.')
			}
		}).catch(error => {
			console.log("Coupon Generate Error:", error)
		})
	}


	/**
	 * SurveyJs complete event to store feedback information
	 */
	const onComplete = (result) => {
		clearInterval(timerId);
		clearTimeout(autoSaveTimer)
		let timeSpent = getSurveySpentTime();
		const mappedData = getMappedResponse(result);
		let formData = new FormData();
		formData.append("survey_id", urlParams.survey_id);
		formData.append("answer_json", JSON.stringify(mappedData));
		formData.append("response_language", CUSTOM_LANGUAGES[result.localeValue]?.label || "English");
		if (urlParams.company_id !== undefined && urlParams.company_id !== "") {
			formData.append("company_id", urlParams.company_id || null);
		}
		if (urlParams.respondant_id !== undefined && urlParams.respondant_id !== "") {
			formData.append("respondant_id", urlParams.respondant_id || null);
		}
		if (surveyMeta.response_user_id && surveyMeta.response_user_id !== "") {
			formData.append("response_user_id", surveyMeta.response_user_id);
		}
		formData.append("submission_time", JSON.stringify(new Date()));
		formData.append("duration", timeSpent || 0);
		formData.append("page_duration", JSON.stringify(surveyPageTime));
		formData.append("status", "complete");
		formData.append("user_agent", window.navigator.userAgent);
		formData.append("user_platform", plateformInfo.os.name);
		formData.append("user_browser", plateformInfo.browser.name);
		formData.append("user_os", navigator.platform);
		formData.append("public_ip", ipData.current || '');
		formData.append("geo_tag", JSON.stringify(coordinates));
		axios.post(configURL.save_survey_response_url, formData).then(response => {
			if (response.status === 200 && response.data.success === true) {
				/** Generate coupon for ezicoupon */
				let { has_promotion = false, respondant_id = null } = response.data
				if (has_promotion && respondant_id && respondant_id !== "") handleGenerateCoupon(respondant_id)
				/** Redirect to reports */
				let redirect_link = null;

				if (surveyMeta.redirect_to_report === true && respondant_id && respondant_id !== "" /*&& identifier_data.current && identifier_data.current !== ""*/) {
					redirect_link = `${RPT_URL_BASE}external/get-result-respondent?survey_id=${urlParams.survey_id}&respondent_id=${respondant_id}`
					if (identifier_data.current && identifier_data.current !== "") {
						redirect_link = redirect_link + `&identifiers=${identifier_data.current}`;
					}
					setTimeout(() => {
						window.location = redirect_link
					}, 5000);
				}
				props.hideVendorInfo(true)
			} else {
				toast.warn(response.data.message || 'Something went wrong.')
			}
			EziLoader.hide()
		}).catch(error => {
			EziLoader.hide()
			toast.error(error.toString());
		})
		/**
		 * Code for redirecting user to member login page after survey
		 */
		let isMemberLogin = (localStorage.getItem('isMemberLogin') && localStorage.getItem('isMemberLogin') !== "") ? JSON.parse(localStorage.getItem('isMemberLogin')) : false
		if (isMemberLogin) {
			localStorage.removeItem('isMemberLogin')
			setTimeout(() => {
				let hostName = window.location.host || window.location.hostname
				if (hostName) {
					window.location = `/participant-login`
				}
			}, 2000);
		}
	}


	/*eslint-disable */
	useEffect(() => {
		handleRenderSurvey()
	}, [surveyJson, surveyMeta.isLoaded, coordinates])
	useEffect(() => {
		getPublicIp();
		if (props.enableSurveyGeolocation) {
			geoLocation();
		}
	}, [props.enableSurveyGeolocation])

	/*eslint-enable */
	return (
		<React.Fragment>
			{(surveyMeta.isLoaded && !surveyMeta.finishedLater) &&
				<div className="feedback-main-wrapper">
					{(props.showElements && surveyLang.length > 1) && <div className="languageDropdown">
						<label className="lang-text">Language :</label>
						<Dropdown tabIndex="0">
							<Dropdown.Toggle id="dropdown-basic">
								<div className="flag-avtar-wrap">
									<label className="country-flag-label">{surveyLocale || "default"}</label>
								</div>
							</Dropdown.Toggle>
							<Dropdown.Menu>
								{surveyLang && surveyLang.map((item, index) => (
									<Dropdown.Item key={index}>
										<div className="flag-img-wrap" onClick={() => handleLanguage(item)}>
											<label className="country-flag-label">{item.name}</label>
										</div>
									</Dropdown.Item>
								))}
							</Dropdown.Menu>
						</Dropdown>
					</div>}
					<div className={`feedback-survey-content ${surveyMeta.userMsg === '' ? '' : 'completed-survey'}`}>
						<div id="surveyPreview">
							{surveyMeta.hasError && (<div className="preview-survey-thank-wrap">
								<span className="preview-survey-complete-ic"></span>
								<span className="preview-survey-feedback-para">{surveyMeta.userMsg}</span>
							</div>)}
						</div>
					</div>
				</div>
			}
		</React.Fragment>
	)
}

export default SurveyFeedback;