import React from 'react';

const SurveyFeedbackError = (props) => (
    <div className={`feedback-survey-content ${props.message === '' ? '' : 'completed-survey'}`}>
        <div id="surveyPreview">
            <div className="preview-survey-thank-wrap">
                <span className="preview-survey-complete-ic"></span>
                <span className="preview-survey-feedback-para">{props.message}</span>
            </div>
        </div>
    </div>
)

export default SurveyFeedbackError;