import React from 'react';

const SurveyFeedbackFooter = () => (
    <div className="preview-survey-poweredby-wrap">
        <span className="preview-survey-poweredby-heading">Powered by</span>
        <a href="https://www.qaizenx.com/" target="_blank" rel="noopener noreferrer" className="preview-survey-poweredby-link" ><img alt="" src={require(`../../assets/images/qaizenx-regx2-logo.png`)} className="preview-survey-poweredby-logo" /></a>
    </div>
)

export default SurveyFeedbackFooter;