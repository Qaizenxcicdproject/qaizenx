import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

const SurveyFeedbackHeader = ({ vendorInfoData, vendorInfo, background_color = '#5A4E63' }) => {
    const [vendorData, setVendorData] = useState(vendorInfo)
    useEffect(() => {
        setVendorData(vendorInfo)
    }, [vendorInfo])

    let company_logo = (vendorData.company && vendorData.company.company_logo) ? vendorData.company.company_logo : vendorInfoData.logo || null;
    let partner_logo = (vendorData.partner && vendorData.partner.partner_logo) ? vendorData.partner.partner_logo : null;
    let partner_name = (vendorData.partner && vendorData.partner.partner_name) ? vendorData.partner.partner_name : null;
    let view_partner_logo = (vendorData.partner && vendorData.partner.view_partner_logo) ? true : false;

    return (
        <div className="preview-survey-header" style={{ backgroundColor: background_color }}>
            <div className="preview-survey-header-left">
                <div className="preview-survey-logo-wrap">
                    {company_logo && <img alt={(vendorData.company && vendorData.company.company_name) ? vendorData.company.company_name : ''} src={company_logo} className="preview-survey-logo" />}
                    {!company_logo &&
                        <span className="initial-letter-block">
                            {(vendorData.company && vendorData.company.company_name) && vendorData.company.company_name.charAt(0).toUpperCase()}
                        </span>
                    }
                </div>
                <div className="preview-text-wrap">
                    <span className="preview-survey-company-name" tabIndex="0">
                        {(vendorData.company && vendorData.company.company_name) && vendorData.company.company_name}
                    </span>
                    <span className="preview-survey-name" tabIndex="0">
                        {(vendorData.company && vendorData.company.survey_name) ? vendorData.company.display_name || vendorData.company.survey_name : ''}
                    </span>
                </div>
            </div>
            {view_partner_logo && <div className="preview-survey-header-right">
                <div className="preview-survey-logo-wrap">
                    {partner_logo && <img alt={partner_name ? partner_name : ''} src={partner_logo} className="preview-survey-logo" />}
                    {(!partner_logo && partner_name) && <span className="initial-letter-block">{partner_name.charAt(0).toUpperCase()}</span>}
                </div>
                {partner_name && <div className="preview-text-wrap">
                    <span className="preview-survey-company-name">In partner with</span>
                    <span className="preview-survey-name">{partner_name}</span>
                </div>}
            </div>}
        </div>
    )
}

const mapStateToProps = state => {
    return {
        vendorInfoData: state.app.vendorInfo,
    }
}

export default connect(mapStateToProps)(SurveyFeedbackHeader);