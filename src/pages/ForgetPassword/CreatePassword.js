import React, { useState } from 'react';
import { Spinner } from 'react-bootstrap';
import 'pages/Login/Login.scss';
import useForm from 'react-hook-form'
import { useParams } from "react-router-dom";
import Axios from 'axios';
import configURL from 'config/config';
import { toast } from 'react-toastify';
import { connect } from "react-redux";
import { APP_ID, APP_FIELD_NAME } from "constants/constants"
import FormData from 'utility/AppFormData';

const CreatePassword = ({ history, vendorInfo }) => {
    const urlParams = useParams()
    const { register, handleSubmit, errors, watch } = useForm()
    const [loader, setLoader] = useState(false);

    const onSubmit = data => {
        setLoader(true)
        let formData = new FormData()
        formData.append('password', data.password)
        formData.append('password_confirmation', data.confirm_password)
        formData.append('token', urlParams.token)
        formData.append("account_name", vendorInfo.webaddress);
        formData.append('user_name', window.atob(urlParams.user_id))
        formData.append(APP_FIELD_NAME, APP_ID);

        Axios.post(configURL.reset_password, formData).then(response => {
            setLoader(false)
            if (response.data.success !== undefined && response.data.success === true) {
                toast.success(response.data.message || 'Password changed successfully. Please login with your account');
                history.push("/");
            } else {
                toast.warn(response.data.message || 'Something went wrong.');
            }
        }).catch(error => {
            console.log(error);
            toast.warn('Something went wrong.');
            setLoader(false)
        })
    }

    return (
        <div className="bg-wrapper">
        <div className="login">
            <div className="login-pink-back">
				<img alt="" src={require(`../../assets/images/login/vector.png`)} className="login-img animated fadeIn" />
			</div>
            <div className="login-detail">
                {/* <div className="login-bg">
                    <img alt="" src={require(`../../assets/images/login-img.png`)} className="login-img animated fadeIn" />
                </div> */}
                <div className="login-form animated fadeIn">
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="login-logo">
                            <img alt="" src={require(`../../assets/images/logo.png`)} />
                        </div>
                        <p className="sign-in">Changing Password For : {window.atob(urlParams.user_id)}</p>
                        <div className="input-wrap">
                            <input type="password" name="password" className={`login-input-control`} ref={register({ required: true, minLength: 8 })} placeholder="Please Enter Password" />
                            {errors.password && errors.password.type === 'required' && <span className={`error-message`}>Please Enter New Password</span>}
                            {errors.password && errors.password.type === 'minLength' && <span className={`error-message`}>Minimum Password length Is 8 Charectors Long</span>}
                        </div>
                        <div className="input-wrap">
                            <input type="text" name="confirm_password" className={`login-input-control password-input`} ref={register({ required: true, validate: (value) => value === watch('password') })} placeholder="Please Enter Password Again" />
                            {errors.confirm_password && errors.confirm_password.type === 'required' && <span className={`error-message`}>Please Enter Password Again</span>}
                            {errors.confirm_password && errors.confirm_password.type === 'validate' && <span className={`error-message`}>Entered Password Not Matched</span>}
                        </div>
                        <div className="otp-btn-wrap">
                            <button type="submit" className={`ezi-btn company-login-btn btn-ripple`}>
                                Reset Password
                                {loader && <Spinner animation="grow" />}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        vendorInfo: state.app.vendorInfo,
    }
}

export default connect(mapStateToProps)(CreatePassword);