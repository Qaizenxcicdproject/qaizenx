import React, { useState, useContext } from 'react';
import 'pages/Login/Login.scss';
import { Spinner } from 'react-bootstrap';
import Axios from 'axios';
import AppContext from 'store/AppContext';
import useForm from 'react-hook-form'
import configURL from 'config/config';
import { toast } from 'react-toastify';
import { connect } from "react-redux";
import { APP_ID, APP_FIELD_NAME } from "constants/constants"
import FormData from 'utility/AppFormData';

const ForgotPassword = ({ vendorInfo }) => {

    const [loading, setLoading] = useState(false)
    const { register, handleSubmit, errors } = useForm()
    const { languageObj } = useContext(AppContext);

    const onSubmit = data => {
        setLoading(true)
        let formData = new FormData()
        formData.append('user_name', data.reset_username)
        formData.append("account_name", vendorInfo.webaddress);
        formData.append(APP_FIELD_NAME, APP_ID);

        Axios.post(configURL.reset_password_link, formData).then(response => {
            setLoading(false)
            if (response.data.success !== undefined && response.data.success === true) {
                toast.success(response.data.message || 'Password reset email sent to given details');
            } else {
                toast.warn(response.data.message || 'Something went wrong.');
            }
        }).catch(error => {
            console.log(error);
            toast.warn('Something went wrong.');
            setLoading(false)
        })
    }

    return (
        <div className="bg-wrapper">
            <div className="login">
                {/* <div className="login-inner"> */}
                    {/* <div className="login-bg">
                        <img alt="" src={require(`../../assets/images/login-img.png`)} className="login-img animated fadeIn" />
                    </div> */}
                    <div className="login-pink-back">
				        <img alt="" src={require(`../../assets/images/login/vector.png`)} className="login-img animated fadeIn" />
			        </div>
                    <div className="login-detail">
                    <div className="login-form animated fadeIn">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="login-logo">
                                <img alt="" src={require(`../../assets/images/logo.png`)} />
                            </div>
                            <p className="sign-in">{languageObj.translate("ChangePassword.1")}</p>
                            <div className="input-wrap">
                                <input type="text" name="reset_username" className={`login-input-control user-input`}
                                    ref={register({
                                        required: true,
                                        // eslint-disable-next-line
                                        pattern: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})|([0-9]{10})+$/i
                                    })}
                                    placeholder="xyz@abc.com"
                                />
                                {(errors.reset_username && errors.reset_username.type === 'required') &&
                                    <span className={`error-message`}>Please Enter Registered Email Id OR Mobile Number</span>
                                }
                                {(errors.reset_username && errors.reset_username.type === 'pattern') &&
                                    <span className={`error-message`}>Please Enter Valid Email OR Mobile Number</span>
                                }
                            </div>
                            <div className="otp-btn-wrap">
                                <button type="submit" className={`ezi-btn company-login-btn btn-ripple`} disabled={loading}>
                                    Send Reset Link
                                    {loading && <Spinner animation="grow" />}
                                </button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            {/* </div> */}
        </div>
    )
}

const mapStateToProps = state => {
    return {
        vendorInfo: state.app.vendorInfo,
    }
}

export default connect(mapStateToProps)(ForgotPassword);