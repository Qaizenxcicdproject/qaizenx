import React, { useContext, useState } from 'react';
import { Spinner } from 'react-bootstrap';
import AppContext from 'store/AppContext';
import useForm from 'react-hook-form'

const PasswordResetEmail = ({ updateStep }) => {

    const { languageObj } = useContext(AppContext);
    const { register, handleSubmit, errors } = useForm()
    const [loader, setLoader] = useState(false);
    const onSubmit = data => {
        console.log(data)
        updateStep('verify_user', data.reset_email)
    }


    return (

        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="login-logo">
                <img alt="" src={require(`../../assets/images/logo.png`)} />
            </div>
            <p className="sign-in">{languageObj.translate("ChangePassword.1")}</p>
            <div className="input-wrap">
                <input type="text" name="reset_email" className={`login-input-control user-input`} ref={register({
                    required: true, pattern: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})|([0-9]{10})+$/i
                })} placeholder="Please Enter Registered Email Or Mobile Number" />
                {(errors.reset_email && errors.reset_email.type === 'required') &&
                    <span className={`error-message`}>Please Enter Registered Email Or Mobile Number</span>
                }
                {(errors.reset_email && errors.reset_email.type === 'pattern') &&
                    <span className={`error-message`}>Please Enter Valid Email Or Mobile Number</span>
                }
            </div>
            <div className="otp-btn-wrap">
                <button type="submit" className={`ezi-btn company-login-btn btn-ripple`}>
                    Send OTP
                    {loader && <Spinner animation="grow" />}
                </button>
            </div>
        </form>
    )
}
export default PasswordResetEmail;