import React, { useState, useEffect, useContext } from "react";
import './GoogleReview.scss';
import { Breadcrumb, Container, Row, Col } from 'react-bootstrap';
import useForm from 'react-hook-form';
import configURL from 'config/config';
import Axios from "utility/Axios";
import { toast } from 'react-toastify';
import AppContext from 'store/AppContext';
import FormData from 'utility/AppFormData';

const GoogleReview = () => {

    const { EziLoader, accesFeature, languageObj } = useContext(AppContext)
    const { register, handleSubmit, reset } = useForm();
    const [googleReviewData, setGoogleReviewData] = useState({});
    const [googleReviewActive, setGoogleReviewActive] = useState(false)
    // const [timeZone, setTimeZone] = useState({ label: "Asia/Kolkata", value: "Asia/Kolkata" });

    /**
     * Save Company
     * @param {Object} data 
     */

 
    const submitFormData = (data) => {
        EziLoader.show()
        const sendData = new FormData();
        const isActive = data.is_active;
        sendData.append("google_place_id", data.google_place_id);
        sendData.append("day_freq", data.day_freq);
        sendData.append("is_active", isActive === true? 1 : 0)
        Axios.post(configURL.google_review_setting, sendData).then(response => {
            EziLoader.hide()
            if (response.data.result.success === true) {
                toast.success(response.data.result.message);
            } else {
                toast.warn(response.data.message || "Something went wrong");
            }
        }).catch(err => {
            console.log(err.toString())
            EziLoader.hide()
        })

    }
    
    const getGoogleReviewDetails = () => {
        // if (company_id) {
            EziLoader.show()
            // let formData = new FormData()
            // formData.append("account_id", company_id)
            Axios.post(configURL.google_review_setting_list).then(response => {
                EziLoader.hide()
                if (response.data.success) {
                    setGoogleReviewData(response.data.result || '')
                    if(response.data.result.is_active === 1){
                        setGoogleReviewActive(true);
                    }else{setGoogleReviewActive(false)};
                    // setCompanyDetails(response.data.result || '')
                }
            }).catch(err => {
                EziLoader.hide()
            })
        // }
    }

    useEffect(getGoogleReviewDetails, []);

    
    /**
     * Handle Form Reset
     */
    const clearForm = () => {
        reset({
            from_name: "",
            from_email_address: "",
            host: "",
            port: "",
            username: "",
            password: "",
            encryption: ""
        })
    }
    return (
        <React.Fragment>
            <section className="Page-SMTPsetting" >
                <form onSubmit={handleSubmit(submitFormData)} className="company-form">
                    <div className="breadcrumb_ezi">
                        <Breadcrumb>
                            <Breadcrumb.Item>{languageObj.translate('Settings.1')}</Breadcrumb.Item>
                            <Breadcrumb.Item>{languageObj.translate('GoogleReviewSetting.1')}</Breadcrumb.Item>
                        </Breadcrumb>
                        <div className="column-header">
                            <h1 className="page-heading">{languageObj.translate('GoogleReviewDetails.1')}</h1>
                            <div className="column-header-btn">
                                <button type="button" className="btn-ripple clear_all" disabled={!accesFeature.update_company} title={!accesFeature.update_company ? 'Unauthorized Operation' : ''} onClick={clearForm} >{languageObj.translate("ClearAll.1")}</button>
                                <button type="submit" className="btn-ripple add-new">Save Settings</button>
                            </div>
                        </div>
                    </div>
                    <div className="add-company-form-wrap">
                        <div className="company-info-header">
                            <div className="conpany-name-id-wrap">
                                <span className="title">Google Review Details</span>
                            </div>
                        </div>
                        <div className="tablist_ezi">
                            <div className="add-company-field-wrapper">
                                <Container>
                                    <Row>
                                       <Col md={6}> 
                                        <label className="w-100">
                                            <input type="text"
                                                placeholder="Google Place Id"
                                                name="google_place_id"
                                                ref={register({ required: true })}
                                                defaultValue={googleReviewData.google_place_id || ""} />
                                            {/* {errors.GooglePlaceId && errors.GooglePlaceId.type === 'required' &&
                                                <span className="error_cu">{languageObj.translate('GooglePlaceId.1')}</span>} */}
                                        </label>
                                        </Col>
                                        <Col md={6}> 
                                        <label className="w-100">
                                            <input type="number"
                                                placeholder="Google Review Pull Frequency"
                                                name="day_freq"
                                                min="1"
                                                max="30"
                                                ref={register({ required: true })}
                                                defaultValue={googleReviewData.day_freq || ""} />
                                            {/* {errors.GoogleReviewPullFrequency && errors.GoogleReviewPullFrequency.type === 'required' &&
                                                <span className="error_cu">{languageObj.translate('hostrequired.1')}</span>} */}
                                        </label>
                                        </Col>
                                        <Col md={6}>
                                        <label className="ezi-checkbox w-100">  
                                            <div className="d-flex">
                                            <input type="checkbox"
                                                className="review_checkbox"
                                                name="is_active"
                                                checked = {googleReviewActive}
                                                onChange={({ target }) =>setGoogleReviewActive(target.checked)}
                                                ref={register} />
                                                <span className="ezi-checkbox-mark"></span>
                                                <span className="enable-google-revw">Enable Google Review </span>
                                            </div>
                                            {/* {errors.from_name && errors.from_name.type === 'required' &&
                                                <span className="error_cu">{languageObj.translate('from_namerequired.1')}</span>} */}
                                        </label>
                                        </Col>
                                    </Row>
                                </Container>
                            </div>
                        </div>
                    </div>
                </form>
                
            </section>
        </React.Fragment>
    )
}

export default GoogleReview