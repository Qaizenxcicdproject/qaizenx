import React, { useState, useEffect, useRef, useContext } from "react";
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { CircularProgressbar } from 'react-circular-progressbar';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { getFirstWord } from 'utility/helper';
import 'utility/i18next.js'
import './KpiSettings.scss';
import { toast } from "react-toastify";
import SweetSearch from "components/SweetSearch";
import Pagination from "react-js-pagination";
import { confirmAlert } from 'react-confirm-alert';
import AppContext from 'store/AppContext';
import FormData from 'utility/AppFormData';

const KpiSettings = ({ history }) => {
    const { EziLoader } = useContext(AppContext)
    const [kpiSettings, setKpiSettings] = useState([]);
    const [pagination, setPagination] = useState({});
    const [searchLoading, setSearchLoading] = useState(false);
    const [kpiStatus, setKpiStatus] = useState('all');
    const [kpiDashCounts, setKpiDashCounts] = useState({
        active: 0,
        inactive: 0,
        total: 0,
    });
    const perPage = 10;
    var searchTimer = null;
    const inputSearch = useRef(null);
    const currentPage = useRef(1);

    /**
     * Get Survey Workflows
     */
    const getKpiSettings = (type = kpiStatus) => {
        let searchVal = inputSearch.current.value;
        let formData = new FormData();
        formData.append("per_page", perPage);
        formData.append("page", currentPage.current);
        formData.append("status", type);
        if (searchVal !== "")
            formData.append("search", searchVal);
        Axios.post(configURL.get_kpi_listing, formData).then(response => {
            EziLoader.hide()
            setSearchLoading(false)
            if (response.data.success === true) {
                setKpiSettings(response.data.results);
                setPagination(response.data.pagination);
            } else {
                toast.warn(response.data.message);
            }
        }).catch(err => {
            console.log(err)
            EziLoader.hide()
        })
    }

    /**
     * Get Survey Workflows Stats
     */
    const getKpiStatics = () => {
        Axios.post(configURL.get_kpi_counts, {}).then(response => {
            if (response.data.success === true) {
                setKpiDashCounts(response.data.results);
            }
        }).catch(err => {
            console.log(err)
        })
    }

    /**
     * Delete KPI
     * @param {number} kpi_id Unique ID
     */
    const deleteKpi = (kpi_id = "") => {
        confirmAlert({
            title: 'Delete KPI',
            message: 'Are you sure you want to delete ?',
            buttons: [
                {
                    label: 'Confirm',
                    onClick: () => {
                        let formData = new FormData();
                        formData.append("kpi_id", kpi_id);
                        Axios.post(configURL.delete_kpi, formData).then(response => {
                            if (response.data.success !== undefined && response.data.success === true) {
                                toast.success(response.data.message || 'KPI deleted successfully.')
                                if (kpiSettings.length === 1)
                                    currentPage.current = (currentPage.current > 1) ? currentPage.current - 1 : 1
                                getKpiSettings()
                            } else {
                                toast.warn(response.data.message || 'Something went wrong here.');
                            }
                        }).catch(err => {
                            console.log(err)
                        })
                    }
                },
                {
                    label: 'Cancel'
                }
            ]
        });
    }

    /**
     * Filter Data based on search.
     * 
     * @param {string} type Input value
     */
    const handleFilterSearch = () => {
        clearTimeout(searchTimer);
        searchTimer = setTimeout(() => {
            setSearchLoading(true)
            currentPage.current = 1
            getKpiSettings();
        }, 800);
    }

    /**
     * Filter Data based on status.
     * 
     * @param {string} type Filter value
     */
    const handleFilterByStatus = (type = 'all') => {
        setKpiStatus(type)
        EziLoader.show()
        getKpiSettings(type)
    }

    /**
     * Handle Pagination
     * 
     * @param {string} type Filter Type
     */
    const handlePagination = (page = 1) => {
        EziLoader.show()
        currentPage.current = page
        getKpiSettings();
    }

    /**
     * Handle KPI Status Change
     *
     * @param {string} status Status
     */
    const handleStatusChange = ({ status = 'inactive', id = null }) => {
        let newStatus = (status === 'active') ? 'inactive' : 'active'
        confirmAlert({
            title: 'KPI Update',
            message: `Are you sure you want to change status from ${status} to ${newStatus}`,
            buttons: [
                {
                    label: 'Confirm',
                    onClick: () => {
                        let formData = new FormData()
                        formData.append("status", newStatus)
                        formData.append("kpi_id", id)
                        Axios.post(configURL.save_kpi, formData).then(response => {
                            if (response.data.success === true) {
                                getKpiSettings()
                                toast.success(response.data.message || 'KPI saved successfully.')
                            } else {
                                toast.warn(response.data.message || 'Something went wrong.')
                            }
                        }).catch(err => {
                            console.log(err);
                        })
                    }
                },
                {
                    label: 'Cancel'
                }
            ]
        });
    }

    useEffect(() => {
        EziLoader.show()
        getKpiStatics()
        getKpiSettings()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <React.Fragment>
            <section className="Page-KPISettings">
                <div className="workflow-dashboard-card-wrap">
                    <div className={`workflow-dashboard-card total`} onClick={() => handleFilterByStatus('all')} >
                        <div className="workflow-dashboard-card_text_wrap">
                            <p className="workflow-dashboard-card-heading">{kpiDashCounts.total || 0}</p>
                            <span className="workflow-dashboard-card-subheading">Total KPI's</span>
                        </div>
                        <div className="category_progress_ring_wrap">
                            <div className="category_progress_ring">
                                <CircularProgressbar className="category_progress_circle" value={Math.ceil((kpiDashCounts.active / kpiDashCounts.total) * 100) || 0} text={`${Math.ceil((kpiDashCounts.active / kpiDashCounts.total) * 100)}%`}
                                    x="100" />
                            </div>
                            <span className="circular-survey-text active--text">Active KPI's</span>
                        </div>
                    </div>
                    <div className={`workflow-dashboard-card active-k ${(kpiStatus === 'active') ? 'active' : ''}`} onClick={() => handleFilterByStatus('active')}>
                        <div className="workflow-dashboard-card_text_wrap">
                            <p className="workflow-dashboard-card-heading">{kpiDashCounts.active || 0}</p>
                            <span className="workflow-dashboard-card-subheading">Active KPI's</span>
                        </div>
                    </div>
                    <div className={`workflow-dashboard-card inactive ${(kpiStatus === 'inactive') ? 'active' : ''}`} onClick={() => handleFilterByStatus('inactive')}>
                        <div className="workflow-dashboard-card_text_wrap">
                            <p className="workflow-dashboard-card-heading">{kpiDashCounts.inactive || 0}</p>
                            <span className="workflow-dashboard-card-subheading">Inactive KPI's</span>
                        </div>
                    </div>
                    <div className={`workflow-dashboard-card create-workflow--c`} onClick={() => history.push('/kpi-settings/save')} >
                        <span className="create-workflow--ic"></span>
                        <p className="create-workflow--name">Create new KPI</p>
                    </div>
                </div>
                <div className="filter-search-wrap">
                    <SweetSearch loading={searchLoading} change={handleFilterSearch} ref={inputSearch} />
                </div>
                <div className="workflow-dashboard-table">
                    <div className="workflow-dashboard-table-row workflow-table-heading">
                        <div className="workflow-dashboard-table-cell">KPI Name</div>
                        <div className="workflow-dashboard-table-cell">Owner</div>
                        <div className="workflow-dashboard-table-cell">Status</div>
                        <div className="workflow-dashboard-table-cell">Action</div>
                    </div>
                    {
                        kpiSettings.length > 0 ? kpiSettings.map((item, index) => {
                            return (
                                <div key={index} className="workflow-dashboard-table-row">
                                    <div className="workflow-dashboard-table-cell" data-title="KPI">
                                        <div className={`workflow-table-survey-wrap`} >
                                            <div className="workflow-table-survey-text-wrap">
                                                <span className="workflow-table-survey-name">{item.name || ''}</span>
                                                <span className="workflow-table-create">
                                                    Last Modified : {getFirstWord(item.updated_at)}   |   Created on : {getFirstWord(item.created_at)}
                                                </span>
                                                <span className="workflow-table-create">Description : {item.description}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="workflow-dashboard-table-cell" data-title="Owner"><span className="owner-name">{item.owner || "---"}</span></div>
                                    <div className={`workflow-dashboard-table-cell table-status`} data-title="Status" onClick={() => handleStatusChange(item)}>
                                        <span className={`dt-status-formatter ${item.status || 'inactive'}`}>{item.status || 'inactive'}</span>
                                    </div>

                                    <div className={`workflow-dashboard-table-cell`} data-title="Action">
                                        <div className="action-wrap">
                                            <OverlayTrigger overlay={<Tooltip>Edit KPI</Tooltip>}>
                                                <button type="button" className="edit_data" onClick={() => {
                                                    history.push(`/kpi-settings/save/${item.id}`)
                                                }}></button>
                                            </OverlayTrigger>
                                            <OverlayTrigger overlay={<Tooltip>Delete KPI</Tooltip>}>
                                                <button type="button" className="delete_data" onClick={() => deleteKpi(item.id)}></button>
                                            </OverlayTrigger>
                                        </div>
                                    </div>
                                </div>
                            )
                        }) : null
                    }
                </div>
                {kpiSettings.length <= 0 && <div className="workflow-table-no-result">No result Found</div>}
                <div className="pagination-plugin-wrap category-pagination-formatter">
                    <Pagination
                        activePage={pagination.current_page}
                        itemsCountPerPage={perPage}
                        totalItemsCount={pagination.total || 0}
                        onChange={handlePagination}
                        hideDisabled={true}
                        firstPageText={<span class="prev-page-text-ic"></span>}
                        lastPageText={<span class="next-page-text-ic"></span>}
                        nextPageText={<span class="next-text-ic"></span>}
                        prevPageText={<span class="prev-text-ic"></span>}
                    />
                </div>
            </section>
        </React.Fragment>
    )
}

export default KpiSettings;