import React, { useState, useEffect, useContext } from "react";
import Axios from "utility/Axios";
import configURL from 'config/config';
import useForm from 'react-hook-form';
import { Spinner, Breadcrumb } from "react-bootstrap";
import { toast } from "react-toastify";
import SelectSearch from "react-select-search";
import "../../assets/scss/SelectBox.scss"
import AppContext from 'store/AppContext';
import { useParams } from "react-router-dom"
import './KpiSettings.scss'

const SaveKpiSetting = (props) => {
    const { EziLoader } = useContext(AppContext)
    const [kpiData, setKpiData] = useState({ name: '', description: '', status: false, days: '' });
    const [loading, setLoading] = useState(false);
    const { register, handleSubmit, errors, setValue } = useForm();
    const [kpiSurvey, setKpiSurvey] = useState(null);
    const [kpiSurveys, setKpiSurveys] = useState([]);
    const { kpi_id } = useParams()

    /**
     * Save Template Data.
     * 
     * @param {object} data 
     */
    const handleFormSubmit = data => {
        setLoading(true);
        let formData = new FormData();
        formData.append("name", data.name);
        formData.append("description", data.description);
        formData.append("survey_id", data.survey_id);
        formData.append("status", (data.status === true) ? 'active' : 'inactive');
        formData.append("days", data.days);
        if (kpi_id && kpi_id !== "") {
            formData.append("kpi_id", kpi_id);
        }

        Axios.post(configURL.save_kpi, formData).then(res => {
            setLoading(false);
            if (res.data.success === true) {
                toast.success(res.data.message);
                props.history.push("/kpi-settings");
            } else {
                toast.warn(res.data.message);
            }
        }).catch(err => {
            setLoading(false)
            console.log(err)
        })
    };

    const getKpiDetails = async () => {
        EziLoader.show()
        let formData = new FormData()
        formData.append("kpi_id", kpi_id)
        EziLoader.show()
        let response = await Axios.post(configURL.get_kpi_detail, formData)
        if (response.data.success === true) {
            let { name = '', description = '', status = 'inactive', survey_id = null, days = '' } = response.data.result
            setKpiData({ name, description, status: (status === 'active') ? true : false, days })
            if (survey_id) {
                setKpiSurvey(survey_id)
                setValue('survey_id', survey_id, { shouldValidate: true })
            }

        } else {
            toast.warn(response.data.message);
        }
        EziLoader.hide()

    }

    const getKpiSurveys = async () => {
        EziLoader.show()
        let response = await Axios.post(configURL.get_kpi_surveys, {})
        if (response.data.success) {
            let typesData = [];
            response.data.result.forEach(element => {
                typesData.push({
                    value: element.id,
                    name: element.name
                })
            });
            setKpiSurveys(typesData)
        }
        EziLoader.hide()
        if (kpi_id && kpi_id !== "") {
            getKpiDetails()
        }
    }

    useEffect(() => {
        register({ name: 'survey_id', type: 'custom' }, { required: true })
        getKpiSurveys();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <React.Fragment>
            <section className="Page-SaveKPISetting">
                <div className="breadcrumb_ezi template-add-beadcrumb">
                    <Breadcrumb>
                        <Breadcrumb.Item onClick={() => props.history.goBack()}>Kpi Settings</Breadcrumb.Item>
                        <Breadcrumb.Item>{(kpi_id) ? 'Update KPI' : "Create New"}</Breadcrumb.Item>
                    </Breadcrumb>
                </div>
                <form onSubmit={handleSubmit(handleFormSubmit)}>
                    <div className="survey-category-wrap">
                        <h1 className="category_label">{(kpi_id) ? 'Update KPI' : "Create New KPI"}</h1>
                        <div className="category-field-control">
                            <input type="text" name="name" defaultValue={kpiData.name} ref={register({ required: true })} placeholder="KPI Name" className="create-category-input" />
                            {errors.name && <span className="error-message">Please enter KPI name</span>}
                        </div>
                        <div className="category-field-control">
                            <select value={kpiData.days} ref={register({ required: true })} name="days" className="create-category-input" onChange={({ target }) => setKpiData({ ...kpiData, [target.name]: target.value })}>
                                <option value="">Please select reporting interval</option>
                                <option value="7">Weekly</option>
                                <option value="30">Monthly</option>
                                <option value="90">Quarterly </option>
                                <option value="180">Semi Annually</option>
                            </select>
                            {errors.days && <span className="error-message">Please select reporting interval</span>}
                        </div>
                        <div className="category-field-control">
                            <div className="react-select-search_c">
                                <SelectSearch
                                    options={kpiSurveys}
                                    search
                                    value={kpiSurvey || null}
                                    onChange={(value) => {
                                        setValue('survey_id', value, { shouldValidate: true })
                                        setKpiSurvey(value)
                                    }}
                                    placeholder="Please select survey"
                                />
                            </div>
                            {errors.survey_id && <span className="error-message">Please select KPI Survey.</span>}
                        </div>
                        <div className="category-field-control">
                            <textarea defaultValue={kpiData.description} rows="4" name="description" ref={register} className="create-category-input" placeholder="KPI Description"></textarea>
                            {errors.description && <span className="error-message">Please enter KPI Description</span>}
                        </div>
                        <div className="category-field-control">
                            <label className="qaizenx-checkbox-input">Is Active
                                <input type="checkbox" ref={register} name="status" checked={kpiData.status} onChange={({ target }) => setKpiData({ ...kpiData, [target.name]: target.checked })} />
                                <span className="checkmark"></span>
                            </label>
                        </div>
                        <div className="category_add_btn_wrap">
                            <button type="submit" className="btn-ripple category_add_btn" disabled={loading}>
                                {(kpi_id) ? 'Save KPI' : "Create New"}
                                {loading && <Spinner animation="grow" variant="light" size="sm" />}
                            </button>
                        </div>
                    </div>
                </form>
            </section>
        </React.Fragment>
    )
}

export default SaveKpiSetting;