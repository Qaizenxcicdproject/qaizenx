import React, { useState, useEffect } from 'react';
import './Login.scss';
import { Spinner, Badge } from 'react-bootstrap';
// import { NavLink } from "react-router-dom";
// import AppContext from 'store/AppContext';
import Axios from 'axios';
import configURL from 'config/config';
import { toast } from 'react-toastify';
import useForm from 'react-hook-form';
import { APP_ID, APP_FIELD_NAME } from "constants/constants"
import { connect } from "react-redux";
import * as AppActions from "store/actions";
import FormData from 'utility/AppFormData';


var timer = null
const Login = ({ loginUser, logOutUser, history, vendorInfo }) => {
	let timerTime = 120
	// const { languageObj } = useContext(AppContext);
	const [loader, setLoader] = useState({ otpLogin: false, otpSend: false, login: false });
	const [otpLogin, setOtpLogin] = useState(false);
	const [optActed, setOptActed] = useState(false);
	const [viewPwd, setViewPwd] = useState(false);
	const [userName, setUserName] = useState("");
	const [otpTimer, setOtpTimer] = useState(timerTime);
	const [password, setPassword] = useState();
	// const { register, handleSubmit, errors, triggerValidation, setValue } = useForm()
	const { register, handleSubmit, errors, setValue } = useForm()

	/**
	 * Login User
	 * 
	 * @param {target} 
	 */
	const handleLogin = (data) => {
		if (!optActed) {
			handleSubmitForm(data)
		}
		if (optActed) {
			handeleSubmitOtp(data.userId)
		}
	}

	const startTimer = () => {
		setOtpTimer(timerTime)
		timer = setInterval(() => {
			setOtpTimer(otpTimer => otpTimer - 1)
		}, 1000);
	}

	const handeleSubmitOtp = (username = userName) => {
		setLoader({ ...loader, otpSend: true })
		let formData = new FormData()
		formData.append('user_name', username)
		formData.append("account_name", vendorInfo.webaddress);
		formData.append(APP_FIELD_NAME, APP_ID);
		Axios.post(configURL.send_otp, formData).then(response => {
			setLoader({ ...loader, otpSend: false })
			if (response.data.success !== undefined && response.data.success === true) {
				setOtpLogin(true)
				startTimer()
				toast.success(response.data.message || `OTP Code sent to ${username}`)
			} else {
				toast.warn(response.data.message || 'Something went wrong');
			}
		}).catch(error => {
			setLoader({ ...loader, otpSend: false })
			let messageWrong = "Something went wrong"
			if (error.response && error.response.status) {
				const { data = {} } = error.response
				toast.warn(data.message || messageWrong)
			}
		})
	}

	/**
	 * Clear login Data.
	 */
	useEffect(() => {
		localStorage.removeItem("appState")
		localStorage.removeItem("isLogin")
		logOutUser()
		return () => clearInterval(timer)
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])

	/**
	 * Login With username and password
	 * 
	 * @param {Object} data 
	 */
	const handleSubmitForm = (data = {}) => {
		let formData = new FormData();
		formData.append("user_name", data.userId);
		formData.append("password", data.password);
		formData.append("account_name", 'fristcompany.app.teamqaizenx.com');
		formData.append(APP_FIELD_NAME, APP_ID);
		setLoader({ ...loader, login: true })
		submitLoginData(formData, configURL.loginURL)

	}


	/**
	 * Login With username and password
	 * 
	 * @param {Object} data 
	 */
	const handleSubmitOtpForm = (data = {}) => {
		let formData = new FormData();
		formData.append("otp", data.otp_code);
		formData.append("user_name", userName);
		formData.append("account_name", vendorInfo.webaddress);
		formData.append(APP_FIELD_NAME, APP_ID);
		setLoader({ ...loader, otpLogin: true })
		submitLoginData(formData, configURL.otp_login)

	}

	const submitLoginData = (data = {}, API_LINK) => {
		if (data instanceof FormData === false) {
			throw new Error('Data must be a FormData instance')
		}
		Axios.post(API_LINK, data).then(response => {
			setLoader({ otpLogin: false, otpSend: false, login: false })
			if (response.data.success !== undefined && response.data.success === true) {
				let userData = {
					name: response.data.user.first_name,
					first_name: response.data.user.first_name,
					last_name: response.data.user.last_name,
					cell_number: response.data.user.cell_number,
					id: response.data.user.id,
					email: response.data.user.email,
					auth_token: response.data.access_token,
					profile_pic_url: response.data.user.profile_pic_url,
					assigned_roles: response.data.user.assigned_roles || [],
					company_id: response.data.current_account.id,
					partner_id: response.data.current_partner_account.id || null,
					timestamp: new Date().toString(),
					timezone: response.data.current_account.timezone || "Asia/Kolkata"
				};
				localStorage.setItem("appState", JSON.stringify({
					isLoggedIn: true,
					user: userData,
					access_matrix: response.data.access_matrix || [],
					allowed_apps: response.data.applications || [],
					current_app_detail: response.data.current_account || null,
					current_partner_account: response.data.current_partner_account || null,
				}));

				localStorage.setItem('isLogin', true);
				loginUser()
				history.push("/");
			} else {
				toast.warn(response.data.message);
			}
		}).catch(error => {
			setLoader({ ...loader, login: false })
			let messageWrong = "Something went wrong Please try later!!"
			let messageIntr = "Server is not responding please try after some time."
			if (!error.response) {
				toast.warn(messageIntr);
			}
			if (error.response && error.response.status) {
				const { data = {} } = error.response
				toast.warn(data.message || messageWrong)
			}
		})
	}

	/**
	 * Trigger validation on OTP
	 */
	// const validateUserName = () => {
	// 	triggerValidation("userId");
	// 	setOptActed(true);
	// }
	if (otpTimer === 0) {
		clearInterval(timer)
		setOtpTimer(timerTime)
	}

	return (
		<div className="bg-wrapper">
			<div className="login">
				<div className="login-pink-back">
					<img alt="" src={require(`../../assets/images/login/vector.png`)} className="login-img animated fadeIn" />
				</div>
				<div className="login-detail">
					{/* <img alt="" src={require(`../../assets/images/login/qZnX-logo.png`)} className="logo-img animated fadeIn" /> */}
					<div className="login-form animated fadeIn">
						{!otpLogin && <form onSubmit={handleSubmit(handleLogin)}>
							<div className="login-logo">
								<img alt="" src={vendorInfo.logo || require(`../../assets/images/login/qZnX-logo.png`)} />
							</div>
							<p className="sign-in">{vendorInfo.name || 'Company'} Login</p>
							<div className="input-wrap">
								<input
									type="text"
									name="userId"
									className={`login-input-control user-input`}
									ref={register({
										required: true,
										// eslint-disable-next-line
										pattern: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})|([0-9]{10})+$/i
									})}
									placeholder="Registered Email / Mobile Number"
									onChange={({ target }) => {
										setUserName(target.value.trim())
										setValue("userId", target.value.trim(), { shouldValidate: true })
									}}
									value={userName}
								/>
								{errors.userId && errors.userId.type === 'required' && <span className={`error-message`}>Please enter Email or Mobile Number</span>}
								{errors.userId && errors.userId.type === 'pattern' && <span className={`error-message`}>Please enter valid Email or Mobile Number</span>}
							</div>

							<div className="input-wrap">
								<input type={`${viewPwd ? "text" : "password"}`} name="password" className={`login-input-control password-input`} ref={register({
									required: !optActed
								})}
									placeholder="Password"
									onChange={({ target }) => {
										setOptActed(false)
										setPassword(target.value.trim())
									}}
									value={password || ''}
								/>
								<button type="button" className="ezi-btn visibility-button" onClick={() => setViewPwd(!viewPwd)}>
									<span className={`eye_icon ${viewPwd ? "visible_on" : "visiblke_off"}`}></span></button>
								{errors.password && <span className={`error-message`}>Please enter password</span>}
							</div>
							<div className="login-end">
								<div className="otp-btn-wrap main-signIn">
									<button type="submit" id="login" className={`ezi-btn company-login-btn btn-ripple`} onClickCapture={() => setOptActed(false)}>
										SignIn
										{/* {languageObj.translate("SignIn.1")} */}
										{loader.login && <Spinner animation="grow" />}
									</button>
									{/* <button className="ezi-btn request-otp-btn btn-ripple" onClick={validateUserName}>
									{languageObj.translate('RequestOTP.1')}
									{loader.otpSend && <Spinner animation="grow" />}
								</button> */}
								</div>
								<div className="forgot-wrapper">
									{/* <label className="login-checkbox">{languageObj.translate("KeepMeLoggedIn.1")}
								<input type="checkbox" />
								<span className="checkmark"></span>
							</label> */}
									<div className="forgot-text">
										<a href="../reset-password-link">Forgot password ?</a>
										{/* <NavLink activeClassName='link-active' to="/reset-password-link">
											ForgotPassword.1
										</NavLink> */}
									</div>
								</div>
							</div>
						</form>}

						{otpLogin && <form onSubmit={handleSubmit(handleSubmitOtpForm)}>
							<div className="login-logo">
								<img alt="" src={vendorInfo.logo || require(`../../assets/images/logo.png`)} />
							</div>
							<p className="sign-in">{vendorInfo.name || 'Company'} Otp Login</p>
							<div className="input-wrap">
								<input type="text" name="otp_code" className={`login-input-control password-input`} ref={register({
									required: true
								})} placeholder={'Please Enter OTP'} />
								{errors.otp_code && <span className={`error-message`}>Please Enter OTP Code</span>}
							</div>
							<div className="otp-btn-wrap">
								<button type="button" disabled={otpTimer !== timerTime} onClick={() => handeleSubmitOtp()} className={`ezi-btn company-login-btn btn-ripple`}>
									{'Resend OTP '}
									{loader.otpSend && <Spinner animation="grow" />}
									{otpTimer !== timerTime && <Badge variant="light" style={{ borderRadius: "100%" }}>{otpTimer}</Badge>}
								</button>
								<button type="submit" id="login" className={`ezi-btn company-login-btn btn-ripple`}>
									{'Submit OTP'}
									{loader.otpLogin && <Spinner animation="grow" />}
								</button>
							</div>

						</form>}
						{/* <div className="forgot-wrapper"> */}
						{/* <label className="login-checkbox">{languageObj.translate("KeepMeLoggedIn.1")}
								<input type="checkbox" />
								<span className="checkmark"></span>
							</label> */}
						{/* <div className="forgot-text">
								<NavLink activeClassName='link-active' to="/reset-password-link">
									{languageObj.translate("ForgotPassword.1")}
								</NavLink>
							</div> */}
						{/* </div> */}
						{/* <div className="language-change">
							<button className="language-text" type="button" onClick={() => languageObj.changeLang("En")}>English</button>
							<button className="language-text" type="button" onClick={() => languageObj.changeLang("Arb")}>عربى</button>
							<button className="language-text" type="button" onClick={() => languageObj.changeLang("Jp")}>日本語</button>
						</div>
						<div className="language-dropdown">
							<span className="change-lang">{languageObj.translate("ChangeLang.1")}</span>
							<Dropdown>
								<Dropdown.Toggle id="lang">select</Dropdown.Toggle>
								<Dropdown.Menu>
									{languageObj.languages.length > 0 && languageObj.languages.map((item, index) => (
										<Dropdown.Item key={item.id}>
											<div className="flag-img-wrap" onClick={() => languageObj.changeLang(item.lang_code)}>
												<label className="country-flag-label">{item.lang_code}</label>
											</div>
										</Dropdown.Item>
									))}
								</Dropdown.Menu>
							</Dropdown>
						</div> */}
					</div>
				</div>

			</div>
		</div>
	)
}

const mapDispatchToProps = dispatch => {
	return {
		loginUser: () => dispatch(AppActions.loginUser()),
		logOutUser: () => dispatch(AppActions.logOutUser())
	}
}
const mapStateToProps = state => {
	return {
		vendorInfo: state.app.vendorInfo,
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);