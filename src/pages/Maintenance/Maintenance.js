import React from 'react';
import "./Maintenance.scss";

const Maintenance = () => (
    <div id="article" className="card">
        <img src="https://app.ezi2survey.com/static/media/logo.f385eae3.png" />
        <div style={{ textAlign: "left" }}>
            <p>Site is under Maintenance Apologies for the inconvenience.<br></br>
                You can still contact us at
                 <a href="mailto: info@gpitservices.com"> info@gpitservices.com</a>. We'll be back up soon!</p>
            <p style={{ textAlign: "right" }}>&mdash; GPIT TEAM</p>
        </div>
    </div >
)


export default Maintenance