import React, { useEffect, useState, useRef } from "react";
import './Notifications.scss';
import EziLoader from "components/EziLoader";
import Axios from "utility/Axios";
import { NOTIFICATION_COLS } from "constants/constants";
import configURL from "config/config";
import DataTable from "components/DataTable";
import SweetSearch from "components/SweetSearch";
import { Breadcrumb } from 'react-bootstrap';

const Notifications = (props) => {

    const [pagination, setPagination] = useState({})
    const [loading, setLoading] = useState(false)
    const [searchLoading, setSearchLoading] = useState(false)
    const [notificationsData, setNotificationsData] = useState([])
    const per_page = 10;
    const inputSearch = useRef(null);
    var searchTimer = null;
    
    const handleTableChange = (type, props) => {
        setLoading(true)
        switch (type) {
            case "pagination":
                getAllNotifications(props.page);
                break;
            default:
                break
        }

    }

    const getAllNotifications = (page = 1) => {
        let userSearch = inputSearch.current.value;
        let formData = new FormData()
        formData.append("page", page)
        formData.append("per_page", per_page)
        formData.append("search", userSearch)
        formData.append("status", 'all')
        Axios.post(configURL.get_notifications, formData).then(response => {
            if (response.data.success === true) {
                setNotificationsData(response.data.results)
                setPagination(response.data.pagination)
                setLoading(false)
                setSearchLoading(false)
            }
            const notificationStatus = response.data.results.map(el => el.status)
            if(notificationStatus.includes("unread") === true){
                handleReadAll();
            }
        })
    }

    const handleReadAll = () => {
        const notificationIds = notificationsData.map(el => el.id)
        let formData = new FormData()
        formData.append("notification_ids", JSON.stringify(notificationIds))
        Axios.post(configURL.update_notifications, formData).catch(err => {
            console.log(err);
        })
    }

    const handleTableSearch = () => {
        clearTimeout(searchTimer);
        const notificationStatus = notificationsData.map(el => el.status)
        searchTimer = setTimeout(() => {
            setSearchLoading(true);
            if(notificationStatus.includes("unread") === true){
                handleReadAll();
            }
            getAllNotifications();
        }, 500);
    }

    useEffect(() => {
        setLoading(true)
        // handleReadAll();
        getAllNotifications();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <React.Fragment>
            <section className="Page-Notifications" >
                <div className="breadcrumb_ezi">
                    <Breadcrumb>
                        <Breadcrumb.Item onClick={() => props.history.push("/")}>Dashboard</Breadcrumb.Item>
                        <Breadcrumb.Item >All Notifications</Breadcrumb.Item>
                    </Breadcrumb>

                </div>
                <div className="users-table-content">
                    <SweetSearch ref={inputSearch} change={handleTableSearch} loading={searchLoading} />
                    {(notificationsData.length > 0) &&
                        <div className="saved_participants_table">
                            <DataTable
                                data={notificationsData}
                                columns={NOTIFICATION_COLS}
                                hideSelectAll
                                page={pagination.current_page}
                                sizePerPage={per_page}
                                onTableChange={handleTableChange}
                                keyField="id"
                                totalSize={pagination.total}
                                hideSelect
                            />
                        </div>
                    }
                    {(notificationsData.length === 0) &&
                        <div className="saved_participants_table">
                            <DataTable data={[]} columns={[{}]} keyField="id" hideSelect />
                        </div>
                    }
                </div>
            </section>
            {loading && <EziLoader />}
        </React.Fragment>
    )
}

export default Notifications;