import React, { useState } from 'react';
import './ParticipantLogin.scss';
import { Spinner } from 'react-bootstrap';
import Axios from "axios"
import { toast } from 'react-toastify';
import configURL from 'config/config';
import { connect } from "react-redux";

const ParticipantLogin = ({ vendorInfo }) => {
    let inputVal = null;
    const [inputErr, setInputErr] = useState(null)
    const [saving, setSaving] = useState(false)
    // localStorage.setItem('isMemberLogin', true)
    const handleContinue = () => {
        if (!inputVal || inputVal.value === "") {
            setInputErr('Please enter unique code')
            return;
        }
        setSaving(true)
        let formData = new FormData()
        formData.append('unique_code', inputVal.value)
        formData.append("account_name", vendorInfo.webaddress);
        Axios.post(configURL.verify_survey_member, formData).then(response => {
            setSaving(false)
            if (response.data.success === true) {
                let redirectUri = response.data.url || 'https://www.qaizenx.com/'
                window.location = redirectUri
            } else {
                toast.warn('Something went wrong.')
            }
        }).catch(error => {
            setSaving(false)
            toast.warn('Something went wrong.')
            console.log(error);
        })
    }

    return (
        <React.Fragment>
            <section className={`Page-ParticipantLogin `}>
                <div className="bg-wrapper">
                    <div className="login">
                        <div className="login-inner">
                            <div className="login-bg">
                                <img alt="" src={require(`../../assets/images/login-img.png`)} className="login-img animated fadeIn" />
                            </div>
                            <div className="captcha_verification login-form animated fadeIn">
                                <div className="login-logo">
                                    <img alt="" src={vendorInfo.logo || require(`../../assets/images/logo.png`)} />
                                </div>
                                <p className="participant-in">{vendorInfo.name || 'Company'} Participant Login </p>
                                <div className="captcha_input_wrapper">
                                    <div className="captcha_input_inner input-wrap">
                                        <input
                                            ref={(input) => inputVal = input}
                                            type="text"
                                            onChange={() => setInputErr(null)}
                                            className="captcha_input login-input-control user-input"
                                            placeholder="Please enter your unique key" />
                                        {inputErr && <span className="error-message">{inputErr}</span>}
                                    </div>
                                    <div className="otp-btn-wrap">
                                        <button type="button" className="captcha_submit_btn ezi-btn company-login-btn btn-ripple" onClick={handleContinue}>
                                            Continue to survey {saving && <Spinner className="burgundy-spinner" animation="border" size="sm" />}
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {/* <div className="preview-survey-poweredby-wrap">
                                <span className="preview-survey-poweredby-heading">Powered by</span>
                                <a href="https://www.ezi2survey.com/" target="_blank" rel="noopener noreferrer" className="preview-survey-poweredby-link" ><img alt="" src={require(`../../assets/images/logo.png`)} className="preview-survey-poweredby-logo" /></a>
                            </div> */}
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    );
}
const mapStateToProps = state => {
    return {
        vendorInfo: state.app.vendorInfo,
    }
}
export default connect(mapStateToProps)(ParticipantLogin);