import React, { useState, useEffect } from "react";
import Axios from "utility/Axios"
import { Tab, Breadcrumb, Nav } from 'react-bootstrap';
import configURL from 'config/config';
import './Question.scss';
import FormData from 'utility/AppFormData';

const Question = () => {
    const [search, setSearch] = useState('');
    const [rowData, setRowData] = useState([]);
    const [themeId, setThemeId] = useState('');
    const [themes, setThemes] = useState([]);
    const [subthemes, setSubthemes] = useState([]);
    const [columns, setColumns] = useState([{}]);
    const [pagination, setPagination] = useState({});
    const axiosApiCallPage = (pageno = 1, themeIdp = themeId, subthemeIdp = '', searchp = search) => {
        let formData = new FormData();
        formData.append("page", pageno);
        formData.append("search", searchp);
        formData.append("theme_id", (themeIdp == null) ? '' : themeIdp);
        formData.append("sub_theme_id", (subthemeIdp == null) ? '' : subthemeIdp);
        Axios.post(configURL.questionBank, formData).then(res => {
            let questionsArr = [...res.data.data];
            if (res.data.success !== undefined && res.data.success) {
                setRowData(questionsArr);
                setPagination(res.data.data.pagination);
            } else {
                setRowData([]);
            }
        })
    }

    const changeTheme = (e) => {
        let tId = e.target[e.target.selectedIndex].getAttribute('data-id');
        setThemeId(tId);
        setSubtheme(tId);
        axiosApiCallPage(1, tId);
    }

    const setSubtheme = (tId) => {
        let formData = new FormData();
        formData.append("theme_id", tId);
        formData.append("page", 1);
        Axios.post(configURL.subthemeListing, formData).then(res => {
            setSubthemes(res.data.data);
        })
    }

    const changeSubtheme = (e) => {
        let stId = e.target[e.target.selectedIndex].getAttribute('data-id');
        axiosApiCallPage(1, themeId, stId);
    }

    const searchQuestion = (e) => {
        axiosApiCallPage(1, themeId, null, (e.target.value != null) ? e.target.value : '');
    }

    const themeDs = () => {
        Axios.post(configURL.themeDropdown, {}).then(res => {
            if (res.data.success !== undefined && res.data.success) {
                setThemes(res.data.data);
            } else {
                setThemes([]);
            }
        })
    }

    useEffect(() => {
        axiosApiCallPage(1);
        themeDs();
    }, []);

    useEffect(() => {

    }, [rowData]);

    return (
        <React.Fragment>
            <div className="Page-Question">
                <div className="breadcrumb_ezi">
                    <Breadcrumb>
                        <Breadcrumb.Item href="#">PeopleDirectory</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="column-header">
                        <h1 className="page-heading">Directory</h1>
                    </div>
                </div>
                {/* --------------------------------------------------
                        - START
                        - employee , customer , other content tabs 
                ------------------------------------------------------ */}
                <div className="tablist_ezi">
                    <Tab.Container defaultActiveKey="employee">
                        <div className="tab-header-wrap">
                            <div className="tab-left-header">
                                <Nav variant="pills" >
                                    <Nav.Item>
                                        <Nav.Link eventKey="employee">My Question</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link eventKey="customer">Global Question</Nav.Link>
                                    </Nav.Item>
                                </Nav>
                            </div>
                        </div>
                        <Tab.Content>
                            <Tab.Pane eventKey="employee">
                                <React.Fragment>
                                    <div className="dt-table-search-wrap">
                                        <input type="text" name="search" className="dt-table-search" placeholder="Search" onChange={(e) => searchQuestion(e)} />
                                    </div>

                                    <div className="question-header">
                                        <label className="ezi-checkbox">Statement
                                            <input type="checkbox" />
                                            <span className="ezi-checkbox-mark"></span>
                                        </label>
                                        <select className="question-select" placeholder="themes" onChange={(e) => changeTheme(e)}>
                                            <option data-id="">Select Theme</option>
                                            {
                                                themes.map((theme) =>
                                                    <option data-id={theme.id}>{theme.name}</option>
                                                )
                                            }
                                        </select>
                                        <select className="question-select" type="text" placeholder="sub-theme" onChange={(e) => changeSubtheme(e)}>
                                            <option>Select Sub Theme</option>
                                            {
                                                subthemes.map((subtheme) =>
                                                    <option data-id={subtheme.id}> {subtheme.name} </option>
                                                )
                                            }
                                        </select>
                                    </div>
                                    <ul className="list_rr">
                                        {
                                            rowData && rowData.map((questionObj) => (
                                                <li>
                                                    <label className="ezi-checkbox">
                                                        <input type="checkbox" />
                                                        <span className="ezi-checkbox-mark"></span>
                                                    </label>
                                                    <span className="list_ct">{questionObj.question}</span>
                                                </li>
                                            )
                                            )
                                        }
                                    </ul>
                                </React.Fragment>
                            </Tab.Pane>
                            <Tab.Pane eventKey="customer">
                                <React.Fragment>
                                    <div className="dt-table-search-wrap">
                                        <input type="text" className="dt-table-search" placeholder="Search" />
                                    </div>
                                </React.Fragment>
                            </Tab.Pane>
                        </Tab.Content>
                    </Tab.Container>
                </div> {/* Tabs End */}
            </div>
        </React.Fragment>
    )
}

export default Question;
