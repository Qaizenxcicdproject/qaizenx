import React, { useState } from 'react';
import { Spinner } from 'react-bootstrap';
import Axios from 'utility/Axios';
import configURL from 'config/config';
import { toast } from 'react-toastify';
import { useParams } from "react-router-dom";
import useForm from 'react-hook-form';
import FormData from 'utility/AppFormData';

const RawDataUpload = () => {
    const [loading, setLoading] = useState(false);
    const { register, handleSubmit, errors } = useForm()
    const urlParams = useParams()
	/**
	 * Upload Raw Data
	 * 
	 * @param {Object} data 
	 */
    const handleSubmitForm = async (data = {}) => {
        try {
            setLoading(true)
            let formData = new FormData()
            formData.append("survey_id", urlParams.survey_id)
            formData.append("file", data.file[0])
            let rawDataDownload = await Axios.post(configURL.raw_data_upload, formData)
            if (rawDataDownload.data.success === true) {
                toast.success(rawDataDownload.data.message || 'Data uploaded')
            } else {
                toast.warn(rawDataDownload.data.message || 'Something went wrong')
            }
            setLoading(false)
        } catch (error) {
            console.log(error);
            toast.warn('Something went wrong')
            setLoading(false)
        }
    }

    /**
	 * Download template
	 * 
	 * @param {Object} data 
	 */
    const handleDownloadTemplate = async () => {
        try {
            let formData = new FormData()
            formData.append("survey_id", urlParams.survey_id)
            let rawDataTemplate = await Axios.post(configURL.raw_data_download, formData)
            if (rawDataTemplate.data.success === true) {
                window.open(rawDataTemplate.data.url, "_blank")
            }
        } catch (error) {
            console.log(error);
            toast.warn('Something went wrong')
        }
    }
    return (
        <div className="raw_data_upload">
            <form onSubmit={handleSubmit(handleSubmitForm)}>
                <p className="">Upload Raw Data For Survey</p>
                <div className="raw_data_upload_fields">
                    <input type="file" name="file" ref={register({ required: true })} placeholder={'Raw data file'} />
                    {errors.file && <span className={`error-message`}>Please select file to upload</span>}
                </div>
                <div className="raw_data_upload_fields">
                    <button type="submit" disabled={loading}>
                        Upload {loading && <Spinner animation="border" />}
                    </button>
                    <button type="button" onClick={handleDownloadTemplate}>Download template</button>
                </div>
            </form>

        </div>
    )
}

export default RawDataUpload;