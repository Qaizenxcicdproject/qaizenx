import React, { useState, useEffect } from "react";
import { Spinner } from 'react-bootstrap';
import Axios from "utility/Axios";
import configURL from 'config/config';
import SelectSearch from "react-select-search";
import { useParams, useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import { confirmAlert } from 'react-confirm-alert';
import JsxParser from "html-react-parser"

const EmailSetting = ({ defaultData, step, updatedStep, handlePrev, selected_survey = null, patternDetails, questionsData, participantData }) => {
    const [saving, setSaving] = useState(false)
    const [activeTab, setActiveTab] = useState('invitation')
    const [templates, setTemplates] = useState([])
    const [invitationTemplate, setInvitationTemplate] = useState(defaultData.invitation_template || null)
    const [remainderTemplate, setRemainderTemplate] = useState(defaultData.remainder_template || null)
    const [remainderLists, setRemainderLists] = useState(defaultData.remainders || [])
    const { recurring_id = "" } = useParams();
    const history = useHistory();

    const handleSaveQuestions = () => {

        if (!invitationTemplate && activeTab === 'invitation') {
            toast.warn('Please select Invitation Template.')
            setSaving(false);
            return false;
        }
        if (remainderTemplate) {
            if (remainderLists.length <= 0 && activeTab === 'remainder') {
                toast.warn('Please add Remainder List.')
                setSaving(false);
                return false;
            }
            if (remainderLists.every((v, i) => parseInt(v) === 0) && activeTab === 'remainder') {
                toast.warn('Please select no. of days greater than 0 .')
                setSaving(false);
                return false;
            }
        }
        else if (activeTab === 'remainder') {
            toast.warn('Please select Remainder Template.')
            setSaving(false);
            return false;
        }
        confirmAlert({
            title: 'Save Recurring Details',
            message: JsxParser('Please verify all setting before saving.'),
            buttons: [
                {
                    label: 'Confirm',
                    onClick: () => {
                        let communication_setting = { invitation_template: invitationTemplate, remainder_template: remainderTemplate, remainders: remainderLists }
                        // ((((remainderTemplate && activeTab === "remainder") && (remainderLists.length > 0 && !remainderLists.every(v => parseInt(v) === parseInt(remainderLists[0]))))) || (invitationTemplate && activeTab === "invitation"))

                        let formData = new FormData()
                        if (recurring_id && recurring_id !== "") {
                            formData.append('recurring_id', recurring_id)
                        }
                        formData.append('communication_setting', JSON.stringify(communication_setting))
                        formData.append('status', 'active')
                        Axios.post(configURL.save_recurring_details, formData).then(res => {
                            setSaving(false)

                            if (res.data.success === true) {
                                toast.success(res.data.message || 'Recurring Details saved successfully.')
                                if (step <= 4) updatedStep(5)
                                history.push('/recurring-setting')
                            } else {
                                toast.warn(res.data.message || 'Unable to save recurring details.')
                            }
                        }).catch(err => {
                            setSaving(false)
                            console.log(err.toString());
                        })
                    }
                },
                {
                    label: 'Cancel'
                }
            ]
        });
    }

    const handleAddReminder = () => {
        let oldData = [...remainderLists]
        oldData.push(0)
        setRemainderLists(oldData)
    }
    const handleTimeChange = ({ value }, index) => {
        let oldData = [...remainderLists]
        oldData[index] = value
        setRemainderLists(oldData)
    }
    const handleRemoveReminder = (index) => {
        let oldData = [...remainderLists]
        oldData.splice(index, 1)
        setRemainderLists(oldData)
    }

    const addDays = (date, days) => {
        let daysAdd = parseInt(days)
        let actualDate = new Date(date);
        if (daysAdd === 0 || daysAdd === "") {
            actualDate.toDateString();
        }
        actualDate.setDate(actualDate.getDate() + parseInt(days));
        return actualDate.toDateString();
    }

    useEffect(() => {
        console.log(invitationTemplate);
        console.log(remainderTemplate);
        console.log(remainderLists);
        if (selected_survey && selected_survey !== "") {
            let formData = new FormData()
            formData.append('survey_id', selected_survey)
            formData.append('type', 'webEmail')
            Axios.post(configURL.get_recurring_communication_templates, formData).then(res => {
                if (res.data.success === true) {
                    let resTemplates = res.data.results.map(el => ({ name: el.name, value: el.id }))
                    setTemplates(resTemplates)
                }
            }).catch(err => {
                console.log(err.toString());
            })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [invitationTemplate, remainderTemplate, remainderLists])

    return (
        <React.Fragment>
            <div className="email-setting-wrapper">
                <div className="selection-tab">
                    <button type="button" className={`ezi-btn ${(activeTab === 'invitation') && 'active'} invitation`} onClick={() => setActiveTab('invitation')}>Invitation Setting</button>
                    <button type="button" className={`ezi-btn ${(activeTab === 'remainder') && 'active'} remainder`} onClick={() => setActiveTab('remainder')}> Reminder Setting</button>
                </div>
                <div className="selection-tab-content">
                    {(activeTab !== '') &&
                        <h4 className="setting-header email-header">
                            {(activeTab === 'invitation') ? 'Invitation Communication Template Setting' : 'Reminder Communication Template Setting'}
                        </h4>
                    }
                    {(activeTab === 'invitation') &&
                        <SelectSearch
                            options={templates}
                            search
                            value={invitationTemplate || null}
                            placeholder="Please choose Invitation template"
                            onChange={setInvitationTemplate}
                        />
                    }
                    {(activeTab === 'remainder') &&
                        <SelectSearch
                            options={templates}
                            search
                            value={remainderTemplate || null}
                            placeholder="Please choose Reminder template"
                            onChange={setRemainderTemplate}
                        />
                    }
                    <div className="schedule-details">
                        <div className="pattern-details">
                            <h4 className="setting-header email-header">Recurrence Pattern</h4>
                            <p>Pattern Type : {patternDetails.recurrence_type || '---'}</p>
                            <p>Start Date : {(patternDetails.recurrence_range_start_date) ? patternDetails.recurrence_range_start_date.toDateString() : '---'}</p>
                            <p>End Date : {(patternDetails.recurrence_range_end_date) ? patternDetails.recurrence_range_end_date.toDateString() : '---'}</p>
                        </div>
                        {(activeTab === 'remainder') &&
                            <div className="remainders-list">
                                <h4 className="setting-header email-header">Reminder(s) List</h4>
                                <span className="add-remainder" title="Add new Reminder" onClick={handleAddReminder}></span>
                                <ul className="remainders-list-item">
                                    {remainderLists.map((el, i) => (
                                        <li key={i}>
                                            After
                                            <input min={0} type="number" value={el} onChange={({ target }) => handleTimeChange(target, i)} />
                                            on
                                            <span> {addDays(patternDetails.recurrence_range_start_date, el)}</span>
                                            <span className="remove-item" title="Delete" onClick={() => handleRemoveReminder(i)}></span>
                                        </li>
                                    ))}
                                    {remainderLists.length === 0 && <p>No Reminder(s) Added</p>}
                                </ul>
                            </div>
                        }
                    </div>

                </div>
            </div>
            <div className="navigation-btn-wrapper">
                <button className="prev-btn ezi-btn" onClick={handlePrev}>
                    Previous
                </button>
                <button className="next-btn ezi-btn" onClick={handleSaveQuestions} disabled={saving}>
                    Save & Continue
                    {saving && <Spinner className="burgundy-spinner" animation="border" size="sm" />}
                </button>

            </div>
        </React.Fragment>
    )
}

export default EmailSetting;