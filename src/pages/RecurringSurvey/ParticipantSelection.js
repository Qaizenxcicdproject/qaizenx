import React, { useState, useEffect } from "react";
import { Spinner } from 'react-bootstrap';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";

const ParticipantSelection = ({ defaultData, step, updateData, updatedStep, handleNext, handlePrev, selected_survey = null }) => {
    const [saving, setSaving] = useState(false)
    const [selectionMode, setSelectionMode] = useState(defaultData.selection_mode || '')
    const [random_part, setRandomPart] = useState(defaultData.random_part || 0)
    const [totalCount, setTotalCount] = useState(0)
    const { recurring_id = "" } = useParams();

    const handleSaveQuestions = () => {
        let participantData = { selection_mode: selectionMode, random_part: random_part }
        let formData = new FormData()
        if (recurring_id && recurring_id !== "") {
            formData.append('recurring_id', recurring_id)
        }
        if (selectionMode !== "all" && selectionMode !== "random") {
            toast.warn('Please select Participants setting.')
            setSaving(false);
            return false;
        }
        if (selectionMode === "random") {
            if (random_part <= 0) {
                toast.warn('Please add No of Participants.')
                setSaving(false);
                return false;
            }
        }
        formData.append('participant_data', JSON.stringify(participantData))
        Axios.post(configURL.save_recurring_details, formData).then(res => {
            setSaving(false)
            if (res.data.success === true) {
                toast.success(res.data.message || 'Recurring participants saved successfully.')
                updateData(participantData)
                if (step <= 3) updatedStep(4)
                handleNext()
            } else {
                toast.warn(res.data.message || 'Unable to save recurring details.')
            }
        }).catch(err => {
            setSaving(false)
            console.log(err.toString());
        })
    }

    useEffect(() => {
        if (selected_survey && selected_survey !== "") {
            let formData = new FormData()
            formData.append('survey_id', selected_survey)
            Axios.post(configURL.survey_people_count, formData).then(res => {
                if (res.data.success === true) {
                    setTotalCount(res.data.total_count || 0)
                }
            }).catch(err => {
                console.log(err.toString());
            })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <React.Fragment>
            <div className="participant-selection-wrapper">
                <h4 className="setting-header header-pad5">Participants selection</h4>
                <div className="selection-mode-wrapper">
                    <label className="checkbox-label">
                        <input type="radio" name="selection_mode" value='all' className="qaizenx-radio-input" checked={(selectionMode === 'all')} onChange={({ target }) => setSelectionMode(target.value)} />
                        <span>All the participants</span>
                    </label>
                    <label className="checkbox-label">
                        <input type="radio" name="selection_mode" value='random' className="qaizenx-radio-input" checked={(selectionMode === 'random')} onChange={({ target }) => setSelectionMode(target.value)} />
                        <span>Random</span>
                    </label>
                    {selectionMode === 'all' && <h4 className="setting-header mode-all">All {totalCount} participants selected</h4>}
                    {selectionMode === 'random' &&
                        <div className="part-node-random">
                            <h4 className="setting-header part-node-random-txt">No of Participants: </h4>
                            <input type="number" onChange={({ target }) => {
                                if (parseInt(target.value) > 100) {
                                    setRandomPart(100)
                                }
                                else {
                                    setRandomPart(target.value)
                                }
                            }} min={0} max={100} value={random_part} />
                            <span> % Out of </span>
                            <span className="data-cl"> 100%</span>
                        </div>
                    }
                </div>
            </div>
            <div className="navigation-btn-wrapper">
                <button className="prev-btn ezi-btn" onClick={handlePrev}>
                    Previous
                </button>

                < button className="next-btn ezi-btn" onClick={handleSaveQuestions} disabled={saving}>
                    Save & Continue
                    {saving && <Spinner className="burgundy-spinner" animation="border" size="sm" />}
                </button>


            </div>
        </React.Fragment>
    )
}

export default ParticipantSelection;