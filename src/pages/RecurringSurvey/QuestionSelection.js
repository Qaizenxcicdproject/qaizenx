import React, { useState, useEffect } from "react";
import { Spinner } from 'react-bootstrap';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from "react-toastify";
import InnerLoader from "components/InnerLoader"
import { useParams } from "react-router-dom";

const QuestionSelection = ({ defaultData, updateData, updatedStep, step, handleNext, handlePrev, selected_survey = null }) => {
    const [surveyQuestions, setSurveyQuestions] = useState([])
    const [selectedQuestions, setSelectedQuestions] = useState(defaultData.question_ids || [])
    const [questionMode, setQuestionMode] = useState(defaultData.question_mode)
    const [randomNo, setRandomNo] = useState(defaultData.random_no)
    const [saving, setSaving] = useState(false)
    const [loading, setLoading] = useState(false)
    const { recurring_id = "" } = useParams();

    const handleSaveQuestions = () => {
        setSaving(true)
        let questionData = { question_mode: questionMode, question_ids: selectedQuestions, random_no: randomNo };
        let formData = new FormData()
        if (recurring_id && recurring_id !== "") {
            formData.append('recurring_id', recurring_id)
        }
        if (questionMode !== "mixed" && questionMode !== "all") {
            toast.warn('Please select Statement.')
            setSaving(false);
            return false;
        }
        if (questionMode === 'mixed') {
            if (selectedQuestions.length <= 0 && randomNo <= 0) {
                toast.warn('Please select Fixed/Random statement.')
                setSaving(false);
                return false;
            }
        }
        formData.append('questions_data', JSON.stringify(questionData))
        Axios.post(configURL.save_recurring_details, formData).then(res => {
            setSaving(false)
            if (res.data.success === true) {
                toast.success(res.data.message || 'Recurring questions saved successfully.')
                updateData(questionData)
                if (step <= 2) updatedStep(3)
                handleNext()
            } else {
                toast.warn(res.data.message || 'Unable to save recurring details.')
            }
        }).catch(err => {
            setSaving(false)
            console.log(err.toString());
        })
    }

    const updateQuestionMode = ({ target }) => {
        let selected_questions = []
        if (target.value === 'all') {
            selected_questions = surveyQuestions.map(el => el.id)
            setRandomNo(0)
        }
        if (target.value === 'mixed') {
            selected_questions = []
        }
        setSelectedQuestions([...new Set(selected_questions)])
        setQuestionMode(target.value)
    }

    const handleCheckQuestion = ({ target }) => {
        let isChecked = target.checked
        let oldSelected = [...new Set(selectedQuestions)]
        let newData = []
        if (isChecked) {
            newData = [...oldSelected, target.value]
        } else {
            newData = oldSelected.filter(el => el !== target.value)
        }
        setRandomNo(0)
        setSelectedQuestions(newData)
    }

    useEffect(() => {
        if (selected_survey && selected_survey !== "") {
            setLoading(true)
            let formData = new FormData()
            formData.append('survey_id', selected_survey)
            Axios.post(configURL.recurring_survey_questions, formData).then(res => {
                setLoading(false)
                if (res.data.success === true) {
                    setSurveyQuestions(res.data.results || [])
                } else {
                    toast.warn(res.data.message || 'Unable to get Questions.')
                }
            }).catch(err => {
                setLoading(false)
                console.log(err.toString());
            })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <React.Fragment>
            <div className="question-selection-wrapper">
                <h4 className="setting-header header-pad5">Statement selection:</h4>
                <div className="question-selection-mode">
                    <label className="checkbox-label">
                        <input
                            type="radio"
                            name="question_mode"
                            className="qaizenx-radio-input"
                            value="all"
                            checked={questionMode === 'all' ? true : false}
                            onChange={updateQuestionMode}
                        />
                        <span>All the statement</span>
                    </label>
                    <label className="checkbox-label">
                        <input
                            type="radio"
                            name="question_mode"
                            className="qaizenx-radio-input"
                            value="mixed"
                            checked={questionMode === 'mixed' ? true : false}
                            onChange={updateQuestionMode}
                        />
                        <span>Combination of Fixed and Random</span>
                    </label>
                </div>
                <div className="question-list-wrapper">
                    {(questionMode === 'all') && <h4 className="setting-header question-mode-header header-pad5">All the statement</h4>}
                    {(questionMode === 'mixed') && <h4 className="setting-header question-mode-header header-pad5">Combination of Fixed and Random</h4>}
                    {questionMode &&
                        <div className="list-data-wrap">
                            {
                                surveyQuestions.map((el, i) => (
                                    <label key={i + 1} className="qaizenx-checkbox-input">
                                        {el.title || el.name || '--'}
                                        <input
                                            type="checkbox"
                                            name="recurrence_type"
                                            value={el.id}
                                            checked={(selectedQuestions.includes(el.id))}
                                            disabled={(questionMode === 'all')}
                                            onChange={handleCheckQuestion}
                                        />
                                        <span className="checkmark"></span>
                                    </label>
                                ))
                            }
                            {surveyQuestions.length === 0 && <div className="no-result"><p>No questions found</p></div>}
                            {loading && <InnerLoader />}
                        </div>
                    }
                    {questionMode &&
                        <div className="question-lists-details">
                            <div className="statement-detail">
                                <span className="left-detail">No of Fixed Statement Selected : </span>
                                <span className="right-detail">{selectedQuestions.length || 0}</span>
                            </div>
                            {questionMode === 'mixed' && <div className="statement-detail">
                                <span className="left-detail">No of Random Statement : </span>
                                <input type="number" min={0} className="right-detail" value={randomNo} onChange={({ target }) => {
                                    if (parseInt(target.value) > (surveyQuestions.length - selectedQuestions.length)) {
                                        setRandomNo(surveyQuestions.length - selectedQuestions.length)
                                    } else {
                                        setRandomNo(target.value)
                                    }
                                }} />
                            </div>}
                            <div className="statement-detail">
                                <span className="left-detail">Total No of Statements : </span>
                                <span className="right-detail">{(selectedQuestions.length + parseInt(randomNo || 0)) || 0} </span><span > of </span> <span className="right-detail"> {surveyQuestions.length || 0}</span>
                            </div>
                        </div>
                    }

                </div>
            </div>
            <div className="navigation-btn-wrapper">
                <button className="prev-btn ezi-btn" onClick={handlePrev}>
                    Previous
                </button>
                {

                    <button className="next-btn ezi-btn" onClick={handleSaveQuestions} disabled={saving}>
                        Save & Continue
                        {saving && <Spinner className="burgundy-spinner" animation="border" size="sm" />}
                    </button>

                }

            </div>
        </React.Fragment>
    )
}

export default QuestionSelection;