import React, { useState, useEffect, useContext } from "react";
import Axios from "utility/Axios";
import configURL from 'config/config';
import { Breadcrumb, Tab, Nav } from "react-bootstrap";
import 'utility/i18next.js'
import './RecurringSurvey.scss';
import { toast } from "react-toastify";
import { useParams } from "react-router-dom";
import AppContext from 'store/AppContext';
import RecurrencePattern from "./RecurrencePattern"
import SurveySelection from "./SurveySelection"
import QuestionSelection from "./QuestionSelection"
import ParticipantSelection from "./ParticipantSelection"
import EmailSetting from "./EmailSetting"

const RecurrenceDetails = ({ history }) => {
    const { EziLoader } = useContext(AppContext)
    const [recurringDetails, setRecurringDetails] = useState({})
    const [patternDetails, setPatternDetails] = useState({})
    const [selectedSurvey, setSelectedSurvey] = useState(null)
    const [questionsData, setQuestionsData] = useState({ question_mode: '', question_ids: [], random_no: 0 })
    const [participantData, setParticipantData] = useState({ selection_mode: '', random_part: 0 })
    const [communicationSetting, setCommunicationSetting] = useState({ invitation_template: null, remainder_template: null, remainders: [] })
    const [activeTab, setActivetab] = useState('pattern')
    const { recurring_id = "" } = useParams();
    const [steps, setSteps] = useState(0);

    useEffect(() => {
        if (recurring_id && recurring_id !== "") {
            EziLoader.show()
            let formData = new FormData()
            formData.append('recurring_id', recurring_id)
            Axios.post(configURL.get_recurring_details, formData).then(res => {
                EziLoader.hide()
                if (res.data.success === true) {
                    let { recurring_name = null,
                        recurring_description = null,
                        pattern_details = {},
                        survey_id = null,
                        questions_data = {},
                        participant_data = {},
                        communication_setting = {}
                    } = res.data.result
                    let step = 0;
                    setRecurringDetails({ recurring_name, recurring_description })
                    if (pattern_details?.recurrence_range_start_date) {
                        pattern_details.recurrence_range_start_date = new Date(pattern_details.recurrence_range_start_date)
                    }
                    if (pattern_details?.recurrence_range_end_date) {
                        pattern_details.recurrence_range_end_date = new Date(pattern_details.recurrence_range_end_date)
                    }
                    setPatternDetails(pattern_details || {})
                    setSelectedSurvey(survey_id || null)
                    setQuestionsData(questions_data || {})
                    setParticipantData(participant_data || {})
                    setCommunicationSetting(communication_setting || {})
                    if (Object.keys(pattern_details).length > 0) step = 1;
                    if (survey_id) step = 2;
                    if (questions_data.question_mode && questions_data.question_ids && questions_data.question_mode !== "" && questions_data.question_ids.length > 0) step = 3;
                    if (communication_setting) step = 4;
                    setSteps(step);
                } else {
                    toast.warn(res.data.message || 'Unable to get recurring details.')
                }
            }).catch(err => {
                EziLoader.hide()
                console.log(err.toString());
            })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <React.Fragment>
            <section className="Page-RecurrenceDetails">
                <div className="breadcrumb_ezi save-workflow-beadcrumb">
                    <Breadcrumb>
                        <Breadcrumb.Item onClick={() => history.push('/recurring-setting')}>Recurrings</Breadcrumb.Item>
                        <Breadcrumb.Item>Recurrence Details</Breadcrumb.Item>
                    </Breadcrumb>
                </div>
                <div className="recurrence-wrapper">
                    <h3 className="header-details">Recurring Flow Setting for {recurringDetails.recurring_name || '---'}</h3>
                    <div className="recurrence-setting-wrapper">
                        <Tab.Container activeKey={activeTab} onSelect={setActivetab}>
                            <div className="side-tab">
                                <Nav variant="pills" className="side-tab-nav" >
                                    <Nav.Item>
                                        <Nav.Link eventKey="pattern">Recurrence Pattern</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link eventKey="survey_selection" disabled={steps > 0 ? false : true}>Survey Selection</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link eventKey="question_selection" /*disabled={!selectedSurvey}*/ disabled={steps > 1 ? false : true}>Specify Questions/Statements</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link
                                            eventKey="participant_selection"
                                            // disabled={((questionsData.question_mode && questionsData.question_ids && questionsData.question_mode !== "" && questionsData.question_ids.length > 0)) ? false : true}
                                            disabled={steps > 2 ? false : true}
                                        >Participants Selection</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link eventKey="email_setting" disabled={steps > 3 ? false : true}>Email Settings</Nav.Link>
                                    </Nav.Item>
                                </Nav>
                            </div>
                            <Tab.Content>
                                <Tab.Pane eventKey="pattern" mountOnEnter unmountOnExit>
                                    <div className="tab-content-item">
                                        <RecurrencePattern defaultData={patternDetails} updateData={setPatternDetails} step={steps} updatedStep={setSteps} handleNext={() => setActivetab('survey_selection')} />
                                    </div>
                                </Tab.Pane>
                                <Tab.Pane eventKey="survey_selection" mountOnEnter unmountOnExit>
                                    <div className="tab-content-item">
                                        <SurveySelection defaultData={selectedSurvey} updateData={setSelectedSurvey} step={steps} updatedStep={setSteps} handleNext={() => setActivetab('question_selection')} handlePrev={() => setActivetab('pattern')} />
                                    </div>
                                </Tab.Pane>
                                <Tab.Pane eventKey="question_selection" mountOnEnter unmountOnExit>
                                    <div className="tab-content-item">
                                        <QuestionSelection defaultData={questionsData} updateData={setQuestionsData} step={steps} updatedStep={setSteps} handleNext={() => setActivetab('participant_selection')} handlePrev={() => setActivetab('survey_selection')} selected_survey={selectedSurvey || null} />
                                    </div>
                                </Tab.Pane>
                                <Tab.Pane eventKey="participant_selection" mountOnEnter unmountOnExit>
                                    <div className="tab-content-item">
                                        <ParticipantSelection defaultData={participantData} updateData={setParticipantData} step={steps} updatedStep={setSteps} handleNext={() => setActivetab('email_setting')} handlePrev={() => setActivetab('question_selection')} selected_survey={selectedSurvey || null} />
                                    </div>
                                </Tab.Pane>
                                <Tab.Pane eventKey="email_setting" mountOnEnter unmountOnExit>
                                    <div className="tab-content-item">
                                        <EmailSetting defaultData={communicationSetting} updateData={setCommunicationSetting} step={steps} updatedStep={setSteps} handlePrev={() => setActivetab('participant_selection')} selected_survey={selectedSurvey || null} patternDetails={patternDetails} questionsData={questionsData || null} participantData={participantData} />
                                    </div>
                                </Tab.Pane>
                            </Tab.Content>
                        </Tab.Container>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default RecurrenceDetails;