import React, { useEffect, useState } from "react";
import { RECURENCE_TIMEFRAMES, RECURENCE_WEEK_DAYS, RECURENCE_MONTHS } from 'constants/constants';
import 'react-day-picker/lib/style.css';
import { Spinner } from 'react-bootstrap';
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { confirmAlert } from 'react-confirm-alert';
import Axios from "utility/Axios";
import DateTimePicker from 'react-datetime-picker';
import configURL from 'config/config';
import {
    countOccuranceBetweenDates,
    getDateAfterOccurance,
    getCountBetweenMonths,
    getLastDateAfter,
    getOccouranceCountBetweenYears,
    getCountAfterYears
} from "utility/DateHelpers";
import TimePicker from 'react-time-picker';

const RecurrencePattern = ({ defaultData, updateData, step, updatedStep, handleNext }) => {
    const [saving, setSaving] = useState(false)
    const { recurring_id = "" } = useParams();
    const [enableRecurringSection, setEnableRecurringSection] = useState(defaultData?.recurrence_type && (parseInt(defaultData.recurrence_week_day) >= 0 || (parseInt(defaultData.month_date) >= 0 && parseInt(defaultData.every_month_count) >= 0) || (parseInt(defaultData.year_on_month) >= 0 && parseInt(defaultData.year_month_date) >= 0)) ? true : false);
    let todayDate = new Date();
    const [monthDate, setMonthDate] = useState(defaultData.month_date || '');
    const [everyMonthCount, setEveryMonthCount] = useState(defaultData.every_month_count || '');
    const [yearMonthDate, setYearMonthDate] = useState(defaultData.year_month_date || '');
    const [yearOccurenceCount, setYearOccurenceCount] = useState(defaultData.year_occurence_count || '');
    // const [isDateDisabled, setIsDateDisabled] = useState(true);
    const handlePatternChange = ({ target }) => {

        if (target.name === "month_date") {
            if (parseInt(target.value) > 31) {
                setMonthDate(31)
                target.value = 31;
            } else if (parseInt(target.value) === 0) {
                setMonthDate(1)
                target.value = 1;
            } else {
                setMonthDate(target.value);
            }

        }
        if (target.name === "every_month_count") {
            if (parseInt(target.value) > 12) {
                setEveryMonthCount(12)
                target.value = 12;
            }
            else if (parseInt(target.value) === 0) {
                setEveryMonthCount(1)
                target.value = 1;
            } else {
                setEveryMonthCount(target.value);
            }
        }
        if (target.name === "year_month_date") {
            if (parseInt(target.value) > 31) {
                setYearMonthDate(31)
                target.value = 31;
            } else if (parseInt(target.value) === 0) {
                setYearMonthDate(1)
                target.value = 1;
            } else {
                setYearMonthDate(target.value);
            }
        }
        if (target.name === "year_occurence_count") {
            if (parseInt(target.value) <= 0) {
                setYearOccurenceCount(1)
                target.value = 1;
            } else if (parseInt(target.value) > 5) {
                setYearOccurenceCount(5)
                target.value = 5;
            }
            else {
                setYearOccurenceCount(target.value);
            }
        }

        let newData = { ...defaultData, [target.name]: target.value }
        if (target.name === 'recurrence_type') {
            newData = { recurrence_type: target.value }
        }
        if (newData?.recurrence_type && (parseInt(newData.recurrence_week_day) >= 0 || (parseInt(newData.month_date) >= 0 && parseInt(newData.every_month_count) >= 0) || (parseInt(newData.year_on_month) >= 0 && parseInt(newData.year_month_date) >= 0 && parseInt(newData.year_occurence_count >= 0)))) {
            setEnableRecurringSection(true);
        }
        else {
            setEnableRecurringSection(false);
        }

        newData.recurrence_range_start_date = newData.recurrence_range_start_date || todayDate;
        if (newData?.recurrence_type && (newData?.recurrence_range_start_date)) {
            switch (defaultData.recurrence_type) {
                case 'weekly':
                    if (newData?.recurrence_range_end === 'end_date' && newData?.recurrence_range_end_date && newData?.recurrence_week_day) {
                        newData.recurrence_range_occurrence_count = countOccuranceBetweenDates(
                            newData.recurrence_range_start_date.toString(),
                            newData.recurrence_range_end_date.toString(),
                            parseInt(newData.recurrence_week_day)
                        )
                    }
                    if (newData?.recurrence_range_end === 'end_after_recurrence' && newData?.recurrence_range_occurrence_count !== "") {
                        newData.recurrence_range_end_date = getDateAfterOccurance(parseInt(newData.recurrence_range_occurrence_count), parseInt(newData.recurrence_week_day), newData.recurrence_range_start_date.toString())

                    }

                    break;
                case 'monthly':
                    if (newData?.recurrence_range_end === 'end_date' && newData?.recurrence_range_end_date && newData?.month_date && newData?.every_month_count) {
                        newData.recurrence_range_occurrence_count = getCountBetweenMonths(
                            newData.recurrence_range_start_date.toString(),
                            newData.recurrence_range_end_date.toString(),
                            parseInt(newData.every_month_count),
                            parseInt(newData.month_date),
                        )
                    }
                    if (newData?.recurrence_range_end === 'end_after_recurrence' && newData?.recurrence_range_occurrence_count !== "" && newData?.month_date && newData?.every_month_count) {
                        newData.recurrence_range_end_date = getLastDateAfter(newData.recurrence_range_start_date.toString(), parseInt(newData.every_month_count), parseInt(newData.recurrence_range_occurrence_count), parseInt(newData.month_date))
                    }
                    break;
                case 'yearly':
                    if (newData?.recurrence_range_end === 'end_date' && newData?.recurrence_range_end_date && newData?.year_occurence_count && newData?.year_on_month && newData?.year_month_date) {

                        newData.recurrence_range_occurrence_count = getOccouranceCountBetweenYears(
                            newData.recurrence_range_start_date.toString(),
                            newData.recurrence_range_end_date.toString(),
                            parseInt(newData.year_occurence_count),
                            parseInt(newData.year_on_month),
                            parseInt(newData.year_month_date)
                        )
                    }
                    if (newData?.recurrence_range_end === 'end_after_recurrence' && newData?.recurrence_range_occurrence_count !== "" && newData?.year_on_month && newData?.year_occurence_count) {
                        newData.recurrence_range_end_date = getCountAfterYears(newData.recurrence_range_start_date.toString(), parseInt(newData.year_occurence_count), parseInt(newData.year_on_month), parseInt(newData.year_month_date), newData?.recurrence_range_occurrence_count)
                    }
                    break;

                default:
                    break;
            }
        }
        updateData(newData)
    }

    const handleSavePattertn = (data) => {
        setSaving(true)
        if (defaultData.recurrence_range_start_date.getTime() > defaultData.recurrence_range_end_date.getTime()) {
            confirmAlert({
                title: 'Date Error!',
                message: 'Start date could not be greater than End Date..',
                buttons: [{ label: 'I Understood' }]
            });
            setSaving(false)
            return;
        }
        if (defaultData.recurrence_range_end_date.getTime() < new Date().getTime() && new Date(new Date(defaultData.recurrence_range_end_date).toDateString()).getTime() !== new Date(new Date().toDateString()).getTime() ) {
            confirmAlert({
                title: 'Date Error!',
                message: "End date could not be less than Today's Date..",
                buttons: [{ label: 'I Understood' }]
            });
            setSaving(false)
            return;
        }
        let formData = new FormData()
        if (recurring_id && recurring_id !== "") {
            formData.append('recurring_id', recurring_id)
        }
        if (Object.keys(defaultData).length <= 0 || defaultData?.recurrence_range_end === undefined) {
            toast.warn('Please select End Date.')
            setSaving(false);
            return false;
        }
        if (defaultData.recurrence_range_occurrence_count <= 0 || defaultData.recurrence_range_occurrence_count === undefined) {
            toast.warn('Occurrences must be greater than 0.')
            setSaving(false);
            return false;

        }
        if (Object.keys(defaultData).length <= 0 || defaultData?.schedule_time === undefined || defaultData?.schedule_time === null) {
            toast.warn('Please select schedule time.')
            setSaving(false);
            return false;
        }
        formData.append('pattern_details', JSON.stringify(defaultData))
        Axios.post(configURL.save_recurring_details, formData).then(res => {
            setSaving(false)
            if (res.data.success === true) {
                toast.success(res.data.message || 'Recurring saved successfully.')
                if (step <= 0) updatedStep(1)
                handleNext()
            } else {
                toast.warn(res.data.message || 'Unable to save recurring details.')
            }
        }).catch(err => {
            setSaving(false)
            console.log(err.toString());
        })
    }

    // if(new Date(new Date(defaultData.recurrence_range_start_date).toDateString()).getTime() < new Date(new Date().toDateString()).getTime()){
    //     setIsDateDisabled(false);
    // }
    // if( new Date(new Date(defaultData.recurrence_range_start_date).toDateString()).getTime() > new Date(date.toDateString()).getTime() || new Date(new Date(defaultData.recurrence_range_start_date).toDateString()).getTime() === new Date(date.toDateString()).getTime()){
    //     setIsDateDisabled(true);
    // }

    useEffect(() => {
        setEnableRecurringSection(defaultData?.recurrence_type && (parseInt(defaultData.recurrence_week_day) >= 0 || (parseInt(defaultData.month_date) >= 0 && parseInt(defaultData.every_month_count) >= 0) || (parseInt(defaultData.year_on_month) >= 0 && parseInt(defaultData.year_month_date) >= 0 && parseInt(defaultData.year_occurence_count))) ? true : false)
        setYearMonthDate(defaultData.year_month_date);
        setEveryMonthCount(defaultData.every_month_count);
        setMonthDate(defaultData.month_date);
        setYearOccurenceCount(defaultData.year_occurence_count);
    }, [defaultData])

    return (
        <React.Fragment>
            <div className="recurrence-pattern">
                <div className="recurrence-pattern-types">
                    {
                        RECURENCE_TIMEFRAMES.map((el, i) => (
                            <label key={i + 1} className="checkbox-label">
                                <input
                                    type="radio"
                                    name="recurrence_type"
                                    className="qaizenx-radio-input"
                                    value={el.toLowerCase()}
                                    onChange={handlePatternChange}
                                    checked={(defaultData.recurrence_type && defaultData.recurrence_type === el.toLowerCase()) ? true : false}
                                />
                                <span>{el}</span>
                            </label>
                        ))
                    }

                </div>
                <div className="recurrence-pattern-type-setting">
                    {(defaultData && defaultData.recurrence_type === 'weekly') &&
                        <div className="weekly">
                            <div className="weekly-checkboxes">
                                <h4 className="setting-header header-pad5">Every week on : </h4>
                                {
                                    RECURENCE_WEEK_DAYS.map((el, i) => (
                                        <label className="qaizenx-checkbox-input" key={i + 1}>
                                            {el}
                                            <input type="checkbox"
                                                name="recurrence_week_day"
                                                value={i}
                                                onChange={handlePatternChange}
                                                checked={(defaultData.recurrence_week_day && parseInt(defaultData.recurrence_week_day) === i) ? true : false}
                                            />
                                            <span className="checkmark"></span>
                                        </label>
                                    ))
                                }
                            </div>
                        </div>
                    }
                    {(defaultData && defaultData.recurrence_type === 'monthly') &&
                        <div className="monthly">
                            <h4 className="setting-header header-pad5">The</h4>
                            <input type="number" min="1" max="31" className="monthly-time-input" value={monthDate} name="month_date" onChange={handlePatternChange} />
                            <sup className="monthly-time-text add-left">th </sup>
                            <span className="monthly-time-text">of every</span>
                            <input type="number" min="1" max="12" className="monthly-time-input" value={everyMonthCount} name="every_month_count" onChange={handlePatternChange} />
                            <span className="monthly-time-text">month(s)</span>
                        </div>
                    }
                    {(defaultData && defaultData.recurrence_type === 'yearly') &&
                        <div className="yearly">
                            <h4 className="setting-header header-pad5">Every</h4>
                            <input type="number" min="1" max="5" className="yearly-time-input" value={yearOccurenceCount} name="year_occurence_count" onChange={handlePatternChange} />
                            <h4 className="setting-header header-pad5">Year(s)</h4>
                            <h4 className="setting-header header-pad5">in</h4>
                            <select className="yearly-select" value={defaultData?.year_on_month || ''} name="year_on_month" onChange={handlePatternChange}>
                                <option value="">Choose</option>
                                {RECURENCE_MONTHS.map((el, i) => <option key={i} value={i}>{el}</option>)}
                            </select>
                            <h4 className="setting-header header-pad5">on</h4>
                            <input type="number" min="1" max="31" className="yearly-time-input" value={yearMonthDate} name="year_month_date" onChange={handlePatternChange} />
                            <sup className="monthly-time-text add-left">th </sup>
                        </div>
                    }
                </div>
            </div>
            {(defaultData?.recurrence_type && enableRecurringSection) &&
                <div className="recurrence-range">
                    <h4 className="setting-header header-pad5">Range of Recurrence</h4>
                    <div className="recurrence-range-types">
                        <label className="checkbox-label arrow-icon">
                            <span>Start by : </span>
                        </label>
                        <DateTimePicker
                            onChange={(date) => {
                                handlePatternChange({ target: { name: 'recurrence_range_start_date', value: date } })
                            }}
                            className="ezi-datepicker-custom"
                            value={(defaultData.recurrence_range_start_date) ? new Date(defaultData.recurrence_range_start_date) : new Date()}
                            clearIcon={null}
                            disableClock={true}
                            format={'y-MM-dd'}
                            dayPlaceholder="dd"
                            monthPlaceholder="MM"
                            yearPlaceholder="Y"
                            minDate={ new Date(new Date(defaultData.recurrence_range_start_date).toDateString()).getTime() < new Date(new Date().toDateString()).getTime() ? false : new Date() }
                            disabled={ new Date(new Date(defaultData.recurrence_range_start_date).toDateString()).getTime() < new Date(new Date().toDateString()).getTime() ? true : false }
                        />
                    </div>
                    <div className="recurrence-range-types">
                        <label className="checkbox-label">
                            <input
                                type="radio"
                                name="recurrence_range_end"
                                className="qaizenx-radio-input"
                                value="end_date"
                                onChange={handlePatternChange}
                                checked={(defaultData.recurrence_range_end && defaultData.recurrence_range_end === 'end_date') ? true : false}
                            />
                            <span>End by : </span>
                        </label>

                        <DateTimePicker
                            onChange={(date) => {
                                handlePatternChange({ target: { name: 'recurrence_range_end_date', value: date } })
                            }}
                            className="ezi-datepicker-custom"
                            value={(defaultData.recurrence_range_end_date) ? new Date(defaultData.recurrence_range_end_date) : null}
                            clearIcon={null}
                            disabled={!(defaultData.recurrence_range_end && defaultData.recurrence_range_end === 'end_date')}
                            disableClock={true}
                            format={'y-MM-dd'}
                            dayPlaceholder="dd"
                            monthPlaceholder="MM"
                            yearPlaceholder="Y"
                            minDate={(defaultData.recurrence_range_start_date) ? new Date(defaultData.recurrence_range_start_date) : new Date()}
                        />
                        <span className="note">Note : Recurring communication will happen on last date of month if days count of month less than specified date of occurrence. </span>
                    </div>
                    <div className="recurrence-range-types">
                        <label className="checkbox-label">
                            <input type="radio" name="recurrence_range_end" className="qaizenx-radio-input" value="end_after_recurrence" onChange={handlePatternChange} checked={(defaultData.recurrence_range_end && defaultData.recurrence_range_end === 'end_after_recurrence') ? true : false} />
                            <span>End after : </span>
                        </label >
                        <input type="number" min={0} className="recurrence-count" onChange={handlePatternChange} name="recurrence_range_occurrence_count" value={defaultData?.recurrence_range_occurrence_count || ''} disabled={!(defaultData.recurrence_range_end && defaultData.recurrence_range_end === 'end_after_recurrence')} />
                        <span> Occurrences </span>
                    </div>
                    <div className="recurrence-range-types">
                        <label className="checkbox-label arrow-icon">
                            <span>Schedule Time</span>
                        </label>
                        <TimePicker
                            disableClock={true}
                            format="h:m: a"
                            closeClock={true}
                            onChange={(data) => {
                                handlePatternChange({ target: { name: 'schedule_time', value: data } })
                            }}
                            value={(defaultData && defaultData.schedule_time) ? defaultData.schedule_time : null}
                        />
                    </div>
                </div>
            }
            {
                enableRecurringSection && <div className="navigation-btn-wrapper">

                    <button className="next-btn ezi-btn" onClick={handleSavePattertn} disabled={saving}>
                        Save & Continue
                        {saving && <Spinner className="burgundy-spinner" animation="border" size="sm" />}
                    </button>


                </div>
            }

        </React.Fragment>
    )
}

export default RecurrencePattern;