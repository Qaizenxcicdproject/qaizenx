import React, { useState, useEffect, useRef, useContext } from "react";
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { CircularProgressbar } from 'react-circular-progressbar';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { getFirstWord } from 'utility/helper';
import 'utility/i18next.js'
import './RecurringSurvey.scss';
import { toast } from "react-toastify";
import SweetSearch from "components/SweetSearch";
import Pagination from "react-js-pagination";
import { confirmAlert } from 'react-confirm-alert';
import AppContext from 'store/AppContext';
import FormData from 'utility/AppFormData';

const RecurringSurvey = ({ history }) => {
    const { EziLoader } = useContext(AppContext)
    const [surveyRecurrings, setSurveyRecurrings] = useState([]);
    const [pagination, setPagination] = useState({});
    const [searchLoading, setSearchLoading] = useState(false);
    const [statusFilter, setStatusFilter] = useState('all');
    const [dashboardCounts, setDashboardCounts] = useState({
        active: 0,
        inactive: 0,
        closed: 0,
        total: 0
    });
    const perPage = 10;
    var searchTimer = null;
    const inputSearch = useRef(null);
    const currentPage = useRef(1);

    /**
     * Get Survey Workflows
     */
    const getSurveyRecurrings = (type = statusFilter) => {
        let searchVal = inputSearch.current.value;
        let formData = new FormData();
        formData.append("per_page", perPage);
        formData.append("page", currentPage.current);
        formData.append("status", type);
        if (searchVal !== "")
            formData.append("search", searchVal);
        Axios.post(configURL.get_recurrings, formData).then(response => {
            EziLoader.hide()
            setSearchLoading(false)
            if (response.data.success !== undefined && response.data.success === true) {
                setSurveyRecurrings(response.data.results);
                setPagination(response.data.pagination);
            } else {
                toast.warn(response.data.message);
            }
        }).catch(err => {
            console.log(err)
            EziLoader.hide()
        })
    }

    /**
     * Get Survey Workflows Stats
     */
    const getSurveyWorkflowsStats = () => {
        Axios.post(configURL.recurring_dash_counts, {}).then(response => {
            if (response.data.success !== undefined && response.data.success === true) {
                setDashboardCounts(response.data.results);
            }
        }).catch(err => {
            console.log(err)
        })
    }

    /**
     * Delete Workflow
     * @param {number} recurring_id Unique ID
     */
    const deleteRecurring = (recurring_id = "") => {
        confirmAlert({
            title: 'Delete Recurring',
            message: 'Are you sure you want to delete ?',
            buttons: [
                {
                    label: 'Confirm',
                    onClick: () => {
                        let formData = new FormData();
                        formData.append("recurring_id", recurring_id);
                        Axios.post(configURL.delete_recurring, formData).then(response => {
                            if (response.data.success !== undefined && response.data.success === true) {
                                toast.success(response.data.message || 'Recurring deleted successfully.')
                                if (surveyRecurrings.length === 1)
                                    currentPage.current = (currentPage.current > 1) ? currentPage.current - 1 : 1
                                getSurveyRecurrings()
                            } else {
                                toast.warn(response.data.message || 'Something went wrong here.');
                            }
                        }).catch(err => {
                            console.log(err)
                        })
                    }
                },
                {
                    label: 'Cancel'
                }
            ]
        });
    }

    /**
     * Filter Data based on search.
     * 
     * @param {string} type Input value
     */
    const handleFilterSearch = () => {
        clearTimeout(searchTimer);
        searchTimer = setTimeout(() => {
            setSearchLoading(true)
            currentPage.current = 1
            getSurveyRecurrings();
        }, 800);
    }

    /**
     * Filter Data based on status.
     * 
     * @param {string} type Filter value
     */
    const handleFilterByStatus = (type = 'all') => {
        setStatusFilter(type)
        EziLoader.show()
        getSurveyRecurrings(type)
    }

    /**
     * Handle Pagination
     * 
     * @param {string} type Filter Type
     */
    const handlePagination = (page = 1) => {
        EziLoader.show()
        currentPage.current = page
        getSurveyRecurrings();
    }

    /**
     * Handle Workflow Status Change
     *
     * @param {string} status Status
     */
    const handleStatusChange = ({ status = 'inactive', id = null, communication_setting = null }) => {
        if (status === 'closed') {
            toast.info('Recurring is already closed.')
            return false
        }
        if (!communication_setting) {
            toast.info('Please complete all steps.')
            return false
        }
        let newStatus = (status === 'active') ? 'inactive' : 'active'
        confirmAlert({
            title: 'Recurring Update',
            message: `Are you sure you want to change status from ${status} to ${newStatus}`,
            buttons: [
                {
                    label: 'Confirm',
                    onClick: () => {
                        let formData = new FormData()
                        formData.append("status", newStatus)
                        formData.append("recurring_id", id)
                        Axios.post(configURL.save_recurring_details, formData).then(response => {
                            if (response.data.success === true) {
                                getSurveyRecurrings()
                                toast.success(response.data.message || 'Recurring saved successfully.')
                            } else {
                                toast.warn(response.data.message || 'Something went wrong.')
                            }
                        }).catch(err => {
                            console.log(err);
                        })
                    }
                },
                {
                    label: 'Cancel'
                }
            ]
        });
    }

    useEffect(() => {
        EziLoader.show()
        getSurveyWorkflowsStats()
        getSurveyRecurrings()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <React.Fragment>
            <section className="Page-RecurringSurvey">
                <div className="recurring-dashboard-card-wrap">
                    <div className={`recurring-dashboard-card total`} onClick={() => handleFilterByStatus('all')} >
                        <div className="recurring-dashboard-card_text_wrap">
                            <p className="recurring-dashboard-card-heading">{dashboardCounts.total || 0}</p>
                            <span className="recurring-dashboard-card-subheading">Total Recurrings</span>
                        </div>
                        <div className="category_progress_ring_wrap">
                            <div className="category_progress_ring">
                                <CircularProgressbar className="category_progress_circle" value={Math.ceil((dashboardCounts.active / dashboardCounts.total) * 100) || 0} text={`${Math.ceil((dashboardCounts.active / dashboardCounts.total) * 100)}%`}
                                />
                            </div>
                            <span className="circular-survey-text active--text">Active Recurrings</span>
                        </div>
                    </div>
                    <div className={`recurring-dashboard-card active ${(statusFilter === 'active') ? 'active-status' : ''}`} onClick={() => handleFilterByStatus('active')}>
                        <div className="recurring-dashboard-card_text_wrap">
                            <p className="recurring-dashboard-card-heading">{dashboardCounts.active || 0}</p>
                            <span className="recurring-dashboard-card-subheading">Active Recurrings</span>
                        </div>
                    </div>
                    <div className={`recurring-dashboard-card inactive ${(statusFilter === 'inactive') ? 'active-status' : ''}`} onClick={() => handleFilterByStatus('inactive')}>
                        <div className="recurring-dashboard-card_text_wrap">
                            <p className="recurring-dashboard-card-heading">{dashboardCounts.inactive || 0}</p>
                            <span className="recurring-dashboard-card-subheading">Inactive Recurrings</span>
                        </div>
                    </div>
                    <div className={`recurring-dashboard-card closed ${(statusFilter === 'closed') ? 'active-status' : ''}`} onClick={() => handleFilterByStatus('closed')}>
                        <div className="recurring-dashboard-card_text_wrap">
                            <p className="recurring-dashboard-card-heading">{dashboardCounts.closed || 0}</p>
                            <span className="recurring-dashboard-card-subheading">Closed Recurrings</span>
                        </div>
                    </div>
                    <div className={`recurring-dashboard-card create-recurring--c`} onClick={() => history.push('/recurring-setting/save')} >
                        <span className="create-recurring--ic"></span>
                        <p className="create-recurring--name">Create new Recurrings</p>
                    </div>
                </div>
                <div className="filter-search-wrap">
                    <SweetSearch loading={searchLoading} change={handleFilterSearch} ref={inputSearch} />
                </div>
                <div className="recurring-dashboard-table">
                    <div className="recurring-dashboard-table-row recurring-table-heading">
                        <div className="recurring-dashboard-table-cell">Recurring</div>
                        <div className="recurring-dashboard-table-cell">Created By</div>
                        <div className="recurring-dashboard-table-cell">Status</div>
                        <div className="recurring-dashboard-table-cell">Action</div>
                    </div>
                    {
                        surveyRecurrings.length > 0 ? surveyRecurrings.map((item, index) => {
                            return (
                                <div key={index} className="recurring-dashboard-table-row">
                                    <div className="recurring-dashboard-table-cell" data-title="Recurring">
                                        <div className={`recurring-table-survey-wrap`} >
                                            <div className="recurring-table-survey-text-wrap">
                                                <span className="recurring-table-survey-name">{item.recurring_name || ''}</span>
                                                <span className="recurring-table-create">
                                                    Last Modified : {getFirstWord(item.updated_at)}   |   Created on : {getFirstWord(item.created_at)}
                                                </span>
                                                <span className="recurring-table-create">Description : {item.recurring_description}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="recurring-dashboard-table-cell" data-title="Owner"><span className="owner-name">{item.owner || "---"}</span></div>
                                    <div className={`recurring-dashboard-table-cell table-status`} data-title="Status" onClick={() => handleStatusChange(item)}>
                                        <span className={`dt-status-formatter ${item.status || 'inactive'}`}>{item.status || 'inactive'}</span>
                                    </div>

                                    <div className={`recurring-dashboard-table-cell`} data-title="Action">
                                        <div className="action-wrap">
                                            <OverlayTrigger overlay={<Tooltip>Edit Recurring</Tooltip>}>
                                                <button type="button" className="edit_data" onClick={() => {
                                                    history.push(`/recurring-setting/save/${item.id}`)
                                                }}></button>
                                            </OverlayTrigger>
                                            <OverlayTrigger overlay={<Tooltip>Delete Recurring</Tooltip>}>
                                                <button type="button" className="delete_data" onClick={() => deleteRecurring(item.id)}></button>
                                            </OverlayTrigger>
                                        </div>
                                    </div>
                                </div>
                            )
                        }) : null
                    }
                </div>
                {surveyRecurrings.length <= 0 && <div className="recurring-table-no-result">No result Found</div>}
                <div className="pagination-plugin-wrap category-pagination-formatter">
                    <Pagination
                        activePage={pagination.current_page}
                        itemsCountPerPage={perPage}
                        totalItemsCount={pagination.total || 0}
                        onChange={handlePagination}
                        hideDisabled={true}
                        firstPageText={<span class="prev-page-text-ic"></span>}
                        lastPageText={<span class="next-page-text-ic"></span>}
                        nextPageText={<span class="next-text-ic"></span>}
                        prevPageText={<span class="prev-text-ic"></span>}
                    />
                </div>
            </section>
        </React.Fragment>
    )
}

export default RecurringSurvey;