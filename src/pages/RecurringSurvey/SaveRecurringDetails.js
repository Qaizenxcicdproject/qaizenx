import React, { useState, useEffect, useContext } from "react";
import Axios from "utility/Axios";
import configURL from 'config/config';
import { Breadcrumb, Spinner } from "react-bootstrap";
import 'utility/i18next.js'
import './RecurringSurvey.scss';
import { toast } from "react-toastify";
import { useParams } from "react-router-dom";
import AppContext from 'store/AppContext';
import useForm from 'react-hook-form';

const SaveRecurringDetails = ({ history }) => {
    const { EziLoader } = useContext(AppContext)
    const [loading, setLoading] = useState(false)
    const [recurringDetails, setRecurringDetails] = useState({ recurring_name: "", recurring_description: "" })
    const { recurring_id = "" } = useParams();
    const { register, handleSubmit, errors } = useForm();

    /**
     * Save Recurring Details
     * @param {Object} data 
     */
    const saveRecurringSetting = (data) => {
        setLoading(true)
        let formData = new FormData()
        if (recurring_id && recurring_id !== "") {
            formData.append('recurring_id', recurring_id)
        }
        formData.append('recurring_name', data.recurring_name)
        formData.append('recurring_description', data.recurring_description)
        let oldId = recurring_id
        Axios.post(configURL.save_recurring_details, formData).then(res => {
            setLoading(false)
            if (res.data.success === true) {
                const { recurring_id = oldId } = res.data
                toast.success(res.data.message || 'Recurring Setting created successfully.')
                history.push(`/recurring-setting/save-recurrence/${recurring_id}`)
            } else {
                toast.warn(res.data.message || 'Unable to save recurring details.')
            }
        }).catch(err => {
            setLoading(false)
            console.log(err.toString());
        })
    }

    useEffect(() => {
        if (recurring_id && recurring_id !== "") {
            EziLoader.show()
            let formData = new FormData()
            formData.append('recurring_id', recurring_id)
            Axios.post(configURL.get_recurring_details, formData).then(res => {
                EziLoader.hide()
                if (res.data.success === true) {
                    const { recurring_name = "", recurring_description = "" } = res.data.result
                    setRecurringDetails({ recurring_name, recurring_description })
                } else {
                    toast.warn(res.data.message || 'Unable to get recurring details.')
                }
            }).catch(err => {
                EziLoader.hide()
                console.log(err.toString());
            })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <React.Fragment>
            <section className="Page-SaveRecurringDetails">
                <div className="breadcrumb_ezi save-workflow-beadcrumb">
                    <Breadcrumb>
                        <Breadcrumb.Item onClick={() => history.push('/recurring-setting')}>Recurrings</Breadcrumb.Item>
                        <Breadcrumb.Item>{recurring_id ? 'Update Details' : 'Create New'}</Breadcrumb.Item>
                    </Breadcrumb>
                </div>
                <div className="recurring-form-wrapper">
                    <form onSubmit={handleSubmit(saveRecurringSetting)}>
                        <div className="recurring-form-fields">
                            <h2 className="recurring-form-header">
                                {recurring_id ? 'Update Recurring Details' : 'Create Recurring Setting'}
                            </h2>
                            <div className="recurring-form-field">
                                <input type="text" className="recurring-form-field-input" name="recurring_name" id="recurring_name" placeholder="Recurring name" ref={register({ required: true })} defaultValue={recurringDetails.recurring_name} />
                                {errors.recurring_name && <span className="field-error">Please enter name</span>}
                            </div>
                            <div className="recurring-form-field">
                                <textarea className="recurring-form-field-input" name="recurring_description" id="recurring_description" placeholder="Recurring Description" maxLength="250" rows="4" ref={register({ required: true })} defaultValue={recurringDetails.recurring_description}></textarea>
                                {errors.recurring_description && <span className="field-error">Please enter description</span>}
                            </div>
                            <div className="recurring-form-field">
                                <button className="recurring-form-field-button" disabled={loading}>
                                    Save & Continue {loading && <Spinner className="burgundy-spinner" animation="border" size="sm" />}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </React.Fragment>
    )
}

export default SaveRecurringDetails;