import React, { useState, useContext, useEffect } from "react";
import { Spinner } from 'react-bootstrap';
import SelectSearch from "react-select-search";
import "../../assets/scss/SelectBox.scss"
import Axios from "utility/Axios";
import configURL from 'config/config';
import AppContext from 'store/AppContext';
import { toast } from "react-toastify";
import { useParams } from "react-router-dom";

const SurveySelection = ({ defaultData, updateData, updatedStep, step, handleNext, handlePrev }) => {
	const { EziLoader } = useContext(AppContext)
	const [activeSurvey, setActiveSurvey] = useState([])
	const [saving, setSaving] = useState(false)
	const { recurring_id = "" } = useParams();

	const handleSaveSurvey = () => {
		setSaving(true)
		let formData = new FormData()
		if (recurring_id && recurring_id !== "") {
			formData.append('recurring_id', recurring_id)
		}
		if (!defaultData) {
			toast.warn('Please select Survey.')
			setSaving(false);
			return false;
		}
		formData.append('survey_id', defaultData)
		Axios.post(configURL.save_recurring_details, formData).then(res => {
			setSaving(false)
			if (res.data.success === true) {
				toast.success(res.data.message || 'Recurring survey saved successfully.')
				if (step <= 1) updatedStep(2);
				handleNext()
			} else {
				toast.warn(res.data.message || 'Unable to save recurring details.')
			}
		}).catch(err => {
			setSaving(false)
			console.log(err.toString());
		})
	}

	useEffect(() => {
		EziLoader.show()
		Axios.post(configURL.get_recurring_surveys, {}).then(res => {
			EziLoader.hide()
			if (res.data.success === true) {
				let surveys = res.data.results.map(el => ({ name: el.name, value: el.id }))
				setActiveSurvey(surveys)
			} else {
				toast.warn(res.data.message || 'Unable to get surveys.')
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err.toString());
		})
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])

	return (
		<React.Fragment>
			<div className="survey-selection-wrapper">
				<SelectSearch
					options={activeSurvey}
					search
					value={defaultData || null}
					placeholder="Please choose survey"
					onChange={updateData}
				/>
				<div className="navigation-btn-wrapper">
					<button className="prev-btn ezi-btn" onClick={handlePrev}>
						Previous
					</button>
					<button className="next-btn ezi-btn" onClick={handleSaveSurvey} disabled={saving}>
						Save & Continue
						{saving && <Spinner className="burgundy-spinner" animation="border" size="sm" />}
					</button>
				</div>
			</div>

		</React.Fragment>
	)
}

export default SurveySelection;