import React, { useState, useEffect, useContext } from 'react'
import { Modal, Spinner, Tab, Nav } from 'react-bootstrap'
import "./ActionModal.scss";
import DatePicker from 'react-date-picker';
import SelectSearch from "react-select-search";
import useForm from 'react-hook-form';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';
import { useParams, useHistory } from "react-router-dom";
import AppContext from 'store/AppContext';
import RespondentResponse from './ShareComponent/RespondentResponse';
import Pagination from "react-js-pagination";
import ShowMoreText from 'react-show-more-text';

const ActionModal = ({ show, onHide, respondent_data, prevTaskData, respondantReport, actionType }) => {
	const { register, handleSubmit, errors, setValue, clearError } = useForm();
	const [saving, setSaving] = useState(false)
	const [options, setOptions] = useState([]);
	const urlParams = useParams();
	const [selectedValue, setSelectedValue] = useState(0);
	const appStore = JSON.parse(localStorage.getItem("appState"));
	const [reasons, setReasons] = useState([]);
	const { EziLoader } = useContext(AppContext);
	const [tabKey, setTabKey] = useState('action_plan_form');
	const [taskListData, settaskListData] = useState([]);
	const [currentPage, setCurrentPage] = useState(1);
	const [listingPagination, setListingPagination] = useState({});
	let timezone = appStore.user.timezone;
	let userName = appStore.user.name;
	let userEmail = appStore.user.email;
	let cellNumber = appStore.user.cell_number;
	let userID = appStore.user.id;
	var todayDate = new Date();
	let time = todayDate.getHours() + ":" + todayDate.getMinutes() + ":" + todayDate.getSeconds();
	let utcTime = new Date().getTimezoneOffset("en-US", { timeZone: timezone });
	let timeZoneString = utcTime.toString();
	//convert timezone into utc time difference
	function time_convert(num, sign) {
		var hours = Math.floor(num / 60);
		var minutes = num % 60;
		return sign + hours + ":" + minutes;
	}
	let UtcDiff = time_convert(timeZoneString.substring(1), timeZoneString[0]);
	const [reportDate, setReportDate] = useState({
		start_date: new Date(),
		due_date: null,
		time: time,
		utc: UtcDiff
	});
	const history = useHistory();
	const [showMore, setShowMore] = useState(false);


	const handleTaskCreate = data => {
		setSaving(true)
		let formData = new FormData()
		let startDate = new Date(reportDate.start_date.getTime() - (reportDate.start_date.getTimezoneOffset() * 60000)).toJSON();
		let dueDate = new Date(reportDate.due_date.getTime() - (reportDate.due_date.getTimezoneOffset() * 60000)).toJSON();
		let selectedUserName = options.filter((item) => {
			return item.value === selectedValue && item.name;
		})
		localStorage.getItem("lastname");
		formData.append("survey_id", urlParams.survey_id)
		if (respondent_data.respondent_id) formData.append("respondent_id", respondent_data.respondent_id)
		formData.append("respondent_fname", respondent_data.fname)
		formData.append("respondent_email", respondent_data.respondent_email)
		if (respondent_data.questionId) {
			formData.append("question_id", respondent_data.questionId)
		}
		else {
			formData.append("ques_comment_id", respondent_data.ques_comment_id);
		}
		formData.append("comment", respondent_data.comment)
		formData.append("name", `${respondent_data.fname}`)
		formData.append("task_type_id", 1)
		formData.append("assigned_to_user_id", selectedValue)
		formData.append("assigned_to_user_name", selectedUserName[0]?.name)
		formData.append("user_name", userName)
		formData.append("user_email", userEmail)
		formData.append("user_contact", cellNumber)
		formData.append("task_priority_level_id", 1)
		formData.append("start_date", startDate)
		formData.append("due_date", dueDate)
		formData.append("task_repeat_enum_id", 1)
		formData.append("task_status_id", 1)
		formData.append("description", data.description)
		formData.append("for_which", actionType)
		formData.append("profile_name", 'action_planning')

		Axios.post(configURL.create_action_task, formData).then(res => {
			setSaving(false)
			if (res.data.success === true) {
				toast.success(res.data.message || `Task Created Successfully`)
				onHide()
			} else {
				toast.warn(res.data.message || "Something went wrong.")
			}
		}).catch(err => {
			setSaving(false)
			console.log(err)
			toast.warn("Something went wrong.")
		})
	};

	const handleChange = value => {
		setSelectedValue(value);
	}

	const getDefaultOptions = () => {
		let formData = new FormData()
		formData.append("survey_id", urlParams.survey_id)
		formData.append("profile_name", 'action_planning')
		Axios.post(configURL.task_master_list, formData).then(res => {
			setOptions(res.data.result.task_assignee.map((user) => {
				return {
					name: user.name, value: user.id
				}
			}));
		}).catch(err => {
			console.log(err)
			toast.warn("Something went wrong.")
		})
	}
	/**
	 * Overall Reason listing
	 * @param {string} - Respondent ID
	 * @param {number} - olap survey response id
	 */
	const overallReasonListing = () => {

		if (respondent_data?.respondent_id && respondantReport === true) {
			EziLoader.show();
			let formData = new FormData();
			formData.append("survey_id", urlParams.survey_id)
			formData.append("respondent_id", respondent_data?.respondent_id);
			Axios.post(configURL.ReportIndividualResponse, formData).then(res => {
				EziLoader.hide();
				if (res.data.success !== undefined && res.data.success) {
					setReasons(res.data.result);
				} else {
					toast.warn(res.data.message || "Something went wrong.")
				}
			}).catch(err => {
				// EziLoader.hide()
				console.log(err)
			})
		}

	}

	/**
	 * Get All Tickets Data on page load
	 */
	const getActionPlanListing = () => {
		EziLoader.show();
		let formData = new FormData()
		if (respondent_data.questionId) formData.append("question_id", respondent_data.questionId)
		formData.append("created_by_user_id", userID)
		timezone && formData.append("timezone", timezone)
		urlParams.survey_id && formData.append("survey_id", urlParams.survey_id)
		formData.append("page", currentPage);
		Axios.post(configURL.action_plan_task_list, formData).then(res => {
			settaskListData(res?.data?.results);
			setListingPagination(res?.data?.pagination)
			EziLoader.hide();
		}).catch(err => {
			console.log(err)
			toast.warn("Something went wrong.")
		})
	}

	const handlePaginate = (pageNumber) => {
		setCurrentPage(pageNumber);
	}
	const historyOnclickhandle = (data) => {
		let survey_id = data.survey_id;
		let task_status_id = data.task_status_id;
		let task_id = data.id;
		let survey_name = data.name;
		if (survey_id && task_status_id) {
			localStorage.setItem('task_status_id', task_status_id);
			localStorage.setItem('task_id', task_id);
			history.push({
				pathname: "/action-plan/" + survey_id,
				state: { task_status_id: task_status_id, task_id: task_id, surveyName: survey_name }
			});
		}

	}

	useEffect(() => {
		register({ name: "task_assignee" }, { required: true });
		register({ name: "due_date" }, { required: true });
		getDefaultOptions();
		overallReasonListing();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])

	useEffect(() => {
		clearError('task_assignee')
		clearError('due_date')
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [show, onHide])

	useEffect(() => {
		getActionPlanListing();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [currentPage])


	return (
		<div className="Page-ActionModal">
			<Modal show={show} onHide={onHide} className="ezi-right-animated-modal actionModalTop">
				<Tab.Container activeKey={tabKey} onSelect={k => setTabKey(k)}>
					<Modal.Header closeButton>
						<div className="action-plan-inner-tab-header-wrap">
							<div className="tab-left-header">
								<Nav variant="pills" >
									<Nav.Item>
										<Nav.Link eventKey="action_plan_form">{!prevTaskData.is_completed ? 'Take Action' : 'Action Created'}</Nav.Link>
									</Nav.Item>
									{(respondent_data?.respondent_id && respondantReport === true) && <Nav.Item>
										<Nav.Link eventKey="action_plan_respondant_response">Respondent Report</Nav.Link>
									</Nav.Item>}
									{
										(actionType && actionType === "QUESTION") && <Nav.Item>
											<Nav.Link eventKey="action_plan_history">Previous Actions</Nav.Link>
										</Nav.Item>
									}

								</Nav>
							</div>
						</div>
					</Modal.Header>
					<Modal.Body>
						<Tab.Content className="action-plan-inner-tab-content">
							<Tab.Pane eventKey="action_plan_form" mountOnEnter>
								{/* {!prevTaskData.is_completed ? */}
								<form onSubmit={handleSubmit(handleTaskCreate)} className="add-theme-form">
									<div className="take-action-form">
										<div className="form-grp">
											<label className="form-heading">{actionType && actionType === "QUESTION" ? "Statement :" : "Participant Comment :"}</label>
											<div className="statement-textarea" name="statement">{respondent_data.comment}</div>
											{errors.statement && <span className="theme-error_cu">* Statement field is required.</span>}
										</div>

										<div className="form-grp">
											<label className="form-heading">Description :</label>
											<textarea ref={register({ required: true })} className="statement-textarea" rows="4" cols="50" name="description" />
											{errors.description && <span className="theme-error_cu">* Description field is required.</span>}
										</div>

										<div className="owner-action-wrapper">
											<div className="owner-action-section">
												<div className="form-grp">
													<label className="form-heading">Owner of Action Plan :</label>
													<SelectSearch
														options={options}
														search
														value={selectedValue}
														placeholder="Please choose task assignee"
														ref={register({ required: true })}
														emptyMessage="Not Found"
														onChange={(value) => {
															setValue('task_assignee', value)
															handleChange(value)
															clearError('task_assignee')
														}}
													/>
													{errors.task_assignee && <span className="theme-error_cu assigne-error">* Owner of Action Plan field is required.</span>}
												</div>

												<div className="form-grp date">
													<label className="form-heading">Start Date :</label>
													<DatePicker
														format={"dd/MM/yyyy"}
														selected={reportDate.start_date}
														value={reportDate.start_date}
														disabled={false}
														minDate={new Date()}
														onChange={(d) => setReportDate({ ...reportDate, start_date: d })}
														clearIcon={null}
														className="sweet-datepicker-custom start"
													/>
													<label className="form-heading">Due Date :</label>
													<DatePicker
														format={"dd/MM/yyyy"}
														selected={reportDate.due_date}
														value={reportDate.due_date}
														disabled={false}
														minDate={reportDate.start_date}
														clearIcon={null}
														className="sweet-datepicker-custom start"
														onChange={(value) => {
															setValue('due_date', value)
															setReportDate({ ...reportDate, due_date: value })
															clearError('due_date')
														}}
													/>
													{errors.due_date && <span className="theme-error_cu">* Due date field is required.</span>}
												</div>
											</div>
											{(respondent_data?.fname || respondent_data?.respondent_contact_number || respondent_data?.respondent_email) &&
												<div className="owner-details-section">
													<div className="form-grp">
														<label className="form-heading">Name :</label>
														<input type="text" className="name-input" value={respondent_data?.fname && respondent_data?.fname} disabled />
													</div>
													<div className="form-grp">
														<label className="form-heading">Phone :</label>
														<input type="phone" className="name-input" value={respondent_data?.respondent_contact_number && respondent_data?.respondent_contact_number} disabled />
													</div>
													<div className="form-grp">
														<label className="form-heading">Email :</label>
														<input type="email" className="name-input" value={respondent_data?.respondent_email && respondent_data?.respondent_email} disabled />
													</div>
												</div>
											}

										</div>



										<div className="submit-button">
											<button className="Submit-btn" type="submit" disabled={saving}> Submit {saving && <Spinner animation="border" size="sm" />}</button>
										</div>

									</div>
								</form>
								{/* :
                                    <div className="take-action-form">
                                        <div className="form-grp">
                                            <label className="submitted-form-heading">Participant Comment :</label>
                                            <span className="submitted-data">{prevTaskData?.data?.comment ? prevTaskData?.data?.comment : "---"}</span>
                                        </div>
                                        <div className="form-grp">
                                            <label className="submitted-form-heading">Assignee :</label>
                                            <span className="submitted-data">{prevTaskData?.data?.assigned_to_user_name ? prevTaskData?.data?.assigned_to_user_name : "---"}</span>
                                        </div>
                                        <div className="form-grp">
                                            <label className="submitted-form-heading">Description :</label>
                                            <span className="submitted-data">{prevTaskData?.data?.description ? prevTaskData?.data?.description : "---"}</span>
                                        </div>
                                        <div className="form-grp">
                                            <label className="submitted-form-heading">Start Date :</label>
                                            <span className="submitted-data">{prevTaskData?.data?.start_date_hf ? prevTaskData?.data?.start_date_hf : "---"}</span>
                                        </div>
                                        <div className="form-grp">
                                            <label className="submitted-form-heading">Due Date :</label>
                                            <span className="submitted-data">{prevTaskData?.data?.due_date_hf ? prevTaskData?.data?.due_date_hf : "---"}</span>
                                        </div>
                                        <div className="form-grp">
                                            <label className="submitted-form-heading">Task Status :</label>
                                            <span className="submitted-data">{prevTaskData?.data?.task_status_name ? prevTaskData?.data?.task_status_name : "---"}</span>
                                        </div>

                                    </div>


                                } */}
							</Tab.Pane>
							<Tab.Pane eventKey="action_plan_respondant_response" mountOnEnter unmountOnExit>
								{(respondent_data?.respondent_id && respondantReport === true) && <RespondentResponse reasonList={reasons} />}
							</Tab.Pane>
							{
								(actionType && actionType === "QUESTION") &&
								<Tab.Pane eventKey="action_plan_history" mountOnEnter unmountOnExit>
									{taskListData && taskListData.length > 0 ? taskListData.map((item, index) =>
										<div className="action_statement_history" key={index}>
											<div className="descrip">
												<ShowMoreText
													lines={2}
													more='Show more'
													less='Show less'
													className='content-css'
													anchorClass='show-more-anchor'
													onClick={() => setShowMore(!showMore)}
													expanded={showMore}
												>
													<p>{item.description}</p>
												</ShowMoreText>
											</div>
											<div className="mode-tags" >
												<div>
													<div className="mode-right" >Created on : &nbsp;<span className="mode-right-date">{item.created_at_hf}</span></div>
													<div className="mode-right">Assigned to : &nbsp;<span className="mode-right-name">{item.assigned_to_user_name}</span></div>
												</div>
												<div className="statement_tags">
													<button className="view_action_plan" onClick={() => historyOnclickhandle(item)}> View Action Plan</button>
												</div>
											</div>

											{/* {urlHistory.replace("/action-plan")} */}
										</div>
									) :
										<div className="action_statement_history"><p className="action-not-created">Action not created yet.</p></div>
									}

									{(listingPagination && listingPagination.current_page && listingPagination.total !== 0 && listingPagination.total > listingPagination.per_page) &&
										<div className="pagination-plugin-wrap">
											<Pagination
												activePage={listingPagination.current_page}
												itemsCountPerPage={10}
												totalItemsCount={listingPagination.total}
												onChange={handlePaginate}
												hideDisabled={true}
												firstPageText={<span className="prev-page-text-ic"></span>}
												lastPageText={<span className="next-page-text-ic"></span>}
												nextPageText={<span className="next-text-ic"></span>}
												prevPageText={<span className="prev-text-ic"></span>}
											/>
										</div>
									}

								</Tab.Pane>
							}
						</Tab.Content>
					</Modal.Body>
				</Tab.Container>
			</Modal>

		</div >
	)
}

export default ActionModal