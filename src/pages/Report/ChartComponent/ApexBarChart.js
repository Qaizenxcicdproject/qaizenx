import React from 'react';
import ReactApexChart from 'react-apexcharts';

const ApexBarChart = (props) => {

    const barChart = {          
        series: [{
          name: props.tooltipName,
          data: props.data ? props.data : [] 
        }],      
        options: {          
            chart: { 
                type: 'bar', height: 350 , toolbar: { show: false },         
            }, 
            labels: props.labels ? props.labels : [] ,
            colors: props.colors ? props.colors : [],      
            noData: {
                text: "No data to display for the selected time period.",
                align: 'center',
                verticalAlign: 'middle',
                offsetX: 0,
                offsetY: 0,
                style: { color: "#716779", fontSize: '13px', fontFamily: "NunitoSansSemiBold" }
            },     
            legend: { show:false },
            plotOptions: { bar: { horizontal: false, columnWidth: props.barWidth || "30%", distributed: true,  dataLabels: { position: 'top', }, }, },
            dataLabels: {
                enabled: true,  
                offsetY: -20,
                style: {
                    fontSize: '11px',
                    colors: ["#504658"]
                  }
              },
            stroke: { show: true, width: 1, colors: ['transparent'] },
            xaxis: {
                categories : props.labels,
                labels: { style: { colors: "#686868", },trim:true,maxHeight:70, }, 
                axisTicks: { show: false, },
                axisBorder: { show: false, },
            },
            yaxis: { 
                title: { text: props.yAxisTitle,    
                style: { color: "#323232", fontSize: '13px', fontFamily: 'NunitoSansSemiBold', },           
                },
                labels: { style: { colors: "#686868", }, }, 
                axisTicks: { show: false, },
                axisBorder: { show: false, },
            },
            fill: { opacity: 1 },
            tooltip: {
                y: {
                    formatter: function (val) {
                    return  val 
                    }
                }
            }
        },      
    }
   
    return(           
        <React.Fragment>
         
            <ReactApexChart  options={barChart.options} series={barChart.series} type="bar"  height={props.height}  />
        
        </React.Fragment>
    )
}

export default ApexBarChart;