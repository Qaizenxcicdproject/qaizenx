import React from 'react';
import ReactApexChart from 'react-apexcharts';

const ApexColumnLineChart = (props) => {

    const ARR_SUM = (arr) => {
        return arr.reduce((a, b) => a + b, 0);
    }    


    const mixedChart = {   
        
        series: [
                    { name: props.Name[0]  || '--', type: 'column', data: ARR_SUM(props.BarData) === 0  ?  [] : props.BarData,  },                    
                    { name: props.Name[1]  || '--', type: 'line', data: ARR_SUM(props.LineData) === 0 ? [] : props.LineData,},
                ],
                options: {
                    chart: { type: 'line', height: 350 , toolbar: { show: false }, }, 
                    fill: { opacity: 1, }  ,                  
                    colors: props.colors ? props.colors : [] ,
                    stroke: { show: true, width: 2,  curve: 'smooth',  lineCap: 'butt', colors: ['transparent','#6b5def'] },
                    // dataLabels: {
                    //     enabled: true,  
                    //     offsetY: -10,
                    //     formatter: function (val, opts) {
                    //        return val ? val : '' 
                    //     },
                    //     style: { fontSize: '11px', colors: ["#504658"] },
                    //     background: { enabled: false,}
                    // },
                    markers: { size: 3, colors: props.lineColor, strokeColors:  props.lineColor, strokeWidth: 4, hover: { size: 3, } },
                    legend: { show: true, markers: { fillColors: props.colors ? props.colors : [], }, },                    
                    labels: props.xAxisLabels ? props.xAxisLabels : [] ,
                    plotOptions: { bar: {horizontal: false, columnWidth: props.barWidth || "30%",   dataLabels: { position: 'top', }, }, },
                    xaxis: { tooltip: { enabled: false },axisTicks: { show: false, }, axisBorder: { show: false, }, },
                    yaxis: [   
                        {  min: props.minCountScale || 0, max: props.maxCountScale || 0,title: { text: props.yTitle,style: { color: "#323232", fontSize: '13px', fontFamily: 'NunitoSansSemiBold', }, },axisTicks: { show: false, }, axisBorder: { show: false, },
                            labels: {
                                // formatter: function (value) {
                                // return value;
                                // }
                                formatter: function (val) {
                                    return val.toFixed(2)
                                }
                            },
                        }, 
                        {  min: props.minScoreScale || 0, max: props.maxScoreScale || 0,opposite: true, title: { text: props.yOppsiteTitle, style: { color: "#323232", fontSize: '13px', fontFamily: 'NunitoSansSemiBold', }, },axisTicks: { show: false, }, axisBorder: { show: false, },
                            labels: {
                                // formatter: function (value) {
                                // return value;
                                // }
                                formatter: function (val) {
                                    return val.toFixed(2)
                                }
                            },
                        }
                        
                    ],
                    tooltip: {
                        enabled: true,
                        y: {
                            formatter: function (value) {
                                return value;
                            }
                        },                       
                    }
                },      
    }
   
    return(           
        <React.Fragment>
         
            <ReactApexChart  options={mixedChart.options} series={mixedChart.series} type="line" height={props.height}  />
        
        </React.Fragment>
    )
}

export default ApexColumnLineChart;