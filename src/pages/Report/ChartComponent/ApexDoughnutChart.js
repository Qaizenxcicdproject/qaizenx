import React from 'react';
import ReactApexChart from 'react-apexcharts';

const ApexDoughnutChart = (props) => {

    const doughnutChart = {
        series: props.data ? props.data : [],
        options: {
            chart: { type: 'donut',height: props.height || 'auto' },
            plotOptions: { pie: { donut: { size: props.size } } },
            stroke: { show: false },
            dataLabels: {
                enabled: true, style: {
                    fontSize: '12px',
                    fontFamily: 'NunitoSansSemiBold',
                    fontWeight: '600',
                    colors: ['#FFFFFF']
                },
                dropShadow: { enabled: false, },
            },
            colors: props.colors,
            labels: props.labels ? props.labels : [],
            noData: {
                text: "No data to display for the selected time period",
                align: 'center',
                verticalAlign: 'middle',
                offsetX: 0,
                offsetY: 0,
                style: { color: "#716779", fontSize: '13px', fontFamily: "NunitoSansSemiBold" }
            },
            legend: {
                formatter: function (seriesName, opts) {
                    let seriesVal = opts.w.globals.series[opts.seriesIndex];
                    let seriesAdd = opts.w.globals.series.reduce((a, b) => a + b, 0);
                    let seriesPer = (seriesVal / seriesAdd) * 100;
                    return [" ", Math.round(seriesPer * 10) / 10 + "%", " : ", seriesName]
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: { width: 200 },
                    legend: { position: 'bottom' }
                },

            }]
        },
    }


    return(           
        <React.Fragment>
         
            <ReactApexChart  options={doughnutChart.options} series={doughnutChart.series} type="donut"  height={props.height}  />
        
        </React.Fragment>
    )
}

export default ApexDoughnutChart;