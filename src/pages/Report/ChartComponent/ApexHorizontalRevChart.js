import React from 'react';
import ReactApexChart from 'react-apexcharts';

const ApexHorizontalRevChart = ({parentCallback , ...props}) => {

    const horizontalRevChart = {
            series: [{
                name: props.tooltipName,
                data: props.data ? props.data : [] ,
            }],
            options: {
                chart: { type: 'bar', height: 350, toolbar: { show: false, },
                    events: {
                        dataPointSelection: function(event, chartContext, config) {
                            if( Array.isArray(config.selectedDataPoints) ){
                                let configData ;
                                if(config.selectedDataPoints[0].length > 0){
                                    configData = config.w.config.labels[config.dataPointIndex]
                                }
                                parentCallback(configData);
                            }
                        }
                    }
                },
                labels: props.xAxislabels,
                colors: props.colors,
                plotOptions: { bar: { horizontal: true }, },
                dataLabels: { formatter: function(val) { return val ? val + '%' : '' } },
                xaxis: {
                    title: { text: props.xAxisTitle , style: { color: "#1c1c1c", fontSize: '13px', fontFamily: 'NunitoSansSemiBold', }, }, 
                    categories: props.xAxislabels ?  props.xAxislabels : [],
                },
                yaxis: { reversed: true, },
                series: props.tooltipPercent ? props.tooltipPercent : [],
                tooltip: { 
                    enabled: true, 
                    custom: function ({ series, seriesIndex, dataPointIndex, w }) { 
                        let selected = props.tooltipPercent[dataPointIndex];
                        let getColor = Array.isArray(w.globals.colors) ?  w.globals.colors[dataPointIndex] : w.globals.colors ;
                        return '<div class="apexchart_tooltip">' 
                            + '<span class="tooltip_name">' +props.reportLabel+ ' Score</span>'
                            + '<span class="tooltip_value"><span class="bar_color" style="background-color:'+getColor+'"></span>'+ selected + '</span>'+
                        '</div>'
                    } 
                },
                
            },
        }

   
    return(           
        <React.Fragment>
         
            <ReactApexChart  options={horizontalRevChart.options} series={horizontalRevChart.series} type="bar"  height={props.height}  />
        
        </React.Fragment>
    )
}

export default ApexHorizontalRevChart;