import React from 'react';
import ReactApexChart from 'react-apexcharts';

const ApexLineChart = (props) => {

    

    const LineChart = {
          
            series: [{
                name: props.tooltipName,
                data: props.data ? props.data : [] ,
            }],
            options: {
              chart: {
                height: 350,
                type: 'line',
                toolbar: { show: false, },
                zoom: {
                  enabled: false
                }
              },
              colors: props.colors,
              dataLabels: {
                enabled: false
              },
              stroke: {
                curve: 'straight'
              },
              markers: {
                size: 4,
                colors: ["#ce2e6c"],
                strokeColors: "#fff",
                strokeWidth: 2,
                hover: {
                  size: 7,
                }
              },
              grid: {
                row: {
                  colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                  opacity: 0.5
                },
              },
              xaxis: {
                categories: props.labels ? props.labels : [] ,
                    tooltip: {
                      enabled: false
                    }
              },
              yaxis: {
                labels: {
                  formatter: function(val, index) {
                    return val.toFixed(2);
                  }
                }
              },
            },
          
          
          };
   
    return(           
        <React.Fragment>
         
            <ReactApexChart  options={LineChart.options} series={LineChart.series} type="line"  height={props.height}  />
        
        </React.Fragment>
    )
}

export default ApexLineChart;