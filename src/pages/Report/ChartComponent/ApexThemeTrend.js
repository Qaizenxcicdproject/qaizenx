import React, { useState, useEffect, useContext } from 'react';
import ReactApexChart from 'react-apexcharts';
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";
import Axios from "utility/Axios";
import configURL from 'config/config';
import AppContext from 'store/AppContext';

const ApexThemeTrend = (props) => {

  const { EziLoader } = useContext(AppContext)
  const urlParams = useParams();
  const [surveyId] = useState(urlParams.survey_id)
  const [queTrendFilter, setQueTrendFilter] = useState('daily');
  const [trendQuestion] = useState(props.trendQuestion);
  const appStore = JSON.parse(localStorage.getItem("appState"));
  const timezone = appStore.user.timezone;
  const [questionTrend, setQuestionTrend] = useState({});
  const { addonFilter = null } = props

  const questionTrendListing = () => {
    EziLoader.show();
    let formData = new FormData();
    formData.append("survey_id", surveyId);
    formData.append("question_id", props.trendQuestion);
    formData.append("filter", queTrendFilter);
    formData.append("filters", JSON.stringify(props.filterData));
    formData.append("report_type", props.reportVersion);
    formData.append("calculation_type", (props.avg ? "average" : "count"));
    formData.append("roundDecimalPoint", "2");
    formData.append("date", JSON.stringify(props.dateFilter));
    formData.append("timezone", timezone);
    if (props.menuTitle === "analysis") {
      formData.append("addon_filter", addonFilter ? JSON.stringify(addonFilter) : JSON.stringify([]));
    }
    Axios.post(configURL.ReportQuestionTrend, formData).then(response => {
      EziLoader.hide();
      if (response.data.success !== undefined && response.data.success) {
        console.log("trned line", response.data)
        console.log("trned line dat", response.data.result.score_data)
        setQuestionTrend(response.data.result);
      }
      else {
        toast.warn(response.data.message || "Something went wrong")
      }
    }).catch(err => {
      EziLoader.hide()
      console.log(err);
    })

  }

  const LineChart = {
    series: [{
      name: "Score",
      data: (questionTrend && questionTrend.score_data) ? questionTrend.score_data : [],
    }],
    options: {
      chart: {
        height: 350,
        type: 'line',
        toolbar: { show: false, },
        zoom: {
          enabled: false
        }
      },
      colors: ['#F8C63D'],
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      markers: {
        size: 4,
        colors: ["#ce2e6c"],
        strokeColors: "#fff",
        strokeWidth: 2,
        hover: {
          size: 7,
        }
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'],
          opacity: 0.5
        },
      },
      xaxis: {
        categories: (questionTrend && questionTrend.score_label) ? questionTrend.score_label : [],
        tooltip: {
          enabled: false
        }
      },
      yaxis: {
        labels: {
          formatter: function (val, index) {
            return val.toFixed(2);
          }
        }
      },
    }
  };

  useEffect(() => {
    if ((props.menuTitle === "analysis" && addonFilter)) {
      questionTrendListing();
    }
    else {
      questionTrendListing();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [trendQuestion, props.filterData, props.dateFilter, queTrendFilter, addonFilter]);

  return (
    <React.Fragment>
      <select className="distribution-select" defaultValue={queTrendFilter} onChange={(e) => setQueTrendFilter(e.target.value)} >
        <option value="hourly">Hourly</option>
        <option value="daily">Daily</option>
        <option value="weekly">Weekly</option>
        <option value="monthly">Monthly</option>
      </select>
      <ReactApexChart options={LineChart.options} series={LineChart.series} type="line" height={270} />

    </React.Fragment>
  )
}

export default ApexThemeTrend;