import React, { useState, useEffect, useContext, useRef } from 'react';
import './CustomerExperience.scss';
import { useParams } from "react-router-dom";
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from "react-toastify";
import AppContext from 'store/AppContext';
import DatePicker from 'react-date-picker';
import ReportDemogrphyFilter from './ShareComponent/ReportDemographicFilter';
import { confirmAlert } from 'react-confirm-alert';
import ReportContext from './ReportContext';
import * as Config from './ReportConstant';
import VersionComponent from './VersionComponent';
// import { Chart } from 'react-chartjs-2';
import { OverlayTrigger, Tooltip, Dropdown } from 'react-bootstrap';
import PageNotFound from 'components/PageNotFound';
// import 'chartjs-plugin-datalabels';
import { HIDE_REPORT_FILTER } from '../../constants/constants';
import { toPng, toBlob } from 'html-to-image';
import pptxgen from "pptxgenjs";
import * as AppActions from "store/actions";
import { connect } from "react-redux";
import JSZip from "jszip";
import { saveAs } from 'file-saver';

// ChartDataLabels by default display false globally 
// Chart.defaults.global.plugins.datalabels.display = false;

// If the bar value 0 then hide value globally
// Chart.defaults.global.plugins.datalabels.display = function(ctx) {
//     return ctx.dataset.data[ctx.dataIndex] !== 0;
// }

// No data is present showing blink message 
// Chart.plugins.register({
// 	afterDraw: function(chart) {
// 		// console.log(chart.data.datasets[0].data.length,  chart.canvas.id, chart.data.datasets[0].data);
// 		if (chart.data.datasets[0].data.length === 0) {
// 			// No data is present
// 			var ctx = chart.chart.ctx;
// 			var width = chart.chart.width;
// 			var height = chart.chart.height;
// 			chart.clear();
// 			ctx.save();
// 			ctx.textAlign = 'center';
// 			ctx.textBaseline = 'middle';
// 			ctx.font = "16px normal 'NunitoSans'";
// 			ctx.fillText('No data to display for selected time period', width / 2, height / 2);
// 			ctx.restore();
// 		}
//     }
// });



const CustomerExperience = (props) => {

	const { EziLoader } = useContext(AppContext)
	const urlParams = useParams();
	const [surveyId] = useState(urlParams.survey_id)
	const [pageLoad, setPageLoad] = useState(false);
	const [switchPage, setSwitchPage] = useState('question');
	const [menuTitle, setMenuTitle] = useState();
	const [surveyQuestionData, setSurveyQuestionData] = useState({});
	const [surveyName, setSurveyName] = useState();
	const [date, setDate] = useState();
	const [showFilter, setShowFilter] = useState(false);
	const [filterData, setFilterData] = useState([]);
	const [datefilter, setDateFilter] = useState([]);
	const [reportType, setReportType] = useState(null);
	const [reportVersion, setReportVersion] = useState();
	const [reportLabel, setReportLabel] = useState();
	const [dayActive, setDayActive] = useState("week");
	const [dayLabel, setDayLabel] = useState("7 Days");
	const [addonDate, setAddonDate] = useState(1);
	const [hideHeader, setHideHeader] = useState(true);
	const [containFile, setContainFile] = useState(false);
	const [insightData, setInsightData] = useState({});
	const [surveyStartDate, setSurveyStartDate] = useState(null);
	const [surveyEndDate, setSurveyEndDate] = useState(null);

	const appStore = JSON.parse(localStorage.getItem("appState"));
	const roleArray = appStore.user.assigned_roles;
	const roleExist = roleArray.includes("Company Admin");
	let timezone = appStore.user.timezone;
	var todayDate = new Date();
	let time = todayDate.getHours() + ":" + todayDate.getMinutes() + ":" + todayDate.getSeconds();
	let utcTime = new Date().getTimezoneOffset("en-US", { timeZone: timezone });
	let timeZoneString = utcTime.toString();
	let UtcDiff = time_convert(timeZoneString.substring(1), timeZoneString[0]);
	const [reportDate, setReportDate] = useState({
		start_date: new Date(),
		end_date: new Date(),
		time: time,
		utc: UtcDiff
	});
	const screenShotref = useRef([]);
	const fullPageScreenShotRef = useRef();

	//convert timezone into utc time difference
	function time_convert(num, sign) {
		var hours = Math.floor(num / 60);
		var minutes = num % 60;
		return sign + hours + ":" + minutes;
	}


	const getCalculatedDate = (count = 0) => {
		let today = new Date();
		let lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - count);
		return {
			date: lastWeek,
			month: lastWeek.getMonth() + 1,
			day: lastWeek.getDate(),
			year: lastWeek.getFullYear()
		}
	}

	const handleManualFilter = (e) => {
		let label = e.target.dataset.label;
		setDayActive(label);
	}

	const handleDayFilter = (e) => {
		let label = e.target.dataset.label;
		setDayLabel(e.target.dataset.text);
		setAddonDate(e.target.dataset.id);
		setDayActive(label);
		if (label === 'week') {
			setDateFilter({ start_date: new Date(getCalculatedDate(6).date.getTime() - (getCalculatedDate(6).date.getTimezoneOffset() * 60000)).toJSON(), end_date: new Date(), time: time, utc: UtcDiff });
			setReportDate({ start_date: getCalculatedDate(6).date, end_date: new Date(), time: time, utc: UtcDiff })
		} else if (label === 'month') {
			setDateFilter({ start_date: new Date(getCalculatedDate(29).date.getTime() - (getCalculatedDate(29).date.getTimezoneOffset() * 60000)).toJSON(), end_date: new Date(), time: time, utc: UtcDiff });
			setReportDate({ start_date: getCalculatedDate(29).date, end_date: new Date(), time: time, utc: UtcDiff })
		} else {
			setDateFilter({ start_date: new Date(getCalculatedDate(89).date.getTime() - (getCalculatedDate(89).date.getTimezoneOffset() * 60000)).toJSON(), end_date: new Date(), time: time, utc: UtcDiff });
			setReportDate({ start_date: getCalculatedDate(89).date, end_date: new Date(), time: time, utc: UtcDiff })
		}
	}

	const questionListing = () => {
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		Axios.post(configURL.ReportSurveyQuestion, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setSurveyName(response.data.result.name);
				// eslint-disable-next-line no-unused-vars
				let start_date = (response.data.result.start_time) ? new Date(response.data.result.start_time.replace(/-/g, "/")) : new Date()
				let end_date = new Date();
				setDate(response.data.result);
				setReportVersion(response.data.result.report_type);
				setReportLabel(response.data.result.report_label);
				setContainFile(response.data.result.is_contain_files);
				setInsightData(response.data.insight)
				setSurveyStartDate(response.data.result.start_time)
				setSurveyEndDate(response.data.result.end_time)
				let surveyType = response.data.result.survey_type.toLowerCase().replace(/-|\s/g, "");
				let typeReport = response.data.result.report_type.toLowerCase().replace(/-|\s/g, "");
				console.log(surveyType);
				if (surveyType === 'onetimesurvey') {
					setDayLabel("manual");
					setDayActive("manual");
					setDateFilter({ start_date: start_date, end_date: end_date, time: time, utc: UtcDiff })
					setReportDate({ start_date: start_date, end_date: end_date, time: time, utc: UtcDiff });
				}
				else if (Config.CONTINUOUS_TYPE.includes(typeReport)) {
					setDateFilter({ start_date: new Date(getCalculatedDate(6).date.getTime() - (getCalculatedDate(6).date.getTimezoneOffset() * 60000)).toJSON(), end_date: new Date(), time: time, utc: UtcDiff })
					setReportDate({ start_date: getCalculatedDate(6).date, end_date: end_date, time: time, utc: UtcDiff });
				}
				else {

					setDateFilter({ start_date: start_date, end_date: end_date, time: time, utc: UtcDiff })
					setReportDate({ start_date: start_date, end_date: end_date, time: time, utc: UtcDiff });
				}
				setReportType(typeReport);
				if (Config.REPORT_TYPE.includes(typeReport)) {
					setSwitchPage('insights');
					setMenuTitle('Insights');
					setSurveyQuestionData(response.data.result);
				}
				setPageLoad(true);
			}
			else {
				setPageLoad(true);
				toast.warn(response.data.message);
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}

	const isToday = (someDate) => {
		const today = new Date()
		return someDate.getDate() === today.getDate() &&
			someDate.getMonth() === today.getMonth() &&
			someDate.getFullYear() === today.getFullYear()
	}

	const handleCalendarClose = () => {
		if (reportDate.start_date.getTime() > reportDate.end_date.getTime()) {
			confirmAlert({
				title: 'Date Error!',
				message: 'Start date could not be greater than End Date..',
				buttons: [{ label: 'I Understood' }]
			});
			return;
		}
		const today = isToday(new Date(reportDate.end_date.getTime()));
		if (!today === true) {
			time = "23:59:59";
		}
		let date = {
			start_date: new Date(reportDate.start_date.getTime() - (reportDate.start_date.getTimezoneOffset() * 60000)).toJSON(),
			end_date: new Date(reportDate.end_date.getTime() - (reportDate.end_date.getTimezoneOffset() * 60000)).toJSON(),
			time: time,
			utc: UtcDiff
		};
		setDateFilter(date);
	};

	/**
	 * Handle Filter Clear
	 */
	const handleClearFilter = () => {
		EziLoader.show()
		setShowFilter(false)
		setFilterData([])
	}

	const processArray = (arr, fn) => {
		return arr.reduce(
			(p, v) => p.then((a) => fn(v).then(r => a.concat([r]))),
			Promise.resolve([])
		);
	}

	const handlePPTDownload = async () => {
		EziLoader.show()
		var pres = new pptxgen();
		var slide = [];
		const convertImage = (currentRef) => toPng(currentRef)
			.then((dataUrl) => {
				return dataUrl;
			});

		var allScreens = screenShotref.current.filter(function (el) {
			return el != null;
		});
		processArray(allScreens, convertImage).then((slideArray) => {
			if (slideArray && slideArray.length > 0) {

				slideArray.forEach((item, index) => {
					slide[index] = pres.addSlide();
					slide[index].addImage({ path: item, x: 0, y: 0, w: '100%', h: '100%', sizing: { type: "cover" } });

					if (index === slideArray.length - 1) {
						pres.writeFile({ fileName: 'Report.pptx' });

					}

				});


			}
		});
		EziLoader.hide()
	}

	const handleAllImagesZipDownload = async () => {
		EziLoader.show()
		var zip = new JSZip();
		var img = zip.folder("report_images");
		const convertImage = (currentRef) => toBlob(currentRef)
			.then((dataUrl) => {
				return dataUrl;
			});

		var allScreens = screenShotref.current.filter(function (el) {
			return el != null;
		});
		processArray(allScreens, convertImage).then((slideArray) => {
			if (slideArray && slideArray.length > 0) {

				slideArray.forEach((item, index) => {
					img.file(`report-${index + 1}.jpeg`, item, { base64: false });
					if (index === slideArray.length - 1) {
						zip.generateAsync({ type: "blob" }).then(function (content) {
							saveAs(content, "report.zip");
						});

					}

				});


			}
		});
		EziLoader.hide()
	}


	// const handleFullScreenDownload = (currentRef) => {
	// 	EziLoader.show()
	// 	if (fullPageScreenShotRef.current === null) {
	// 		return
	// 	}
	// 	toJpeg(fullPageScreenShotRef.current, { cacheBust: true, })
	// 		.then((dataUrl) => {
	// 			const link = document.createElement('a')
	// 			link.download = 'report.png'
	// 			link.href = dataUrl
	// 			link.click()
	// 			EziLoader.hide()
	// 		})
	// 		.catch((err) => {
	// 			EziLoader.hide()
	// 			console.log(err)
	// 		})
	// }

	useEffect(() => {
		props.dispatchScreenShotRef({ screenShotRef: screenShotref, fullPageScreenShotRef: fullPageScreenShotRef });

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [screenShotref]);

	useEffect(() => {
		EziLoader.show()
		questionListing();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const contextValue = { surveyQuestionData, filterData, datefilter, switchPage, menuTitle, timezone, reportDate, reportType, reportVersion, reportLabel, addonDate, dayActive, dayLabel, containFile, roleExist, surveyId, insightData, surveyStartDate, surveyEndDate }

	return (

		<React.Fragment>
			<ReportContext.Provider value={contextValue}>
				{pageLoad ?
					<React.Fragment>
						{((Config.STANDARD_TYPE.includes(reportType)) || (Config.CONTINUOUS_TYPE.includes(reportType)))
							?
							<section className="Page-CX-Report Page-CustomerExperience">
								{Config.STANDARD_TYPE.includes(reportType) &&
									<React.Fragment>
										<OverlayTrigger overlay={<Tooltip>{hideHeader ? 'Hide header' : 'Show header'}</Tooltip>}>
											<button type="button" className={`header-hide-btn ${hideHeader ? 'show' : 'hide'}`} onClick={() => setHideHeader(!hideHeader)}></button>
										</OverlayTrigger>
										<div className={`ce-header one-time-header ${hideHeader ? 'show' : 'hide'}`}>
											<div className="ce-title-wrap">
												<span className="ce-title">{surveyName}</span>
												<div className="ce-date-range-wrap" style={{ visibility: hideHeader ? 'initial' : 'hidden' }}>
													<div className="ce-date-wrap">
														<DatePicker
															format="d/MM/y"
															selected={reportDate.start_date}
															value={reportDate.start_date}
															disabled={false}
															minDate={new Date(date.start_time)}
															maxDate={new Date()}
															onChange={(d) => setReportDate({ ...reportDate, start_date: d })}
															clearIcon={null}
															className="sweet-datepicker-custom"
														/>
													</div>
													<span className="ce-date-devider"></span>
													<div className="ce-date-wrap">
														<DatePicker
															format="d/MM/y"
															selected={new Date()}
															value={reportDate.end_date}
															minDate={new Date(date.start_time)}
															maxDate={new Date()}
															onChange={(d) => setReportDate({ ...reportDate, end_date: d })}
															disabled={false}
															clearIcon={null}
															className="sweet-datepicker-custom"
														/>
													</div>
													<button type="button" className="date-apply-btn" onClick={() => handleCalendarClose()} >Go</button>
												</div>
											</div>
											<div className="ce-right-header" style={{ visibility: hideHeader ? 'initial' : 'hidden' }}>
												<div className="ec-action-header">
													{(!HIDE_REPORT_FILTER.includes(reportType)) &&


														<div className="filter-compare-btn-wrap">
															<button type="button" className="ezi-filter-btn" onClick={() => setShowFilter(!showFilter)}>Filter</button>
															{/* <button type="button" className="compare-button">Compare</button> */}
															<ReportDemogrphyFilter
																show={showFilter}
																hide={() => setShowFilter(false)}
																position="right"
																applyFilter={(filters) => setFilterData(filters)}
																clearFilter={handleClearFilter}
															/>
														</div>


													}
													<Dropdown className="download-page-ppt">
														<Dropdown.Toggle id="dropdown-basic"> <img alt="" src={require(`../../assets/images/report/download.png`)} /> </Dropdown.Toggle>
														<Dropdown.Menu>
															<Dropdown.Item><span onClick={handlePPTDownload} >Save as PPT</span></Dropdown.Item>
															{/* <Dropdown.Item>	<span onClick={handleFullScreenDownload}>Save as Image</span></Dropdown.Item> */}
															<Dropdown.Item>	<span onClick={handleAllImagesZipDownload}>Save as Zip</span></Dropdown.Item>

														</Dropdown.Menu>
													</Dropdown>
												</div>
											</div>
										</div>
									</React.Fragment>
								}

								{Config.CONTINUOUS_TYPE.includes(reportType) &&
									<React.Fragment>
										<OverlayTrigger overlay={<Tooltip>{hideHeader ? 'Hide header' : 'Show header'}</Tooltip>}>
											<button type="button" className={`header-hide-btn ${hideHeader ? 'show' : 'hide'}`} onClick={() => setHideHeader(!hideHeader)}></button>
										</OverlayTrigger>
										<div className={`ce-header continuos-header ${hideHeader ? 'show' : 'hide'}`}>
											<div className="ce-title-wrap">
												<span className="ce-title">{surveyName}</span>
												<div className="stock-wise-header">
													<div className="stock-bt-wrap">
														<button type="button" data-label="manual" data-id={0} data-text="Date Range" className={`st-button ${dayActive === 'manual' && 'active'}`} onClick={handleManualFilter}>Date Range</button>
														<button type="button" data-label="week" data-id={1} data-text="7 Days" className={`st-button ${dayActive === 'week' && 'active'}`} onClick={handleDayFilter}>7 Days</button>
														<button type="button" data-label="month" data-id={2} data-text="30 Days" className={`st-button ${dayActive === 'month' && 'active'}`} onClick={handleDayFilter}>30 Days</button>
														<button type="button" data-label="quarter" data-id={3} data-text="90 Days" className={`st-button ${dayActive === 'quarter' && 'active'}`} onClick={handleDayFilter}>90 Days</button>
													</div>
												</div>
												<div className="ce-date-range-wrap" style={{ visibility: hideHeader ? 'initial' : 'hidden' }}>
													<div className="ce-date-wrap">
														<DatePicker
															format="d/MM/y"
															selected={reportDate.start_date}
															value={reportDate.start_date}
															disabled={(dayActive === "manual") ? false : true}
															minDate={new Date(date.start_time)}
															maxDate={new Date()}
															onChange={(d) => setReportDate({ ...reportDate, start_date: d })}
															clearIcon={null}
															className="sweet-datepicker-custom"
														/>
													</div>
													<span className="ce-date-devider"></span>
													<div className="ce-date-wrap">
														<DatePicker
															format="d/MM/y"
															selected={new Date()}
															value={reportDate.end_date}
															minDate={new Date(date.start_time)}
															maxDate={new Date()}
															onChange={(d) => setReportDate({ ...reportDate, end_date: d })}
															disabled={(dayActive === "manual") ? false : true}
															clearIcon={null}
															className="sweet-datepicker-custom"
														/>
													</div>
													{(dayActive === "manual") && <button type="button" className="date-apply-btn" onClick={() => handleCalendarClose()} >Go</button>}
												</div>
											</div>
											<div className="ce-right-header" style={{ visibility: hideHeader ? 'initial' : 'hidden' }}>
												<div className="ec-action-header">
													<div className="filter-compare-btn-wrap">
														<button type="button" className="ezi-filter-btn" onClick={() => setShowFilter(!showFilter)}>Filter</button>
														<ReportDemogrphyFilter
															show={showFilter}
															hide={() => setShowFilter(false)}
															position="right"
															applyFilter={(filters) => setFilterData(filters)}
															clearFilter={handleClearFilter}
														/>

													</div>
													<Dropdown className="download-page-ppt">
														<Dropdown.Toggle id="dropdown-basic"> <img alt="" src={require(`../../assets/images/report/download.png`)} /> </Dropdown.Toggle>
														<Dropdown.Menu>
															<Dropdown.Item><span onClick={handlePPTDownload} >Save as PPT</span></Dropdown.Item>
															{/* <Dropdown.Item>	<span onClick={handleFullScreenDownload} >Save as Image</span></Dropdown.Item> */}
															<Dropdown.Item>	<span onClick={handleAllImagesZipDownload}>Save as Zip</span></Dropdown.Item>
														</Dropdown.Menu>
													</Dropdown>


												</div>
											</div>
										</div>
									</React.Fragment>
								}

								<section className={`CE-report-content insight-main-section ${hideHeader ? 'show' : 'hide'}`} ref={el => fullPageScreenShotRef.current = el}>
									{!hideHeader && <span className="insight-ce-title">{surveyName}</span>}
									<VersionComponent tag={reportType} />
								</section>

							</section>
							:
							<PageNotFound message='The report type you are looking for is no longer available.' />
						}
					</React.Fragment>
					: null
				}
			</ReportContext.Provider>
		</React.Fragment>
	)
}
const mapDispatchToProps = (dispatch) => {
	return {
		dispatchScreenShotRef: (data) => dispatch(AppActions.reportScreenShot(data)),
	};
};
export default connect(null, mapDispatchToProps)(CustomerExperience);