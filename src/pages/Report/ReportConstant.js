

export const REPORT_TYPE = ['hiv3','csatv3','csatv5','npsv3','esatv3','hiv4','default','wellness','pulse','exit','pf16','diversity','inn','wellnessv2','dmat','hiexp','welexp','pulexp','pmf','epa']

export const CONTINUOUS_TYPE = ['hiv3','hiv4','csatv3','npsv3','csatv5','pulse','esatv3','exit','wellnessv2','pmf']

export const STANDARD_TYPE = ['wellness','default','pf16','diversity','inn','dmat','hiexp','welexp','pulexp','epa']

export const GAUGE_COLOR = ['#e43e3d','#ec654e','#e1c63b','#9fcd35','#5aaf2b']

export const GAUGE_SEGEMENT = [-100,-65,-25,25,65,100]

export const GAUGE_SEGEMENT_AVG = [0,2,4,6,8,10]

export const GAUGE_COLOR_FOUR = ['#e43e3d','#e1c63b','#5aaf2b']

export const GAUGE_SEGEMENT_FOUR = [0,30,70,100]

export const GAUGE_SEGEMENT_AVG_FOUR = [0,2,3,5]

export const TREND_CHART_COLOR = ['#5aaf2b','#e1c63b','#e43e3d','#6b5def']

export const TREND_LINE_COLOR = '#6b5def'

export const BAR_CHART_COLOR =  ['#5aaf2b','#9fcd35','#e1c63b','#ec654e','#e43e3d']

export const BAR_CHART_COLOR_NPS = ['#5aaf2b', '#7fcd31', '#9fcd35', '#c1cc36', '#e1c63b', '#f8c43d', '#f3a74c', '#ef874c', '#ec654e', '#ea484d', '#e43e3d']

export const DOUGHNUT_COLOR_CODE = [ '#5aaf46', '#8fad43', '#abd041', '#e1c638', '#e07e38', '#cc5934', '#e43e3d','#44adad', '#537ca0', '#5853a4', '#8255a3', '#ad58a2', '#e2417e']
