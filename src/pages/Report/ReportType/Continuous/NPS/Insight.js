import React, { useState, useEffect, useRef, useContext } from 'react';
import { Tab, Nav, OverlayTrigger, Tooltip, ProgressBar } from 'react-bootstrap';
import InfoModal from './InfoModal';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from "react-toastify";
import ReactSpeedometer from 'react-d3-speedometer';
import ReactWordcloud from 'react-wordcloud';
import AppContext from 'store/AppContext';
import moment from 'moment';
import SentimentComponent from 'pages/Report/ShareComponent/Sentiment';
import { GAUGE_SEGEMENT, GAUGE_SEGEMENT_AVG, TREND_CHART_COLOR, TREND_LINE_COLOR } from 'pages/Report/ReportConstant';
import ReportContext from 'pages/Report/ReportContext';
import ApexBarChart from 'pages/Report/ChartComponent/ApexBarChart';
import ApexMixedChart from 'pages/Report/ChartComponent/ApexMixedChart';
import { connect } from "react-redux";
import Select from 'react-select';

const InsightComponent = (props) => {

	const { filterData, reportLabel, datefilter, timezone, surveyId, reportVersion, surveyStartDate } = useContext(ReportContext);
	const { EziLoader } = useContext(AppContext)
	const [infoModal, setInfoModal] = useState(false);
	const [tabKey, setTabKey] = useState('break_up');
	const [gaugeScoreData, setGaugeScoreData] = useState({});
	const [gaugeScore, setGaugeScore] = useState([]);
	const [gaugeColors, setGaugeColors] = useState([]);
	const [breakupScoreData, setBreakupScoreData] = useState({});
	const [trendFilter, setTrendFilter] = useState("weekly");
	const [groupBarFirst, setGroupBarFirst] = useState([]);
	const [groupBarSecond, setGroupBarSecond] = useState([]);
	const [groupBarThird, setGroupBarThird] = useState([]);
	const [axisDate, setAxisDate] = useState(null);
	const [trendSum, setTrendSum] = useState(0);
	const [trendHourlyDate, setTrendHourlyDate] = useState(null);
	const [trendData, setTrendData] = useState({});
	const [countScale, setCountScale] = useState({});
	const [scoreScale, setScoreScale] = useState({});
	const [keyPhrase, setKeyPhrase] = useState([]);
	const [wordCount, setWordCount] = useState(40);
	const currentPage = useRef(1);
	const startDateSurvey = moment(surveyStartDate, 'YYYY-MM-DD').format('YYYY-MM-DD')
	const [themeScore, setThemeScore] = useState([]);
	const [commentFilter] = useState([]);
	const [selectedDefaultWordCount, setSelectedDefaultWordCount] = useState({ value: 40, label: '40' });
	const [questionId, setQuestionId] = useState('');
	const wordCountOptions = [
		{ value: 10, label: '10' },
		{ value: 20, label: '20' },
		{ value: 30, label: '30' },
		{ value: 40, label: '40' },
		{ value: 50, label: '50' },
	]
	const [selectedTrendFilter, setSelectedTrendFilter] = useState({ value: 'weekly', label: 'Weekly' });
	const trendFilterOptions = [
		{ value: 'hourly', label: 'Hourly' },
		{ value: 'daily', label: 'Daily' },
		{ value: 'weekly', label: 'Weekly' },
		{ value: 'monthly', label: 'Monthly' },
	]
	const [wordCloudSearch, setWordCloudSearch] = useState('');

	/**
	 *  Trend Select filter for  hourly , daily , weekly , monthly
	 */
	const handleTrendFilter = (filter) => {
		currentPage.current = 1;
		setTrendFilter(filter.value)
		setSelectedTrendFilter({ value: filter.value, label: filter.label })
		trendListing(filter.value);
	}

	/**
	 *  Trend Previous filter for  hourly , daily , weekly , monthly
	 */
	const setPrevPage = () => {
		if ((trendFilter === 'hourly') && (startDateSurvey < axisDate) && (currentPage.current < 4)) {
			currentPage.current = currentPage.current + 1;
			trendListing(trendFilter);
		}
		if ((trendFilter !== 'hourly') && (startDateSurvey < axisDate)) {
			currentPage.current = currentPage.current + 1;
			trendListing(trendFilter);
		}
	}

	/**
	 *  Trend Next filter for  hourly , daily , weekly , monthly
	 */
	const setNextPage = () => {
		if (currentPage.current > 1) {
			currentPage.current = currentPage.current - 1;
			trendListing(trendFilter);
		}
	}


	/**
	 *  Sentiment Word cloud chart 
	 */
	const wordOptions = {
		colors: ["#1D1B28", "#3FD2E6", "#800080", "#718584"],
		enableTooltip: true,
		deterministic: true,
		fontFamily: 'NunitoSansSemiBold',
		fontSizes: [14, 40],
		rotations: 0,

	};
	const wordCloudCallbacks = {
		// getWordTooltip: word => `The word "${word.text}" appears ${word.value} times.`,
		getWordTooltip: word => '',
		onWordClick: word => setWordCloudSearch(word.text)
	};


	/**
	 *  Gauge Score listing
	 */
	const gaugeScoreListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		formData.append("calculation_type", (props.avgScore ? "average" : "count"));
		formData.append("roundDecimalPoint", "0");
		formData.append("report_type", reportVersion);
		Axios.post(configURL.ReportInsightGaugeScores, formData).then(res => {
			EziLoader.hide()
			if (res.data.success !== undefined && res.data.success) {
				setGaugeScoreData(res.data.result);
				setGaugeScore(res.data.result.score);
				setGaugeColors(res.data.result.colors)
			} else {
				toast.warn(res.data.message);
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}

	/**
	 *  Score breakup listing
	 */
	const scoreBreakupListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		formData.append("roundDecimalPoint", "0");
		formData.append("report_type", reportVersion);
		Axios.post(configURL.ReportInsightBreakup, formData).then(res => {
			EziLoader.hide()
			if (res.data.success !== undefined && res.data.success) {
				setBreakupScoreData(res.data.result);
			} else {
				toast.warn(res.data.message);
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}

	/**
	 *  Trend listing for mixed chart
	 */
	const trendListing = (filter = trendFilter) => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("filter", filter);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		formData.append("calculation_type", (props.avgScore ? "average" : "count"));
		formData.append("roundDecimalPoint", "1");
		formData.append("page", currentPage.current);
		formData.append("report_type", reportVersion);
		Axios.post(configURL.ReportInsightTrend, formData).then(res => {
			EziLoader.hide()
			if (res.data.success !== undefined && res.data.success) {
				const columnData = res.data.result.counts;
				let modifiedArr = []
				for (let i = 0; i < 3; i++) {
					let arr = columnData.map(el => el[i])
					modifiedArr.push(arr)
				}
				const [one, two, three] = modifiedArr;
				let arrSum = modifiedArr.concat(res.data.result.scores).flat().reduce((a, b) => a + b, 0);
				setTrendSum(arrSum)
				setGroupBarFirst(one)
				setGroupBarSecond(two)
				setGroupBarThird(three)
				setTrendData(res.data.result)
				setCountScale(res.data.result.countScale)
				setScoreScale(res.data.result.scoreScale)
				setTrendHourlyDate(moment(res.data.result.hourlyDate, 'YYYY-MM-DD').format('DD-MM-YYYY'))
				let dateAxis = moment(res.data.result.option_labels[0], 'DD-MM-YYYY').format('YYYY-MM-DD')
				setAxisDate(dateAxis)
			} else {
				toast.warn(res.data.message);
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}

	/**
	 *  Reason tab theme progress bar listing
	 */
	const reasonThemeListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("report_type", reportVersion);
		formData.append("calculation_type", (props.avgScore ? "average" : "count"));
		formData.append("roundDecimalPoint", "2");
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		Axios.post(configURL.ReportReasonThemes, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setThemeScore(response.data.result);

			} else {
				toast.warn(response.data.message);
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}

	/**
	 *  Word cloud sentiment listing
	 */
	const keyPhraseListing = (filter = commentFilter) => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("report_type", reportVersion);
		formData.append("timezone", timezone);
		formData.append("comment_filters", JSON.stringify(filter));
		formData.append("comment_question_id", questionId);
		Axios.post(`${configURL.ReportCommentsSentiment}`, formData).then(res => {
			EziLoader.hide();
			if (res.data.success !== undefined && res.data.success) {
				setKeyPhrase(res.data.result);
			} else {
				toast.warn(res.data.message);
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}

	const commentFilterHandle = (filter) => {
		if (filter !== 'Total') {
			keyPhraseListing([{ "label": "sentiment", "options": [filter] }]);
		}
		else {
			keyPhraseListing([]);
		}
	}
	const handlewordChange = (data) => {
		setWordCount(data.value)
		setSelectedDefaultWordCount({ value: data.value, label: data.label })
	}
	useEffect(() => {
		scoreBreakupListing();
		keyPhraseListing(commentFilter);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [filterData, datefilter, questionId]);

	useEffect(() => {
		gaugeScoreListing();
		currentPage.current = 1;
		trendListing(trendFilter);
		reasonThemeListing()
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [filterData, datefilter, props.avgScore]);
	return (
		<React.Fragment>
			<section className="CX-QuestionComponent CE-insight-section" >
				<div className="one-four-card-wrapper">
					<div className="chart-box-card_c car-bot-mar increse20 speedometer-widget-card" ref={el => props.screenShotRefVal.current[4] = el}>
						<div className="chart-box-card-header_c">
							<span className="left-text">
								{(props.avgScore ? reportLabel + " Average" : reportLabel)}
								<button type="button" className="ezi-popover-btn" onClick={() => setInfoModal(true)}></button>
							</span>
							<span className="right-text">{(gaugeScoreData && gaugeScoreData.total_respondents) ? gaugeScoreData.total_respondents : 0} Responses</span>                        </div>
						<div className="chart-box-card-body_c">
							<div className="speedometer-wrapper">
								<ReactSpeedometer
									width={330}
									height={176}
									forceRender={true}
									needleHeightRatio={0.8}
									maxSegmentLabels={1}
									customSegmentStops={props.avgScore ? GAUGE_SEGEMENT_AVG : GAUGE_SEGEMENT}
									segmentColors={gaugeColors}
									value={gaugeScore[1] || 0}
									minValue={props.avgScore ? 0 : -100}
									maxValue={props.avgScore ? 10 : 100}
									ringWidth={40}
									needleTransitionDuration={3333}
									needleTransition="easeElastic"
									needleColor={"#504658"}
									textColor={"#504658"}
									labelFontSize="15"
									valueTextFontSize="13"
									paddingHorizontal={10}
									paddingVertical={10}
								/>
								<span className="speedometer-hi-score"> {gaugeScore[1] || 0}</span>
							</div>
							<div className="half-donut-info speedometer-sentiment">
								<div className="half-donut-rating-text">
									{(gaugeScoreData && gaugeScoreData.labels_count && gaugeScoreData.labels && gaugeScoreData.labels_score) &&
										<React.Fragment>
											<div className="half-donut-rating-col half-donut-excellent">
												<span className="ce-title">{gaugeScoreData.labels_count[0] || 0}</span>
												<span className="ce-subtitle">{gaugeScoreData.labels[0].toString() || "--"}</span>
												<span className="ce-title">{gaugeScoreData.labels_score[0] + '%' || 0}</span>
											</div>
											<div className="half-donut-rating-col half-donut-average">
												<span className="ce-title">{gaugeScoreData.labels_count[1] || 0}</span>
												<span className="ce-subtitle">{gaugeScoreData.labels[1].toString() || "--"}</span>
												<span className="ce-title">{gaugeScoreData.labels_score[1] + '%' || 0}</span>
											</div>
											<div className="half-donut-rating-col half-donut-poor">
												<span className="ce-title">{gaugeScoreData.labels_count[2] || 0}</span>
												<span className="ce-subtitle">{gaugeScoreData.labels[2].toString() || "--"}</span>
												<span className="ce-title">{gaugeScoreData.labels_score[2] + '%' || 0}</span>
											</div>
										</React.Fragment>
									}
								</div>
							</div>
						</div>
					</div>
					<div className="chart-box-card_c increse20 card-tab-width" ref={el => props.screenShotRefVal.current[5] = el}>
						<div className="tablist_ezi">
							<Tab.Container activeKey={tabKey} onSelect={(k) => setTabKey(k)}>
								<div className="tab-header-wrap">
									<div className="tab-left-header">
										<Nav variant="pills" >
											<Nav.Item>
												<Nav.Link eventKey="break_up">Score Break-Up</Nav.Link>
											</Nav.Item>
											<Nav.Item>
												<Nav.Link eventKey="trend">Trend</Nav.Link>
											</Nav.Item>
											<Nav.Item>
												<Nav.Link eventKey="reasons">Drivers &#38; Draggers</Nav.Link>
											</Nav.Item>
											<Nav.Item>
												<Nav.Link eventKey="sentiments">Sentiments</Nav.Link>
											</Nav.Item>
										</Nav>
									</div>

								</div>
								<Tab.Content>
									<Tab.Pane eventKey="break_up" mountOnEnter unmountOnExit>
										<div className="tab-chart-body">
											{(breakupScoreData && Array.isArray(breakupScoreData.counts) && breakupScoreData.counts.length) ?
												<ApexBarChart
													tooltipName="Score"
													data={breakupScoreData && breakupScoreData.counts}
													labels={breakupScoreData && breakupScoreData.labels}
													barWidth="40%"
													height="320"
													colors={breakupScoreData && breakupScoreData.colors}
												/>
												:
												<div className="insight-no-data">
													<span className="no-data-display">No data to display for the selected time period.</span>
												</div>
											}
										</div>
									</Tab.Pane>
									<Tab.Pane eventKey="trend" mountOnEnter unmountOnExit>
										<div className="tab-chart-body">
											<div className="distribution-pagination-wrap">
												{/* <select className="distribution-select" defaultValue={trendFilter} onChange={(e) => handleTrendFilter(e.target.value)}  ref={distributionTime} >
													<option value="hourly">Hourly</option>
													<option value="daily">Daily</option>
													<option value="weekly">Weekly</option>
													<option value="monthly">Monthly</option>
												</select> */}
												<Select
													defaultValue={selectedTrendFilter}
													options={trendFilterOptions}
													onChange={handleTrendFilter}
													className="select-change-image trend-filter"
												/>
												<div className="chart-pagination">
													<OverlayTrigger overlay={<Tooltip>Previous</Tooltip>}>
														<button type="button" className={`chart-prev ${((trendFilter === 'hourly') && ((startDateSurvey > axisDate) || (currentPage.current === 4))) ? 'inactive' : ((trendFilter !== 'hourly') && (startDateSurvey > axisDate)) ? 'inactive' : ''}`} onClick={() => setPrevPage()}>Prev</button>
													</OverlayTrigger>
													{(trendFilter === 'hourly') && <span className="hourly_date">{trendHourlyDate}</span>}
													<OverlayTrigger overlay={<Tooltip>Next</Tooltip>}>
														<button type="button" className={`chart-next ${currentPage.current === 1 && 'inactive'}`} onClick={() => setNextPage()}>Next</button>
													</OverlayTrigger>
												</div>
											</div>
											<div>
												{(trendSum === 0) ?
													<div className="insight-no-data">
														<span className="no-data-display">No data to display for the selected time period.</span>
													</div>
													:
													((trendData && trendData.labels && trendData.scores && trendData.option_labels) &&
														<ApexMixedChart
															height='300'
															yTitle='Responses'
															yOppsiteTitle={props.avgScore ? 'AVG' : reportLabel}
															barWidth='90%'
															ReportName={reportLabel}
															Name={trendData.labels}
															FirstBarData={groupBarFirst}
															SecondBarData={groupBarSecond}
															ThirdBarData={groupBarThird}
															LineData={trendData.scores}
															xAxisLabels={trendData.option_labels}
															colors={TREND_CHART_COLOR}
															lineColor={TREND_LINE_COLOR}
															minCountScale={countScale.min}
															maxCountScale={countScale.max}
															minScoreScale={scoreScale.min}
															maxScoreScale={scoreScale.max}
														/>
													)

												}
											</div>
										</div>
									</Tab.Pane>
									<Tab.Pane eventKey="reasons" mountOnEnter unmountOnExit>
										<div className="tab-chart-body chart-box-card-body_c reasons-progress-bar-wrap overflow-scroll">
											{
												Array.isArray(themeScore) && themeScore ?
													<div className="score-progress-bar_c">
														<div className="progress-bar-legend">
															<label className="drivers">Drivers</label>
															<label className="overall">Overall ({reportLabel})</label>
															<label className="draggers">Draggers</label>
														</div>
														<ul className="theme-bar-list">
															{
																themeScore.map((item, id) =>
																	<li className="theme-bar-item" key={id}>
																		<p className="theme-bar-name">{item.label} </p>
																		<div className="theme-bar-progress">
																			<ProgressBar className={item.class} label={item.score[1]} min={item.score[0]} max={item.score[1] === 0 ? 0 : item.score[2]} now={(item.score[1] === 0) ? 0 : item.score[1]} />
																		</div>
																	</li>
																)
															}
														</ul>
													</div>
													:
													<span className="no-data-display">Please select the themes in survey to load the data</span>

											}



										</div>

									</Tab.Pane>

									<Tab.Pane eventKey="sentiments" mountOnEnter unmountOnExit>
										<div className="tab-chart-body">

											<div className="word-count-select">
												<span className="word-count-label">Word Count :</span>
												{/* <select className="distribution-select" onChange={(e) => setWordCount(e.target.value)} defaultValue={'40'}>
													<option value="10">10</option>
													<option value="20">20</option>
													<option value="30">30</option>
													<option value="40">40</option>
													<option value="50">50</option>
												</select> */}
												<Select
													defaultValue={selectedDefaultWordCount}
													options={wordCountOptions}
													onChange={handlewordChange}
													className="select-change-image senti-filter"
												/>
											</div>
											<div className="sentiment-trending-wrapper">
												<div className="sentiment-inner">

													<div className={`word-cloud-wrapper ${!keyPhrase.length && "no-word"}`}>
														{(!keyPhrase.length) ?
															<span className="no-word-exist">No data to display for the selected time period.</span>
															:
															<ReactWordcloud callbacks={wordCloudCallbacks} words={keyPhrase} options={wordOptions} maxWords={wordCount} />
														}
													</div>
												</div>
											</div>
										</div>
									</Tab.Pane>
								</Tab.Content>
							</Tab.Container>
						</div>
					</div>
				</div>
				{(tabKey === "sentiments") &&
					<React.Fragment>
						<div ref={el => props.screenShotRefVal.current[6] = el}>
							<SentimentComponent searchWordCloud={wordCloudSearch} setWordCloudSearch={() => setWordCloudSearch("")} onQuestionChange={(questionId) => setQuestionId(questionId)} callbackSentiment={(filter) => commentFilterHandle(filter)} />
						</div>
					</React.Fragment>
				}
			</section>
			<InfoModal show={infoModal} imageFlag={props.avgScore} onHide={() => setInfoModal(false)} />
		</React.Fragment>
	)
}
const mapStateToProps = (state) => {
	return {
		screenShotRefVal: state.report.screenShotRef,
	};
};
export default connect(mapStateToProps, null)(InsightComponent);