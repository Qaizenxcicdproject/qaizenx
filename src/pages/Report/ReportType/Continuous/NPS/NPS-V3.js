import React, { useState, useContext, useEffect } from 'react';
import { Dropdown } from 'react-bootstrap';
import ReportContext from 'pages/Report/ReportContext';
import InsightComponent from './Insight';
import ThemeAnalysis from 'pages/Report/ReportType/ReportMenuComponent/ThemeAnalysis';
import ReportTypeAnalysis from 'pages/Report/ReportType/ReportMenuComponent/StockAnalysis';
import StatisticalAnalysis from 'pages/Report/ReportType/ReportMenuComponent/StatisticalAnalysis';
import RespondentReport from 'pages/Report/ReportType/ReportMenuComponent/RespondentReport/RespondentReport';
import ResponseRate from 'pages/Report/ReportType/ReportMenuComponent/ResponseRate';
import DownloadReport from 'pages/Report/ReportType/ReportMenuComponent/DownloadReport/DownloadReport';
import HeaderInfoModal from 'pages/Report/ShareComponent/HeaderInfoModal';
import ReportHeaderCard from 'pages/Report/ShareComponent/ReportHeaderCard';
import { toast } from "react-toastify";
import AppContext from 'store/AppContext';
import Axios from "utility/Axios";
import configURL from 'config/config';

const NPSComponent = (props) => {

	const { EziLoader } = useContext(AppContext);
	const { filterData, reportLabel, datefilter, addonDate, switchPage: switchPageData, menuTitle: menuTitleData, dayActive, reportType, dayLabel, timezone, roleExist, surveyId } = useContext(ReportContext);
	const [toggleState, setToggleState] = useState(false);
	const [switchPage, setSwitchPage] = useState(switchPageData);
	const [menuTitle, setMenuTitle] = useState(menuTitleData);
	const [infoModal, setInfoModal] = useState(false);
	const [cardCount, setCardCount] = useState({});

	const handleSwitchPage = (str, title) => {
		setSwitchPage(str);
		setMenuTitle(title);
	}

	const handleToggle = () => {
		setToggleState(!toggleState);
	}

	/**
	 *  Header Card Count listing
	 */
	const HeaderCardListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		if (dayActive !== "manual") {
			formData.append("addon_date_filter", addonDate);
		}
		Axios.post((dayActive !== "manual") ? configURL.ReportHistoryDetails : configURL.ReportHeaderCardCount, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setCardCount(response.data.result);
			} else {
				toast.warn(response.data.message);
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}


	useEffect(HeaderCardListing, [filterData, datefilter, addonDate])

	return (
		<React.Fragment>

			{/* ----------------  Menu Section Start ------------------------- */}
			<div className="analysis-menu-wrap">
				<Dropdown className="analysis-menu">
					<Dropdown.Toggle id="dropdown-basic"> {menuTitle} </Dropdown.Toggle>
					<button className="header-info-btn" onClick={() => setInfoModal(true)}></button>
					<Dropdown.Menu>
						<Dropdown.Item><div className="analysis-menu-list insight" title="Insights" onClick={(e) => handleSwitchPage('insights', e.target.title)}>Insights</div></Dropdown.Item>
						<Dropdown.Item><div className="analysis-menu-list analysis" title="NPS Analysis" onClick={(e) => handleSwitchPage('analysis', e.target.title)} >NPS Analysis</div></Dropdown.Item>
						<Dropdown.Item><div className="analysis-menu-list themes" title="Themes Analysis" onClick={(e) => handleSwitchPage('themes', e.target.title)} >Themes Analysis</div></Dropdown.Item>
						<Dropdown.Item><div className="analysis-menu-list statistics" title="Statistical Analysis" onClick={(e) => handleSwitchPage('statistics', e.target.title)} >Statistical Analysis</div></Dropdown.Item>
						<Dropdown.Item><div className="analysis-menu-list response" title="Response Rate" onClick={(e) => handleSwitchPage('response', e.target.title)} >Response Rate</div></Dropdown.Item>
						<Dropdown.Item><div className="analysis-menu-list respondent" title="Respondent Report" onClick={(e) => handleSwitchPage('respondent', e.target.title)}>Respondent Report</div></Dropdown.Item>
						{roleExist && <Dropdown.Item><div className="analysis-menu-list download-rp" title="Download Reports" onClick={(e) => handleSwitchPage('download', e.target.title)}>Download Reports</div></Dropdown.Item>}
					</Dropdown.Menu>
				</Dropdown>
				{(switchPage === 'insights' || switchPage === 'analysis' || switchPage === 'segment' || switchPage === 'themes') &&
					<div className={`ezi-tabular-toggle ${toggleState ? "on" : "off"}`} data-label={toggleState ? "AVG" : "NPS"} data-placeholder={toggleState ? "NPS" : "AVG"} onClick={handleToggle} > </div>
				}
			</div>
			{/* ----------------  Menu Section End ------------------------- */}

			{(switchPage === 'insights') &&
				<React.Fragment>
					{cardCount &&
						<ReportHeaderCard
							version={reportType}
							title={reportLabel}
							titleScore={cardCount.score}
							scoreTitle={`Latest ${reportLabel} ${toggleState ? '(Avg)' : ''}`}
							latestScore={toggleState ? (cardCount.today_average) : (cardCount.today_score)}
							scoreDate={cardCount.latest_date}
							avgTitle={`${reportLabel} Average`}
							average={cardCount.average}
							responsesTitle="Total Responses"
							totalResponse={cardCount.total_respondents}
							dayActive={dayActive}
							diffScore={cardCount.diff_score}
							sinceText={dayLabel}
							diffAverage={cardCount.diff_average}
							diffRespondents={cardCount.diff_total_respondents}
							diffScorePercent={cardCount.diff_score_percent}
							diffAveragePercent={cardCount.diff_average_percent}
							diffTotalRespondentsPercent={cardCount.diff_total_respondents_percent}

						/>
					}
				</React.Fragment>
			}


			<div className={`aside-content-wrap ${switchPage}-wrapper_c`}>
				{(() => {
					switch (switchPage) {
						case 'insights':
							return (<InsightComponent avgScore={toggleState} />);
						case 'analysis':
							return (<ReportTypeAnalysis avgScore={toggleState} menuTitle={switchPage} />);
						case 'themes':
							return (<ThemeAnalysis avgScore={toggleState} />);
						case 'statistics':
							return (<StatisticalAnalysis />);
						case 'response':
							return (<ResponseRate />);
						case 'respondent':
							return (<RespondentReport />);
						case 'download':
							return (<DownloadReport />);
						default:
							return null
					}
				})()}
			</div>
			<HeaderInfoModal
				show={infoModal}
				title={menuTitle}
				renderModal={switchPage}
				onHide={() => setInfoModal(false)}
				reportLabel={reportLabel}
			/>

		</React.Fragment>
	)
}

export default NPSComponent;