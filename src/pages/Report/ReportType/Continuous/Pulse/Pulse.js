import React, { useState, useContext, useEffect } from 'react';
import { Dropdown } from 'react-bootstrap';
import ReportContext from 'pages/Report/ReportContext';
import PulseInsight from './PulseInsight';
import ThemeAnalysis from 'pages/Report/ReportType/ReportMenuComponent/ThemeAnalysis';
import ReportTypeAnalysis from 'pages/Report/ReportType/ReportMenuComponent/StockAnalysis';
import BenchmarkAnalysis from 'pages/Report/ReportType/ReportMenuComponent/BenchmarkAnalysis';
import StatisticalAnalysis from 'pages/Report/ReportType/ReportMenuComponent/StatisticalAnalysis';
import ResponseRate from 'pages/Report/ReportType/ReportMenuComponent/ResponseRate';
import DownloadReport from 'pages/Report/ReportType/ReportMenuComponent/DownloadReport/DownloadReport';
import ReportHeaderCard from 'pages/Report/ShareComponent/ReportHeaderCard';
import { toast } from "react-toastify";
import AppContext from 'store/AppContext';
import Axios from "utility/Axios";
import configURL from 'config/config';

const PulseComponent = (props) => {
    const { EziLoader } = useContext(AppContext);
    const { filterData, datefilter, addonDate, switchPage: switchPageData, menuTitle: menuTitleData, dayActive, reportType, dayLabel, timezone, roleExist, surveyId } = useContext(ReportContext);
    const [switchPage, setSwitchPage] = useState(switchPageData);
    const [menuTitle, setMenuTitle] = useState(menuTitleData);
    const [cardCount, setCardCount] = useState({});

    const handleSwitchPage = (str, title) => {
        setSwitchPage(str);
        setMenuTitle(title);
    }

    /**
     *  Header Card Count listing
     */
    const HeaderCardListing = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("filters", JSON.stringify(filterData));
        formData.append("date", JSON.stringify(datefilter));
        formData.append("timezone", timezone);
        if (dayActive !== "manual") {
            formData.append("addon_date_filter", addonDate);
        }
        Axios.post((dayActive !== "manual") ? configURL.ReportHistoryDetails : configURL.ReportHeaderCardCount, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                setCardCount(response.data.result);
            } else {
                toast.warn(response.data.message);
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err);
        })
    }

    useEffect(HeaderCardListing, [filterData, datefilter, addonDate])

    return (
        <React.Fragment>
            <div className="analysis-menu-wrap">
                <Dropdown className="analysis-menu">
                    <Dropdown.Toggle id="dropdown-basic"> {menuTitle} </Dropdown.Toggle>
                    <Dropdown.Menu>
                        <Dropdown.Item><div className="analysis-menu-list insight" title="Insights" onClick={(e) => handleSwitchPage('insights', e.target.title)}>Insights</div></Dropdown.Item>
                        <Dropdown.Item><div className="analysis-menu-list analysis" title="Engagement Analysis" onClick={(e) => handleSwitchPage('analysis', e.target.title)} >Engagement Analysis</div></Dropdown.Item>
                        <Dropdown.Item><div className="analysis-menu-list themes" title="Themes Analysis" onClick={(e) => handleSwitchPage('themes', e.target.title)} >Themes Analysis</div></Dropdown.Item>
                        <Dropdown.Item><div className="analysis-menu-list benchmark" title="Benchmark Analysis" onClick={(e) => handleSwitchPage('benchmark', e.target.title)} >Benchmark Analysis</div></Dropdown.Item>
                        <Dropdown.Item><div className="analysis-menu-list statistics" title="Statistical Analysis" onClick={(e) => handleSwitchPage('statistics', e.target.title)} >Statistical Analysis</div></Dropdown.Item>
                        <Dropdown.Item><div className="analysis-menu-list response" title="Response Rate" onClick={(e) => handleSwitchPage('response', e.target.title)} >Response Rate</div></Dropdown.Item>
                        {roleExist && <Dropdown.Item><div className="analysis-menu-list download-rp" title="Download Reports" onClick={(e) => handleSwitchPage('download', e.target.title)}>Download Reports</div></Dropdown.Item>}
                    </Dropdown.Menu>
                </Dropdown>
                {/* {(switchPage === 'insights' || switchPage === 'analysis' || switchPage === 'segment' ||  switchPage === 'themes') &&
                    <div className={`ezi-tabular-toggle ${toggleState ? "on" : "off"}`} data-label={toggleState ? "AVG" : "ENGMT"} data-placeholder={toggleState ? "ENGMT" : "AVG"} onClick={handleToggle} > </div>
                } */}
            </div>
            {(switchPage === 'insights') &&
                <React.Fragment>
                    {cardCount &&
                        <ReportHeaderCard
                            version={reportType}
                            title="Engagement Score"
                            titleScore={cardCount.score}
                            scoreTitle="Latest Engagement"
                            latestScore={cardCount.today_score}
                            scoreDate={cardCount.latest_date}
                            avgTitle="Engagement Average"
                            average={cardCount.average}
                            responsesTitle="Total Responses"
                            totalResponse={cardCount.total_respondents}
                            dayActive={dayActive}
                            diffScore={cardCount.diff_score}
                            sinceText={dayLabel}
                            diffAverage={cardCount.diff_average}
                            diffRespondents={cardCount.diff_total_respondents}
                            diffScorePercent={cardCount.diff_score_percent}
                            diffAveragePercent={cardCount.diff_average_percent}
                            diffTotalRespondentsPercent={cardCount.diff_total_respondents_percent}


                        />
                    }
                </React.Fragment>
            }
            <div className={`aside-content-wrap ${switchPage}-wrapper_c`}>
                {(() => {
                    switch (switchPage) {
                        case 'insights':
                            return (<PulseInsight />);
                        case 'analysis':
                            return (<ReportTypeAnalysis menuTitle={switchPage} />);
                        case 'themes':
                            return (<ThemeAnalysis />);
                        case 'benchmark':
                            return (<BenchmarkAnalysis />);
                        case 'statistics':
                            return (<StatisticalAnalysis />);
                        case 'response':
                            return (<ResponseRate />);
                        case 'download':
                            return (<DownloadReport />);
                        default:
                            return null
                    }
                })()}
            </div>
        </React.Fragment>
    )
}

export default PulseComponent;