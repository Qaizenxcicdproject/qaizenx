
import React from 'react';
import { Modal } from 'react-bootstrap';

const InfoModal = (props) => {
   
    return(           
        <React.Fragment>

            <Modal {...props}  size="lg" aria-labelledby="ezi-modal-for" centered className="ezi-modal-wrapper" >
                <Modal.Header className="ezi-modal-header">
                    <Modal.Title id="ezi-modal-for" className="ezi-modal-title-wrap" >
                        <span className="ezi-modal-header-title">Wellness Index</span>
                        <span className="ezi-modal-close"  onClick={props.onHide}></span>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="info-modal-img-wrap">
                        <img alt="" src={require(`../../../../../assets/images/info_images/wellness.png`)}  />
                    </div>
                </Modal.Body>
            </Modal>

        </React.Fragment>
    )
}

export default InfoModal;