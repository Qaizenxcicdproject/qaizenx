import React, { useState, useEffect, useContext } from 'react';
import AppContext from 'store/AppContext';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { ProgressBar, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { toast } from "react-toastify";
import { confirmAlert } from 'react-confirm-alert';
import ReportContext from 'pages/Report/ReportContext';
import { connect } from "react-redux";

const ReportTypeAnalysis = (props) => {
    const { filterData, reportVersion, datefilter, timezone, surveyId, reportLabel } = useContext(ReportContext);
    const { EziLoader } = useContext(AppContext)
    const [benchmarks, setBenchmarks] = useState(null);
    const [benchmarksName, setBenchmarksName] = useState([]);
    const [themes, setThemes] = useState(null);
    const [themesName, setThemesName] = useState([]);
    const [selectedThemeID, setSelectedThemeID] = useState(null);
    const [benchmarkID, setBenchmarkID] = useState(null);
    const [benchmarkList, setBenchmarkList] = useState([]);
    // const [suggestion, setSuggestion] = useState("");
    // const suggRef = useRef()
    // const [showHistory, setShowHistory] = useState(null);

    /**
     * Handle Theme dropdown 
     */
    const handleThemesDropdown = (e) => {
        let ele = e.target.value;
        if (ele === null || ele === "") {
            confirmAlert({
                title: 'Theme Selection !',
                message: 'Please Select Themes.',
                buttons: [{ label: 'Ok' }]
            });
            return;
        };
        setThemes(ele || null)
        if (ele === "OVERALL") {
            setSelectedThemeID("OVERALL");
        }
        else {
            setSelectedThemeID(JSON.parse(ele).id)
        }
    }

    /**
     * Handle Benchmark dropdown 
     */
    const handleBenchmarkDropdown = (e) => {
        let ele = e.target.value;
        if (ele === null || ele === "") {
            confirmAlert({
                title: 'Benchmark Selection !',
                message: 'Please Select Benchmark.',
                buttons: [{ label: 'Ok' }]
            });
            return;
        };
        setBenchmarks(ele || null)
        setBenchmarkID(JSON.parse(ele).id);
    }



    /**
     * Dropdown Themes listing
     */
    const themeListing = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("report_type", reportVersion);
        Axios.post(configURL.ReportThemeDropdwonListing, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                setThemesName(response.data.result);
            } else {
                toast.warn(response.data.message || "Something went wrong")
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err);
        })
    }

    /**
     * Benchmark dropdown listing
     */
    const benchmarkListing = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("date", JSON.stringify(datefilter));
        formData.append("timezone", timezone);
        formData.append("calculation_type", (props.avgScore ? "average" : "count"));
        formData.append("roundDecimalPoint", "2");
        formData.append("report_type", reportVersion);
        formData.append("filters", JSON.stringify(filterData));
        Axios.post(configURL.ReportBenchmarkList, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                setBenchmarksName(response.data.result);
            } else {
                toast.warn(response.data.message || "Something went wrong")
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err);
        })
    }

    /**
     *  Benchmark Score Listing
     */
    const selectedBenchmarkListing = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("theme_id", selectedThemeID);
        formData.append("date", JSON.stringify(datefilter));
        formData.append("timezone", timezone);
        formData.append("calculation_type", (props.avgScore ? "average" : "count"));
        formData.append("roundDecimalPoint", "2");
        formData.append("report_type", reportVersion);
        formData.append("filters", JSON.stringify(filterData));
        formData.append("benchmark_id", benchmarkID);
        Axios.post(configURL.ReportBenchmarkScore, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                setBenchmarkList(response.data.result.questions);

            } else {
                toast.warn(response.data.message || "Something went wrong")
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err);
        })
    }

    // const showRecommendation = (id,label) => {        
    //     if (id === showHistory) {
    //         setShowHistory(null);
    //         return;
    //     }
    //     setShowHistory(id);
    //     EziLoader.show();
    //     let formData = new FormData();
    //     formData.append("survey_id",surveyId); 
    //     formData.append("theme_id",label);
    //     formData.append("date",JSON.stringify(datefilter)); 
    //     formData.append("timezone",timezone);
    //     formData.append("calculation_type",(props.avgScore?"average":"count"));
    //     formData.append("roundDecimalPoint", "2");
    //     formData.append("report_type",reportVersion);
    //     formData.append("filters", JSON.stringify(filterData));        
    //     formData.append("benchmark_id",benchmarkID);        
    //     Axios.post(configURL.ReportBenchmarkSuggestions, formData).then(response => {
    //         EziLoader.hide();
    //         if (response.data.success !== undefined && response.data.success) {
    //             setSuggestion(response.data.result.suggestions)
    //             // suggRef.current.scrollIntoView({ behavior: 'smooth' })

    //         } else {
    //             toast.warn(response.data.message || "Something went wrong")
    //         }
    //     }).catch(err => {
    //         EziLoader.hide()
    //         console.log(err);
    //     })
    // }

    // const handleRecommendation = (id) => {
    //     EziLoader.show();
    //     let formData = new FormData();
    //     formData.append("survey_id",surveyId); 
    //     formData.append("theme_id",id);
    //     formData.append("date",JSON.stringify(datefilter)); 
    //     formData.append("timezone",timezone);
    //     formData.append("calculation_type",(props.avgScore?"average":"count"));
    //     formData.append("roundDecimalPoint", "2");
    //     formData.append("report_type",reportVersion);
    //     formData.append("filters", JSON.stringify(filterData));        
    //     formData.append("benchmark_id",benchmarkID);        
    //     Axios.post(configURL.ReportBenchmarkSuggestions, formData).then(response => {
    //         EziLoader.hide();
    //         if (response.data.success !== undefined && response.data.success) {
    //             setSuggestion(response.data.result.suggestions)
    //             suggRef.current.scrollIntoView({ behavior: 'smooth' })

    //         } else {
    //             toast.warn(response.data.message || "Something went wrong")
    //         }
    //     }).catch(err => {
    //         EziLoader.hide()
    //         console.log(err);
    //     })

    // }


    /**
     * Default load dropdown demographyListing and theme listing
     */
    useEffect(() => {
        themeListing();
        benchmarkListing();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    /**
     *  On the basis of theme slider range change
     */
    useEffect(() => {
        if (selectedThemeID !== null && benchmarkID !== null) {
            selectedBenchmarkListing();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedThemeID, benchmarkID, datefilter, filterData]);



    return (
        <React.Fragment>
            <section className="Page-TypeAnalysis Page-BenchmarkReport" ref={el => props.screenShotRefVal.current[0] = el}>

                {(Array.isArray(benchmarksName) && benchmarksName.length) ?
                    <div className="select-demography-wrap identifier-slider-wrap" >
                        <div className="select-demography-inner">
                            <label>Analyze Metric</label>
                            <select className="column-select" onChange={(e) => handleThemesDropdown(e)} value={themes || ''}>
                                <option value="" >Select</option>
                                <option value="OVERALL" >Overall ( {reportLabel} )</option>
                                {themesName && themesName.map((key, id) =>
                                    <option value={JSON.stringify(key)} key={id}>{key.label}</option>
                                )}
                            </select>

                        </div>

                        <div className="select-demography-inner">
                            <label>with benchmark</label>
                            <select className="column-select" onChange={(e) => handleBenchmarkDropdown(e)} value={benchmarks || ''}>
                                <option value="">Select</option>
                                {benchmarksName && benchmarksName.map((key, id) =>
                                    <option value={JSON.stringify(key)} key={id}>{key.display_title}</option>
                                )}
                            </select>
                        </div>

                    </div>
                    :
                    <div className="analysis-blank-demography">
                        <span>No data to be displayed, please ask administrator to assign benchmark.</span>
                    </div>

                }




                {(selectedThemeID !== null && benchmarkID !== null) &&

                    <React.Fragment>
                        <div className="response-table-wrap">
                            <table className="download-report-table">
                                <thead className="ezi_custom_table-row category-table-heading">
                                    <tr>
                                        <th>Sr no</th>
                                        <th className="txt-left">{selectedThemeID === "OVERALL" ? "Dimension" : "Statement"}</th>
                                        <th className="comaparison-text">Score Benchmark Comparison</th>
                                        <th>Difference</th>
                                        {/* {(selectedThemeID === 'OVERALL') && <th>Recommendation</th>} */}
                                    </tr>
                                </thead>
                                <tbody>
                                    {benchmarkList && benchmarkList.map((item, index) =>
                                        <React.Fragment key={item.id}>
                                            <tr key={item.id} className="ezi_custom_table-row">
                                                <td data-title="Sr no">{index + 1}</td>
                                                <td data-title="Statement" className={`statement-text ${(selectedThemeID === 'OVERALL') && 'wa'}`}>{item.label || "--"}</td>
                                                <td data-title="Score Benchmark Comparison" className="benchmark-progress" >
                                                    <div className="d-flex progress-main-wrapper">
                                                        <OverlayTrigger overlay={<Tooltip>{`Benchmark Score : ${item.benchmark_score[1] || 0}`}</Tooltip>}>
                                                            <span className="bench-score-value">{item.benchmark_score[1] || 0}</span>
                                                        </OverlayTrigger>

                                                        <div className="benchmark-comparison-wrap">
                                                            <OverlayTrigger overlay={<Tooltip>{`Benchmark Score : ${item.benchmark_score[1] || 0}`}</Tooltip>}>
                                                                <ProgressBar className="benchmark-bscore-progress" min={0} max={100} now={item.benchmark_score[1] === 0 ? 0 : item.benchmark_score[1]} />
                                                            </OverlayTrigger>
                                                            <OverlayTrigger placement="left" overlay={<Tooltip>{`Company Score : ${item.benchmark_score[0] || 0}`}</Tooltip>}>
                                                                <ProgressBar className={`benchmark-score-progress ${item.benchmark_score[0] > item.benchmark_score[1] ? 'greenish' : ''}`} label={item.benchmark_score[0]} min={0} max={100} now={item.benchmark_score[0] === 0 ? 0 : item.benchmark_score[0]} />
                                                            </OverlayTrigger>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td data-title="Difference">
                                                    <span className={`arrow-benchmark ${(item.benchmark_score[2] > 0) ? 'up' : (item.benchmark_score[2] < 0) && 'down'}`}>
                                                        {item.benchmark_score[2] || "0"}
                                                    </span>
                                                </td>
                                                {/* {(selectedThemeID === 'OVERALL') && <th><button className="recommend-btn" type="button" onClick={() => showRecommendation(item.id,item.label)} ><span className="recommend-ic"></span></button></th>} */}
                                            </tr>
                                            {/* {(item.id === showHistory) &&
                                            <tr>
                                                <td colSpan="5" className="colspan-td" >
                                                    <div className="recommendation-list" dangerouslySetInnerHTML={{__html: suggestion}} />
                                                </td>
                                            </tr>
                                        } */}
                                        </React.Fragment>
                                    )}
                                </tbody>
                            </table>
                        </div>

                        {/* {((selectedThemeID === 'OVERALL') && suggestion) && <div ref={suggRef} className="des-card">
                            <h1 className="rec-heading">Recommendation</h1>
                            <div dangerouslySetInnerHTML={{__html: suggestion}} /> */}
                        {/* Warning: Setting HTML from code is risky because it’s easy to 
                            inadvertently expose your users to a cross-site scripting (XSS) attack. */}
                        {/* </div>} */}

                    </React.Fragment>
                }

            </section>
        </React.Fragment>
    )
}
const mapStateToProps = (state) => {
    return {
        screenShotRefVal: state.report.screenShotRef,
    };
};
export default connect(mapStateToProps, null)(ReportTypeAnalysis);


