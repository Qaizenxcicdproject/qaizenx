import React, { useState, useEffect, useContext } from 'react';
import { useParams } from "react-router-dom";
import AppContext from 'store/AppContext';
import Axios from "utility/Axios";
import configURL from 'config/config';
import InputRange from 'react-input-range';
import { toast } from "react-toastify";
import { confirmAlert } from 'react-confirm-alert';
import { Accordion, Card } from "react-bootstrap";
import { useAccordionToggle } from 'react-bootstrap/AccordionToggle';
import AccordionContext from 'react-bootstrap/AccordionContext';
import ApexColumnChart from 'pages/Report/ChartComponent/ApexColumnChart';
import ApexHorizontalChart from 'pages/Report/ChartComponent/ApexHorizontalChart';
import ApexHorizontalRevChart from 'pages/Report/ChartComponent/ApexHorizontalRevChart';
import SentimentComponent from 'pages/Report/ShareComponent/Sentiment';
import ReactApexChart from 'react-apexcharts';
import { MATERIAL_COLOR_CODE } from "constants/constants";

const ReportTypeAnalysis = (props) => {
    const [surveyData] = useState(props.data.surveyQuestionData);
    const urlParams = useParams();
    const appStore = JSON.parse(localStorage.getItem("appState"));
    const timezone = appStore.user.timezone;
    const { EziLoader } = useContext(AppContext)
    const [surveyId] = useState(urlParams.survey_id)
    const [demographics, setDemographics] = useState(null);
    const [demographicsName, setDemographicsName] = useState([]);
    const [idetifier, setIdetifier] = useState("");
    const [themes, setThemes] = useState(null);
    const [themesName, setThemesName] = useState([]);
    const [themesDropdownLabel, setThemesDropdownLabel] = useState("");
    const [insightData] = useState(surveyData.insight);
    const [rangeValue, setRangeValue] = useState({ value: 10, });
    const [reportScore, setReportScore] = useState([]);
    const [reportColor, setReportColor] = useState({});
    const [reportLabel, setReportLabel] = useState([]);
    const [sliderData, setSliderData] = useState([]);
    const [toggleState, setToggleState] = useState(false);
    const [dragging, setDragging] = useState({});
    const [performing, setPerforming] = useState({});
    const [performingPercent, setPerformingPercent] = useState([]);
    const [draggingPercent, setDraggingPercent] = useState([]);
    const [addonFilter, setAddonFilter] = useState([]);
    const [idetifierRange, setIdetifierRange] = useState();
    const [selectedThemeID, setSelectedThemeID] = useState(null);
    const [selectedColorID, setSelectedColorID] = useState(null);
    const [selectedData, setSelectedData] = useState({ identifier_id: null, identifier_type: null, slider_value: null, theme_id: null, identifier_name: null, theme_name: null, addon_filter: null });
    const [reasonData, setReasonData] = useState({});
    const [reasonMood, setReasonMood] = useState([]);
    const [percentAdd, setPercentAdd] = useState([]);
    const [reasonSelected, setReasonSelected] = useState([]);

    function CustomToggle({ children, eventKey, callback }) {
        const currentEventKey = useContext(AccordionContext);
        const decoratedOnClick = useAccordionToggle(eventKey, () => callback && callback(eventKey),);
        const isCurrentEventKey = currentEventKey === eventKey;
        return (
            <div type="button" className="acc-title" onClick={decoratedOnClick} > {children} <span className={`acc-icon ${isCurrentEventKey && 'up-icon'}`}></span></div>
        );
    }

    /**
    * Handle Metric dropdown 
    */
    const handleIdentifier = (e) => {
        let ele = e.target.value;
        setThemes(ele || null)
        if (ele === null || ele === "") {
            confirmAlert({
                title: 'Theme Selection !',
                message: 'Please Select Themes.',
                buttons: [{ label: 'Ok' }]
            });
            return;
        };
        if (ele === "-1") {
            setSelectedColorID("-1");
        }
        else {
            setSelectedColorID(JSON.parse(ele).id)
        }
    }


    /**
     * Handle Demography dropdown ans slider scale through go button
     */
    const handleDemography = () => {
        if (demographics === null || themes === null) {
            confirmAlert({
                title: 'Identifier & Theme Selection !',
                message: 'Please Select Identifiers and Themes.',
                buttons: [{ label: 'Ok' }]
            });
            return;
        };
        setIdetifier(JSON.parse(demographics).label);
        setIdetifierRange(rangeValue.value);
        setSelectedThemeID(JSON.parse(themes).id)
        if (themes === "-1") {
            setSelectedThemeID("-1");
            setThemesDropdownLabel("Overall");
        }
        else {
            setSelectedThemeID(JSON.parse(themes).id);
            setThemesDropdownLabel(JSON.parse(themes).label);
        }
        setSelectedData({
            identifier_id: JSON.parse(demographics).name,
            identifier_type: JSON.parse(demographics).type,
            slider_value: rangeValue.value || null,
            theme_id: JSON.parse(themes).id || "-1",
            identifier_name: JSON.parse(demographics).label || null,
            theme_name: JSON.parse(themes).label || "Overall",
        });
    }

    /**
     * @get positive horizonatal chart data
     */
    const handleCallback = (name) => {
        if (name) {
            setAddonFilter([{
                demographic_id: JSON.parse(demographics).name || "",
                demographic_type: JSON.parse(demographics).type || "",
                value: [name]
            }])
        }
        else {
            setAddonFilter([])
        }

        let filterData = {};
        if (name) {
            filterData = {
                ...selectedData,
                addon_filter: [{
                    demographic_id: JSON.parse(demographics).name || "",
                    demographic_type: JSON.parse(demographics).type || "",
                    value: [name]
                }]
            }
        }
        else {
            filterData = { ...selectedData, addon_filter: [] }
        }
        setSelectedData(filterData)

    }



    /**
     * Dropdown identifier listing
     */
    const demographyListing = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("report_type", props.data.reportVersion);
        formData.append("type", "both");
        Axios.post(configURL.ReportFrontBackDemographic, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                setDemographicsName(response.data.result);
            } else {
                toast.warn(response.data.message || "Something went wrong")
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err);
        })
    }

    /**
     * Dropdown Themes listing
     */
    const themeListing = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("report_type", props.data.reportVersion);
        Axios.post(configURL.ReportThemeDropdwonListing, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                setThemesName(response.data.result);
            } else {
                toast.warn(response.data.message || "Something went wrong")
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err);
        })
    }

    /**
     * Slider color listing
     */
    const sliderColor = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("theme_id", selectedColorID);
        formData.append("date", JSON.stringify(props.data.datefilter));
        formData.append("timezone", timezone);
        formData.append("calculation_type", (props.avgScore ? "average" : "count"));
        formData.append("report_type", props.data.reportVersion);
        formData.append("filters", JSON.stringify(props.data.filterData));
        Axios.post(configURL.ReportPostSliderColor, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                setSliderData(response.data.result.data);
                setRangeValue({ value: response.data.result.data[1] })

            } else {
                toast.warn(response.data.message || "Something went wrong")
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err);
        })
    }

    /**
     * Score column chart bar listing
     */
    const reportListing = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("demographic_id", JSON.parse(demographics).name);
        formData.append("demographic_type", JSON.parse(demographics).type);
        formData.append("sliderValue", rangeValue.value);
        formData.append("roundDecimalPoint", "2");
        formData.append("theme_id", selectedThemeID);
        formData.append("date", JSON.stringify(props.data.datefilter));
        formData.append("timezone", timezone);
        formData.append("calculation_type", (props.avgScore ? "average" : "count"));
        formData.append("report_type", props.data.reportVersion);
        formData.append("filters", JSON.stringify(props.data.filterData));
        Axios.post(configURL.ReportDemographicOptionsScores, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                setReportScore(response.data.result.score);
                setReportColor(response.data.result);
                setReportLabel(response.data.result.label);
            }
            else {
                setToggleState(false);
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err);
        })
    }

    /**
     * Performing & Dragging identifier listing
     */
    const performingDraggingIdentifierListing = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("demographic_id", JSON.parse(demographics).name);
        formData.append("demographic_type", JSON.parse(demographics).type);
        formData.append("sliderValue", rangeValue.value);
        formData.append("roundDecimalPoint", "2");
        formData.append("theme_id", selectedThemeID);
        formData.append("date", JSON.stringify(props.data.datefilter));
        formData.append("timezone", timezone);
        formData.append("calculation_type", (props.avgScore ? "average" : "count"));
        formData.append("report_type", props.data.reportVersion);
        formData.append("filters", JSON.stringify(props.data.filterData));
        Axios.post(configURL.ReportDemographicOptionsPerformances, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                setDragging(response.data.result.dragging);
                setPerforming(response.data.result.performing);
                setPerformingPercent(response.data.result.performing.score_percent);
                setDraggingPercent(response.data.result.dragging.score_percent);
            }
            else {
                toast.warn(response.data.message || "Something went wrong")
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err);
        })
    }



    const setReasonFilter = (filter) => {
        setReasonSelected(filter);
    }

    /**
     *  reason donut chart listing
     */
    const reasonListing = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("question_type", insightData.question_type);
        formData.append("question_id", insightData.id);
        formData.append("filters", JSON.stringify(props.data.filterData));
        formData.append("date", JSON.stringify(props.data.datefilter));
        formData.append("timezone", timezone);
        formData.append("filter", reasonSelected);
        formData.append("demographic_id", JSON.parse(demographics).name);
        formData.append("demographic_type", JSON.parse(demographics).type);
        formData.append("addon_filter", addonFilter ? JSON.stringify(addonFilter) : JSON.stringify([]));
        Axios.post(`${configURL.ReportInsightReason}/${insightData.question_type.toLowerCase()}`, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                setReasonData(response.data.result);
                setReasonMood(response.data.mood);
                let addPer = response.data.result.percentage;
                let addValue = addPer.reduce((acc, val) => acc + val, 0);
                setPercentAdd(addValue);

            } else {
                toast.warn(response.data.message);
            }
        })
    }

    /**
     *  reason  chart 
     */
    const reasonDataDoughnut = {
        series: reasonData.percentage ? reasonData.percentage : [],
        options: {
            chart: { type: 'donut', },
            plotOptions: { pie: { donut: { size: '45%' } } },
            stroke: { show: false },
            dataLabels: {
                enabled: true, style: {
                    fontSize: '12px',
                    fontFamily: 'NunitoSansSemiBold',
                    fontWeight: '600',
                    colors: ['#FFFFFF']
                },
                dropShadow: { enabled: false, },
            },
            colors: MATERIAL_COLOR_CODE,
            labels: reasonData.lables ? reasonData.lables : [],
            noData: {
                text: "No data to display for the selected time period",
                align: 'center',
                verticalAlign: 'middle',
                offsetX: 0,
                offsetY: 0,
                style: { color: "#716779", fontSize: '13px', fontFamily: "NunitoSansSemiBold" }
            },
            legend: {
                formatter: function (seriesName, opts) {
                    let seriesVal = opts.w.globals.series[opts.seriesIndex];
                    let seriesAdd = opts.w.globals.series.reduce((a, b) => a + b, 0);
                    let seriesPer = (seriesVal / seriesAdd) * 100;
                    return [" ", Math.round(seriesPer * 10) / 10 + "%", " : ", seriesName]
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: { width: 200 },
                    legend: { position: 'bottom' }
                },

            }]
        },
    }
    useEffect(() => {
        demographyListing();
        themeListing();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        sliderColor();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.avgScore, selectedColorID]);

    useEffect(() => {
        if (demographics !== null) {
            reasonListing();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [addonFilter, reasonSelected]);

    useEffect(() => {
        if (demographics !== null) {
            setToggleState(true);
            reportListing();
            performingDraggingIdentifierListing();
            reasonListing();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [idetifier, themesDropdownLabel, idetifierRange, props.data.datefilter, props.data.filterData, props.avgScore]);

    return (
        <React.Fragment>
            <section className="Page-TypeAnalysis">

                <div className="select-demography-wrap identifier-slider-wrap">
                    <div className="select-demography-inner">
                        <label>Analyze Metric</label>
                        <select className="column-select" onChange={(e) => handleIdentifier(e)} value={themes || ''}>
                            <option value="" >Select</option>
                            <option value="-1" >Overall ( {props.data.reportLabel} )</option>
                            {themesName && themesName.map((key, id) =>
                                <option value={JSON.stringify(key)} key={id}>{key.label}</option>
                            )}
                        </select>

                    </div>
                    <div className="color-slider-scale">
                        <label>on a scale</label>
                        <InputRange
                            step={props.avgScore ? 0.5 : 1}
                            maxValue={sliderData && sliderData[0]}
                            minValue={sliderData && sliderData[2]}
                            value={rangeValue.value}
                            onChange={value => setRangeValue({ value })}
                        />
                    </div>
                    <div className="select-demography-inner">
                        <label>with Identifier</label>
                        <select className="column-select" onChange={(e) => setDemographics(e.target.value || null)} value={demographics || ''}>
                            <option value="">Select</option>
                            {demographicsName && demographicsName.map((key, id) =>
                                <option value={JSON.stringify(key)} key={id}>{key.label}</option>
                            )}
                        </select>
                    </div>

                    <button type="button" className="select-demography-btn" onClick={() => handleDemography()} >Go</button>
                </div>

                {(toggleState) &&

                    <React.Fragment>

                        <div className="chart-box-card_c card-flex car-bot-mar">
                            <div className="chart-box-card-header_c">
                                <label>{(themesDropdownLabel === 'Overall') ? `Overall ( ${props.data.reportLabel} )` : themesDropdownLabel} Analysis</label>
                            </div>
                            <div className="chart-box-card-body_c">
                                {reportScore ?
                                    <ApexColumnChart
                                        tooltipName="Score"
                                        data={reportScore}
                                        labels={reportLabel}
                                        colors={reportColor.colors}
                                        barLength='70%'
                                        yAxisTitle={`${themesDropdownLabel} Score`}
                                        height="300"
                                    />
                                    :
                                    <span className="no-data-display">No data to display for the selected time period.</span>
                                }
                            </div>
                        </div>


                        <div className="d-flex align-items-start card-margin-return">
                            <div className="chart-box-card_c card-flex card-margin">
                                <div className="chart-box-card-header_c">
                                    <label>Top Performing Identifiers</label>
                                </div>
                                <div className="chart-box-card-body_c">
                                    {(performing && performing.score)
                                        ?
                                        <ApexHorizontalChart
                                            data={performingPercent}
                                            xAxislabels={performing && performing.score_label}
                                            colors="#A9D18E"
                                            tooltipPercent={performing && performing.score}
                                            barLength={performing && performing.score}
                                            xAxisTitle={`${themesDropdownLabel} Percentage Contribution to ${props.data.reportLabel}`}
                                            height="300"
                                            reportLabel={themesDropdownLabel}
                                            parentCallback={handleCallback}
                                        />
                                        :
                                        <span className="no-data-display">No data to display for the selected time period.</span>
                                    }
                                </div>
                            </div>
                            <div className="chart-box-card_c card-flex card-margin">
                                <div className="chart-box-card-header_c">
                                    <label>Top Dragging Identifiers</label>
                                </div>
                                <div className="chart-box-card-body_c">
                                    {(dragging && dragging.score)
                                        ?
                                        <ApexHorizontalRevChart
                                            data={draggingPercent}
                                            xAxislabels={dragging && dragging.score_label}
                                            colors="#FF6464"
                                            tooltipPercent={dragging && dragging.score}
                                            barLength={dragging && dragging.score}
                                            xAxisTitle={`${themesDropdownLabel} Percentage Contribution to ${props.data.reportLabel}`}
                                            height="300"
                                            reportLabel={themesDropdownLabel}
                                            parentCallback={handleCallback}
                                        />
                                        :
                                        <span className="no-data-display">No data to display for the selected time period.</span>
                                    }
                                </div>
                            </div>
                        </div>

                        <div className="report-accordian car-top-mar trend-bg">
                            <Accordion>

                                <Card>
                                    <Card.Header>
                                        <CustomToggle eventKey="0">Theme Analysis : Overall ({props.data.reportLabel})</CustomToggle>
                                    </Card.Header>
                                    <Accordion.Collapse eventKey="0">
                                        <Card.Body>

                                            <div className="reasons-header-wrapper">
                                                <select className="distribution-select" defaultValue={reasonSelected} onChange={(e) => setReasonFilter(e.target.value)}>
                                                    {reasonMood && reasonMood.map((key, id) =>
                                                        <option value={key.value} key={id}>{key.name}</option>
                                                    )}
                                                </select>
                                            </div>

                                            <div className="reasons-data-chart">
                                                {
                                                    (percentAdd === 0)
                                                        ?
                                                        <span className="no-data-display">No data to display for the selected time period.</span>
                                                        :
                                                        <React.Fragment>
                                                            <ReactApexChart options={reasonDataDoughnut.options} series={reasonDataDoughnut.series} type="donut" height={270} />
                                                        </React.Fragment>

                                                }
                                            </div>
                                        </Card.Body>
                                    </Accordion.Collapse>
                                </Card>


                                <Card>
                                    <Card.Header>
                                        <CustomToggle eventKey="1">Identifier Sentiments</CustomToggle>
                                    </Card.Header>
                                    <Accordion.Collapse eventKey="1">
                                        <Card.Body>
                                            <SentimentComponent data={props.data} selectedData={selectedData} avgScore={props.avgScore ? "average" : "count"} menuTitle={props.menuTitle} />
                                        </Card.Body>
                                    </Accordion.Collapse>
                                </Card>

                            </Accordion>
                        </div>







                    </React.Fragment>
                }
                {(!toggleState) &&
                    <div className="analysis-blank-demography">
                        <span>No data to display for the selected identifier</span>
                    </div>
                }



            </section>
        </React.Fragment>
    )
}

export default ReportTypeAnalysis;


