import React, { useState } from "react";
import { Modal, Spinner } from 'react-bootstrap';
import useForm from 'react-hook-form';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';

function CreateReportModel({ show, onHide, survey_id, report_id ,timezone , date,reportVersion}) {
    const { register, handleSubmit, errors } = useForm();
    const [saving, setSaving] = useState(false)

    const onSubmit = data => {
        setSaving(true)
        let formData = new FormData()
        formData.append("survey_id", survey_id)
        formData.append("name", data.file_name)
        formData.append("extension_type", "xlsx")
        formData.append("report_id", report_id)
        formData.append("date", JSON.stringify(date));
        formData.append("timezone", timezone);
        formData.append("roundDecimalPoint", "2");
        Axios.post(configURL.create_raw_report, formData).then(res => {
            setSaving(false)
            if (res.data.success === true) {
                toast.success(res.data.message || "Request sent. We will notify you one report is generated")
                onHide()
            } else {
                toast.warn(res.data.message || "Something went wrong.")
            }
        }).catch(err => {
            setSaving(false)
            console.log(err)
            toast.warn("Something went wrong.")
        })
    };

    return (

        <Modal show={show} onHide={onHide} size="md" aria-labelledby="contained-modal-title-vcenter" centered className="theme-modal-wrapper" >
            <Modal.Header className="ezi-modal-header">
                <Modal.Title id="contained-modal-title-vcenter" className="theme-modal-title ezi-modal-header-title" >
                    <span className="theme-modal-title-text">Create New Report</span>
                    <span className="ezi-modal-close" onClick={onHide}></span>
                </Modal.Title>
            </Modal.Header>

            <Modal.Body>

                <form onSubmit={handleSubmit(onSubmit)} className="add-theme-form">
                    <div className="theme-field-wrapper">
                        <div className="theme-field-50">
                            <div className="theme-field">
                                <input type="text" className="theme-field-control" name="file_name" ref={register({ required: true })} placeholder="Please Enter Report Name" />
                                {errors.file_name && <span className="theme-error_cu">* Report name is required.</span>}
                            </div>
                        </div>
                        {/* <div className="theme-field">
                            <div className="theme-field">
                                <label>Report Extension</label>
                                <select className="theme-field-control" name="file_extension" ref={register({ required: true })}>
                                    <option value="">Please Select extension</option>
                                    <option value="xlsx">XLSX</option>
                                    {(reportVersion === 'INN') && <option value="pdf">PDF</option>}
                                </select>
                                {errors.file_extension && <span className="theme-error_cu">* Extension type is required.</span>}
                            </div>
                        </div> */}
                    </div>
                    <div className="theme-modal-footer">
                        <button type="button" className="close-theme-btn" onClick={onHide}>Close</button>

                        <button type="submit" className="btn-ripple ezi-pink-btn add-theme-btn" disabled={saving}>
                            Save {saving && <Spinner animation="border" size="sm" />}
                        </button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}


export default CreateReportModel;