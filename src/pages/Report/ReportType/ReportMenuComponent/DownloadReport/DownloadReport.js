import React, { useState, useEffect, useContext } from 'react';
import CreateReportModel from './CreateReportModel';
import CONFIG from "config/config";
import Axios from 'utility/Axios';
import { toast } from 'react-toastify';
import AppContext from "store/AppContext"
import ReportContext from 'pages/Report/ReportContext';
import { connect } from "react-redux";

const DownloadReport = (props) => {

    const { reportVersion, datefilter, timezone, surveyId } = useContext(ReportContext);
    const { EziLoader } = useContext(AppContext)
    const [showHistory, setShowHistory] = useState(null);
    const [selectedReport, setSelectedReport] = useState(null);
    const [showCreateModel, setShowCreateModel] = useState(null);
    const [reportsData, setReportsData] = useState([])
    const viewType = ['innReport', 'htmlReport'];

    // Truncate a string if it is longer than the specified number of characters
    const text_truncate = function (str, length, ending) {
        if (length == null) {
            length = 100;
        }
        if (ending == null) {
            ending = '...';
        }
        if (str.length > length) {
            return str.substring(0, length - ending.length) + ending;
        } else {
            return str;
        }
    }

    const showDownload = (id) => {
        if (id === showHistory) {
            setShowHistory(null);
            return;
        }
        setShowHistory(id);
    }

    const getReportsData = () => {
        EziLoader.show()
        let formData = new FormData()
        formData.append("survey_id", surveyId)
        Axios.post(CONFIG.raw_report_lists, formData).then(res => {
            EziLoader.hide()
            if (res.data.success === true) {
                setReportsData(res.data.result)
            } else {
                toast.warn(res.data.message || "Something went wrong.")
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err)
            toast.warn("Something went wrong.")
        })
    }



    const handleReportDownload = (data) => {
        if (data.status !== "complete") {
            toast.warn("Report is not generated yet.")
            return;
        }
        let formData = new FormData()
        formData.append("survey_id", surveyId)
        formData.append("history_id", data.id)
        Axios.post(CONFIG.download_raw_report, formData).then(res => {
            console.log(res.data);
            if (res.data.success === true) {
                window.open(res.data.url, "_blank")
            } else {
                toast.warn(res.data.message || "Something went wrong.")
            }
        }).catch(err => {
            console.log(err)
            toast.warn("Something went wrong.")
        })

    }

    useEffect(getReportsData, [])

    return (
        <React.Fragment>
            <section className="Page-DownloadReport" ref={el => props.screenShotRefVal.current[0] = el}>
                <CreateReportModel show={showCreateModel}
                    reportVersion={reportVersion} survey_id={surveyId} onHide={() => {
                        setSelectedReport(null)
                        setShowCreateModel(false)
                        getReportsData()

                    }}
                    report_id={selectedReport}
                    timezone={timezone}
                    date={datefilter}
                />
                <table className="download-report-table">
                    <thead className="ezi_custom_table-row category-table-heading">
                        <tr>
                            <th>Sr No.</th>
                            <th>Report Type</th>
                            <th>Description</th>
                            <th>Last Downloaded</th>
                            <th><span className="custom_table-heading-wrap">Action</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            reportsData.map((item, index) => (
                                <React.Fragment key={item.id}>
                                    <tr className={`ezi_custom_table-row  ${item.id === showHistory && 'active'}`}>
                                        <td data-title="Sr No">{index + 1}</td>
                                        <td data-title="Report Type">{item.display_report_name}</td>
                                        <td data-title="Description">{(item.id === showHistory) ? '' : text_truncate(item.description, 40)}</td>
                                        <td data-title="Last Downloaded">{(item.id === showHistory) ? '' : item.downloaded || "NULL"}</td>
                                        <td data-title="Action">
                                            <div className="custom_table_action-wrap">
                                                <button type="button" onClick={() => {
                                                    setSelectedReport(item.id)
                                                    setShowCreateModel(true)

                                                }} className="custom_table_action-btn"> Create Report </button>
                                                <button type="button" className={`download_report_arrow ${item.id === showHistory && 'active'}`} onClick={() => showDownload(item.id)}></button>
                                            </div>
                                        </td>
                                    </tr>
                                    {(item.id === showHistory) &&
                                        <tr>
                                            <td colSpan="5" className="description-download-bg">
                                                <div className="description-download-wrap" >
                                                    <div className="description-section">
                                                        <label>Description</label>
                                                        <span className="des">{item.description}</span>
                                                    </div>
                                                    <div className="download-section">
                                                        <label>Download History</label>
                                                        <ul>
                                                            {
                                                                item.history.map((subitem, subindex) => (
                                                                    <li key={`history${subindex}`}>
                                                                        <span className="title">{subitem.title || ""}</span>
                                                                        <span className="report-format">.{subitem.format || '--'}</span>
                                                                        <button type="button" className={`table-download-btn ${viewType.includes(item.type) ? 'view' : 'dw'}`} onClick={() => handleReportDownload(subitem)}> {viewType.includes(item.type) ? 'View' : 'Download'}</button>
                                                                    </li>
                                                                ))
                                                            }
                                                            {/* <button type="button" className="table-show-more">Show More</button> */}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    }
                                </React.Fragment>
                            ))
                        }
                    </tbody>
                </table>
                {/* <div className="category-table-no-result">No result Found</div> */}
            </section>
        </React.Fragment>
    )
}
const mapStateToProps = (state) => {
    return {
        screenShotRefVal: state.report.screenShotRef,
    };
};
export default connect(mapStateToProps, null)(DownloadReport);