import React, { useState, useEffect, useRef, useContext } from 'react';
import { Tab, Nav } from 'react-bootstrap';
import { useParams } from "react-router-dom";
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from "react-toastify";
import AppContext from 'store/AppContext';
import ReactWordcloud from 'react-wordcloud';
import ApexBarChart from 'pages/Report/ChartComponent/ApexBarChart';
import ApexColumnLineChart from 'pages/Report/ChartComponent/ApexColumnLineChart';
import SentimentComponent from 'pages/Report/ShareComponent/Sentiment';
import { connect } from "react-redux";
import * as AppActions from "store/actions";
import Select from 'react-select';

const QuestionAnalysisComponent = (props) => {
	const [surveyData] = useState(props.QAData);
	const [surveyQuestionData] = useState((surveyData.surveyQuestionData ? surveyData.surveyQuestionData.data : surveyData));
	const urlParams = useParams();
	const [questionData, setQuestionData] = useState(surveyQuestionData[0]);
	const [questionList, setQuestionList] = useState(surveyQuestionData);
	const [key, setKey] = useState(!(surveyQuestionData && surveyQuestionData.question_type === "MULTIPLELINETEXT") ? 'distribution' : 'comments');
	const [surveyId] = useState(urlParams.survey_id);
	const [questionBarData, setQuestionBarData] = useState([]);
	const [questionStatusBarData, setQuestionStatusBarData] = useState([]);
	const [keyPhrase, setKeyPhrase] = useState([]);
	const [distributionData, setDistributionData] = useState([]);
	const [trend, setTrendSum] = useState(0);
	const [KeywordCount, setKeywordCount] = useState([]);
	const [CountsCL, setCountsCL] = useState([]);
	const [KeywordLabel, setKeywordlabel] = useState([]);
	const [trendOptions, setTrendOptions] = useState([]);
	const { EziLoader } = useContext(AppContext)
	let showCommentTab = false;
	let distributionName = useRef();
	const appStore = JSON.parse(localStorage.getItem("appState"));
	const timezone = appStore.user.timezone;
	const [activeClass, setActiveClass] = useState(null);
	const removeQuestion = ['NPS', 'HI', 'CSAT', 'HTML', 'SINGLELINETEXT', 'PERSONALINFO', 'FILE', 'SUB_QUESTION', 'RADIOMATRIX']
	// const questionArray = ['THUMBUPDOWN', 'SINGLELINETEXT', 'YESNO', 'IMAGETYPE', 'STARRATING', 'SLIDERSCALE', 'DATEPICKER'];
	// var questionExist = (questionData?.question_type && questionArray.indexOf(questionData.question_type) > -1);
	let keyword_bar = ["Total Responses"];
	keyword_bar.push(KeywordLabel);

	if (questionList) {
		removeQuestion.forEach(data => {
			let presentIndex = questionList.findIndex(item => item.question_type === data);
			if (presentIndex !== -1) {
				let removedIndex = [...questionList];
				removedIndex.splice(presentIndex, 1);
				setQuestionList(removedIndex);
			}
		});
	}


	// let questionBarDataCount = questionBarData.count;
	const [selectedDefaultWordCount, setSelectedDefaultWordCount] = useState({ value: 40, label: '40' });
	const wordCountOptions = [
		{ value: 10, label: '10' },
		{ value: 20, label: '20' },
		{ value: 30, label: '30' },
		{ value: 40, label: '40' },
		{ value: 50, label: '50' },
	]
	const [selectedTrendFilter, setSelectedTrendFilter] = useState({ value: 'weekly', label: 'Weekly' });
	const trendFilterOptions = [
		// { value: 'hourly', label: 'Hourly' },
		{ value: 'daily', label: 'Daily' },
		{ value: 'weekly', label: 'Weekly' },
		{ value: 'monthly', label: 'Monthly' },
	]
	const [wordCloudSearch, setWordCloudSearch] = useState('');
	const [distributionNameFilter, setDistributionNameFilter] = useState({});
	const [distributiontimeFilter, setDistributiontimeFilter] = useState('weekly');
	let defaultValueTrendFilter = [];

	const handleTrendFilter = (filter) => {
		setDistributiontimeFilter(filter.value);
		setSelectedTrendFilter({ value: filter.value, label: filter.label })
		//setFilter(filter.value, distributionNameFilter);
		distributionListing(filter.value, distributionNameFilter);
	}

	const handleTrendNameFilter = (filter) => {
		setDistributionNameFilter(filter);
		// setFilter(filter.value);
		distributionListing(distributiontimeFilter, filter);
	}

	const pageChange = (item, _) => {
		setDistributionNameFilter({})
		setQuestionData(item);
		setToggle(false)
		setActiveClass(item.id);
	}

	const [wordCount, setWordCount] = useState(40);
	const wordOptions = {
		colors: ["#1D1B28", "#3FD2E6", "#800080", "#718584"],
		enableTooltip: true,
		deterministic: true,
		fontFamily: 'NunitoSansSemiBold',
		fontSizes: [14, 40],
		rotations: 0,

	};

	const wordCloudCallbacks = {
		// getWordTooltip: word => `The word "${word.text}" appears ${word.value} times.`,
		getWordTooltip: word => '',
		onWordClick: word => setWordCloudSearch(word.text)
	};

	// const setFilter = () => {
	// 	distributionListing();
	// }
	const handlewordChange = (data) => {
		setWordCount(data.value)
		setSelectedDefaultWordCount({ value: data.value, label: data.label })
	}
	const questionAnalysisListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("question_type", questionData.question_type);
		formData.append("question_id", questionData.id);
		formData.append("filters", JSON.stringify(props.QAData.filterData ? props.QAData.filterData : props.filterData));
		formData.append("date", JSON.stringify(props.QAData.datefilter ? props.QAData.datefilter : props.dateFilter));
		formData.append("timezone", timezone);
		Axios.post(configURL.ReportAnalysisNewBarChart, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setQuestionBarData(response.data.result.data.analysis_data_formated);
				setQuestionStatusBarData(response.data.result.data.analysis_data);
				setTrendOptions(response.data.result.params);
				let trenOptions = response.data.result.params;
				// defaultValueTrendFilter = trenOptions && trenOptions.filter((key, id) => {
				// 	if (id === 0) {
				// 		return ({ value: JSON.stringify(key), label: key.name })
				// 	}
				// })
				defaultValueTrendFilter = trenOptions && trenOptions.map((key, id) => ({ value: key.value, label: key.name })
				)
				setDistributionNameFilter(defaultValueTrendFilter[0]);

			} else {
				toast.warn(response.data.message);
			}
		})
	}
	const commentCount = () => {
		let formData = new FormData();
		EziLoader.show();
		formData.append("survey_id", surveyId);
		formData.append("question_type", questionData.question_type);
		formData.append("question_id", questionData.id);
		formData.append("filters", JSON.stringify(props.QAData.filterData ? props.QAData.filterData : props.filterData));
		formData.append("date", JSON.stringify(props.QAData.datefilter ? props.QAData.datefilter : props.dateFilter));
		formData.append("timezone", timezone);
		Axios.post(`${configURL.ReportCommentsCount}`, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				// setSentimentCount(response.data.result.percentage)
				const commnetValue = [];
				var dougnutCount = response.data.result.commentcount;
				for (var index in dougnutCount) {
					if (dougnutCount.hasOwnProperty(index)) {
						commnetValue.push(dougnutCount[index]);

					}
				}
				showCommentTab = response.data.success;
				keyPhraseListing();
				// commentListing();
			}

		})
	}
	const keyPhraseListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("question_type", questionData.question_type);
		formData.append("question_id", questionData.id);
		formData.append("filters", JSON.stringify(props.QAData.filterData ? props.QAData.filterData : props.filterData));
		formData.append("date", JSON.stringify(props.QAData.datefilter ? props.QAData.datefilter : props.dateFilter));
		formData.append("timezone", timezone);
		Axios.post(`${configURL.ReportCommentsSentiment}`, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setKeyPhrase(response.data.result);

			} else {
				toast.warn(response.data.message);
			}
		})
	}

	const distributionListing = (disTime, disName) => {
		let formData = new FormData();
		EziLoader.show();
		let distributionFilter = (disTime ? disTime : distributiontimeFilter);
		formData.append("survey_id", surveyId);
		formData.append("filter", distributionFilter);
		formData.append("roundDecimalPoint", 1);
		// formData.append("keyword", (distributionName.current ? JSON.parse(distributionName.current.value).value : ""));
		// formData.append("keyword_name", (distributionName.current ? JSON.parse(distributionName.current.value).name : ""));
		// console.log(Object.getOwnPropertyNames(disName).length !== 0 ? 'test' : distributionNameFilter.value || "");
		formData.append("keyword", (disName && Object.getOwnPropertyNames(disName).length !== 0 ? disName.value : (distributionNameFilter && distributionNameFilter.value) || ""));
		formData.append("keyword_name", (disName && Object.getOwnPropertyNames(disName).length !== 0 ? disName.label : (distributionNameFilter && distributionNameFilter.value) || ""));
		formData.append("question_type", questionData.question_type);
		formData.append("question_id", questionData.id);
		formData.append("filters", JSON.stringify(props.QAData.filterData ? props.QAData.filterData : props.filterData));
		formData.append("date", JSON.stringify(props.QAData.datefilter ? props.QAData.datefilter : props.dateFilter));
		formData.append("timezone", timezone);
		formData.append("calculation_type", "count");
		Axios.post(`${configURL.ReportAnalysisDistribution}`, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				let columnData = response.data.result.keyword_count;
				let arrSum = columnData.concat(response.data.result.counts).flat().reduce((a, b) => a + b, 0);
				setTrendSum(arrSum);
				setKeywordCount(response.data.result.keyword_count);
				setCountsCL(response.data.result.counts);
				setKeywordlabel(response.data.result.keyword);
				setDistributionData(response.data.result);
			} else {
				toast.warn(response.data.message);
			}
		})
	}



	useEffect(() => {
		// eslint-disable-next-line
		distributionName = (distributionName.current ? "" : "");
		setQuestionData(questionData);
		setKey(!(questionData && questionData.question_type === "MULTIPLELINETEXT") ? 'distribution' : 'comments');
		commentCount();

		if (!(questionData && questionData.question_type === "MULTIPLELINETEXT")) {
			questionAnalysisListing();
			distributionListing(distributiontimeFilter, distributionNameFilter);
		}


	}, [questionData, props.QAData.filterData, props.filterData, props.QAData.datefilter, props.dateFilter]);


	const [toggle, setToggle] = useState(false);

	const commentFilterHandle = (filter) => {
		if (filter !== 'Total') {
			keyPhraseListing([{ "label": "sentiment", "options": [filter] }]);
		}
		else {
			keyPhraseListing([]);
		}
	}

	useEffect(() => {
		props.dispatchScreenShotRef({ screenShotRef: props.screenShotRefVal, fullPageScreenShotRef: props.fullPageScreenShotRef });

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [props.screenShotRefVal, distributionData]);
	return (
		<React.Fragment>

			<div className="aside-wrap questionAnalysis">
				<div className="menu-dropdown-wrap">
					{!toggle && <button type="button" className="question-toggle-btn" onClick={() => setToggle(!toggle)}>Questions</button>}
					<div className={`question-toggle-overlay ${toggle ? 'active' : ''}`} onClick={() => setToggle(!toggle)}></div>
				</div>
				<aside className={`report-sidebar ${toggle ? 'active' : ''}`}>
					<div className="question-list-wrap">
						{
							questionList && questionList.map((item, index) =>
								<div className={`question-list ${((index === 0 && activeClass === null) || item.id === activeClass) ? "active" : ""}`} key={`question_number_${index}`} onClick={() => pageChange(item, index)}>
									<span className="question">{(typeof item.title === "object" ? item.title.default : item.title) || item.name} </span>
								</div>

							)
						}
					</div>
				</aside>
			</div>

			<section className="CE-report-content questionAnalysisCE-report">
				<main>
					<section className="CX-QuestionComponent CE-CommentType">



						{!(questionData && questionData.question_type === "MULTIPLELINETEXT") &&
							(
								<React.Fragment>
									<div className="clear-both" ref={el => props.screenShotRefVal.current[0] = el}>
										<div className="question-type-wrap">
											<label className="question">{questionData && (typeof questionData.title === "object" ? questionData.title.default : questionData.title)}</label>
											{/* <div className="question-type checkbox-multi">{questionData && questionData.type} Type Question</div> */}
										</div>
										<div className="ce-chart-box-wrap response-rate-box">
											<div className="ce-chart-box no-shadow">
												<div className="ce-chart-box-header">
													<span className="left-text">Analysis</span>
												</div>
												<div className="">
													<div className="">
														{(questionBarData && Array.isArray(questionBarData.count) && questionBarData.count.length) ?
															<ApexBarChart
																tooltipName="Count"
																data={questionBarData && questionBarData.count}
																labels={questionBarData && questionBarData.lables}
																barWidth="40%"
																height="320"
																colors={questionBarData && questionBarData.colors}
															/> :
															<div className="insight-no-data">
																<span className="no-data-display">No data to display for the selected time period.</span>
															</div>
														}
													</div>
												</div>
											</div>
										</div>
										<div className="bar-status-wrapper">
											{questionStatusBarData && questionStatusBarData.map((item, index) =>
												<div className="bar-status-row" key={`bar_status_${index}`}>
													<span style={{ backgroundColor: item.color }} className="bar-status-color"></span>
													<span style={{ color: item.color }} className="bar-status-title">{item.name}</span>
													<div className="bar-status-right-content">
														<span style={{ color: item.color }} className="bar-status-percentage">{item.percentage}%</span>
														<span style={{ color: item.color }} className="bar-status-value">{item.count}</span>
														<span style={{ backgroundColor: item.color }} className="bar-status-color"></span>
													</div>
												</div>
											)}
										</div>
									</div>
								</React.Fragment>
							)
						}

						<div className="clear-both" ref={el => props.screenShotRefVal.current[1] = el}>
							{(showCommentTab || (questionData && questionData.question_type === "MULTIPLELINETEXT")) && <div className="question-type-wrap">
								<label className="question">{questionData && (typeof questionData.title === "object" ? questionData.title.default : questionData.title)}</label>
								{/* <div className="question-type checkbox-multi">{questionData && questionData.type} Type Question</div> */}
							</div>}
							<div className="question-comment-tab no-shadow" >
								<div className="tablist_ezi">
									<Tab.Container activeKey={key} onSelect={(k) => setKey(k)}  >
										<div className="tab-header-wrap">
											<div className="tab-left-header">
												<Nav variant="pills" >
													{!(questionData && questionData.question_type === "MULTIPLELINETEXT") &&
														<Nav.Item>
															<Nav.Link eventKey="distribution">Trend</Nav.Link>
														</Nav.Item>
													}
													{(showCommentTab || (questionData && questionData.question_type === "MULTIPLELINETEXT")) &&
														<Nav.Item>
															<Nav.Link eventKey="comments">Comments</Nav.Link>
														</Nav.Item>
													}
												</Nav>
											</div>
										</div>
										<Tab.Content>
											{!(questionData && questionData.question_type === "MULTIPLELINETEXT") &&
												<Tab.Pane eventKey="distribution" className="question-tab-body question-distribution-body" mountOnEnter unmountOnExit>

													<div className="question-distribution-chart-wrap">
														<div className="question-distribution-chart">
															<div className="question-distribution-select-wrap">
																<div className="dist-lebel-select">
																	<label>Trend :</label>
																	{/* <select className="distribution-select" onChange={(e) => setFilter(e.target.value)} ref={distributionName}>
																		{trendOptions && trendOptions.map((key, id) =>
																			<option value={JSON.stringify(key)} key={id}>{key.name}</option>
																		)}
																	</select> */}
																	{(trendOptions && distributionNameFilter) && <Select
																		onChange={handleTrendNameFilter}
																		className="select-change-image trend-filter"
																		// ref={distributionName}
																		value={distributionNameFilter}
																		//defaultValue={distributionNameFilter} 
																		options={trendOptions && trendOptions.map((data, id) => ({ value: data.value, label: data.name })
																		)}

																	/>}
																</div>
																{/* <select className="distribution-select" onChange={(e) => setFilter(e.target.value)} ref={distributionTime} defaultValue='weekly'>
																	<option value="hourly">Hourly</option>
																	<option value="daily">Daily</option>
																	<option value="weekly">Weekly</option>
																	<option value="monthly">Monthly</option>
																</select> */}
																<Select
																	defaultValue={selectedTrendFilter}
																	options={trendFilterOptions}
																	onChange={handleTrendFilter}
																	className="select-change-image trend-filter"
																/>
															</div>
														</div>
														<div className="question-distribution-chart-body">
															{(trend === 0) ?


																<div className="insight-no-data">
																	<span className="no-data-display">No data to display for the selected time period.</span>
																</div>
																: <ApexColumnLineChart
																	height='300'
																	yTitle='Total Responsness'
																	yOppsiteTitle='Count'
																	Name={keyword_bar}
																	BarData={CountsCL}
																	LineData={KeywordCount}
																	xAxisLabels={distributionData.lables}
																	colors={["#ef5350", "#ec407a"]}
																	lineColor={["#ef5350", "#ec407a"]}
																	minScoreScale={0}
																	maxScoreScale={Math.max(...KeywordCount)}
																	minCountScale={0}
																	maxCountScale={Math.max(...CountsCL)}
																/>


															}

														</div>
													</div>

												</Tab.Pane >
											}
											<Tab.Pane eventKey="comments" className="question-tab-body question-comments-body" mountOnEnter unmountOnExit>

												<div className="comment-chart-component">

													<div className="ce-chart-content word-cloud-content">
														<div className="hortalzontal-line-box">
															<div className="ce-chart-box no-shadow no-border">
																<div className="ce-chart-box-header word-count-select-header">
																	<span className="left-text">Trending Keywords : </span>
																	<div className="word-count-select">
																		<span className="word-count-label">Word Count :</span>
																		{/* <select className="distribution-select" onChange={(e) => setWordCount(e.target.value)} defaultValue={'40'}>
																			<option value="10">10</option>
																			<option value="20">20</option>
																			<option value="30">30</option>
																			<option value="40">40</option>
																			<option value="50">50</option>
																		</select> */}
																		<Select
																			defaultValue={selectedDefaultWordCount}
																			options={wordCountOptions}
																			onChange={handlewordChange}
																			className="select-change-image"
																		/>
																	</div>
																</div>
																<div className="hortalzontal-line-box-body ">
																	<div className="word-cloud-wrapper">
																		<ReactWordcloud callbacks={wordCloudCallbacks} words={keyPhrase} options={wordOptions} maxWords={wordCount} />
																	</div>
																</div>

															</div>
														</div>
													</div>
												</div>

											</Tab.Pane>
										</Tab.Content>
									</Tab.Container>
								</div>
							</div>
						</div>

						{
							(key === "comments") && <React.Fragment>
								<div className="clear-both" ref={el => props.screenShotRefVal.current[2] = el}>
									<SentimentComponent searchWordCloud={wordCloudSearch} setWordCloudSearch={() => setWordCloudSearch("")} callbackSentiment={(filter) => commentFilterHandle(filter)} />
								</div>

							</React.Fragment>
						}

					</section >
				</main >
			</section >


		</React.Fragment >
	)


}
const mapStateToProps = (state) => {
	return {
		screenShotRefVal: state.report.screenShotRef,
		fullPageScreenShotRef: state.report.fullPageScreenShotRef,
	};
};
const mapDispatchToProps = (dispatch) => {
	return {
		dispatchScreenShotRef: (data) => dispatch(AppActions.reportScreenShot(data)),
	};
};
export default connect(mapStateToProps, mapDispatchToProps)(QuestionAnalysisComponent);