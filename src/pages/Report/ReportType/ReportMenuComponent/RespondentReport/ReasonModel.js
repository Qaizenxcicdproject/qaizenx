import React from "react";
import { Modal } from 'react-bootstrap';

const ReasonModel = (props) => {
    
    return (
        <Modal {...props} size="md" aria-labelledby="contained-modal-title-vcenter" centered className="theme-modal-wrapper" scrollable={true}>
            <Modal.Header className="ezi-modal-header">
                <Modal.Title id="contained-modal-title-vcenter" className="theme-modal-title ezi-modal-header-title" >
                    <span className="theme-modal-title-text">Questions Response</span>
                    <span className="ezi-modal-close" onClick={props.onHide}></span>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <React.Fragment>
                    {
                        Array.isArray(props.reasonList) && props.reasonList
                            ?                               
                                props.reasonList.map((item,index)=>
                                    <div className="ind-reasons-list-wrapper" key={index}>
                                        <p className="list-item-que" index={index + 1}>{item.question}</p>
                                        <span className="list-item-ans">{item.answer || '--'}</span>
                                    </div>
                                )                               
                            :
                        <span>no reason found</span>
                    }
                </React.Fragment>
            </Modal.Body>
        </Modal>
    );
}

export default ReasonModel