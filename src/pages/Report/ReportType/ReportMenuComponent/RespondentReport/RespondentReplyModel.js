import React, { useState , useContext, useEffect } from "react";
import { Modal, Spinner } from 'react-bootstrap';
import useForm from 'react-hook-form';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';
import { useParams } from "react-router-dom";
import AppContext from 'store/AppContext';
import ReportContext from 'pages/Report/ReportContext';

const SentimentReplyModel = ({ show, onHide, respondent_data, survey_name }) => {
    const { register, handleSubmit, errors } = useForm();
    const urlParams = useParams();
    const { EziLoader } = useContext(AppContext);
    const reportContextData = useContext(ReportContext);
    const [saving, setSaving] = useState(false);
    const [commentData,setCommentData] = useState([]);
    const [showReply, setShowReply] = useState(false);
    const [comments , setComments] = useState("");


    /**
     * Sentiment comment listing
     */
    const sentimentListing = () => {   
        let formData = new FormData();
        formData.append("survey_id", urlParams.survey_id);   
        formData.append("report_type",reportContextData.reportVersion);
        formData.append("respondent_id", respondent_data.respondent_id)
        formData.append("search","");
        formData.append("comment_filters",[]); 
        formData.append("filters",JSON.stringify(reportContextData.filterData));
        formData.append("date",JSON.stringify(reportContextData.datefilter));
        formData.append("timezone",reportContextData.timezone);
        formData.append("current_page",1);   
        Axios.post(configURL.ReportPostComments, formData).then(res => {
            EziLoader.hide();
            if (res.data.success !== undefined && res.data.success) {   
                setCommentData(res.data.result.data); 
                setComments(res.data.result.data[0].comments); 
            }
            else {
                toast.warn(res.data.message || "Something went wrong")
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err);
        }) 
    }


    const onSubmit = data => {
        setSaving(true)
        let formData = new FormData()
        formData.append("survey_id", urlParams.survey_id)
        formData.append("respondent_id", respondent_data.respondent_id)
        formData.append("reply_msg", data.reply_msg)
        formData.append("reply_subject", data.reply_subject)
        formData.append("comment", comments)
        Axios.post(configURL.reply_survey_comment, formData).then(res => {
            setSaving(false)
            if (res.data.success === true) {
                toast.success(res.data.message || `Reply email sent to ${respondent_data.fname || 'User'} ${respondent_data.lname || ''}`)
                onHide()
            } else {
                toast.warn(res.data.message || "Something went wrong.")
            }
        }).catch(err => {
            setSaving(false)
            console.log(err)
            toast.warn("Something went wrong.")
        })
    };

    
    useEffect(sentimentListing,[])

    return (
        <Modal show={show} onHide={onHide} size="md" aria-labelledby="contained-modal-title-vcenter" centered className="theme-modal-wrapper" >
            <Modal.Header className="ezi-modal-header">
                <Modal.Title id="contained-modal-title-vcenter" className="theme-modal-title ezi-modal-header-title" >
                    <span className="theme-modal-title-text">Comments </span>
                    <span className="ezi-modal-close" onClick={onHide}></span>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <React.Fragment>
                    {
                        commentData.length > 0 ? commentData.map((item)=>
                            <div className="individual-comment-list" key={item.respondent_id}>
                                <label>{item.comments}</label>
                                <div className="individual-comment-badge-wrap">
                                        {item.key_phrases && item.key_phrases.map((key)=>  
                                        <span className="individual-comment-badge">{key.Text}</span>
                                        )}
                                    </div>
                                <div className="individual-comment-info">
                                    <span className="individual-comment-name">{item.respondent_fname + ' ' + item.respondent_lname}</span>
                                    <span className="individual-comment-date">{item.sentiment_created_at} </span>
                                    <span className="individual-comment-reply" onClick={() => setShowReply(!showReply)}>{showReply ? "Hide" : "Reply" }</span>
                                </div>
                            </div>
                        ) :  <span className="comment-not-found">Comments Not Found</span>
                    }
                    
                { showReply &&
                <form onSubmit={handleSubmit(onSubmit)} className="add-theme-form">
                    <div className="theme-field-wrapper individual-comment-form-wrap">
                        <div className="theme-field">
                            <div className="theme-field">
                                <input type="text" className="theme-field-control" name="reply_subject" ref={register({ required: true })} defaultValue={`RE: ${survey_name}`} placeholder="Please enter reply subject" />
                                {errors.reply_subject && <span className="theme-error_cu">* Reply subject is required.</span>}
                            </div>
                        </div>
                        <div className="theme-field">
                            <div className="theme-field">
                                <textarea className="theme-field-control area" name="reply_msg" ref={register({ required: true })} rows={6} placeholder="Please enter reply message"></textarea>
                                {errors.reply_msg && <span className="theme-error_cu">* Reply message is required.</span>}
                            </div>
                        </div>
                    </div>
                    <div className="theme-modal-footer">
                        <button type="button" className="close-theme-btn" onClick={onHide}>Close</button>
                        <button type="submit" className="btn-ripple ezi-pink-btn add-theme-btn" disabled={saving}>
                            Reply {saving && <Spinner animation="border" size="sm" />}
                        </button>
                    </div>
                </form>
                }
                </React.Fragment>
            </Modal.Body>
        </Modal>
    );
}

export default SentimentReplyModel