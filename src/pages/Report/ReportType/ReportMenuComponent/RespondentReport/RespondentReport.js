import React, { useState, useEffect, useContext, useRef } from 'react';
import Axios from 'utility/Axios';
import { toast } from 'react-toastify';
import AppContext from "store/AppContext"
import configURL from 'config/config';
import SweetSearch from 'components/SweetSearch';
import Pagination from "react-js-pagination";
import IndividualReplyModel from "./RespondentReplyModel";
import ReasonModel from "./ReasonModel";
import ReportContext from "pages/Report/ReportContext";
import { connect } from "react-redux";

const IndividualReport = (props) => {
	const { filterData, datefilter, reportVersion, timezone, surveyId, is_contain_files: containFile, surveyQuestionData: { name: survey_name } } = useContext(ReportContext)
	const { EziLoader } = useContext(AppContext)
	const [reportsData, setReportsData] = useState([])
	const inputSearch = useRef(null);
	const [searchLoading, setSearchLoading] = useState(false);
	const [searchValue, setSearchValue] = useState("");
	var searchTimer = null;
	const [reasons, setReasons] = useState([]);
	const [reasonHeader, setReasonHeader] = useState([]);
	const [tablePagination, setTablePagination] = useState({});
	const [showReply, setShowReply] = useState(false)
	const [respondentData, setRespondentData] = useState({ respondent_id: null, fname: null, lname: null })
	const [reasonModel, setReasonModel] = useState(false);
	const [sortFlag, setSortFlag] = useState(false);
	const [key, setKey] = useState({ name: '', sortBy: '', mood: '' });
	const [columnName, setColumnName] = useState("");
	const [active, setActive] = useState(false);
	const currentPage = useRef(1);

	const hideReplyModel = () => {
		setShowReply(false)
		setRespondentData({ respondent_id: null, fname: null, lname: null })
	}

	/**
	 * Handle View Report
	 * @param {object} - Respondent ID , Respondent First Name , Respondent Last Name
	 */
	const handleViewReport = (item) => {
		setShowReply(true)
		setRespondentData({ respondent_id: item.respondent_id, fname: item.respondent_fname, lname: item.respondent_lname })
	}

	/**
	* Handle Reasons
	* @param {string} - Respondent ID
	* @param {number} - olap survey response id
	*/
	const handleRespondentID = (res_id, olap_id) => {
		overallReasonListing(res_id, olap_id)
	}

	/**
	 * Handle Pagination
	 * @param {number} - PageNumber
	 */
	const handlePaginate = (pageNumber) => {
		currentPage.current = pageNumber;
		getReportsData(key.name, sortFlag ? 'DESC' : 'ASC', key.mood, searchValue)
	}

	/**
	 * Handle comment search
	 */
	const handleCommentSearch = () => {
		clearTimeout(searchTimer);
		searchTimer = setTimeout(() => {
			setSearchLoading(true)
			let searchVal = inputSearch.current.value;
			setSearchValue(searchVal)
			currentPage.current = 1;
			getReportsData(key.name, sortFlag ? 'DESC' : 'ASC', key.mood, searchVal)
		}, 800);
	}

	/**
	 * Handle Table Data Ascending and Descending Order
	 * @param {string} - Respondent Type Name
	 * @param {string} - Mood positive , negative , neutral
	 */
	const sortByColumn = (label, sentiment = key.mood) => {
		setColumnName(label)
		if ('sentiment_score' === label) {
			currentPage.current = 1;
			setKey({ name: label, mood: sentiment, sortBy: '' })
			getReportsData(label, '', sentiment, searchValue)
			return;
		}
		if (columnName === label) {
			setSortFlag(!sortFlag)
			getReportsData(label, sortFlag ? 'ASC' : 'DESC', sentiment, searchValue)
			setActive(false)
		} else {
			setActive(true)
			setSortFlag(false)
			getReportsData(label, 'ASC', sentiment, searchValue)
		}
		setKey({ name: label, mood: sentiment, sortBy: sortFlag ? 'ASC' : 'DESC' })
	}

	/**
	 * @get overall report data listing
	 * @param {string} - Respondent Type Name
	 * @param {string} - Ascending / Descending
	 * @param {string} - Mood positive , negative , neutral
	 * @param {string} - Search value 
	 */
	const getReportsData = (col = key.name, order = key.sortBy, sentiment = key.mood, searchVal = searchValue) => {
		EziLoader.show()
		let formData = new FormData()
		formData.append("survey_id", surveyId)
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		formData.append("report_type", reportVersion);
		formData.append("search", searchVal);
		formData.append("current_page", currentPage.current);
		formData.append("sentiment_score", true);
		formData.append("score", true);
		formData.append("sortByColumn", col);
		formData.append("sortOrder", order);
		formData.append("sentimentFilter", sentiment);
		if (containFile)
			formData.append("is_download", 1);
		Axios.post(configURL.ReportRespondentsList, formData).then(res => {
			EziLoader.hide()
			setSearchLoading(false);
			if (res.data.success !== undefined && res.data.success) {
				setReportsData(res.data.result.data)
				setReasonHeader(res.data.result.header)
				setTablePagination(res.data.pagination);
			} else {
				toast.warn(res.data.message || "Something went wrong.")
			}
		}).catch(err => {
			EziLoader.hide()
			setSearchLoading(false);
			console.log(err)
		})
	}

	/**
	 * Overall Reason listing
	 * @param {string} - Respondent ID
	 * @param {number} - olap survey response id
	 */
	const overallReasonListing = (res_id, olap_id) => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("respondent_id", res_id);
		formData.append("olap_survey_response_id", olap_id);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		Axios.post(configURL.ReportIndividualResponse, formData).then(res => {
			EziLoader.hide();
			if (res.data.success !== undefined && res.data.success) {
				setReasons(res.data.result);
				setReasonModel(true);
			} else {
				toast.warn(res.data.message || "Something went wrong.")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err)
		})
	}

	useEffect(() => {
		getReportsData(key.name, key.sortBy, key.mood)
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [datefilter, filterData])

	return (
		<React.Fragment>

			<section className="Page-IndividualReport" ref={el => props.screenShotRefVal.current[0] = el}>
				{reportsData &&
					<div className="individual-table-wrap">
						<SweetSearch loading={searchLoading} change={handleCommentSearch} ref={inputSearch} />
						<table className="download-report-table">
							<thead className="ezi_custom_table-row category-table-heading">
								<tr>
									{
										reasonHeader.map((item, index) =>
											<th key={`header${index}`}>
												<div className="sort-label-wrap">
													{item.label}
													{['respondent_fname', 'score', 'response_date'].includes(item.type) &&
														<button className={`sort-btn ${(((item.type === columnName) && (key.sortBy === 'ASC')) || ((item.type === columnName) && (active === true))) ? 'active' : ''}`} onClick={() => sortByColumn(item.type)}></button>
													}
												</div>
											</th>
										)
									}
									<th>
										<div className="sentiment-wrap">
											<p>Sentiment</p>
											<div className="sentiment-inner">
												<button className="sort-mood-btn" onClick={() => sortByColumn('sentiment_score', 'POSITIVE')}></button>
												<button className="sort-mood-btn" onClick={() => sortByColumn('sentiment_score', 'NEGATIVE')}></button>
												<button className="sort-mood-btn" onClick={() => sortByColumn('sentiment_score', 'NEUTRAL')}></button>
											</div>
										</div>
									</th>
									<th><span className="custom_table-heading-wrap">Reason</span></th>
									{containFile && <th>Action</th>}
								</tr>
							</thead>
							<tbody>
								{
									reportsData.map((item, index) => (
										<React.Fragment key={`tbody${index}`}>
											<tr className="ezi_custom_table-row">
												<td data-title="Name">{`${item.respondent_fname || ''} ${item.respondent_lname || ''}`}</td>
												<td data-title="Mobile No">{item.respondent_contact_number ? item.respondent_contact_number : "--"}</td>
												<td data-title="Email">{item.respondent_email ? item.respondent_email : "--"}</td>
												<td data-title="SAT Index">{item.score ? item.score : "0"}</td>
												<td data-title="Date">{item.response_date}</td>
												<td data-title="Sentiment"><span className={`sentiment ${item.sentiment_score && item.sentiment_score.toLowerCase()}`} onClick={() => handleViewReport(item)}>{item.sentiment_score ? item.sentiment_score : "--"}</span></td>
												<td data-title="Reason">
													<div className="custom_table_action-wrap">
														<button className="view_report_ic" onClick={() => handleRespondentID(item.respondent_id, item.olap_survey_response_id)}></button>
													</div>
												</td>
												{containFile &&
													<td data-title="Action">
														<a href={`${item.file}`} rel="noopener noreferrer" target="_blank" className="view_report">Download</a>
													</td>
												}
											</tr>
										</React.Fragment>
									))
								}
							</tbody>
						</table>
					</div>
				}
				{showReply && <IndividualReplyModel show={showReply} onHide={hideReplyModel} respondent_data={respondentData} survey_name={survey_name} />}
				{reasonModel && <ReasonModel show={reasonModel} onHide={() => setReasonModel(false)} reasonList={reasons} /* response_flag={responseFlag} */ />}

				{!reportsData.length && <div className="no-data-section"><span>No Result Found</span></div>}

				{(tablePagination && tablePagination.current_page && tablePagination.total) &&
					<div className="pagination-plugin-wrap">
						<Pagination
							activePage={tablePagination.current_page}
							itemsCountPerPage={10}
							totalItemsCount={tablePagination.total}
							onChange={handlePaginate}
							hideDisabled={true}
							firstPageText={<span className="prev-page-text-ic"></span>}
							lastPageText={<span className="next-page-text-ic"></span>}
							nextPageText={<span className="next-text-ic"></span>}
							prevPageText={<span className="prev-text-ic"></span>}
						/>
					</div>
				}

			</section>

		</React.Fragment>
	)
}
const mapStateToProps = (state) => {
	return {
		screenShotRefVal: state.report.screenShotRef,
	};
};
export default connect(mapStateToProps, null)(IndividualReport);