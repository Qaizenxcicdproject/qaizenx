import React, { useState, useEffect, useContext, Fragment } from 'react';
import { toast } from "react-toastify";
import AppContext from 'store/AppContext';
import Axios from "utility/Axios";
import configURL from 'config/config';
import Select from 'react-select';
import { AiFillPlusCircle, AiFillMinusCircle } from 'react-icons/ai'
import './Responserate.scss';
import { confirmAlert } from 'react-confirm-alert';
import { CSVLink } from "react-csv";
import ReportContext from 'pages/Report/ReportContext';
import makeAnimated from 'react-select/animated';
import ResponseRateTree from './ResponseRateTree';
import { RESPONSE_REPORT_TREE, DEFAULT_ALL_REMOVE } from '../../../../constants/constants';
import { connect } from "react-redux";


const ResponseRate = (props) => {

	const { EziLoader } = useContext(AppContext);
	const { filterData, datefilter, timezone, surveyId, reportVersion } = useContext(ReportContext);
	const [demographicsName, setDemographicsName] = useState([]);
	const [demographicsNameTree, setDemographicsNameTree] = useState([]);
	const [selectedDemographics, setSelectedDemographics] = useState([]);
	const animatedComponents = makeAnimated();
	// const [demographicLevel, setDemographicLevel] = useState(0);
	const [toggleList, setToggleList] = useState([])
	const [toggleData, setToggleData] = useState([]);
	const [filledNodes, setFilledNodes] = useState([]);
	const [responseTrackerDataTree, setResponseTrackerDataTree] = useState([]);
	const [demographicsHeader, setDemographicsHeader] = useState([]);
	const [demographics, setDemographics] = useState(null);
	const [idetifier, setIdetifier] = useState("");
	const [demographicResponse, setDemographicResponse] = useState([]);
	const today = new Date().toJSON().slice(0, 10).replace(/-/g, '');
	const response_tree = RESPONSE_REPORT_TREE;
	const defaulAllRemove = DEFAULT_ALL_REMOVE;
	let demographicLevel = 0;
	const [demographicsNewHeader, setDemographicsNewHeader] = useState([]);
	const [demographicNewResponse, setDemographicNewResponse] = useState([]);
	const [defaultSelection, setDefaultSelection] = useState(true);
	let defaultdemoGraphic = [{ "demographic_id": "all", "option_id": -1, "demographic_type": "backend" }];
	/**
		 * Handle Demography dropdown through go button
		 */
	const handleDemography = () => {
		if (demographics === null) {
			confirmAlert({
				title: 'Identifier Selection !',
				message: 'Please Select Identifiers',
				buttons: [{ label: 'Ok' }]
			});
			return;
		};
		setIdetifier(JSON.parse(demographics).label);
	}

	/**
	 * Dropdown Demographic listing
	 */
	const demographyListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("report_type", reportVersion);
		response_tree.includes(reportVersion) ? formData.append("type", "backend") : formData.append("type", "both");
		Axios.post(configURL.ReportFrontBackDemographic, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				response_tree.includes(reportVersion) ?
					setDemographicsNameTree(response.data.result.map((demographic) => {
						return {
							label: demographic.label, value: JSON.stringify(demographic)
						}
					})) :
					setDemographicsName(response.data.result);
			} else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}

	/**
	 *  Demographic Response Listing 
	 */
	const demographicResponseListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("demographic_id", JSON.parse(demographics).name);
		formData.append("demographic_type", JSON.parse(demographics).type);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		Axios.post(configURL.ReportResponseTracker, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setDemographicsHeader(response.data.result.headers);
				setDemographicResponse(response.data.result.data);
			} else {
				toast.warn(response.data.message);
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}

	const handleOnchangeDemographic = (data) => {
		let firstData = [];
		let newData = [];
		let newDemographicHeader = [];
		setToggleList([]);
		setFilledNodes([]);
		setResponseTrackerDataTree([]);
		setSelectedDemographics([]);
		setDemographicNewResponse([]);

		if (data && data.length > 0) {
			setDefaultSelection(false)
			EziLoader.show();
			firstData = JSON.parse(data[0].value);
			newData = [
				{
					demographic_id: firstData.name,
					option_id: -1,
					demographic_type: firstData.type,
				}
			];
			setToggleData(data.map((item, index) => {
				let data_extract = JSON.parse(item.value);
				return {
					demographic_id: data_extract.name,
					option_id: -1,
					demographic_type: data_extract.type,
				}

			}))
			data.forEach(item => {
				newDemographicHeader.push({
					'label': item.label,
					'key': item.label.toLowerCase(),
				});
			})
			newDemographicHeader.push({
				'label': 'Responses',
				'key': 'responses'
			}, {
				'label': 'Participants',
				'key': 'participants'
			},
				{
					'label': 'Percentage',
					'key': 'percentage'
				},
			);
			setDemographicsNewHeader(newDemographicHeader);
			setSelectedDemographics(data)
			if (newData) {
				let formData = new FormData();
				formData.append("survey_id", surveyId);
				formData.append("filters", JSON.stringify(filterData));
				formData.append("date", JSON.stringify(datefilter));
				formData.append("timezone", timezone);
				formData.append("roundDecimalPoint", 2);
				formData.append("response_params", JSON.stringify(newData));

				Axios.post(configURL.post_demographic_response_v2, formData).then(response => {
					EziLoader.hide();
					if (response.data.success !== undefined && response.data.success) {
						setResponseTrackerDataTree(response.data.result);
						let demographicResponseFiltered = [];
						let trakerData = response.data.result;
						let list_of_parent = []
						trakerData.forEach((responseData, index) => {
							let iterationData = {};
							list_of_parent.push(getPath(response.data.result, responseData.option_id));
							let listArray = list_of_parent[index];

							newDemographicHeader.forEach((item, i) => {

								if (list_of_parent[index][i] !== "" && list_of_parent[index][i] !== undefined) {
									let res = item.key;
									let option = listArray[i];

									iterationData[res] = option;
								}
								else {

									let res = item.key;
									let option = 'N/A';
									iterationData[res] = option;
								}
								if (i === newDemographicHeader.length - 1) {
									let resp = responseData.total_respondents;
									let participant = responseData.total_participants;
									let percentage = responseData.percentage;
									iterationData.responses = resp;
									iterationData.participants = participant;
									iterationData.percentage = percentage;
								}

							});
							demographicResponseFiltered.push(iterationData)

						});
						setDemographicNewResponse(demographicResponseFiltered);

					} else {
						toast.warn(response.data.message);
					}
				}).catch(err => {
					EziLoader.hide()
					console.log(err);
				})
			}
		}
		else {
			setDefaultSelection(true);
		}

	}

	const appendChildren = (selecteddem, listing = [], insertNode, level) => {

		let newArray = listing;
		for (var j = 0; j < selecteddem.length; j++) {

			if (j === level) {
				newArray[selecteddem[j].index] = { ...newArray[selecteddem[j].index], children: insertNode }
				return listing;
			}
			else {
				newArray = newArray[selecteddem[j].index].children;
			}
		}

	}


	const onAddChild = (data, parentData, level, index) => {

		let optionId = data.option_id;
		let demographicId = data.demographic_id;
		let parent_demographic__option_id = data.parent_demographic__option_id;
		let currentIndex = index;
		let currentNode = [{
			demographic_id: demographicId,
			option_id: optionId,
			parent_demographic__option_id: parent_demographic__option_id,
			index: currentIndex
		}];
		let currentNodeIndex = toggleList.findIndex(item => (item.demographic_id === demographicId && item.option_id === optionId && item.parent_demographic__option_id === parent_demographic__option_id && item.index === currentIndex));
		let filledNodeIndex = filledNodes.findIndex(item => (item.demographic_id === demographicId && item.option_id === optionId && item.parent_demographic__option_id === parent_demographic__option_id && item.index === currentIndex));
		if (toggleList && currentNodeIndex > -1) {
			let removedIndex = [...toggleList];
			removedIndex.splice(currentNodeIndex, 1);
			setToggleList(removedIndex);

		}
		else {
			setToggleList(toggleList.concat(currentNode));
		}

		if (filledNodes && filledNodeIndex !== -1) {
			return false;
		}

		EziLoader.show();
		let newData = [...parentData];
		newData = newData.concat(toggleData[level + 1])
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		formData.append("roundDecimalPoint", 2);
		formData.append("response_params", JSON.stringify(newData));

		Axios.post(configURL.post_demographic_response_v2, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setResponseTrackerDataTree(appendChildren(parentData, responseTrackerDataTree, response.data.result, level))
				setFilledNodes(filledNodes.concat(currentNode));
				let responseOutput = response.data.result;
				// eslint-disable-next-line array-callback-return
				responseOutput.filter((item) => {
					item.index = currentIndex;
				})

				let treeArray = appendChildren(parentData, responseTrackerDataTree, responseOutput, level);
				let flatArray = getChildrens(treeArray)

				let demographicResponseFiltered = [];
				let list_of_parent = []
				flatArray.forEach((responseData, index) => {
					let iterationData = {};
					list_of_parent.push(getPath(treeArray, responseData.option_id, responseData?.index));
					let listArray = list_of_parent[index];

					demographicsNewHeader.forEach((item, i) => {

						if (list_of_parent[index][i] !== "" && list_of_parent[index][i] !== undefined) {
							let res = item.key;
							let option = listArray[i];

							iterationData[res] = option;
						}
						else {
							let res = item.key;
							let option = 'N/A';
							iterationData[res] = option;
						}
						if (i === demographicsNewHeader.length - 1) {
							let resp = responseData.total_respondents;
							let participant = responseData.total_participants;
							let percentage = responseData.percentage;
							iterationData.responses = resp;
							iterationData.participants = participant;
							iterationData.percentage = percentage;
						}

					});
					demographicResponseFiltered.push(iterationData)

				});
				setDemographicNewResponse(demographicResponseFiltered);
				//console.log(demographicResponseFiltered);

			} else {
				toast.warn(response.data.message);
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})

	}
	const getChildrens = (members) => {
		let children = [];
		const flattenMembers = members.map(m => {
			if (m.children && m.children.length) {
				children = [...children, ...m.children];
			}
			return m;
		});

		return flattenMembers.concat(children.length ? getChildrens(children) : children);
	};

	const getPath = (object, search, indexSearch) => {
		if ('index' in object) {
			if (object.index === indexSearch && object.option_id === search) return [object.option_id];
			else if ((object.children) || Array.isArray(object)) {
				let children = Array.isArray(object) ? object : object.children;
				for (let child of children) {
					let result = getPath(child, search, indexSearch);
					if (result) {
						if (object.demographic_id) result.unshift(object.option_id);
						return result;
					}
				}
			}
		}
		else {
			if (object.option_id === search) return [object.option_id];
			else if ((object.children) || Array.isArray(object)) {
				let children = Array.isArray(object) ? object : object.children;
				for (let child of children) {
					let result = getPath(child, search, indexSearch);
					if (result) {
						if (object.demographic_id) result.unshift(object.option_id);
						return result;
					}
				}
			}
		}

	}



	const handleOnDefaultAllDemographic = () => {
		let newDemographicHeader = [];
		setDemographicNewResponse([]);
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		formData.append("roundDecimalPoint", 2);
		formData.append("response_params", JSON.stringify(defaultdemoGraphic));
		newDemographicHeader.push({
			'label': 'All',
			'key': 'all',
		});
		newDemographicHeader.push({
			'label': 'Responses',
			'key': 'responses'
		}, {
			'label': 'Participants',
			'key': 'participants'
		},
			{
				'label': 'Percentage',
				'key': 'percentage'
			},
		);
		setDemographicsNewHeader(newDemographicHeader);
		Axios.post(configURL.post_demographic_response_v2, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setResponseTrackerDataTree(response.data.result);
				let demographicResponseFiltered = [];
				let trakerData = response.data.result;
				let list_of_parent = []
				trakerData.forEach((responseData, index) => {
					let iterationData = {};
					list_of_parent.push(getPath(response.data.result, responseData.option_id));
					let listArray = list_of_parent[index];

					newDemographicHeader.forEach((item, i) => {

						if (list_of_parent[index][i] !== "" && list_of_parent[index][i] !== undefined) {
							let res = item.key;
							let option = listArray[i];

							iterationData[res] = option;
						}
						else {

							let res = item.key;
							let option = 'N/A';
							iterationData[res] = option;
						}
						if (i === newDemographicHeader.length - 1) {
							let resp = responseData.total_respondents;
							let participant = responseData.total_participants;
							let percentage = responseData.percentage;
							iterationData.responses = resp;
							iterationData.participants = participant;
							iterationData.percentage = percentage;
						}

					});
					demographicResponseFiltered.push(iterationData)

				});
				setDemographicNewResponse(demographicResponseFiltered);

			} else {
				toast.warn(response.data.message);
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}



	useEffect(() => {

		demographyListing();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])

	useEffect(() => {
		if (response_tree.includes(reportVersion)) {
			setToggleList([]);
			setFilledNodes([]);
			setResponseTrackerDataTree([]);
			if (selectedDemographics && selectedDemographics.length > 0) {
				handleOnchangeDemographic(selectedDemographics);
			}
			if (defaultSelection && !defaulAllRemove.includes(reportVersion)) {
				handleOnDefaultAllDemographic();
			}

		}
		if (demographics !== null) {
			demographicResponseListing();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [datefilter, filterData, idetifier, defaultSelection])
	return (
		< React.Fragment >
			{(response_tree.indexOf(reportVersion) !== -1) ?

				<section className="Page-ResponseRate" ref={el => props.screenShotRefVal.current[0] = el}>
					<div className="inner-response">
						<div className="demographic-panel">
							<div class="st_heading">Demography Wise Response </div>
							<div className="demography heading-well-row">
								<label className="control-label-demo">Identifier </label>
								<div className="select-div">
									<Select
										// value={selectedDemographics}
										options={demographicsNameTree}
										defaultValue={selectedDemographics}
										onChange={handleOnchangeDemographic}
										className="select-demography"
										isMulti
										components={animatedComponents}
									/>
								</div>
								{(defaultSelection && !defaulAllRemove.includes(reportVersion)) && <span className='demo_all'>ALL</span>}
								{/* <a className="heading-well-btn dw-xl excel-response-rate" target="_blank" onClick={onExcelDownload}> Download CSV </a> */}
								{((demographicNewResponse && demographicNewResponse.length > 0) && (demographicsNewHeader && demographicsNewHeader.length > 0)) && <CSVLink data={demographicNewResponse} headers={demographicsNewHeader} filename={`responserate_${today}.csv`} className="heading-well-btn dw-xl" target="_blank"> Download CSV </CSVLink>}
							</div>
							<div className="selected-div">
								<div className="selected-demography">
									<div className="slected-elements">
										{/* {

										(selectedDemographics && selectedDemographics.length > 0) && selectedDemographics.map((item) =>
											<div key={item.value}><span>{item.label} <button name={item.value} onClick={handleRemoveSelectedDemographic}>X</button></span></div>

										)} */}
										{/* <div><span>Location <button>X</button></span></div>
									<div><span>Gender <button>X</button></span></div>
									<div><span>Age <button>X</button></span></div>
									<div><span>Grade <button>X</button></span></div> */}
									</div>
								</div>
							</div>
						</div>
						<div className="demo-tree-wrapper">
							<div className="demography-tree-section response-tree">
								{(responseTrackerDataTree && responseTrackerDataTree.length > 0) && <div className="res-coun-per-flex">
									<h3 className="response-title">Responses</h3>
									<h3 className="response-title">Participants</h3>
									<h3 className="response-title">Percentage</h3>
								</div>
								}

								{(responseTrackerDataTree && responseTrackerDataTree.length > 0) ? <ul className="main-tree first-item" >
									{responseTrackerDataTree.map((item, index) => (
										<li className="main-tree-branch section" key={index}  >

											{/* <AiFillMinusCircle className="indicator"  /> */}
											{selectedDemographics && selectedDemographics.length - 1 > demographicLevel ? <Fragment><span className="response-tracker-fetch" onClick={() => onAddChild(item, [{
												demographic_id: item.demographic_id,
												option_id: item.option_id,
												demographic_type: item.demographic_type,
												parent_demographic__option_id: item.parent_demographic__option_id,
												index: index,
												chainindex: `${index}-${item.option_id}-${item.demographic_id}`
											}], demographicLevel, `${index}-${item.option_id}-${item.demographic_id}`)}>{(toggleList.length > 0 && toggleList.findIndex(data => ((data.demographic_id === item.demographic_id && data.option_id === item.option_id && data.parent_demographic__option_id === item.parent_demographic__option_id && data.index === `${index}-${item.option_id}-${item.demographic_id}`))) !== -1) ? <AiFillMinusCircle className="indicator" /> : <AiFillPlusCircle className="indicator" />} {item.option_id}</span></Fragment> : <span>{item.option_id}</span>}

											<div className="repo-score repo-scoreflex">
												<span className="r-score r-score1">{item.total_respondents}</span>
												<span className="r-score r-score2">{item.total_participants}</span>
												<span className="r-score r-score3">{item.percentage}</span>
											</div>

											{((item.children && item.children.length > 0) && (toggleList.length > 0 && toggleList.findIndex(data => (data.demographic_id === item.demographic_id && data.option_id === item.option_id && data.parent_demographic__option_id === item.parent_demographic__option_id && data.index === `${index}-${item.option_id}-${item.demographic_id}`)) !== -1)) && <ResponseRateTree data={item.children} onAddChild={onAddChild} toggleList={toggleList} level={demographicLevel} toggleData={toggleData} selectedDemographics={selectedDemographics} parentData={[{
												demographic_id: item.demographic_id,
												option_id: item.option_id,
												demographic_type: item.demographic_type,
												index: index
											}]} indexChain={`${index}-${item.option_id}-${item.demographic_id}`} />}
										</li>
									))}
								</ul>
									:
									<div className="no-data-section"><span>No Demographic Found</span></div>
								}

							</div>
						</div>
					</div>
				</section >
				:
				<section className="Page-ResponseRate" ref={el => props.screenShotRefVal.current[0] = el}>

					<div className="heading-well">
						<div className="heading-well-row">
							<label>Identifier</label>
							{/* <select className="heading-select" onChange={(e) => setDemographics(e.target.value || null)} value={demographics || ''}>
								<option value="">Select</option>
								{demographicsName && demographicsName.map((key, id) =>
									<option value={JSON.stringify(key)} key={id}>{key.label}</option>
								)}
							</select> */}
							<Select
								// defaultValue={queSelectedTrendFilter}
								options={demographicsName && demographicsName.map((key, id) =>
									({ value: JSON.stringify(key), label: key.label })
								)}
								onChange={(e) => setDemographics(e.value || null)}
								className="select-change-image responserate-filter"
							/>
							<button type="button" className="heading-well-btn" onClick={() => handleDemography()} >Go</button>
							{idetifier && <CSVLink data={demographicResponse} headers={demographicsHeader} filename={`${idetifier}_${today}.csv`} className="heading-well-btn dw-xl" target="_blank"> Download CSV </CSVLink>}
						</div>
					</div>

					{idetifier ?
						<React.Fragment>
							<div className="response-table-wrap">
								<table className="download-report-table">
									<thead className="ezi_custom_table-row category-table-heading">
										<tr>
											{demographicsHeader && demographicsHeader.map((item, index) =>
												<th key={index}>{item.label}</th>
											)}
										</tr>
									</thead>
									<tbody>
										{demographicResponse && demographicResponse.map((key, id) =>
											<tr key={id} className="ezi_custom_table-row">
												{/* <td data-title="Demographic">{key.demographic_id}</td> */}
												<td data-title="Option">{key.option_id}</td>
												<td data-title="Participants">{key.total_participants}</td>
												<td data-title="Respondents">{key.total_respondents}</td>
												<td data-title="Response Rate">{key.response_rate}</td>
											</tr>
										)}
									</tbody>
								</table>
							</div>
						</React.Fragment>
						:
						<div className="no-data-section"><span>No Demographic Found</span></div>
					}

				</section>

			}
		</React.Fragment>
	)
}
const mapStateToProps = (state) => {
	return {
		screenShotRefVal: state.report.screenShotRef,
	};
};
export default connect(mapStateToProps, null)(ResponseRate);