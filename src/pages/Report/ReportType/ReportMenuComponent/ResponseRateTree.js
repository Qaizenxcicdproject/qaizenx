import React, { Fragment, useEffect, useState } from 'react';
import { AiFillPlusCircle, AiFillMinusCircle } from 'react-icons/ai'

const ResponseRateTree = ({ data = [], toggleList = [], onAddChild, level, toggleData, selectedDemographics, parentData, indexChain }) => {

    const [demoGraphLevel, setdemoGraphLevel] = useState(level);
    let lastElement = JSON.parse(selectedDemographics[selectedDemographics.length - 1].value);


    useEffect(() => {
        setdemoGraphLevel(demoGraphLevel + 1);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    return (

        <ul className="main-tree nested"> {/*2nd ul Gender*/}
            {(data && data.length > 0) && data.map((item, indexchild) => (
                <li className="main-tree-branch section" key={indexchild}>

                    {selectedDemographics && lastElement.name !== item.demographic_id ? <Fragment> <span className="response-tracker-fetch" onClick={() => onAddChild(item, [...parentData, {
                        demographic_id: item.demographic_id,
                        option_id: item.option_id,
                        demographic_type: item.demographic_type,
                        parent_demographic__option_id: item.parent_demographic__option_id,
                        index: indexchild,
                        chainindex: `${indexChain}-${indexchild}-${item.option_id}-${item.demographic_id}`
                    }], demoGraphLevel, `${indexChain}-${indexchild}-${item.option_id}-${item.demographic_id}`)}>
                        {(toggleList && (toggleList.length > 0 && (toggleList.findIndex(data => ((data.demographic_id === item.demographic_id && data.option_id === item.option_id && data.parent_demographic__option_id === item.parent_demographic__option_id && data.index === `${indexChain}-${indexchild}-${item.option_id}-${item.demographic_id}`))) !== -1))) ? <AiFillMinusCircle className="indicator" /> : <AiFillPlusCircle className="indicator" />} {item.option_id}</span></Fragment> : <span>{item.option_id}</span>}
                    <div className="repo-score repo-scoreflex">
                        <span className="r-score r-score1">{item.total_respondents}</span>
                        <span className="r-score r-score2">{item.total_participants}</span>
                        <span className="r-score r-score3">{item.percentage}</span>
                    </div>
                    {/* {(toggleList.length > 0 && toggleList.includes(item.option_id)) && <ResponseRateTree onClick={onClick} data={data} />} */}
                    {((item.children && item.children.length > 0) && (toggleList.length > 0 && (toggleList.length > 0 && toggleList.findIndex(data => (data.demographic_id === item.demographic_id && data.option_id === item.option_id && data.parent_demographic__option_id === item.parent_demographic__option_id && data.index === `${indexChain}-${indexchild}-${item.option_id}-${item.demographic_id}`)) !== -1))) && <ResponseRateTree data={item.children} onAddChild={onAddChild} toggleList={toggleList} level={demoGraphLevel} toggleData={toggleData} parentData={[...parentData, {
                        demographic_id: item.demographic_id,
                        option_id: item.option_id,
                        demographic_type: item.demographic_type,
                        parent_demographic__option_id: item.parent_demographic__option_id,
                        index: indexchild
                    }]} selectedDemographics={selectedDemographics} indexChain={`${indexChain}-${indexchild}-${item.option_id}-${item.demographic_id}`} />}
                </li>
            ))}

        </ul>

    )
}

export default ResponseRateTree;