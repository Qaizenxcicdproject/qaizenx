import React, { useState, useEffect, useContext } from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useParams } from "react-router-dom";
import AppContext from 'store/AppContext';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from "react-toastify";
import { confirmAlert } from 'react-confirm-alert';
import TableTag from "pages/Report/ShareComponent/TableTag";
import ReactApexChart from 'react-apexcharts';

const SegmentAnalysis = (props) => {
    const { EziLoader } = useContext(AppContext)
    const appStore = JSON.parse(localStorage.getItem("appState"));
    const timezone = appStore.user.timezone;
    const urlParams = useParams();
    const [surveyId] = useState(urlParams.survey_id)
    const [toggleState, setToggleState] = useState(false);
    const [demographicsName, setDemographicsName] = useState([]);
    const [mostGrabSegment, setMostGrabSegment] = useState([]);
    const [reportData, setReportData] = useState([]);
    const [reportRowData, setReportRowData] = useState([]);
    const [rowIdentifier, setRowIdentifier] = useState("");
    const [columnIdentifier, setColumnIdentifier] = useState("");
    const [demographics, setDemographics] = useState({ rowDemography: null, columnDemography: null });

    const capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    const getKeyName = (count) => {
        const rowData = [];
        for (var index in count) {
            if (count.hasOwnProperty(index)) {
                rowData.push(count[index]);
            }
        }
        return rowData;
    }

    /**
     * Handle Demography dropdown through go button
     */
    const handleDemography = () => {
        let rowParse = demographics.rowDemography;
        let columnParse = demographics.columnDemography;
        if (((rowParse === null) || (rowParse === "")) || ((columnParse === null) || (columnParse === ""))) {
            confirmAlert({
                title: 'Identifier Selection!',
                message: 'Please select row and column identifiers to see analysis..',
                buttons: [{ label: 'Ok' }]
            });
            return;
        }
        if (rowParse === columnParse) {
            confirmAlert({
                title: 'Demographic Selection!',
                message: 'Please select two different identifiers for Row and Column..',
                buttons: [{ label: 'Ok' }]
            });
            return;
        }
        setRowIdentifier(JSON.parse(rowParse).name);
        setColumnIdentifier(JSON.parse(columnParse).name);
        setToggleState(true);
    }

    /**
     * Row and Column demography listing
     */
    const demographyListing = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("report_type", props.data.reportVersion);
        formData.append("type", "both");
        Axios.post(configURL.ReportFrontBackDemographic, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                setDemographicsName(response.data.result);
            }
            else {
                toast.warn(response.data.message || "Something went wrong")
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err);
        })
    }

    /**
     * Row and Column demography table listing
     */
    const reportListing = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("report_type", props.data.reportVersion);
        formData.append("row_name", JSON.parse(demographics.rowDemography).name);
        formData.append("row_demographic_type", JSON.parse(demographics.rowDemography).type);
        formData.append("column_name", JSON.parse(demographics.columnDemography).name);
        formData.append("column_demographic_type", JSON.parse(demographics.columnDemography).type);
        formData.append("calculation_type", (props.avgScore ? "average" : "count"));
        formData.append("roundDecimalPoint", "2");
        formData.append("date", JSON.stringify(props.data.datefilter));
        formData.append("timezone", timezone);
        Axios.post(configURL.ReportPostCrosstabReport, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                const rowData = [];
                setReportData(response.data.result);
                response.data.result.data.forEach((key, value) => {
                    let keyName = getKeyName(key);
                    rowData.push(keyName);
                });
                setReportRowData(rowData);
            }
            else {
                toast.warn(response.data.message || "Something went wrong")
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err);
        })
    }

    /**
     * Most Grabbing Segment Listing 
     */
    const mostGrabSegmentListing = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("report_type", props.data.reportVersion);
        formData.append("row_name", JSON.parse(demographics.rowDemography).name);
        formData.append("row_demographic_type", JSON.parse(demographics.rowDemography).type);
        formData.append("column_name", JSON.parse(demographics.columnDemography).name);
        formData.append("column_demographic_type", JSON.parse(demographics.columnDemography).type);
        formData.append("calculation_type", (props.avgScore ? "average" : "count"));
        formData.append("roundDecimalPoint", "2");
        formData.append("date", JSON.stringify(props.data.datefilter));
        formData.append("timezone", timezone);
        Axios.post(`${configURL.ReportMostImpacted}`, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                setMostGrabSegment(response.data.result);
            }
            else {
                toast.warn(response.data.message || "Something went wrong")
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err);
        })
    }

    /**
     * Most Grabbing Segment Column Chart 
     */
    const columnChart = {
        series: [{
            name: "Score",
            data: (mostGrabSegment && mostGrabSegment.contribution) ? mostGrabSegment.contribution : [],
        }],
        options: {
            chart: {
                type: 'bar', height: 350, toolbar: { show: false },
            },
            labels: (mostGrabSegment && mostGrabSegment.lables) ? mostGrabSegment.lables : [],
            colors: ['#e40078', '#ecb176', '#b3d9b1'],
            noData: {
                text: "No data to display for the selected time period.",
                align: 'center',
                verticalAlign: 'middle',
                offsetX: 0,
                offsetY: 0,
                style: { color: "#716779", fontSize: '13px', fontFamily: "NunitoSansSemiBold" }
            },
            legend: { show: false },
            plotOptions: { bar: { horizontal: false, columnWidth: "50%", distributed: true, }, },
            dataLabels: { style: { colors: ['#1c1c1c'] }, formatter: function (val) { return val ? val + '%' : '' } },
            stroke: { show: true, width: 1, colors: ['transparent'] },
            grid: { show: false, },
            yaxis: { show: false, },
            xaxis: {
                categories: (mostGrabSegment && mostGrabSegment.lables) ? mostGrabSegment.lables : [],
                labels: { style: { colors: "#686868", }, trim: true, maxHeight: 70, },
                axisTicks: { show: false, },
                axisBorder: { show: false, },
            },
            fill: { opacity: 1 },
            series: (mostGrabSegment && mostGrabSegment.score) ? mostGrabSegment.score : [],
            tooltip: {
                enabled: true,
                custom: function ({ series, seriesIndex, dataPointIndex, w }) {
                    let selected = mostGrabSegment.score[dataPointIndex];
                    let getColor = Array.isArray(w.globals.colors) ? w.globals.colors[dataPointIndex] : w.globals.colors;
                    return '<div class="apexchart_tooltip">'
                        + '<span class="tooltip_name">' + props.data.reportLabel + ' Score</span>'
                        + '<span class="tooltip_value"><span class="bar_color" style="background-color:' + getColor + '"></span>' + selected + '</span>' +
                        '</div>'
                }
            },
        },
    }


    useEffect(demographyListing, []);

    useEffect(() => {
        if (demographics.rowDemography !== null || demographics.columnDemography !== null) {
            reportListing();
            mostGrabSegmentListing();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [rowIdentifier, columnIdentifier, props.data.datefilter, props.data.filterData, props.avgScore]);

    return (
        <React.Fragment>
            <section className="Page-TypeAnalysis">
                <div className="select-demography-wrap">
                    <div>
                        <div className="select-demography-inner">
                            <label>Row Identifier</label>
                            <select className="row-select" onChange={(e) => setDemographics({ ...demographics, rowDemography: e.target.value })} value={demographics.rowDemography || ''}>
                                <option value="">Select</option>
                                {demographicsName && demographicsName.map((key, id) =>
                                    <option value={JSON.stringify(key)} key={id}>{key.label}</option>
                                )}
                            </select>
                        </div>
                        <div className="select-demography-inner">
                            <label>Column Identifier</label>
                            <select className="column-select" onChange={(e) => setDemographics({ ...demographics, columnDemography: e.target.value })} value={demographics.columnDemography || ''}>
                                <option value="">Select</option>
                                {demographicsName && demographicsName.map((key, id) =>
                                    <option value={JSON.stringify(key)} key={id}>{key.label}</option>
                                )}
                            </select>
                        </div>
                        <button type="button" className="select-demography-btn" onClick={() => handleDemography()} >Go</button>
                    </div>
                </div>


                {(!toggleState) ?
                    <div className="analysis-blank-demography">
                        <span>No Demographic Selected</span>
                    </div>
                    : <div className="Main">
                        <div className="table-bar-chart-wrap">

                            <div className="analysis-table-wrap analysis-outer-table-wrap">

                                <div className="download_heading-wrap">
                                    <div className="top_heading">
                                        {(reportData.column_name ? capitalizeFirstLetter(reportData.column_name) : reportData.column_name)}
                                    </div>
                                    <OverlayTrigger overlay={<Tooltip>Download to Excel</Tooltip>}>
                                        <button className="report_download_icon" onClick={() => alert("Feature yet to be developed")}>download</button>
                                    </OverlayTrigger>
                                </div>
                                <div className="table-subheader">
                                    <div className="left-header-title">
                                        <span>{(reportData.row_name ? capitalizeFirstLetter(reportData.row_name) : reportData.row_name)}</span>
                                    </div>
                                    <div className="analysis-table-inner-wrap">
                                        <table>
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    {reportData.header && reportData.header.map((key, id) =>
                                                        <th key={id}>{key}</th>
                                                    )}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {reportRowData && reportRowData.map((key, id) =>
                                                    <TableTag array={key} key={id} />
                                                )}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div className="analysis-bar-chart-wrap">
                                <div className="analysis-bar-chart-header">Top Dragging Segments : <span className="analysis-percent">% contribution</span></div>
                                <div className="analysis-bar-chart-body">
                                    {mostGrabSegment
                                        ?
                                        <ReactApexChart options={columnChart.options} series={columnChart.series} type="bar" height={260} />
                                        :
                                        <span className="no-data-display">No data to display for the selected time period.</span>
                                    }
                                </div>
                            </div>
                        </div>


                    </div>

                }

            </section>
        </React.Fragment>
    )
}

export default SegmentAnalysis;