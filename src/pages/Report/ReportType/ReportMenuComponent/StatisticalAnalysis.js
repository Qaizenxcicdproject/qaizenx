import React, { useState, useEffect, useContext } from 'react';
import { Tab, Nav } from 'react-bootstrap';
import AppContext from 'store/AppContext';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from "react-toastify";
import ReportContext from 'pages/Report/ReportContext';
import JSXParser from 'html-react-parser';
import { connect } from "react-redux";

const CrosstabAnalysis = (props) => {

    const { filterData, reportVersion, datefilter, timezone, surveyId } = useContext(ReportContext);
    const { EziLoader } = useContext(AppContext)
    const [cocData, setCocData] = useState([]);
    // const [cocQuestionData, setCocQuestionData] = useState([]);
    const [cocMessage, setCocMessage] = useState("");
    const [isOveriding, setIsOveriding] = useState(false);
    const [title, setTitle] = useState("");
    const [statisticalList, setStatisticalList] = useState([]);

    const cocListing = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("date", JSON.stringify(datefilter));
        formData.append("timezone", timezone);
        formData.append("calculation_type", (props.avgScore ? "average" : "count"));
        formData.append("report_type", reportVersion);
        formData.append("filters", JSON.stringify(filterData));
        formData.append("roundDecimalPoint", "2");
        Axios.post(`${configURL.ReportStatisticsCOC}`, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                if (response.data.result.data !== null) {
                    setIsOveriding(true);
                    // setCocQuestionData((typeof response.data.result.data.selected_questions.question_label === "object" ? response.data.result.data.selected_questions.question_label.default : response.data.result.data.selected_questions.question_label));
                    setCocData(response.data.result.data);
                    setTitle(response.data.result.data.label);
                    setStatisticalList(response.data.result.data.labels);

                }
                else {
                    setIsOveriding(false);
                    setCocMessage(response.data.message)
                }
            }
            else {
                toast.warn(response.data.message || "Something went wrong")
            }
        }).catch(err => {
            EziLoader.hide()
            console.log(err);
        })
    }

    useEffect(cocListing, [datefilter, filterData]);

    return (
        <React.Fragment>
            <section className="Page-CrosstabAnalysis" ref={el => props.screenShotRefVal.current[0] = el}>
                <div className="tablist_ezi">
                    <Tab.Container defaultActiveKey='correlation'>
                        <div className="tab-header-wrap">
                            <div className="tab-left-header">
                                <Nav variant="pills" >
                                    <Nav.Item>
                                        <Nav.Link eventKey="correlation" >Coefficient-of-Correlation</Nav.Link>
                                    </Nav.Item>
                                </Nav>
                            </div>

                        </div>
                        <Tab.Content>
                            <Tab.Pane eventKey="correlation" mountOnEnter unmountOnExit>
                                <React.Fragment>
                                    {isOveriding ?
                                        <div className="statistical-insights-wrap coc-table-wrap">

                                            <h3>{title}</h3>
                                            <ul>
                                                {statisticalList.length > 0 && statisticalList.map((item, index) =>
                                                    <li key={index}>
                                                        {item && JSXParser(item)}
                                                    </li>
                                                )
                                                }
                                            </ul>
                                            <div className="correlation-table-wrap">
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th>Statement</th>
                                                            <th>COC</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {cocData.questions && cocData.questions.map((key) =>
                                                            <tr>
                                                                <td>{(typeof key.question_label === "object" ? key.question_label.default : key.question_label)}</td>
                                                                <td>{key.coc}</td>
                                                            </tr>
                                                        )}

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        :
                                        <div className="analysis-blank-demography">
                                            <span className="font11">{cocMessage}</span>
                                        </div>
                                    }
                                </React.Fragment>
                            </Tab.Pane>

                        </Tab.Content>
                    </Tab.Container>
                </div>
            </section>
        </React.Fragment>
    )
}
const mapStateToProps = (state) => {
    return {
        screenShotRefVal: state.report.screenShotRef,
    };
};
export default connect(mapStateToProps, null)(CrosstabAnalysis);