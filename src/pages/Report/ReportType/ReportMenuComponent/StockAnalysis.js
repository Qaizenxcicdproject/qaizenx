import React, { useState, useEffect, useContext, useRef } from 'react';
import AppContext from 'store/AppContext';
import Axios from "utility/Axios";
import configURL from 'config/config';
import InputRange from 'react-input-range';
import { toast } from "react-toastify";
import { confirmAlert } from 'react-confirm-alert';
import { Accordion, Card, ProgressBar, Tab, Nav, OverlayTrigger, Tooltip } from "react-bootstrap";
import { useAccordionToggle } from 'react-bootstrap/AccordionToggle';
import AccordionContext from 'react-bootstrap/AccordionContext';
import ApexLineChart from 'pages/Report/ChartComponent/ApexLineChart';
import SentimentComponent from 'pages/Report/ShareComponent/Sentiment';
import ReactApexChart from 'react-apexcharts';
import ReportContext from 'pages/Report/ReportContext';
import AliceCarousel from 'react-alice-carousel'
import 'react-alice-carousel/lib/alice-carousel.css'
import ActionModal from '../../ActionModal';
import { connect } from "react-redux";
import Select from 'react-select';

const ReportTypeAnalysis = (props) => {

	const { filterData, reportVersion, datefilter, timezone, surveyId, reportDate, addonDate, dayActive, reportLabel, dayLabel } = useContext(ReportContext);
	const { EziLoader } = useContext(AppContext)
	const [demographics, setDemographics] = useState(null);
	const [demographicsName, setDemographicsName] = useState([]);
	const [idetifier, setIdetifier] = useState("");
	const [themes, setThemes] = useState(null);
	// const [themesName, setThemesName] = useState([]);
	const [themesDropdownLabel, setThemesDropdownLabel] = useState("");
	const [rangeValue, setRangeValue] = useState({ value: 10, });
	const [sliderData, setSliderData] = useState([]);
	const [toggleState, setToggleState] = useState(false);
	const [addonFilter, setAddonFilter] = useState([]);
	const [idetifierRange, setIdetifierRange] = useState();
	const [selectedThemeObj, setSelectedThemeObj] = useState({});
	const [selectedThemeID, setSelectedThemeID] = useState(null);
	// const [selectedColorID,setSelectedColorID] = useState(null);
	const [selectedThemeTrend, setSelectedThemeTrend] = useState({});
	const [themeTrend, setThemeTrend] = useState('theme_trend');
	const [trendFilter, setTrendFilter] = useState('weekly');
	const [selectedData, setSelectedData] = useState({ identifier_id: null, identifier_type: null, slider_value: null, theme_id: null, identifier_name: null, theme_name: null, addon_filter: null });
	const [historyData, setHistoryData] = useState([]);
	const [historyID, setHistoryID] = useState(false);
	const currentPage = useRef(1);
	const [cardPagination, setCardPagination] = useState({});
	const [themeToggle, setThemeToggle] = useState(true);
	const [themeActive, setThemeActive] = useState('active');
	const [queTrendFilter, setQueTrendFilter] = useState('weekly');
	const [questionTrend, setQuestionTrend] = useState({});
	const [questionId, setQuestionId] = useState(null)
	const [suggestion, setSuggestion] = useState({});
	const suggRef = useRef()
	const [show, setShow] = useState(false);
	const [respondentData, setRespondentData] = useState({ respondent_id: null, fname: null, lname: null, comment: null, respondent_contact_number: null, respondent_email: null })
	const [prevTaskData, setPrevTaskData] = useState({ is_completed: false, data: null })
	const [selectedTrendFilter, setSelectedTrendFilter] = useState({ value: 'weekly', label: 'Weekly' });
	const [queSelectedTrendFilter, setQueSelectedTrendFilter] = useState({ value: 'weekly', label: 'Weekly' });
	let trendFilterOptions = []
	if (['CSAT', 'NPS'].includes(reportLabel)) {
		trendFilterOptions = [
			{ value: 'hourly', label: 'Hourly' },
			{ value: 'daily', label: 'Daily' },
			{ value: 'weekly', label: 'Weekly' },
			{ value: 'monthly', label: 'Monthly' },
		]
	}
	else {
		trendFilterOptions = [
			{ value: 'daily', label: 'Daily' },
			{ value: 'weekly', label: 'Weekly' },
			{ value: 'monthly', label: 'Monthly' },
		]
	}


	const [analyzeMetricOption, setAnalyzeMetricOption] = useState([]);

	const suggestionItem = {
		0: { items: 1 },
		768: { items: 1 },
		1024: { items: 1 },
	}

	const questionHandle = (id) => {
		setThemeToggle(false)
		setQuestionId(id)
		setThemeActive(false)
		suggestionListing(id);
	}

	const themeHandle = () => {
		setThemeToggle(true)
		setQuestionId(null)
		setThemeActive(true)
		setSuggestion({})
	}

	const trendThemeFilterHandle = (val) => {
		setTrendFilter(val.value)
		themeTrendListing(val.value)
		setSelectedTrendFilter({ value: val.value, label: val.label })
	}

	const handleQueTrendFilter = (val) => {
		setQueTrendFilter(val.value)
		setQueSelectedTrendFilter({ value: val.value, label: val.label })
	}

	function CustomToggle({ children, eventKey, callback }) {
		const currentEventKey = useContext(AccordionContext);
		const decoratedOnClick = useAccordionToggle(eventKey, () => callback && callback(eventKey),);
		const isCurrentEventKey = currentEventKey === eventKey;
		return (
			<div type="button" className="acc-title" onClick={decoratedOnClick} > {children} <span className={`acc-icon ${isCurrentEventKey && 'up-icon'}`}></span></div>
		);
	}

	const toTitleCase = (phrase) => {
		return phrase
		  .toLowerCase()
		  .split(' ')
		  .map(word => word.charAt(0).toUpperCase() + word.slice(1))
		  .join(' ');
	  };

	/**
	* Handle Metric dropdown 
	*/
	const handleIdentifier = (e) => {
		let ele = e.value;
		console.log(ele);
		setThemes(ele || null)
		if (ele === null || ele === "") {
			confirmAlert({
				title: 'Theme Selection !',
				message: 'Please Select Themes.',
				buttons: [{ label: 'Ok' }]
			});
			return;
		};
		// if (ele === "-1"){
		//     setSelectedColorID("-1");
		// }
		// else {
		//     setSelectedColorID(JSON.parse(ele).id)
		// } 
	}


	/**
	 * Handle Demography dropdown ans slider scale through go button
	 */
	const handleDemography = () => {
		if (demographics === null || themes === null) {
			confirmAlert({
				title: 'Identifier & Theme Selection !',
				message: 'Please Select Identifiers and Themes.',
				buttons: [{ label: 'Ok' }]
			});
			return;
		};
		setIdetifier(JSON.parse(demographics).label);
		setIdetifierRange(rangeValue.value);
		setSelectedThemeID(JSON.parse(themes).id)
		if (themes === "-1") {
			setSelectedThemeID("-1");
			setThemesDropdownLabel("Overall");
		}
		else {
			setSelectedThemeID(JSON.parse(themes).id);
			setThemesDropdownLabel(JSON.parse(themes).label);
		}
		setSelectedData({
			identifier_id: JSON.parse(demographics).name,
			identifier_type: JSON.parse(demographics).type,
			slider_value: rangeValue.value || null,
			theme_id: JSON.parse(themes).id || "-1",
			identifier_name: JSON.parse(demographics).label || null,
			theme_name: JSON.parse(themes).label || "Overall",
		});
	}

	/**
	 * @get all card data
	 */
	const handleStockCard = (item) => {
		setHistoryID(item.id)
		setAddonFilter([{
			demographic_id: JSON.parse(demographics).name || "",
			demographic_type: JSON.parse(demographics).type || "",
			value: [item.name]
		}])
		let filterData = {};
		filterData = {
			...selectedData,
			addon_filter: [{
				demographic_id: JSON.parse(demographics).name || "",
				demographic_type: JSON.parse(demographics).type || "",
				value: [item.name]
			}]
		}
		setSelectedData(filterData)
		setSuggestion({})
		setQuestionId(null)
	}

	/**
	 * false all values when switch header trend button 
	 */
	const handleTrendvalues = () => {
		setHistoryID(false);
		setAddonFilter([]);
		let filterData = {};
		filterData = {
			...selectedData,
			addon_filter: []
		}
		setSelectedData(filterData);
	}


	/**
	 * Dropdown identifier listing
	 */
	const demographyListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("report_type", reportVersion);
		formData.append("type", "both");
		Axios.post(configURL.ReportFrontBackDemographic, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setDemographicsName(response.data.result);
			} else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}

	/**
	 * Dropdown Themes listing
	 */
	const themeListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("report_type", reportVersion);
		Axios.post(configURL.ReportThemeDropdwonListing, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				// setThemesName(response.data.result);
				let theme = response.data.result;
				let analyseOption = [{ value: '-1', label: `Overall(${reportLabel})` }];
				// eslint-disable-next-line array-callback-return
				theme && theme.map((key, id) => {
					analyseOption.push({ value: JSON.stringify(key), label: key.label });
				}
				)
				setAnalyzeMetricOption(analyseOption);
				console.log(analyzeMetricOption);
			} else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}

	/**
	 * Slider color listing
	 */
	const sliderColor = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		// formData.append("theme_id",selectedColorID);
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		formData.append("calculation_type", (props.avgScore ? "average" : "count"));
		formData.append("report_type", reportVersion);
		formData.append("filters", JSON.stringify(filterData));
		Axios.post(configURL.ReportPostSliderColor, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setSliderData(response.data.result.data);
				setRangeValue({ value: response.data.result.data[1] })

			} else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}

	/**
	 * Theme Question Listing and get specific object from theme dropdown id
	 */
	const themeQuestionListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("report_type", reportVersion);
		formData.append("calculation_type", (props.avgScore ? "average" : "count"));
		formData.append("roundDecimalPoint", "2");
		formData.append("theme_id", selectedThemeID);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		formData.append("addon_filter", addonFilter ? JSON.stringify(addonFilter) : JSON.stringify([]));
		Axios.post(configURL.ReportReasonThemes, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setSelectedThemeObj(response.data.result)
			}
			else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})

	}

	/**
	 * Theme Analysis Question Trend Listing 
	 */
	const themeTrendListing = (filters = trendFilter) => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("theme_id", selectedThemeID);
		formData.append("filter", filters);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("report_type", reportVersion);
		formData.append("calculation_type", (props.avgScore ? "average" : "count"));
		formData.append("roundDecimalPoint", "2");
		let date = {
			start_date: new Date(reportDate.start_date.getTime() - (reportDate.start_date.getTimezoneOffset() * 60000)).toJSON(),
			end_date: new Date(reportDate.end_date.getTime() - (reportDate.end_date.getTimezoneOffset() * 60000)).toJSON(),
			time: reportDate.time,
			utc: reportDate.utc
		};
		formData.append("date", JSON.stringify(date));
		formData.append("timezone", timezone);
		formData.append("addon_filter", addonFilter ? JSON.stringify(addonFilter) : JSON.stringify([]));
		Axios.post(configURL.ReportThemeTrend, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setSelectedThemeTrend(response.data.result)
			}
			else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})

	}


	const historyOptionScore = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("demographic_id", JSON.parse(demographics).name);
		formData.append("demographic_type", JSON.parse(demographics).type);
		formData.append("sliderValue", rangeValue.value);
		formData.append("roundDecimalPoint", "2");
		formData.append("theme_id", selectedThemeID);
		let date = {
			start_date: new Date(reportDate.start_date.getTime() - (reportDate.start_date.getTimezoneOffset() * 60000)).toJSON(),
			end_date: new Date(reportDate.end_date.getTime() - (reportDate.end_date.getTimezoneOffset() * 60000)).toJSON(),
			time: reportDate.time,
			utc: reportDate.utc
		};
		formData.append("date", JSON.stringify(date));
		// formData.append("date",JSON.stringify(datefilter)); 
		formData.append("timezone", timezone);
		formData.append("calculation_type", (props.avgScore ? "average" : "count"));
		formData.append("report_type", reportVersion);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("current_page", currentPage.current);
		if (dayActive !== "manual") {
			formData.append("addon_date_filter", addonDate);
		}
		Axios.post(configURL.ReportHistoryOptionScore, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				let historyArg = response.data.result.data;
				if (currentPage.current > 1) {
					historyArg = [...historyData, ...response.data.result.data]
				}
				setCardPagination(response.data.pagination);
				setHistoryData(historyArg);
			}
			else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})

	}

	const handleLoadMore = () => {
		currentPage.current = currentPage.current + 1;
		historyOptionScore();
	}

	const questionListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("question_id", questionId);
		formData.append("filter", queTrendFilter);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("report_type", reportVersion);
		formData.append("calculation_type", (props.avg ? "average" : "count"));
		formData.append("roundDecimalPoint", "2");
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		Axios.post(configURL.ReportQuestionTrend, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setQuestionTrend(response.data.result);
			}
			else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})

	}

	const LineChart = {
		series: [{
			name: "Score",
			data: (questionTrend && questionTrend.score_data) ? questionTrend.score_data : [],
		}],
		options: {
			chart: { height: 350, type: 'line', toolbar: { show: false, }, zoom: { enabled: false } },
			colors: ['#F8C63D'],
			dataLabels: { enabled: false },
			stroke: { curve: 'straight' },
			markers: { size: 4, colors: ["#ce2e6c"], strokeColors: "#fff", strokeWidth: 2, hover: { size: 7, } },
			grid: { row: { colors: ['#f3f3f3', 'transparent'], opacity: 0.5 }, },
			xaxis: { categories: (questionTrend && questionTrend.score_label) ? questionTrend.score_label : [], tooltip: { enabled: false } },
			yaxis: { labels: { formatter: function (val, index) { return val.toFixed(2); } } },
		}
	};


	/**
	 * suggestion Listing for Recoomendation
	 *  on the basis of questions
	 */
	const suggestionListing = (qid = questionId) => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("question_id", qid);
		formData.append("filter", queTrendFilter);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("report_type", reportVersion);
		formData.append("calculation_type", (props.avg ? "average" : "count"));
		formData.append("roundDecimalPoint", "2");
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		Axios.post(configURL.ReportQuestionSuggestions, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setSuggestion(response.data.result)
				suggRef.current.scrollIntoView({ behavior: 'smooth' })
			}
			else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}
	const handleShowAction = ({ label = "", id = "" }) => {
		setShow(true)
		// checkActionStatus(respondent_id);
		setRespondentData({ respondent_id: null, fname: null, lname: null, comment: label, respondent_contact_number: null, respondent_email: null, questionId: id })
	}
	const handleCloseAction = () => {
		setShow(false)
		setPrevTaskData({ is_completed: false, data: null })
		setRespondentData({ respondent_id: null, fname: null, lname: null, comment: null, respondent_contact_number: null, respondent_email: null })
	}

	/**
	 * Default load dropdown demographyListing and theme listing
	 */
	useEffect(() => {
		demographyListing();
		themeListing();


		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	/**
	 *  On the basis of theme slider range change
	 */
	useEffect(() => {
		sliderColor();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [props.avgScore, datefilter, filterData]);

	/**
	 *  Accordian question & theme analysis trend filter
	 */
	// useEffect(() => { 
	//     if(demographics !== null && themes !== '-1'){
	//         themeTrendListing();
	//     }  
	//     // eslint-disable-next-line react-hooks/exhaustive-deps
	// },[trendFilter]); 

	/**
	 *  On the basis of horizonatal and Rev Horizontal chart bar click call listing
	 */
	useEffect(() => {
		if (demographics !== null && themes !== '-1') {
			themeQuestionListing();
			themeTrendListing();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [historyID]);

	useEffect(() => {
		if (demographics !== null) {
			setToggleState(true);
			handleTrendvalues();
			currentPage.current = 1;
			historyOptionScore();
			themeQuestionListing();
		}
		if (demographics !== null && themes !== '-1') {
			themeTrendListing();
		}

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [idetifier, themesDropdownLabel, idetifierRange, datefilter, filterData, props.avgScore, addonDate]);

	useEffect(() => {
		if (questionId !== null) {
			questionListing();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [datefilter, filterData, props.avgScore, addonDate, queTrendFilter, questionId]);

	return (
		<React.Fragment>
			<section className="Page-TypeAnalysis">

				<div className="select-demography-wrap identifier-slider-wrap">
					<div className="select-demography-inner">
						<label>Analyze Metric</label>

						{/* <select className="column-select" onChange={(e) => handleIdentifier(e)} value={themes || ''}>
							<option value="" >Select</option>
							<option value="-1" >Overall ( {reportLabel} )</option>
							{themesName && themesName.map((key, id) =>
								<option value={JSON.stringify(key)} key={id}>{key.label}</option>
							)}
						</select> */}
						<Select
							// defaultValue={selectedTrendFilter}
							options={analyzeMetricOption}
							onChange={handleIdentifier}
							className="select-change-image analysis-filter"
						/>

					</div>
					<div className="color-slider-scale">
						<label>on a scale</label>
						<InputRange
							step={props.avgScore ? 0.5 : 1}
							maxValue={sliderData && sliderData[0]}
							minValue={sliderData && sliderData[2]}
							value={rangeValue.value}
							onChange={value => setRangeValue({ value })}
						/>
					</div>
					<div className="select-demography-inner">
						<label>with Identifier</label>
						{/* <select className="column-select" onChange={(e) => setDemographics(e.target.value || null)} value={demographics || ''}>
							<option value="">Select</option>
							{demographicsName && demographicsName.map((key, id) =>
								<option value={JSON.stringify(key)} key={id}>{key.label}</option>
							)}
						</select> */}
						<Select
							// defaultValue={selectedTrendFilter}
							options={demographicsName && demographicsName.map((key, id) =>
								({ value: JSON.stringify(key), label: key.label })
							)}
							onChange={(e) => setDemographics(e.value || null)}
							className="select-change-image analysis-filter"
						/>
					</div>

					<button type="button" className="select-demography-btn" onClick={() => handleDemography()} >Go</button>
				</div>



				{(toggleState) &&

					<React.Fragment>
						<div className="infinite-scroll-card car-bot-mar" ref={el => props.screenShotRefVal.current[0] = el}>
							<label>{(themesDropdownLabel === 'Overall') ? `Overall ( ${reportLabel} )` : themesDropdownLabel} Analysis</label>
							{!historyData.length ?
								<span className="stock-no-data">No data to display for the selected identifier</span>
								:
								<div className="stock-card-wrapper">
									{historyData && historyData.map((item, index) => (
										<div className={`stock-card ${(historyID === item.id) && 'active'}`} key={index} onClick={(e) => handleStockCard(item)}>
											<OverlayTrigger overlay={<Tooltip>{toTitleCase(item.name)}</Tooltip>}>
											<span className="stock-card-title">{item.name}</span>
											</OverlayTrigger>
											<span className="stock-card-number">{item.score}</span>
											{(dayActive !== "manual") && <span className="stock-arrow-number" style={{ color: (item.diff_score < 0) ? '#FF1616' : '#5AAF2B' }} >{item.diff_score}<span className={`stock-arrow ${(item.diff_score < 0) ? 'stock-down' : 'stock-up'}`}></span><span className="day-text">since last {dayLabel}</span></span>}
											<span className="stock-badge" style={{ backgroundColor: item.color }}></span>
										</div>
									))}
									{(cardPagination && cardPagination.current_page !== cardPagination.last_page) &&
										<button type="button" onClick={handleLoadMore} className="load_more">Load More</button>
									}
								</div>
							}

						</div>
						{historyID &&
							<div className="report-accordian car-top-mar trend-bg">
								<Accordion defaultActiveKey="0">
									{themes !== "-1" &&

										<Card>
											<Card.Header>
												<CustomToggle eventKey="0">
													<div className="score-progress-bar_c">
														<ul className="theme-bar-list">
															{(selectedThemeObj && selectedThemeObj.score && selectedThemeObj.today_score) &&

																<li className="theme-bar-item theme-row">
																	<p className="theme-bar-name bl">Theme Analysis : {selectedThemeObj.label}</p>
																	<div className="theme-bar-progress">
																		<ProgressBar label={selectedThemeObj.score[1]} min={selectedThemeObj.score[0]} max={selectedThemeObj.score[1] === 0 ? 0 : selectedThemeObj.score[2]} now={(selectedThemeObj.score[1] === 0) ? 0 : selectedThemeObj.score[1]} />
																	</div>
																</li>

															}

														</ul>
													</div>
												</CustomToggle>
											</Card.Header>
											<Accordion.Collapse eventKey="0">
												<Card.Body>

													<div className="one-four-card-wrapper" ref={el => props.screenShotRefVal.current[1] = el}>
														<div className="chart-box-card_c car-bot-mar  speedometer-widget-card shadow-none theme-progressor-wrapper w50_card">
															<div className="chart-box-card-body_c overflow-scroll">
																<div className="question-cm-box-body speedometer-body">
																	<div className={`theme-progressor ${themeActive && 'active'}`}>
																		<h1 className="themes">{selectedThemeObj.label}</h1>
																		<span className={`progress-selection themes-bar ${themeActive && 'active'}`}></span>
																		<ProgressBar onClick={themeHandle} className="theme-bar">
																			{selectedThemeObj.scores && selectedThemeObj.scores.map((item, index) =>
																				<ProgressBar label={item} now={item} key={index + 1} />
																			)
																			}
																		</ProgressBar>
																	</div>
																	{
																		selectedThemeObj.questions && selectedThemeObj.questions.map((item, index) =>
																			<div className={`theme-progressor ${(item.id === questionId) && 'active'}`} key={item.id} >
																				<div className="stock-enagagement-action-button">
																					<h1 >{item.label}</h1>
																					<button className="engagement-action-btn" onClick={() => handleShowAction(item)}> Action</button>
																				</div>
																				<span className={`progress-selection ${(item.id === questionId) && 'active'}`}></span>
																				<ProgressBar onClick={() => questionHandle(item.id)}>
																					{item.scores.map((item, index) =>
																						<ProgressBar label={item} now={item} key={index + 1} />
																					)
																					}
																				</ProgressBar>
																			</div>
																		)
																	}
																	{(show) &&
																		<ActionModal
																			show={show}
																			onHide={handleCloseAction}
																			respondent_data={respondentData} prevTaskData={prevTaskData} respondantReport={false} actionType='QUESTION'
																		/>
																	}
																</div>
															</div>
														</div>
														<div className="chart-box-card_c card-tab-width shadow-none w50_card">
															<div className="tablist_ezi">
																<Tab.Container activeKey={themeTrend} onSelect={(k) => setThemeTrend(k)}>
																	<div className="tab-header-wrap">
																		<div className="tab-left-header">
																			<Nav variant="pills" >
																				<Nav.Item>
																					<Nav.Link eventKey="theme_trend">Positive Score Trend</Nav.Link>
																				</Nav.Item>
																			</Nav>
																		</div>
																	</div>
																	<Tab.Content>
																		<Tab.Pane eventKey="theme_trend" mountOnEnter unmountOnExit>
																			<div className="tab-chart-body">
																				{themeToggle
																					?
																					<React.Fragment>
																						{/* <select className="distribution-select" onChange={(e) => trendThemeFilterHandle(e.target.value)} value={trendFilter || ''}>
																							{['CSAT', 'NPS'].includes(reportLabel) && <option value="hourly">Hourly</option>}
																							<option value="daily">Daily</option>
																							<option value="weekly">Weekly</option>
																							<option value="monthly">Monthly</option>
																						</select> */}
																						<Select
																							defaultValue={selectedTrendFilter || ''}
																							options={trendFilterOptions}
																							onChange={trendThemeFilterHandle}
																							className="select-change-image trend-filter"
																						/>
																						<ApexLineChart
																							tooltipName="Score"
																							data={selectedThemeTrend.scores}
																							labels={selectedThemeTrend.labels}
																							colors={['#F8C63D']}
																							height="270"
																						/>
																					</React.Fragment>
																					:
																					<React.Fragment>
																						{/* <select className="distribution-select" defaultValue={queTrendFilter} onChange={(e) => setQueTrendFilter(e.target.value)} >
																							{['CSAT', 'NPS'].includes(reportLabel) && <option value="hourly">Hourly</option>}
																							<option value="daily">Daily</option>
																							<option value="weekly">Weekly</option>
																							<option value="monthly">Monthly</option>
																						</select> */}
																						<Select
																							defaultValue={queSelectedTrendFilter}
																							options={trendFilterOptions}
																							onChange={handleQueTrendFilter}
																							className="select-change-image trend-filter"
																						/>

																						<ReactApexChart options={LineChart.options} series={LineChart.series} type="line" height={270} />
																					</React.Fragment>
																				}
																			</div>
																		</Tab.Pane>
																	</Tab.Content>
																</Tab.Container>
															</div>
														</div>
													</div>

												</Card.Body>
											</Accordion.Collapse>
										</Card>
									}
								</Accordion>

								{(suggestion && suggestion.data && suggestion.data.length > 0) &&
									<Accordion defaultActiveKey="1">
										<Card ref={suggRef}>
											<Card.Header>
												<CustomToggle eventKey="1">People Practice for Improving {suggestion.sub_dimension}</CustomToggle>
											</Card.Header>
											<Accordion.Collapse eventKey="1">
												<Card.Body>
													<div className="suggestion-carousel">
														<AliceCarousel
															items={suggestion.data.map((item, index) =>
																<section className="suggestion-section" key={`suggestion${index}`}>
																	<p className="suggestion-title">{item.title}</p>
																	<span>{item.suggestion}</span>
																</section>
															)}
															responsive={suggestionItem}
															autoPlayDirection="rtl"
															mouseTracking={true}
															autoHeight={true}
														/>
													</div>
												</Card.Body>
											</Accordion.Collapse>
										</Card>
									</Accordion>
								}
								<Accordion>
									<Card>
										<Card.Header>
											<CustomToggle eventKey="2">Identifier Sentiments</CustomToggle>
										</Card.Header>
										<Accordion.Collapse eventKey="2">
											<Card.Body>
												<div ref={el => props.screenShotRefVal.current[2] = el}>
													<SentimentComponent selectedData={selectedData} avgScore={props.avgScore ? "average" : "count"} menuTitle={props.menuTitle} />
												</div>
											</Card.Body>
										</Accordion.Collapse>
									</Card>

								</Accordion>
							</div>
						}

					</React.Fragment>
				}
				{(!toggleState) &&
					<div className="analysis-blank-demography">
						<span>No data to display for the selected identifier</span>
					</div>
				}



			</section>
		</React.Fragment>
	)
}
const mapStateToProps = (state) => {
	return {
		screenShotRefVal: state.report.screenShotRef,
	};
};
export default connect(mapStateToProps, null)(ReportTypeAnalysis);

