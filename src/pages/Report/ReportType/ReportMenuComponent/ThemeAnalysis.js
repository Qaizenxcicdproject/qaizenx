/* eslint-disable react/jsx-no-duplicate-props */
import React, { useState, useEffect, useContext, useRef } from 'react';
import { Tab, Nav, ProgressBar, Accordion, Card } from 'react-bootstrap';
import { useAccordionToggle } from 'react-bootstrap/AccordionToggle';
import AccordionContext from 'react-bootstrap/AccordionContext';
import { toast } from "react-toastify";
import Axios from "utility/Axios";
import configURL from 'config/config';
import AppContext from 'store/AppContext';
import ReactSpeedometer from 'react-d3-speedometer';
import ApexLineChart from 'pages/Report/ChartComponent/ApexLineChart';
import AliceCarousel from 'react-alice-carousel'
import 'react-alice-carousel/lib/alice-carousel.css'
import ReactApexChart from 'react-apexcharts';
import { GAUGE_SEGEMENT_FOUR, GAUGE_SEGEMENT_AVG_FOUR, GAUGE_COLOR_FOUR } from 'pages/Report/ReportConstant';
import ReportContext from 'pages/Report/ReportContext';
import ActionModal from '../../ActionModal';
import { connect } from "react-redux";
import Select from 'react-select';

const ThemeAnalysis = (props) => {

	const { EziLoader } = useContext(AppContext)
	const { filterData, reportVersion, datefilter, timezone, surveyId, addonDate, dayActive, dayLabel, reportLabel } = useContext(ReportContext);
	const [themeID, setThemeID] = useState(null);
	const [trendFilter, setTrendFilter] = useState('weekly');
	const [activeClass, setActiveClass] = useState(null);
	const [selectedThemeObj, setSelectedThemeObj] = useState({});
	const [themeTrend, setThemeTrend] = useState('theme_trend');
	const [selectedThemeTrend, setSelectedThemeTrend] = useState({});
	const [galleryItems, setGalleryItems] = useState([])
	const [questionId, setQuestionId] = useState(null)
	const [queTrendFilter, setQueTrendFilter] = useState('weekly');
	const [questionTrend, setQuestionTrend] = useState({});
	const [themeToggle, setThemeToggle] = useState(true);
	const [themeActive, setThemeActive] = useState('active');
	const [suggestion, setSuggestion] = useState({});
	const [carouselIndex, setCarouselIndex] = useState(0);
	const suggRef = useRef()
	const [show, setShow] = useState(false);
	const [respondentData, setRespondentData] = useState({ respondent_id: null, fname: null, lname: null, comment: null, respondent_contact_number: null, respondent_email: null })
	const [prevTaskData, setPrevTaskData] = useState({ is_completed: false, data: null })
	const [selectedTrendFilter, setSelectedTrendFilter] = useState({ value: 'weekly', label: 'Weekly' });
	const [queSelectedTrendFilter, setQueSelectedTrendFilter] = useState({ value: 'weekly', label: 'Weekly' });
	let trendFilterOptions = []
	if (['CSAT', 'NPS'].includes(reportLabel)) {
		trendFilterOptions = [
			{ value: 'hourly', label: 'Hourly' },
			{ value: 'daily', label: 'Daily' },
			{ value: 'weekly', label: 'Weekly' },
			{ value: 'monthly', label: 'Monthly' },
		]
	}
	else {
		trendFilterOptions = [
			{ value: 'daily', label: 'Daily' },
			{ value: 'weekly', label: 'Weekly' },
			{ value: 'monthly', label: 'Monthly' },
		]
	}

	function CustomToggle({ children, eventKey, callback }) {
		const currentEventKey = useContext(AccordionContext);
		const decoratedOnClick = useAccordionToggle(eventKey, () => callback && callback(eventKey),);
		const isCurrentEventKey = currentEventKey === eventKey;
		return (
			<div type="button" className="acc-title" onClick={decoratedOnClick} > {children} <span className={`acc-icon ${isCurrentEventKey && 'up-icon'}`}></span></div>
		);
	}

	const responsive = {
		0: { items: 1 },
		768: { items: 2 },
		1024: { items: 3 },
	}
	const suggestionItem = {
		0: { items: 1 },
		768: { items: 1 },
		1024: { items: 1 },
	}

	/**
	* Handle Theme Card
	* @param {object} - id -Theme ID
	* @param {number} - index of card
	*/
	const handleAnalyse = (item, index) => {
		setCarouselIndex(index)
		setThemeID(item.id);
		setActiveClass(item.id);
		setSuggestion({})
	}

	/**
	 *  Overall Report History Theme Score Listing
	 */
	const historyThemeScore = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("report_type", reportVersion);
		formData.append("calculation_type", (props.avgScore ? "average" : "count"));
		formData.append("roundDecimalPoint", "2");
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		if (dayActive !== "manual") {
			formData.append("addon_date_filter", addonDate);
		}
		Axios.post((dayActive !== "manual") ? configURL.ReportHistoryThemeScore : configURL.ReportReasonThemes, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setGalleryItems(response.data.result || []);
			}
			else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})

	}



	/**
	 * Theme Question Listing @get specific theme id
	 */
	const themeQuestionListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("report_type", reportVersion);
		formData.append("calculation_type", (props.avgScore ? "average" : "count"));
		formData.append("roundDecimalPoint", "2");
		formData.append("theme_id", themeID);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		Axios.post(configURL.ReportReasonThemes, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setSelectedThemeObj(response.data.result)
			}
			else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})

	}

	/**
	 * Theme Analysis Question Trend Listing 
	 * @param {string} - hourly , daily , weekly , monthly Filter
	 */
	const themeTrendListing = (filters = trendFilter) => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("theme_id", themeID);
		formData.append("filter", filters);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("report_type", reportVersion);
		formData.append("calculation_type", (props.avgScore ? "average" : "count"));
		formData.append("roundDecimalPoint", "2");
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		Axios.post(configURL.ReportThemeTrend, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setSelectedThemeTrend(response.data.result)
			}
			else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})

	}

	/**
	* handle Question Listing 
	*/
	const questionListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("question_id", questionId);
		formData.append("filter", queTrendFilter);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("report_type", reportVersion);
		formData.append("calculation_type", (props.avgScore ? "average" : "count"));
		formData.append("roundDecimalPoint", "2");
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		Axios.post(configURL.ReportQuestionTrend, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setQuestionTrend(response.data.result);
			}
			else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})

	}


	const LineChart = {
		series: [{
			name: "Score",
			data: (questionTrend && questionTrend.score_data) ? questionTrend.score_data : [],
		}],
		options: {
			chart: { height: 350, type: 'line', toolbar: { show: false, }, zoom: { enabled: false } },
			colors: ['#F8C63D'],
			dataLabels: { enabled: false },
			stroke: { curve: 'straight' },
			markers: { size: 4, colors: ["#ce2e6c"], strokeColors: "#fff", strokeWidth: 2, hover: { size: 7, } },
			grid: { row: { colors: ['#f3f3f3', 'transparent'], opacity: 0.5 }, },
			xaxis: { categories: (questionTrend && questionTrend.score_label) ? questionTrend.score_label : [], tooltip: { enabled: false } },
			yaxis: { labels: { formatter: function (val, index) { return val.toFixed(2); } } },
		}
	};

	const getGalleryItems = () => {
		return galleryItems.map((item, index) =>
			<div data-value={index + 1} style={{ width: '100%', height: 300 }} className={`theme-spedomter-wrapper  chart-box-card_c car-bot-mar  speedometer-widget-card ${item.id === activeClass ? "active" : ""}`} key={item.id}>
				<div className="chart-box-card-header_c">
					<label>{item.label}</label>
					<button type="button" className="analyze-btn" onClick={() => handleAnalyse(item, index)}>Analyze</button>
				</div>
				<div className="chart-box-card-body_c">
					<div className="question-cm-box-body speedometer-body">
						<div className="speedometer-wrapper">
							<ReactSpeedometer
								width={270}
								height={144}
								forceRender={true}
								needleHeightRatio={0.8}
								maxSegmentLabels={1}
								customSegmentStops={props.avgScore ? GAUGE_SEGEMENT_AVG_FOUR : GAUGE_SEGEMENT_FOUR}
								segmentColors={GAUGE_COLOR_FOUR}
								value={item.score[1]}
								minValue={props.avgScore ? 0 : 0}
								maxValue={props.avgScore ? 5 : 100}
								ringWidth={30}
								needleTransitionDuration={3333}
								needleTransition="easeElastic"
								needleColor={"#504658"}
								textColor={"#504658"}
								labelFontSize="15"
								valueTextFontSize="13"
								paddingHorizontal={5}
								paddingVertical={10}
							/>
						</div>
						<div className="speed-text-wrap">
							<span className="speed-title">{item.score[1]}</span>
							<span className="speed-subtitle"></span>
							{dayActive !== 'manual' && <span className="stock-arrow-number" style={{ color: (item.diff_score < 0) ? '#FF1616' : '#5AAF2B' }} >{item.diff_score}<span className={`stock-arrow ${(item.diff_score < 0) ? 'stock-down' : 'stock-up'}`}></span><span className="since-text">since last {dayLabel}</span></span>}
						</div>
					</div>
				</div>
			</div>
		)
	}

	/**
	 * suggestion Listing for Recoomendation
	 *  on the basis of questions
	 */
	const suggestionListing = (qid = questionId) => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("question_id", qid);
		formData.append("filter", queTrendFilter);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("report_type", reportVersion);
		formData.append("calculation_type", (props.avgScore ? "average" : "count"));
		formData.append("roundDecimalPoint", "2");
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		Axios.post(configURL.ReportQuestionSuggestions, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setSuggestion(response.data.result)
				suggRef.current.scrollIntoView({ behavior: 'smooth' })
			}
			else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}

	const questionHandle = (id) => {
		setThemeToggle(false)
		setQuestionId(id)
		setThemeActive(false)
		suggestionListing(id);

	}

	const themeHandle = () => {
		setThemeToggle(true)
		setQuestionId(null)
		setThemeActive(true)
		setSuggestion({})
	}

	const handleHistoryvalues = () => {
		setThemeID(null);
		setActiveClass(null);
	}

	const trendThemeFilterHandle = (val) => {
		setTrendFilter(val.value)
		themeTrendListing(val.value)
		setSelectedTrendFilter({ value: val.value, label: val.label })
	}
	const handleShowAction = ({ label = "", id = "" }) => {
		setShow(true)
		// checkActionStatus(respondent_id);
		setRespondentData({ respondent_id: null, fname: null, lname: null, comment: label, respondent_contact_number: null, respondent_email: null, questionId: id })
	}
	const handleCloseAction = () => {
		setShow(false)
		setPrevTaskData({ is_completed: false, data: null })
		setRespondentData({ respondent_id: null, fname: null, lname: null, comment: null, respondent_contact_number: null, respondent_email: null, questionId: null })
		themeQuestionListing();
	}
	const handleQueTrendFilter = (val) => {
		setQueTrendFilter(val.value)
		setQueSelectedTrendFilter({ value: val.value, label: val.label })
	}

	useEffect(() => {
		setSuggestion({})
		handleHistoryvalues();
		historyThemeScore();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [datefilter, filterData, props.avgScore, addonDate]);

	useEffect(() => {
		if (themeID && themeID !== null) {
			themeQuestionListing();
			themeTrendListing();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [datefilter, filterData, themeID, props.avgScore, addonDate]);


	useEffect(() => {
		console.log(dayActive);
		if (questionId !== null) {
			questionListing();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [datefilter, filterData, props.avgScore, addonDate, queTrendFilter, questionId]);


	return (
		<section className="theme-que-analysis-wrap">

			{galleryItems && (galleryItems.length) > 0 ?
				<div ref={el => props.screenShotRefVal.current[0] = el}>
					<AliceCarousel
						items={getGalleryItems()}
						responsive={responsive}
						autoPlayDirection="rtl"
						mouseTracking={true}
						activeIndex={carouselIndex}
					/>
				</div>
				:
				<div className="no-data-section">No data to display for the selected time period.</div>
			}

			{(themeID && themeID !== null) &&
				<React.Fragment>
					<div className="one-four-card-wrapper">
						<div className="chart-box-card_c car-bot-mar  speedometer-widget-card shadow-none theme-progressor-wrapper w50_card" ref={el => props.screenShotRefVal.current[1] = el}>
							<div className="chart-box-card-body_c overflow-scroll">
								<div className="question-cm-box-body speedometer-body">
									<div className={`theme-progressor border_c ${themeActive && 'active'}`}>
										<h1 className="themes">{selectedThemeObj.label}</h1>
										<span className={`progress-selection themes-bar ${themeActive && 'active'}`}></span>
										<ProgressBar onClick={themeHandle} className="theme-bar">
											{!props.avgScore ? selectedThemeObj.scores && selectedThemeObj.scores.map((item, index) =>
												<ProgressBar label={item} now={item} key={index + 1} max={100} />
											)
												:
												selectedThemeObj.score && <ProgressBar className={(selectedThemeObj.score[1] > 0 && selectedThemeObj.score[1] <= 2) ? "red" : ((selectedThemeObj.score[1] > 2 && selectedThemeObj.score[1] <= 3) ? "yellow" : "green")} label={selectedThemeObj.score[1]} min={selectedThemeObj.score[0]} max={selectedThemeObj.score[1] === 0 ? 0 : selectedThemeObj.score[2]} now={(selectedThemeObj.score[1] === 0) ? 0 : selectedThemeObj.score[1]} />
												// selectedThemeObj.score && selectedThemeObj.score.map((item,index)=> 
												//     <ProgressBar label={item} now={item} key={index+1} max={5} />
												// )
											}
										</ProgressBar>
									</div>
									{
										selectedThemeObj.questions && selectedThemeObj.questions.map((item, index) =>
											<div className={`theme-progressor ${(item.id === questionId) && 'active'}`} key={item.id} >
												<div className="stock-enagagement-action-button">
													<h1>{item.label}</h1>
													<button className="engagement-action-btn" onClick={() => handleShowAction(item)}> Action {item.is_task_created === true && <img className="checked-complete" alt="" src={require(`../../../../assets/images/report/check.png`)} />}</button>
												</div>
												<span className={`progress-selection ${(item.id === questionId) && 'active'}`}></span>
												<ProgressBar onClick={() => questionHandle(item.id)}>
													{
														!props.avgScore ? item.scores.map((item, index) =>
															<ProgressBar label={item} now={item} key={`question_count${index}`} max={100} />
														)
															:
															// item.score.map((item,index) => 
															<ProgressBar className={(item.score[1] > 0 && item.score[1] <= 2) ? "red" : ((item.score[1] > 2 && item.score[1] <= 3) ? "yellow" : "green")} label={item.score[1]} min={item.score[0]} max={item.score[1] === 0 ? 0 : item.score[2]} now={(item.score[1] === 0) ? 0 : item.score[1]} />
														//     <ProgressBar label={item} now={item} key={`question_avg${index}`} max={5} />
														// )
													}
												</ProgressBar>
											</div>
										)
									}
									{(show) &&
										<ActionModal
											show={show}
											onHide={handleCloseAction}
											respondent_data={respondentData}
											prevTaskData={prevTaskData}
											respondantReport={false}
											actionType='QUESTION'
										/>
									}
								</div>
							</div>
						</div>
						<div className="chart-box-card_c card-tab-width shadow-none w50_card" ref={el => props.screenShotRefVal.current[2] = el}>
							<div className="tablist_ezi">
								<Tab.Container activeKey={themeTrend} onSelect={(k) => setThemeTrend(k)}>
									<div className="tab-header-wrap">
										<div className="tab-left-header">
											<Nav variant="pills" >
												<Nav.Item>
													<Nav.Link eventKey="theme_trend">Positive Score Trend</Nav.Link>
												</Nav.Item>
											</Nav>
										</div>
									</div>
									<Tab.Content>
										<Tab.Pane eventKey="theme_trend" mountOnEnter unmountOnExit>
											<div className="tab-chart-body">
												{themeToggle
													?
													<React.Fragment>
														{/* <select className="distribution-select" onChange={(e) => trendThemeFilterHandle(e.target.value)} value={trendFilter || ''}>
															{['CSAT', 'NPS'].includes(reportLabel) && <option value="hourly">Hourly</option>}
															<option value="daily">Daily</option>
															<option value="weekly">Weekly</option>
															<option value="monthly">Monthly</option>
														</select> */}
														<Select
															defaultValue={selectedTrendFilter || ''}
															options={trendFilterOptions}
															onChange={trendThemeFilterHandle}
															className="select-change-image trend-filter"
														/>
														<ApexLineChart
															tooltipName="Score"
															data={selectedThemeTrend.scores}
															labels={selectedThemeTrend.labels}
															colors={['#F8C63D']}
															height="270"
														/>
													</React.Fragment>
													:
													<React.Fragment>
														{/* <select className="distribution-select" defaultValue={queTrendFilter} onChange={(e) => setQueTrendFilter(e.target.value)} >
															{['CSAT', 'NPS'].includes(reportLabel) && <option value="hourly">Hourly</option>}
															<option value="daily">Daily</option>
															<option value="weekly">Weekly</option>
															<option value="monthly">Monthly</option>
														</select> */}
														<Select
															defaultValue={queSelectedTrendFilter}
															options={trendFilterOptions}
															onChange={handleQueTrendFilter}
															className="select-change-image trend-filter"
														/>
														<ReactApexChart options={LineChart.options} series={LineChart.series} type="line" height={270} />
													</React.Fragment>
												}
											</div>
										</Tab.Pane>
									</Tab.Content>
								</Tab.Container>
							</div>
						</div>
					</div>

					{/* // eslint-disable-next-line react/jsx-no-duplicate-props */}
					{(suggestion && suggestion.data && suggestion.data.length > 0) && <div className="report-accordian car-top-mar trend-bg" ref={suggRef} ref={el => props.screenShotRefVal.current[3] = el}>
						<Accordion defaultActiveKey="1">
							<Card>
								<Card.Header>
									<CustomToggle eventKey="1">People Practice for Improving {suggestion.sub_dimension}</CustomToggle>
								</Card.Header>
								<Accordion.Collapse eventKey="1">
									<Card.Body>

										<div className="suggestion-carousel">
											<AliceCarousel
												items={suggestion.data.map((item, index) =>
													<section className="suggestion-section" key={`suggestion${index}`}>
														<p className="suggestion-title">{item.title}</p>
														<span>{item.suggestion}</span>
													</section>
												)}
												responsive={suggestionItem}
												autoPlayDirection="rtl"
												mouseTracking={true}
												autoHeight={true}
											/>
										</div>
									</Card.Body>
								</Accordion.Collapse>
							</Card>
						</Accordion>
					</div>}

				</React.Fragment>
			}
		</section>
	)
}
const mapStateToProps = (state) => {
	return {
		screenShotRefVal: state.report.screenShotRef,
	};
};
export default connect(mapStateToProps, null)(ThemeAnalysis);