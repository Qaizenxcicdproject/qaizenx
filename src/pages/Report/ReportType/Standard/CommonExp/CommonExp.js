import React, { useState, useContext } from 'react';
import { Dropdown } from 'react-bootstrap';
import ReportContext from 'pages/Report/ReportContext';
import Insights from './Insight';
import DownloadReport from 'pages/Report/ReportType/ReportMenuComponent/DownloadReport/DownloadReport';
import HeaderInfoModal from 'pages/Report/ShareComponent/HeaderInfoModal';

const INNComponent = () => {

    const {reportLabel,switchPage:switchPageData, menuTitle:menuTitleData} = useContext(ReportContext);
    const [toggleState] = useState(false);
    const [switchPage, setSwitchPage] = useState(switchPageData);
    const [menuTitle, setMenuTitle] = useState(menuTitleData);
    const [infoModal, setInfoModal] = useState(false);

    const handleSwitchPage = (str, title) => {
        setSwitchPage(str);
        setMenuTitle(title);
    }

    return (
        <React.Fragment>
            <div className="analysis-menu-wrap">
                <Dropdown className="analysis-menu">
                    <Dropdown.Toggle id="dropdown-basic"> {menuTitle} </Dropdown.Toggle>
                    <Dropdown.Menu>
                        <Dropdown.Item><div className="analysis-menu-list insight" title="Insights" onClick={(e) => handleSwitchPage('insights', e.target.title)}>Insights</div></Dropdown.Item>
                        <Dropdown.Item><div className="analysis-menu-list download-rp" title="Download Reports" onClick={(e) => handleSwitchPage('download', e.target.title)}>Download Reports</div></Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </div>
            <div className={`aside-content-wrap ${switchPage}-wrapper_c`}>
                {(() => {
                    switch (switchPage) {
                        case 'insights':
                            return (<Insights avgScore={toggleState} />);
                        case 'download':
                            return (<DownloadReport />);
                        default:
                            return null
                    }
                })()}
            </div>
            <HeaderInfoModal
                show={infoModal}
                title={menuTitle}
                renderModal={switchPage}
                onHide={() => setInfoModal(false)}
                reportLabel={reportLabel}
            />
        </React.Fragment>
    )
}

export default INNComponent;