import React, { useState, useContext } from 'react';
import { Dropdown } from 'react-bootstrap';
import ReportContext from 'pages/Report/ReportContext';
import DefaultInsight from './DefaultInsight';
import DownloadReport from 'pages/Report/ReportType/ReportMenuComponent/DownloadReport/DownloadReport';
import QuestionAnalysis from 'pages/Report/ReportType/ReportMenuComponent/QuestionAnalysis';
import RespondentReport from 'pages/Report/ReportType/ReportMenuComponent/RespondentReport/RespondentReport';
import HeaderInfoModal from 'pages/Report/ShareComponent/HeaderInfoModal';

const DefaultComponent = () => {

    const { reportLabel, switchPage: switchPageData, menuTitle: menuTitleData } = useContext(ReportContext);
    const reportContextData = useContext(ReportContext);
    const [toggleState] = useState(false);
    const [switchPage, setSwitchPage] = useState(switchPageData);
    const [menuTitle, setMenuTitle] = useState(menuTitleData);
    const [infoModal, setInfoModal] = useState(false);

    const handleSwitchPage = (str, title) => {
        setSwitchPage(str);
        setMenuTitle(title);
    }

    return (
        <React.Fragment>
            <div className="analysis-menu-wrap">
                <Dropdown className="analysis-menu">
                    <Dropdown.Toggle id="dropdown-basic"> {menuTitle} </Dropdown.Toggle>
                    <Dropdown.Menu>
                        <Dropdown.Item><div className="analysis-menu-list insight" title="Insights" onClick={(e) => handleSwitchPage('insights', e.target.title)}>Insights</div></Dropdown.Item>
                        <Dropdown.Item><div className="analysis-menu-list question" title="Question Analysis" onClick={(e) => handleSwitchPage('questions', e.target.title)} >Question Analysis</div></Dropdown.Item>
                        {(reportContextData.containFile) && <Dropdown.Item><div className="analysis-menu-list download-rp" title="Respondent Report" onClick={(e) => handleSwitchPage('respondent', e.target.title)}>Respondent Report</div></Dropdown.Item>}
                        <Dropdown.Item><div className="analysis-menu-list download-rp" title="Download Reports" onClick={(e) => handleSwitchPage('download', e.target.title)}>Download Reports</div></Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </div>
            <div className={`aside-content-wrap ${switchPage}-wrapper_c`}>
                {(() => {
                    switch (switchPage) {
                        case 'questions':
                            return (<QuestionAnalysis QAData={reportContextData} avgScore={toggleState} />);
                        case 'insights':
                            return (<DefaultInsight data={reportContextData} avgScore={toggleState} />);
                        case 'respondent':
                            return (<RespondentReport />);
                        case 'download':
                            return (<DownloadReport />);
                        default:
                            return null
                    }
                })()}
            </div>
            <HeaderInfoModal
                show={infoModal}
                title={menuTitle}
                renderModal={switchPage}
                onHide={() => setInfoModal(false)}
                reportLabel={reportLabel}
            />
        </React.Fragment>
    )
}

export default DefaultComponent;