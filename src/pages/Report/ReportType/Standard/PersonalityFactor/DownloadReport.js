import React, { useState, useEffect, useContext, useRef } from 'react';
import { useParams } from "react-router-dom";
import Axios from 'utility/Axios';
import { toast } from 'react-toastify';
import AppContext from "store/AppContext"
import configURL, { RPT_URL } from 'config/config';
import SweetSearch from 'components/SweetSearch';

const DownloadReport = (props) => {

    const { EziLoader } = useContext(AppContext)
    const urlParams = useParams()
    const [reportsData, setReportsData] = useState([])
    const inputSearch = useRef(null);
    const [searchLoading, setSearchLoading] = useState(false);
    const [searchValue, setSearchValue] = useState("");
    var searchTimer = null;


    /**
     * Handle comment search
     */
    const handleCommentSearch = () => {
        clearTimeout(searchTimer);
        searchTimer = setTimeout(() => {
            setSearchLoading(true)
            let searchVal = inputSearch.current.value;
            setSearchValue(searchVal)
        }, 800);
    }

    /**
     * @get overall report data listing
     */
    const getReportsData = () => {
        EziLoader.show()
        let formData = new FormData()
        formData.append("survey_id", urlParams.survey_id)
        formData.append("filters", JSON.stringify(props.data.filterData));
        formData.append("date", JSON.stringify(props.data.datefilter));
        formData.append("timezone", props.data.timezone);
        formData.append("report_type", props.data.reportVersion);
        formData.append("search", searchValue);
        Axios.post(configURL.ReportRespondentsList, formData).then(res => {
            EziLoader.hide()
            setSearchLoading(false);
            if (res.data.success !== undefined && res.data.success) {
                setReportsData(res.data.result.data)
            } else {
                toast.warn(res.data.message || "Something went wrong.")
            }
        }).catch(err => {
            EziLoader.hide()
            setSearchLoading(false);
            console.log(err)
        })
    }

    useEffect(() => {
        getReportsData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.data.datefilter, props.data.filterData, searchValue])

    return (
        <React.Fragment>

            <section className="Page-DownloadReport">
                {reportsData &&
                    <React.Fragment>
                        <SweetSearch loading={searchLoading} change={handleCommentSearch} ref={inputSearch} />
                        <table className="download-report-table">
                            <thead className="ezi_custom_table-row category-table-heading">
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Name</th>
                                    <th>Gender</th>
                                    <th>Age</th>
                                    <th>Response Date</th>
                                    <th>Report Type</th>
                                    {/* <th>Description</th> */}
                                    <th><span className="custom_table-heading-wrap">Action</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    reportsData.map((item, index) => (
                                        <React.Fragment key={item.respondent_i}>
                                            <tr className="ezi_custom_table-row">
                                                <td data-title="Sr No">{index + 1}</td>
                                                <td data-title="Name">{item.respondent_fname + item.respondent_lname}</td>
                                                <td data-title="Gender">{item.gender_demographics}</td>
                                                <td data-title="Age">{item.age_demographics}</td>
                                                <td data-title="Response Date">{item.response_date}</td>
                                                <td data-title="Report Type">16PF</td>
                                                {/* <td data-title="Description">16PF Report</td> */}
                                                <td data-title="Action">
                                                    <div className="custom_table_action-wrap">
                                                        <a href={`${RPT_URL}reports/post-sixteen-pf?survey_id=${urlParams.survey_id}&date=${JSON.stringify(props.data.datefilter)}&timezone=${props.data.timezone}&roundDecimalPoint=2&calculation_type=VALUE&respondent_id=${item.respondent_id}`} rel="noopener noreferrer"  target="_blank" className="view_report">View Report</a>
                                                    </div>
                                                </td>
                                            </tr>

                                        </React.Fragment>
                                    ))
                                }
                            </tbody>
                        </table>
                    </React.Fragment>
                }

                {!reportsData.length && <div className="no-data-section"><span>No Result Found</span></div>}

            </section>

        </React.Fragment>
    )
}

export default DownloadReport;