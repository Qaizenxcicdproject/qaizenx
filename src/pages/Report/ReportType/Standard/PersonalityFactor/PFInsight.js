import React, { useState, useEffect, useContext, useRef } from 'react';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from "react-toastify";
import { Tab, Nav, OverlayTrigger, Tooltip } from 'react-bootstrap';
import AppContext from 'store/AppContext';
import ReactApexChart from 'react-apexcharts'
import moment from 'moment';
import ApexBarChart from 'pages/Report/ChartComponent/ApexBarChart';
import ReportContext from 'pages/Report/ReportContext';
import { connect } from "react-redux";
import Select from 'react-select';

const Insight = (props) => {

	const { filterData, datefilter, timezone, surveyId, reportVersion, surveyStartDate } = useContext(ReportContext);
	const [trendFilter, setTrendFilter] = useState("weekly");
	const { EziLoader } = useContext(AppContext)
	const [trendlabels, setTrendlabels] = useState([]);
	const [trendCounts, setTrendCounts] = useState([]);
	const [key, setKey] = useState('response_tracker');
	const [totalResponse, setTotalResponse] = useState();
	const [responseArray, setResponseArray] = useState([]);
	const [trendHourlyDate, setTrendHourlyDate] = useState(null);
	const [axisDate, setAxisDate] = useState(null);
	const currentPage = useRef(1);
	const startDateSurvey = moment(surveyStartDate, 'YYYY-MM-DD').format('YYYY-MM-DD')
	const [selectedTrendFilter, setSelectedTrendFilter] = useState({ value: 'weekly', label: 'Weekly' });
	const trendFilterOptions = [
		// { value: 'hourly', label: 'Hourly' },
		{ value: 'daily', label: 'Daily' },
		{ value: 'weekly', label: 'Weekly' },
		{ value: 'monthly', label: 'Monthly' },
	]
	/**
	 *  Select filter for  Trend 
	 *  @param {string} - hourly , daily , weekly , monthly Filter
	 */
	const handleTrendFilter = (filter) => {
		currentPage.current = 1;
		setTrendFilter(filter.value)
		setSelectedTrendFilter({ value: filter.value, label: filter.label })
		trendListing(filter.value);
	}

	/**
	 *  Trend Previous filter for  hourly , daily , weekly , monthly
	 */
	const setPrevPage = () => {
		if ((trendFilter === 'hourly') && (startDateSurvey < axisDate) && (currentPage.current < 4)) {
			currentPage.current = currentPage.current + 1;
			trendListing(trendFilter);
		}
		if ((trendFilter !== 'hourly') && (startDateSurvey < axisDate)) {
			currentPage.current = currentPage.current + 1;
			trendListing(trendFilter);
		}
	}

	/**
	 *  Trend Next filter for  hourly , daily , weekly , monthly
	 */
	const setNextPage = () => {
		if (currentPage.current > 1) {
			currentPage.current = currentPage.current - 1;
			trendListing(trendFilter);
		}
	}


	/**
	 * Donut total response listing
	 */
	const getTotalResponse = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		Axios.post(`${configURL.ReportGetTotalResponse}`, formData).then(res => {
			EziLoader.hide();
			if (res.data.success !== undefined && res.data.success) {
				setTotalResponse(res.data.result);
				setResponseArray(res.data.result.response);
			} else {
				toast.warn(res.data.message);
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}

	/**
	 * Response tracker trend listing
	 * @param {string} - hourly , daily , weekly , monthly Filter
	 */
	const trendListing = (filter = trendFilter) => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("filter", filter);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		formData.append("report_type", reportVersion);
		formData.append("page", currentPage.current);
		formData.append("calculation_type", "count");
		formData.append("roundDecimalPoint", "1");
		Axios.post(configURL.ReportInsightTrend, formData).then(res => {
			EziLoader.hide();
			if (res.data.success !== undefined && res.data.success) {
				setTrendCounts(res.data.result.counts)
				setTrendlabels(res.data.result.option_labels)
				setTrendHourlyDate(moment(res.data.result.hourlyDate, 'YYYY-MM-DD').format('DD-MM-YYYY'))
				let dateAxis = moment(res.data.result.option_labels[0], 'DD-MM-YYYY').format('YYYY-MM-DD')
				setAxisDate(dateAxis)

			} else {
				toast.warn(res.data.message);
			}
		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}

	const statisticsDougnut = {
		series: (responseArray && responseArray) ? responseArray : [],
		options: {
			chart: { type: 'donut', },
			labels: ["Respondents", "Non Respondents"],
			colors: ["#548235", "#FF3C23"],
			data: (responseArray && responseArray) ? responseArray : [],
			dataLabels: { enabled: true, dropShadow: { enabled: false, }, },
			legend: { show: false, },
			stroke: { show: false },
			plotOptions: {
				pie: {
					donut: {
						size: '60%',
						labels: {
							show: true,
							name: { show: true, fontSize: 13, fontFamily: "NunitoSansBold", color: "#ce2e6c", fontWeight: 700 },
							value: { offsetY: -1, show: true, fontSize: 13, fontFamily: "NunitoSansBold", color: "#504658", fontWeight: 700 },
							total: { show: true, fontSize: 13, fontFamily: "NunitoSansBold", color: "#ce2e6c", fontWeight: 700 }
						}
					}
				}
			},
			noData: {
				text: "No data to display for the selected time period.",
				align: 'center',
				verticalAlign: 'middle',
				offsetX: 0,
				offsetY: 0,
				style: { color: "#716779", fontSize: '13px', fontFamily: "NunitoSansSemiBold" }
			},
			responsive: [{
				breakpoint: 480,
				options: {
					chart: { width: 200 },
					legend: { position: 'bottom' }
				}
			}]
		},
	}

	useEffect(() => {
		getTotalResponse();
		currentPage.current = 1;
		trendListing(trendFilter);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [filterData, datefilter]);

	return (
		<React.Fragment>
			<section className="CX-QuestionComponent CE-insight-section default-insight-page" >
				<div className="one-four-card-wrapper">
					<div className="chart-box-card_c car-bot-mar increse20 speedometer-widget-card" ref={el => props.screenShotRefVal.current[0] = el}>
						<div className="chart-box-card-header_c">
							<span className="left-text"> Statistics </span>
						</div>
						<div className="chart-box-card-body_c">
							<div className="speedometer-wrapper">
								<ReactApexChart options={statisticsDougnut.options} series={statisticsDougnut.series} type="donut" height={220} />
							</div>
							<div className="half-donut-info speedometer-sentiment">
								<div className="half-donut-rating-text mt-30">
									{totalResponse && totalResponse.params && totalResponse.params.map((item, index) =>
										<div className="sentiment-mood-label">
											<span className="ce-title">{item.count || 0}</span>
											<span className="ce-subtitle">{item.label}</span>
										</div>
									)
									}
								</div>
							</div>
						</div>
					</div>
					<div className="chart-box-card_c increse20 card-tab-width" ref={el => props.screenShotRefVal.current[1] = el}>
						<div className="tablist_ezi">
							<Tab.Container activeKey={key} onSelect={(k) => setKey(k)}>
								<div className="tab-header-wrap">
									<div className="tab-left-header">
										<Nav variant="pills" >
											<Nav.Item>
												<Nav.Link eventKey="response_tracker">Response Tracker</Nav.Link>
											</Nav.Item>
										</Nav>
									</div>

								</div>
								<Tab.Content>
									<Tab.Pane eventKey="response_tracker" mountOnEnter unmountOnExit>
										<div className="tab-chart-body ">

											<div className="distribution-pagination-wrap">
												{/* <select className="distribution-select" defaultValue={trendFilter} onChange={(e) => handleTrendFilter(e.target.value)}>
													<option value="hourly">Hourly</option>
													<option value="daily">Daily</option>
													<option value="weekly">Weekly</option>
													<option value="monthly">Monthly</option>
												</select> */}
												<Select
													defaultValue={selectedTrendFilter}
													options={trendFilterOptions}
													onChange={handleTrendFilter}
													className="select-change-image trend-filter"
												/>
												<div className="chart-pagination">
													<OverlayTrigger overlay={<Tooltip>Previous</Tooltip>}>
														<button type="button" className={`chart-prev ${((trendFilter === 'hourly') && ((startDateSurvey > axisDate) || (currentPage.current === 4))) ? 'inactive' : ((trendFilter !== 'hourly') && (startDateSurvey > axisDate)) ? 'inactive' : ''}`} onClick={() => setPrevPage()}>Prev</button>
													</OverlayTrigger>
													{(trendFilter === 'hourly') && <span className="hourly_date">{trendHourlyDate}</span>}
													<OverlayTrigger overlay={<Tooltip>Next</Tooltip>}>
														<button type="button" className={`chart-next ${currentPage.current === 1 && 'inactive'}`} onClick={() => setNextPage()}>Next</button>
													</OverlayTrigger>
												</div>
											</div>

											<div>
												{(trendCounts && Array.isArray(trendCounts) && trendCounts.length) ?
													<ApexBarChart
														tooltipName="Response(s)"
														data={trendCounts}
														labels={trendlabels}
														barWidth="40%"
														height="290"
														colors={['#ce2e6c']}
													/>
													:
													<div className="insight-no-data">
														<span className="no-data-display">No data to display for the selected time period.</span>
													</div>
												}
											</div>
										</div>
									</Tab.Pane>
								</Tab.Content>
							</Tab.Container>
						</div>
					</div>
				</div>
			</section>
		</React.Fragment>
	)
}
const mapStateToProps = (state) => {
	return {
		screenShotRefVal: state.report.screenShotRef,
	};
};
export default connect(mapStateToProps, null)(Insight);