import React, { useState, useContext } from 'react';
import { Dropdown } from 'react-bootstrap';
import ReportContext from 'pages/Report/ReportContext';
import DownloadReport from 'pages/Report/ReportType/ReportMenuComponent/DownloadReport/DownloadReport';
import ResponseRate from 'pages/Report/ReportType/ReportMenuComponent/ResponseRate';
import HeaderInfoModal from 'pages/Report/ShareComponent/HeaderInfoModal';

const DefaultComponent = () => {

    const {reportLabel,roleExist} = useContext(ReportContext);
    const [toggleState, setToggleState] = useState(false);
    const [switchPage, setSwitchPage] = useState('response');
    const [menuTitle, setMenuTitle] = useState('Response Rate');
    const [infoModal, setInfoModal] = useState(false);

    const handleSwitchPage = (str, title) => {
        setSwitchPage(str);
        setMenuTitle(title);
    }

    const handleToggle = () => {
        setToggleState(!toggleState);
    }

    return (
        <React.Fragment>
            <div className="analysis-menu-wrap">
                <Dropdown className="analysis-menu">
                    <Dropdown.Toggle id="dropdown-basic"> {menuTitle} </Dropdown.Toggle>
                    <Dropdown.Menu>
                        <Dropdown.Item><div className="analysis-menu-list response" title="Response Rate" onClick={(e) => handleSwitchPage('response', e.target.title)} >Response Rate</div></Dropdown.Item>
                        { roleExist && <Dropdown.Item><div className="analysis-menu-list download-rp" title="Download Reports" onClick={(e) => handleSwitchPage('download', e.target.title)}>Download Reports</div></Dropdown.Item>}
                    </Dropdown.Menu>
                </Dropdown>
                {(switchPage === 'insights' || switchPage === 'analysis' || switchPage === 'segment') &&
                    <div className={`ezi-tabular-toggle ${toggleState ? "on" : "off"}`} data-label={toggleState ? "AVG" : "Wellness"} data-placeholder={toggleState ? "Wellness" : "AVG"} onClick={handleToggle} > </div>
                }
            </div>
            <div className={`aside-content-wrap ${switchPage}-wrapper_c`}>
                {(() => {
                    switch (switchPage) {
                        case 'response':
                            return (<ResponseRate />);
                        case 'download':
                            return (<DownloadReport />);
                        default:
                            return null
                    }
                })()}
            </div>
            <HeaderInfoModal
                show={infoModal}
                title={menuTitle}
                renderModal={switchPage}
                onHide={() => setInfoModal(false)}
                reportLabel={reportLabel}
            />
        </React.Fragment>
    )
}

export default DefaultComponent;