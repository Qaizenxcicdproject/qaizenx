import React , { useState } from 'react';
import { Breadcrumb , Dropdown } from "react-bootstrap";

const AnalysisMenu = (props) => {

    const [ switchPage , setSwitchPage] = useState('question');
   
    return(           
        <React.Fragment>
         
            <div className="analysis-menu-wrap">
                <Dropdown className="analysis-menu">
                    <Dropdown.Toggle id="dropdown-basic"> Question Analysis  </Dropdown.Toggle>
                    <Dropdown.Menu>
                        <Dropdown.Item><div className="analysis-menu-list insight" onClick={() => setSwitchPage('insight')}>Insight</div></Dropdown.Item>
                        <Dropdown.Item><div className="analysis-menu-list qa" onClick={() => setSwitchPage('question')} >Question Analysis</div></Dropdown.Item>
                        <Dropdown.Item><div className="analysis-menu-list download-rp" onClick={()=> setSwitchPage('report')}>Download Report</div></Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </div>

        </React.Fragment>
    )
}

export default AnalysisMenu;