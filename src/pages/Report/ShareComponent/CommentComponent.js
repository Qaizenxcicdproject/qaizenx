import React, { useState, useEffect, useRef, useContext } from 'react';
import { useParams } from "react-router-dom";
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from "react-toastify";
import CommentFilter from './ReportCommentFilter'
import SweetSearch from "components/SweetSearch";
import AppContext from 'store/AppContext';
import Comments from './Comments';
// import Pagination from './CommentPagination';
import Pagination from "react-js-pagination";

const CommentSection = (props) => {
    const urlParams = useParams();
    const [showFilter, setShowFilter] = useState(false);
    const surveyId = urlParams.survey_id;
    const [commentList, setCommentList] = useState([]);
    const commentInsightData = props.insightData;
    let inputSearch = useRef(null)
    let searchTimer = null
    const searchLoading = false;
    const { EziLoader } = useContext(AppContext)
    const loading = false;
    const [currentPage, setCurrentPage] = useState(1);
    const [commentPagination, setCmmentPagination] = useState([]);
    const [commentFilter, setCommentFilter] = useState([]);
    const appStore = JSON.parse(localStorage.getItem("appState"));
    const timezone = appStore.user.timezone;

    const paginate = (pageNumber) => {
        setCurrentPage(pageNumber);
    }


    const handleFilterSearch = () => {
        clearTimeout(searchTimer);
        searchTimer = setTimeout(() => {
            commentListing();
        }, 1000);
    }
    const commentListing = (filters = []) => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        let search = inputSearch.current.value;
        formData.append("search", search);
        formData.append("question_type", commentInsightData.question_type);
        formData.append("question_id", commentInsightData.id);
        formData.append("comment_filters", JSON.stringify(commentFilter));
        formData.append("filters", JSON.stringify(props.filterData));
        formData.append("date", JSON.stringify(props.dateFilter));
        formData.append("timezone", timezone);
        formData.append("page", currentPage);
        Axios.post(`${configURL.ReportInsightComment}`, formData).then(response => {
            EziLoader.hide();
            if (response.data.success !== undefined && response.data.success) {
                setCommentList(response.data.result.data);
                setCmmentPagination(response.data.pagination)
            } else {
                toast.warn(response.data.message);
            }
        })
    }
    useEffect(() => {
        commentListing();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.filterData, props.dateFilter, currentPage, commentFilter]);
    return (
        <React.Fragment>
            <div className="ce-comment-section">

                <div className="ce-comment-section-header">
                    <label className="commnet-label">Comments</label>
                    <div className="comment-right-header">

                        {/* <input type="text" className="comment-header-search" placeholder="Search by keyword" change={handleFilterSearch} ref={inputSearch} /> */}
                        {/* <div className="comment-checkbox-wrap">
                            {
                                checkboxValue.map((item,index) =>  
                                    <div key={index} className="comment-checkbox-inner">
                                        <label className="ezi-checkbox">
                                            <input type="checkbox" />
                                            <span className="ezi-checkbox-mark"></span>
                                        </label>
                                        <span className="commnet-check-name">{item}</span>
                                    </div>                                  
                                )                                
                            }
                        </div> */}
                        <SweetSearch loading={searchLoading} change={handleFilterSearch} ref={inputSearch} />
                        <div className="comment-filter-btn-wrap">
                            <button type="button" className="ezi-filter-btn" onClick={() => setShowFilter(!showFilter)}>Filter</button>
                            <CommentFilter
                                data={commentInsightData}
                                show={showFilter}
                                hide={() => setShowFilter(false)}
                                position="right"
                                applyFilter={(filters) => setCommentFilter(filters)}
                            />
                        </div>
                        {/* <button type="button" className="comment-download">Download</button> */}

                    </div>
                </div>
                <label className="ce-comment-title">Please share any other comments to serve you better</label>
                <div className="ce-comment-body">
                    <Comments comments={commentList} loading={loading} />
                </div>
                <div className="pagination-plugin-wrap">
                    <Pagination
                        activePage={commentPagination.current_page}
                        itemsCountPerPage={10}
                        totalItemsCount={commentPagination.total}
                        pageRangeDisplayed={5}
                        onChange={paginate}
                        hideDisabled={true}
                        firstPageText={<span class="prev-page-text-ic"></span>}
                        lastPageText={<span class="next-page-text-ic"></span>}
                        nextPageText={<span class="next-text-ic"></span>}
                        prevPageText={<span class="prev-text-ic"></span>}
                    />
                </div>
            </div>
        </React.Fragment>
    )
}

export default CommentSection;