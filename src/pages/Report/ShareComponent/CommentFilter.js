import React, { useState, useEffect, Fragment, useRef, useContext } from 'react';
import { Tab, Nav } from 'react-bootstrap';
import useOutsideClick from 'hooks/useOutsideClick';
import configURL from 'config/config';
import Axios from 'utility/Axios';
import { demographicFilter } from 'constants/dummy-data';
import AppContext from 'store/AppContext';
const CommentFilter = (props) => {

    const [filterActiveTab, setFilterActiveTab] = useState("sentiment");
    const { languageObj = {} } = useContext(AppContext)
    const [filterData, setFilterData] = useState(demographicFilter)
    const [filterHolderData, setFilterHolderData] = useState(demographicFilter)
    const [selectedFilter, setSelectedFilter] = useState([]);
    const [searchLoading, setSearchLoading] = useState(false);
    const [selectAll, setSelectAll] = useState([]);

    let filterRef = useRef();

    useOutsideClick(filterRef, () => {
        props.show && props.hide();
    });

    const sentimentData = [
        { id : '1' , label : 'Positive' , value: "postive"},
        { id : '2' , label : 'Neutral' , value: "neutral"},
        { id : '3' , label : 'Negative' , value: "Negative"}
    ]

    const moodData = [
        { id : '1' , label : 'Loving' , value: "loving"},
        { id : '2' , label : 'Happy' , value: "happy"},
        { id : '3' , label : 'Angry' , value: "angry"},
        { id : '4' , label : 'Sad' , value: "sad"},
        { id : '5' , label : 'Boring' , value: "boring"}
    ]
    const handleApplyFilter = () => {
        let filterData = [...selectedFilter];
        let tableFilters = [];
        filterData.forEach(item => {
            if (item.active === true) {
                tableFilters.push({ label: item.name, options: item.options })
            }
        });
        if (filterData.length == 0) {
            return;
        }
        props.applyFilter(tableFilters);
        props.hide();
    }
    const handleCheckOption = ({ target }) => {
        console.log(target);
        let checkedValue = target.value;
        let iSChecked = target.checked;
        let filters = [...selectedFilter];
        
        let searchIndex = filters.findIndex(target.value);
        console.log(searchIndex);
        if (iSChecked && checkedValue != "") {
            //filters[searchIndex].active = true;
            filters[searchIndex].push(checkedValue);
        }
        // if (!iSChecked && checkedValue != "") {
        //     let valueIndex = filters[searchIndex].options.indexOf(checkedValue);
        //     filters[searchIndex].options.splice(valueIndex, 1);
        //     filters[searchIndex].active = filters[searchIndex].options.length > 0;
        // }
        setSelectedFilter(filters);
    }
    const isValueChecked = (filterDemographic, value) => {
        

        let index = selectedFilter.findIndex(item => (item.name === filterDemographic && item.active === true));
        if (index >= 0) {
            let status = selectedFilter[index].options.includes(value);
            return status;
        } else {
            return false;
        }
    }
    const handleClearFilter = () => {
        const initialFilters = [];
        filterData.forEach(element => {
            initialFilters.push({
                name: element.name,
                options: [],
                active: false
            })
        });
        setSelectedFilter(initialFilters);
        setSelectAll([]);
        props.clearFilter();
    }
    return (
       <React.Fragment>
            <div ref={filterRef} className={`custom-demography-select-dropdown ${props.position} ${props.show ? "active" : ""}`}>
                <div className="demograpgy-filter-wrap">
                    <div className="demograpgy-filter-header">
                        <p className="demograpgy-filter-heading">Filters</p>
                        <div className="filter-btn-wraps">
                            <button type="button" className="btn-ripple ezi-pink-btn filter-apply-btn" onClick={handleApplyFilter}>Apply</button>
                            <button type="button" className="btn-ripple ezi-pink-btn filter-clear-btn" onClick={handleClearFilter}>Clear</button>
                            <button type="button" className="filter-close-btn" onClick={props.hide}>X</button>
                        </div>
                    </div>
                    <div className="demography-filter-inner">
                        <Tab.Container activeKey={filterActiveTab} onSelect={k => setFilterActiveTab(k)}>
                            <div className="filter-tab-header">
                                <Nav variant="pills">                                            
                                    <Nav.Item >
                                        <Nav.Link eventKey="sentiment">Sentiment</Nav.Link>
                                    </Nav.Item>                                            
                                    <Nav.Item >
                                        <Nav.Link eventKey="mood">Mood</Nav.Link>
                                    </Nav.Item>                                            
                                </Nav>
                            </div>
                            <Tab.Content className="demography-filter-tab-content">                               
                                <Tab.Pane eventKey="sentiment">
                                    <div className="comment-filter-list-wrapper">
                                        <ul> 
                                            {
                                            sentimentData.map((item,index) =>
                                                <li key={item.id}>
                                                    <div className="demography-checkbox-wrap">
                                                        <label className="ezi-checkbox">
                                                            <input type="checkbox" value={item.value} onChange={(e) => handleCheckOption(e, item.value)} checked={isValueChecked(item)} />
                                                            <span className="ezi-checkbox-mark"></span>
                                                        </label>
                                                        <span>{item.label}</span>
                                                    </div>
                                                </li> 
                                            )              
                                            }    
                                        </ul>
                                    </div>

                                </Tab.Pane>
                                <Tab.Pane  eventKey="mood">
                                    <div className="comment-filter-list-wrapper">
                                        <ul> 
                                            {
                                            moodData.map((item,index) =>
                                                <li key={item.id}>
                                                    <div className="demography-checkbox-wrap">
                                                        <label className="ezi-checkbox">
                                                            <input type="checkbox" value={item.value} />
                                                            <span className="ezi-checkbox-mark"></span>
                                                        </label>
                                                        <span>{item.label}</span>
                                                    </div>
                                                </li> 
                                            )              
                                            }    
                                        </ul>
                                    </div>
                                </Tab.Pane>
                            </Tab.Content>
                        </Tab.Container>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )

}

export default CommentFilter;