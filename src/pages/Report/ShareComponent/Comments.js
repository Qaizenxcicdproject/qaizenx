import React, { useState, Fragment, useContext } from 'react';
import ReplyCommentModel from 'pages/Report/ShareComponent/ReplyCommentModel';
import ReportContext from "pages/Report/ReportContext";

const Comments = ({ comments, loading }) => {
	const [showReply, setShowReply] = useState(false)
	const [respondentData, setRespondentData] = useState({ respondent_id: null, fname: null, lname: null, comment: null })
	const { surveyQuestionData: { survey_type = null, name: survey_name = null } } = useContext(ReportContext)

	const hideReplyModel = () => {
		setShowReply(false)
		setRespondentData({ respondent_id: null, fname: null, lname: null, comment: null })
	}

	const showReplyModel = ({ comments = "", respondent_fname = "", respondent_lname = "", respondent_id = "" }) => {
		setShowReply(true)
		setRespondentData({ respondent_id: respondent_id, fname: respondent_fname, lname: respondent_lname, comment: comments })
	}

	if (loading) {
		return <h2>Loading...</h2>;
	}

	return (
		<Fragment>
			{comments.length > 0 ? comments.map((item, index) =>
				<div className={`comment-box-emoji-wrap ${item.sentiment_score !== null && item.sentiment_score.toLowerCase()}`}>
					<div className="ce-comment-box">
						<div className="comment-box-text-wrap">
							<span className="comment-heading">{item.comments} </span>
							<div className="comment-badge-wrap">
								{item.key_phrases && item.key_phrases.map((key) => <span className="comment-badge">{key.Text}</span>)}
								{/* <span className="comment-badge">Variety</span> */}
							</div>
						</div>
						<div className="comment-box-right-text">
							<span className="comment-date-title">{(item.respondent_fname)}</span>
							<span className="comment-date">{item.sentiment_created_at} </span>
							{survey_type !== 'openSurvey' && <span className="comment-reply comment-date" onClick={() => showReplyModel(item)}>Reply</span>}
						</div>
					</div>
				</div>
			) : <span className="comment-not-found">Comments Not Found</span>}
			{(comments.length > 0 && survey_type !== 'openSurvey') && <ReplyCommentModel show={showReply} onHide={hideReplyModel} respondent_data={respondentData} survey_name={survey_name} />}
		</Fragment>
	);
}

export default Comments;