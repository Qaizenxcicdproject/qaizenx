import React, { useState, useEffect, useRef, useContext } from 'react';
import Axios from "utility/Axios";
import { ProgressBar } from "react-bootstrap";
import configURL from 'config/config';
import { toast } from "react-toastify";
import AppContext from 'store/AppContext';
import Pagination from "react-js-pagination";
import SweetSearch from 'components/SweetSearch';
import ReportContext from "pages/Report/ReportContext";
import ShowMoreText from 'react-show-more-text';
import ActionModal from '../ActionModal';

const GoogleReviewComponent = (props) => {

	const { filterData, datefilter, timezone, surveyId, reportVersion } = useContext(ReportContext);
	const { EziLoader } = useContext(AppContext)
	const [currentPage, setCurrentPage] = useState(1);
	const [commentFilter, setCommentFilter] = useState([]);
	const [commentData, setCommentData] = useState([]);
	const [commentPagination, setCommentPagination] = useState({});
	const inputSearch = useRef(null);
	const [searchLoading, setSearchLoading] = useState(false);
	const [searchValue, setSearchValue] = useState("");
	var searchTimer = null;
	const [commentCount, setCommentCount] = useState({})
	const [respondentData, setRespondentData] = useState({ google_id: null, fname: null, lname: null, comment: null, respondent_contact_number: null, respondent_email: null, ques_comment_id: null })
	const { selectedData: { identifier_id = null, identifier_type = null, identifier_name = null, theme_name = null, slider_value = null, theme_id = null, addon_filter = null } = {} } = props
	const [progressClass, setProgressClass] = useState(null);
	const [showMore, setShowMore] = useState(false);
	const [prevTaskData, setPrevTaskData] = useState({ is_completed: false, data: null })
	const [show, setShow] = useState(false);
	const handleCloseAction = () => {
		setShow(false)
		setPrevTaskData({ is_completed: false, data: null })
		setRespondentData({ google_id: null, fname: null, lname: null, comment: null, respondent_contact_number: null, respondent_email: null, ques_comment_id: null })
	}
	const handleShowAction = ({ comments = "", respondent_fname = "", respondent_lname = "", id = "", respondent_contact_number = "", respondent_email = "", ques_comment_id = "" }) => {
		setShow(true)
		checkActionStatus(id);
		setRespondentData({ google_id: id, fname: respondent_fname, lname: respondent_lname, comment: comments, respondent_contact_number: respondent_contact_number, respondent_email: respondent_email, ques_comment_id: ques_comment_id })
	}

	const handlePaginate = (pageNumber) => {
		setCurrentPage(pageNumber);
	}

	/**
	 * Handle comment search
	 */
	const handleCommentSearch = () => {
		clearTimeout(searchTimer);
		searchTimer = setTimeout(() => {
			setSearchLoading(true)
			if (props.searchWordCloud !== "" && props.searchWordCloud !== undefined) {
				props.setWordCloudSearch();
			}
			let searchVal = inputSearch.current.value;
			setSearchValue(searchVal)
		}, 800);
	}

	const onProgressHandle = (e) => {
		if (props.menuTitle !== "analysis") {
			props.callbackSentiment(e.target.title)
		}
		setCommentFilter([{ "label": "sentiment", "options": [e.target.title] }])
		setProgressClass(e.target.title)
	}

	const handleTotalComment = (e) => {
		if (props.menuTitle !== "analysis") {
			props.callbackSentiment('Total')
		}
		setCommentFilter([])
		setProgressClass(null)
	}

	/**
	 * Sentiment comment listing
	 */
	const googleReviewListing = () => {
		
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("report_type", reportVersion);
		if (props.searchWordCloud !== "" && props.searchWordCloud !== undefined) {
			formData.append("search", props.searchWordCloud);
		}
		else {
			formData.append("search", searchValue);
		}
		formData.append("comment_filters", JSON.stringify(commentFilter));
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		formData.append("current_page", currentPage);
		if (props.menuTitle === "analysis") {
			formData.append("demographic_id", identifier_id);
			formData.append("demographic_type", identifier_type);
			formData.append("sliderValue", slider_value);
			formData.append("theme_id", theme_id);
			formData.append("addon_filter", addon_filter ? JSON.stringify(addon_filter) : JSON.stringify([]));
			formData.append("roundDecimalPoint", "2");
			formData.append("calculation_type", props.avgScore);
		}
		Axios.post(configURL.google_review_list, formData).then(response => {
			EziLoader.hide();
			setSearchLoading(false);
			if (response.data.success !== undefined && response.data.success) {
				let comments = response.data.result.data;
				let validComments = [];
				comments.forEach(item => {
					if (item.respondent_email && validateEmail(item.respondent_email)) {
						validComments.push(item);
					}
				});
				setCommentData(response.data.result.data);
				setCommentPagination(response.data.result.current_page);
			}
			else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			setSearchLoading(false);
			console.log(err);
		})
	}

	/**
	*  Comment Count Listing
	* */
	const commentCountListing = () => {
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		if (props.menuTitle === "analysis") {
			formData.append("demographic_id", identifier_id);
			formData.append("demographic_type", identifier_type);
			formData.append("sliderValue", slider_value);
			formData.append("theme_id", theme_id);
			formData.append("addon_filter", addon_filter ? JSON.stringify(addon_filter) : JSON.stringify([]));
			formData.append("roundDecimalPoint", "2");
			formData.append("calculation_type", props.avgScore);
		}
		Axios.post(configURL.google_review_count, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setCommentCount(response.data.result)
			}
			else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			setSearchLoading(false);
			console.log(err);
		})
	}
	const checkActionStatus = (respondentId) => {
		if (respondentId !== "") {
			EziLoader.show();
			Axios.get(configURL.task_status + respondentId).then(res => {
				setPrevTaskData({ is_completed: res.data.result.is_completed || false, data: res.data.result.data || null })
				EziLoader.hide();
			}).catch(err => {
				console.log(err)
				EziLoader.hide();
				toast.warn("Something went wrong.")
			})
		}
	}
	
	const validateEmail = (email) => {
		return email.match(
			// eslint-disable-next-line no-control-regex
			/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
		);
	}

	useEffect(() => {
		EziLoader.show();
		if (props.searchWordCloud !== "" && props.searchWordCloud !== undefined) {
			inputSearch.current.value = props.searchWordCloud;
			googleReviewListing();
		}
		else if ((props.menuTitle === "analysis" && identifier_name && theme_name && slider_value && addon_filter)) {
			googleReviewListing();
		}
		else {
			googleReviewListing();
		}

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [filterData, datefilter, currentPage, commentFilter, searchValue, props.avgScore, identifier_name, theme_name, slider_value, addon_filter, respondentData, props.searchWordCloud]);

	useEffect(() => {
		EziLoader.show();
		if ((props.menuTitle === "analysis" && identifier_name && theme_name && slider_value && addon_filter)) {
			commentCountListing();
		}
		else {
			commentCountListing();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [filterData, datefilter, props.avgScore, identifier_name, theme_name, slider_value, addon_filter]);


	return (
        <div className="Page-GoogleReview">
		<div className="ce-comment-section">
			<div className="ce-comment-section-header">
				<label className="commnet-label" onClick={() => handleTotalComment()}>
					<span className="commnet-label-text">Total Comments</span>
					<span className="commnet-label-count">{commentCount.total}</span>
				</label>
				<ProgressBar>
					<ProgressBar title="Positive" className={`positive ${("Positive" === progressClass) && "activeProgress"}`} key={1} label={(commentCount.positive === 0) ? 0 : `${commentCount.positive}%`} min={commentCount.positive} max={(commentCount.positive === 0) ? 0 : commentCount.positive} now={commentCount.positive} onClick={(e) => onProgressHandle(e)} style={{ width: `${commentCount.positive}%` }} />
					<ProgressBar title="Neutral" className={`neutral ${("Neutral" === progressClass) && "activeProgress"}`} key={2} label={(commentCount.neutral === 0) ? 0 : `${commentCount.neutral}%`} min={commentCount.neutral} max={(commentCount.neutral === 0) ? 0 : commentCount.neutral} now={commentCount.neutral} onClick={(e) => onProgressHandle(e)} style={{ width: `${commentCount.neutral}%` }} />
					<ProgressBar title="Negative" className={`negative ${("Negative" === progressClass) && "activeProgress"}`} key={3} label={(commentCount.negative === 0) ? 0 : `${commentCount.negative}%`} min={commentCount.negative} max={(commentCount.negative === 0) ? 0 : commentCount.negative} now={commentCount.negative} onClick={(e) => onProgressHandle(e)} style={{ width: `${commentCount.negative}%` }} />
				</ProgressBar>
				<div className="comment-right-header">
					<SweetSearch loading={searchLoading} change={handleCommentSearch} ref={inputSearch} />
				</div>
			</div>

			<div className="ce-comment-body">
				{
					commentData.length > 0 ? commentData.map((item, index) =>
						<div key={`clist${item.id}`} className={`comment-box-emoji-wrap ${item.sentiment_score !== null && item.sentiment_score.toLowerCase()}`}>
							<div className="ce-comment-box">
								<div className="comment-box-text-wrap">
									<span className="comment-heading">
										<ShowMoreText
											lines={3}
											more='Show more'
											less='Show less'
											className='content-css'
											anchorClass='show-more-anchor'
											onClick={() => setShowMore(!showMore)}
											expanded={showMore}
										>
											{item.comments}
										</ShowMoreText>
									</span>
								</div>
								<div className="comment-box-right-text">
									<div className="comment-box-right-text-section">
										<span className="comment-date-title">{(item.google_user_name)}</span>
										<span className="comment-date">{item.comment_date} </span>
									</div>
									<div className="comment-box-right-text-section">

										{item.account_id &&
											<div className='action-sentiment'>
												
												<div className="action-sentiment">
													<span className="comment-reply comment-date" onClick={() => handleShowAction(item)}>Action {item.is_task_created === true && <img className="checked-complete" alt="" src={require(`../../../assets/images/report/check.png`)} />}</span>
												</div>
											</div>
										}
									</div>
								</div>
							</div>
						</div>
					) : <span className="comment-not-found">Comments Not Found</span>
				}
				{((commentData.length > 0) && show) &&
					<ActionModal
						show={show}
						onHide={handleCloseAction}
						respondent_data={respondentData}
						prevTaskData={prevTaskData}
						respondantReport={true}
						actionType='COMMENT'
					/>
				}
			</div>
			{(commentPagination && commentPagination.current_page && commentPagination.total !== 0) &&
				<div className="pagination-plugin-wrap">
					<Pagination
						activePage={commentPagination.current_page}
						itemsCountPerPage={10}
						totalItemsCount={commentPagination.total}
						onChange={handlePaginate}
						hideDisabled={true}
						firstPageText={<span className="prev-page-text-ic"></span>}
						lastPageText={<span className="next-page-text-ic"></span>}
						nextPageText={<span className="next-text-ic"></span>}
						prevPageText={<span className="prev-text-ic"></span>}
					/>
				</div>
			}

		</div>
        </div>
	);
}

export default GoogleReviewComponent;