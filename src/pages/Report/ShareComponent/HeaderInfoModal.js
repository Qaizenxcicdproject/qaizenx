
import React from 'react';
import { Modal } from 'react-bootstrap';

const HeaderInfoModal = (props) => {
   
    return(           
        <React.Fragment>

            <Modal {...props}  size="lg" aria-labelledby="ezi-modal-for" centered className="ezi-modal-wrapper info-alert_c" >
                <Modal.Header className="ezi-modal-header">
                    <Modal.Title id="ezi-modal-for" className="ezi-modal-title-wrap" >
                        <span className="ezi-modal-header-title">{props.title}</span>
                        <span className="ezi-modal-close"  onClick={props.onHide}></span>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>

                {(() => {
                    switch (props.renderModal) {

                        case 'questions':
                            return (
                                <React.Fragment>
                                    <p className="header-info-description">It represents the quantitative analysis of all questions which are asked in the feedback where we want to do further analysis.</p>
                                    <p className="header-info-description">It shows the responses trend and score break-up within the particular time period. We can apply various demographic filters to get the intended results.</p>
                                </React.Fragment>
                            );

                        case 'themes':
                            return (
                                <React.Fragment>
                                    <p className="header-info-description">It represents the quantitative analysis of the follow up questions which are asked in the feedback. These questions are categorized in different themes where we want to do further analysis.</p>
                                    <p className="header-info-description">It shows the responses trend and score break-up within the particular time period. We can apply various demographic filters to get the intended results.</p>
                                </React.Fragment>
                            );

                        case 'insights':
                            return (
                                <React.Fragment>
                                    <p className="header-info-description">Shows the overall analysis of the feedbacks gathered over period.</p>
                                    <p className="header-info-description">The main KPI <span className="bold_c">({props.reportLabel})</span> value helps to understand the customer satisfaction or advocacy during defined time-period. It also shows the latest ‘Score’ which is either today’s or last date of select date of selected range.</p>
                                    <p className="header-info-description"><span className="bold_c">‘Score Break-up’</span> is the break-up of satisfaction choices respondent have chosen.</p>
                                    <p className="header-info-description"><span className="bold_c">‘Trend’</span> defines how is the satisfaction level is varying over ‘Hourly’, ‘Daily’, ‘Weekly’, & ‘Monthly’. It helps to understand which Hr or Day is bad. Similarly <span className="bold_c">‘Distribution’</span> shows which is the dominant customer mood in particular Hour of the day or Day of the Month.</p>
                                    <p className="header-info-description"><span className="bold_c">‘Reason’</span> will help you to understand what are the reasons as per customer for their particular ratings of your service/product.</p>
                                    <p className="header-info-description"><span className="bold_c">‘Sentiments’</span> tells us about what is the opinion of the customers, what is bothering them, what are their suggestions to improve the services/products. Each comment is categorized as ‘Positive’, ’Negative’, & ‘Neutral’ sentiments. These comments sentiments can be combined with actual ratings to understand what is missing and what is working well with our products & services. From the <span className="bold_c">‘Word Cloud’</span> check the frequent words and search them in comments to make sense of overall feeling.</p>
                                    <p className="header-info-description">All the analysis can be done on particular ‘Demographics’/’Identifiers’ of the respondents shown in ‘Filter’. These identifiers are applied with ‘And’ logic. The reports period can also be changed.</p>
                                </React.Fragment>
                                );

                        case 'analysis':
                            return (
                                <React.Fragment>
                                    <p className="header-info-description"><span className="bold_c">'{props.reportLabel} Analysis’</span> helps to understand how {props.reportLabel} by particular demographics / identifiers is changing / evolving over the selected period. Understand the reasons and negative comments which are bothering the selected demographics of participants. You can use the filters along with selected identifiers to have deep understanding to act accordingly to improve them.</p>
                                </React.Fragment>
                            );

                        case 'statistics':
                            return (
                                <React.Fragment>
                                    {/* <p className="header-info-description"><span className="bold_c">‘Chi-Square’</span> statistical analysis is used to understand the relationship between identifiers of the respondents and questions they are answering. This analysis is important to understand the pattern in which respondents gives feedback. Is there any influence of their Age, City, Location, Department, Gender on the feedback they are giving. Use this analysis in conjunction with the demographic filters in ‘Insights’ section to take informed decision on the areas & demographic segments to improve {props.reportLabel}.</p> */}
                                    <p className="header-info-description"><span className="bold_c">‘Coefficient of Correlation’</span> analysis helps to understand which are those statements or activities which has biggest impact on driving the overall satisfaction ( from overriding statement ). More is the <span className="bold_c">COC</span> ( &gt; 0.1 ) bigger is the impact. We should focus on those activities/features to make them perfect. Negative COC indicates the reverse relationship i.e if we improve statements whose values  &lt; -0.7 , it would rather reduce the satisfaction.</p>
                                </React.Fragment>
                            );

                        case 'response':
                            return (
                                <React.Fragment>
                                    <p className="header-info-description">User will get the demographic wise response rate as per the selected demographics.</p>
                                </React.Fragment>
                            );

                        case 'respondent':
                            return (
                                <React.Fragment>
                                    <p className="header-info-description">User will get the individual response report from this section.</p>
                                </React.Fragment>
                            );

                        case 'download':
                            return (
                                <React.Fragment>
                                    <p className="header-info-description">This section Allows to download reports of various types.</p>
                                </React.Fragment>
                            );
                        
                        default:
                            return null


                    }
                })()}
                    
                </Modal.Body>
            </Modal>

        </React.Fragment>
    )
}

export default HeaderInfoModal;