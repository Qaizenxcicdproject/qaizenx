import React, { useState } from "react";
import { Modal, Spinner } from 'react-bootstrap';
import useForm from 'react-hook-form';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';
import { useParams } from "react-router-dom";


const ReplyCommentModel = ({ show, onHide, respondent_data, survey_name }) => {
    const { register, handleSubmit, errors } = useForm();
    const [saving, setSaving] = useState(false)
    const urlParams = useParams();

    const onSubmit = data => {
        setSaving(true)
        let formData = new FormData()
        formData.append("survey_id", urlParams.survey_id)
        formData.append("respondent_id", respondent_data.respondent_id)
        formData.append("reply_msg", data.reply_msg)
        formData.append("reply_subject", data.reply_subject)
        formData.append("comment", respondent_data.comment)

        Axios.post(configURL.reply_survey_comment, formData).then(res => {
            setSaving(false)
            if (res.data.success === true) {
                toast.success(res.data.message || `Reply email sent to ${respondent_data.fname || 'User'} ${respondent_data.lname || ''}`)
                onHide()
            } else {
                toast.warn(res.data.message || "Something went wrong.")
            }
        }).catch(err => {
            setSaving(false)
            console.log(err)
            toast.warn("Something went wrong.")
        })
    };

    return (
        <Modal show={show} onHide={onHide} size="md" aria-labelledby="contained-modal-title-vcenter" centered className="theme-modal-wrapper" >
            <Modal.Header className="ezi-modal-header">
                <Modal.Title id="contained-modal-title-vcenter" className="theme-modal-title ezi-modal-header-title" >
                    <span className="theme-modal-title-text">Reply to {respondent_data.fname || "User"} {respondent_data.lname || ''}</span>
                    <span className="ezi-modal-close" onClick={onHide}></span>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit(onSubmit)} className="add-theme-form">
                    <div className="theme-field-wrapper">
                        <div className="theme-field">
                            <div className="theme-field">
                                <input type="text" className="theme-field-control" name="reply_subject" ref={register({ required: true })} defaultValue={`RE: ${survey_name}`} placeholder="Please enter reply subject" />
                                {errors.reply_subject && <span className="theme-error_cu">* Reply subject is required.</span>}
                            </div>
                        </div>
                        <div className="theme-field">
                            <div className="theme-field">
                                <textarea className="theme-field-control" name="reply_msg" ref={register({ required: true })} rows={6} placeholder="Please enter reply message"></textarea>
                                {errors.reply_msg && <span className="theme-error_cu">* Reply message is required.</span>}
                            </div>
                        </div>
                    </div>
                    <div className="theme-modal-footer">
                        <button type="button" className="close-theme-btn" onClick={onHide}>Close</button>
                        <button type="submit" className="btn-ripple ezi-pink-btn add-theme-btn" disabled={saving}>
                            Reply {saving && <Spinner animation="border" size="sm" />}
                        </button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}

export default ReplyCommentModel