import React, { useState, useEffect, Fragment, useRef } from 'react';
import { Tab, Nav } from 'react-bootstrap';
import useOutsideClick from 'hooks/useOutsideClick';
import configURL from 'config/config';
import Axios from 'utility/Axios';
import { useParams } from "react-router-dom";
import { Modal } from 'react-bootstrap';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import DateRangePicker from '@wojtekmaj/react-daterange-picker'

const ReportCommentFilter = (props) => {
    const urlParams = useParams();
    const [surveyId] = useState(urlParams.survey_id);
    const [filterActiveTab, setFilterActiveTab] = useState();
    const [filterData, setFilterData] = useState([]);
    const [selectedFilter, setSelectedFilter] = useState([]);
    const [selectAll, setSelectAll] = useState([]);
    let filterRef = useRef();
    const [filterTabKey, setFilterTabKey] = useState('participant');
    const [filterActiveTabParticipant, setFilterActiveTabParticipant] = useState();
    const [filterDataParticipant, setFilterDataParticipant] = useState([]);
    const [selectedFilterParticipant, setSelectedFilterParticipant] = useState([]);
    const [selectAllParticipant, setSelectAllParticipant] = useState([]);
    const [participantFlag, setParticipantFlag] = useState(false);
    const [surveyFlag, setSurveyFlag] = useState(false);
    const [visibleFlag,setVisibleFlag] =useState(false);
    const [showModel, setShowModel] = useState(false);    
    const [additionalFilters, setAdditionalFilters] = useState({})

    const handleShow = () => {
        setShowModel(true);
        props.hide();
    }
    
    const handleVisibilty = () =>{
        let filterParticipantFlag=false;
        let filterFlag=false;
        selectedFilter && selectedFilter.map((key)=>{
             if(key.active=== true){
              if(key.options.length>=1){
                filterFlag =true;
                return filterFlag;
              }                              
            }
             return null;
         
        })
        selectedFilterParticipant && selectedFilterParticipant.map((key)=>{
            if(key.active===true){
                if(key.options.length>=2){
                    filterParticipantFlag =true;
                    return filterParticipantFlag;                                  
                }
            }
            return null;
       })   
       if(filterFlag === true || filterParticipantFlag ===true ){
         setVisibleFlag(true);
       } else{
         setVisibleFlag(false);
       }  
    
    }

    useOutsideClick(filterRef, () => {
        props.show && props.hide();
    });
    const surveyDemographics = () => {
        let initialFilters = [];
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        Axios.post(configURL.ReportDemographicFilter, formData).then(response => {
            if (response.data.success !== undefined && response.data.success === true) {
                // responseData = response.data.data[languageObj.curLang];
                setSurveyFlag(true);
                setFilterData(response.data.result)
                if (response.data.result.length > 1) {
                    setFilterTabKey('survey');
                    setFilterActiveTab(response.data.result[0]['label'])
                }
                response.data.result.forEach(element => {
                    initialFilters.push({
                        name: element.name,
                        options: [],
                        active: false,
                        question_type:element.question_type
                    })
                });
                setSelectedFilter(initialFilters);
            }
        })
    }
    const participantDemographics = () => {
        let initialFilters = [];
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        Axios.post(configURL.ReportFrontEndDemographicFilter, formData).then(response => {

            if (response.data.success !== undefined && response.data.success === true) {
                setParticipantFlag(true);
                setFilterDataParticipant(response.data.result.data);
                if (response.data.result.data.length >= 1) {
                    setFilterActiveTabParticipant(response.data.result.data[0]['label'])
                }
                response.data.result.data.forEach(element => {
                    initialFilters.push({
                        name: element.name,
                        options: [],
                        active: false,
                        label: element.label,
                        question_type:element.question_type
                    })
                });
                setSelectedFilterParticipant(initialFilters);
            } else {
                setFilterDataParticipant();
            }
        })
    }

    const hanleCheckFilterAll = ({ target }, demography) => {
        let isAllChecked = target.checked;
        let allFilters = JSON.parse(JSON.stringify(filterData));
        let selectedFilters = JSON.parse(JSON.stringify(selectedFilter));
        let searchIndex = allFilters.findIndex(item => item.name === demography);
        let checkOpts = [...selectAll];
        if (isAllChecked) {
            let allOpts = allFilters[searchIndex].options;
            selectedFilters[searchIndex].options = allOpts;
            selectedFilters[searchIndex].active = true;
            checkOpts.push(demography);

        } else {
            selectedFilters[searchIndex].options = [];
            selectedFilters[searchIndex].active = false;
            checkOpts.splice(checkOpts.indexOf(demography), 1);
        }
        setSelectAll(checkOpts);
        setSelectedFilter(selectedFilters);
    }
    const hanleCheckFilterAllParticipant = ({ target }, demography) => {

        const filterValue = [];
        let isAllChecked = target.checked;
        let allFilters = JSON.parse(JSON.stringify(filterDataParticipant));
        let selectedFilters = JSON.parse(JSON.stringify(selectedFilterParticipant));
        // console.log("selected Filter",selectedFilters);
        let searchIndex = allFilters.findIndex(item => item.name === demography);
        let checkOpts = [...selectAllParticipant];
        if (isAllChecked) {
            let allOpts = allFilters[searchIndex].options;
            selectedFilters[searchIndex].options = allOpts;
            selectedFilters[searchIndex].active = true;
            checkOpts.push(demography);

        } else {
            selectedFilters[searchIndex].options = [];
            selectedFilters[searchIndex].active = false;
            checkOpts.splice(checkOpts.indexOf(demography), 1);
        }
        // console.log("all",selectedFilters);
        selectedFilters.map((item, i) =>
            filterValue.push({
                name: item.name,
                label: item.label,
                options: item.options.map((el) =>
                    (el.value ? el.value : el)
                ),
                question_type:item.question_type,
                active: item.active,
            })
        )
        setSelectAllParticipant(checkOpts);
        setSelectedFilterParticipant(filterValue);
    }

    const handleCheckOption = ({ target }, demograpgy) => {
   
        let checkedValue = target.value;
        let iSChecked = target.checked;
        let filters = [...selectedFilter];
        
        let searchIndex = filters.findIndex(item => item.name === demograpgy);
        if (iSChecked && checkedValue !== "") {
            filters[searchIndex].active = true;
            filters[searchIndex].options.push(checkedValue);
        }
        if (!iSChecked && checkedValue !== "") {
            let valueIndex = filters[searchIndex].options.indexOf(checkedValue);
            filters[searchIndex].options.splice(valueIndex, 1);
            filters[searchIndex].active = filters[searchIndex].options.length > 0;
        }
        setSelectedFilter(filters);
    }
    
    const handleCheckParticipantOption = ({ target }, demograpgy) => {
        let checkedValue = target.value;
        let iSChecked = target.checked;
        let filters = [...selectedFilterParticipant]
        let searchIndex = filters.findIndex(item => item.name === demograpgy);
        if (iSChecked && checkedValue !== "") {
            filters[searchIndex].active = true;
            filters[searchIndex].options.push(checkedValue);
        }
        if (!iSChecked && checkedValue !== "") {
            let valueIndex = filters[searchIndex].options.indexOf(checkedValue);
            filters[searchIndex].options.splice(valueIndex, 1);
            filters[searchIndex].active = filters[searchIndex].options.length > 0;
        }
        setSelectedFilterParticipant(filters);
    }
    const handleApplyFilter = () => {
        let filterData = [...selectedFilter];
        let filterDataParticipant = [...selectedFilterParticipant];
        let tableFilters = [];
        filterData.forEach(item => {           
            if (item.active === true) {
                tableFilters.push({ label: item.name, options: item.options, question_type: item.question_type })
            }
        });
        let tableFiltersParticipant = [];
        filterDataParticipant.forEach(item => {
            if (item.active === true) {
                tableFiltersParticipant.push({ label: item.name, options: item.options, displayLabel: item.label,question_type: item.question_type  })
            }
        })
        Object.keys(additionalFilters).forEach(el=>{
            tableFilters.push({ label: el, options: additionalFilters[el].options, question_type:additionalFilters[el].question_type})
        })
        props.applyFilter({ 'participant': tableFilters, 'survey': tableFiltersParticipant });
        props.hide();
    }

    const isValueChecked = (filterDemographic, value) => {
        let index = selectedFilter.findIndex(item => (item.name === filterDemographic && item.active === true))
        if (index >= 0) {
            let status = selectedFilter[index].options.includes(value);
            return status;
        } else {
            return false;
        }
    }
    const isValueCheckedParticipant = (filterDemographic, value) => {
        let index = selectedFilterParticipant.findIndex(item => (item.name === filterDemographic && item.active === true))
        if (index >= 0) {
            let status = selectedFilterParticipant[index].options.includes(value);
            return status;
        } else {
            return false;
        }
    }

    const handleClearFilter = () => {
        const initialFilters = [];
        const initialFiltersParticipant = [];
        Array.isArray(filterData) && filterData.forEach(element => {
            initialFilters.push({
                name: element.name,
                options: [],
                active: false,
                question_type:element.question_type
            })
        });
        Array.isArray(filterDataParticipant) && filterDataParticipant.forEach(element => {
            initialFiltersParticipant.push({
                name: element.name,
                options: [],
                active: false,
                label:element.label,
                question_type:element.question_type
            })
        });
        setSelectedFilterParticipant(initialFiltersParticipant);
        setSelectedFilter(initialFilters)
        setSelectAll([]);
        setSelectAllParticipant([]);
        setVisibleFlag(false);
        setAdditionalFilters({})
        props.clearFilter();
    }
    const getSelectedFilters = (node = "") => {
        let index = selectedFilter.findIndex(item => (item.name === node && item.active === true));
        if (index >= 0) {
            return selectedFilter[index].options.length;
        } else {
            return 0;
        }
    }
    const getSelectedFiltersParticipant = (node = "") => {
        let index = selectedFilterParticipant.findIndex(item => (item.name === node && item.active === true));

        if (index >= 0) {
            return selectedFilterParticipant[index].options.length;
        } else {
            return 0;
        }
    }
    const removeDemographicOption = (demograpgy,checkedValue,type) => {            
        if(type==="participant"){       
            let filters = [...selectedFilter];
            let searchIndex = filters.findIndex(item => item.name === demograpgy);    
            let valueIndex = filters[searchIndex].options.indexOf(checkedValue);
            filters[searchIndex].options.splice(valueIndex, 1);
            filters[searchIndex].active = filters[searchIndex].options.length > 0;
            setSelectedFilter(filters);   
        } 
        if(type==="survey"){                     
            let filters = [...selectedFilterParticipant];
            let searchIndex = filters.findIndex(item => item.name === demograpgy);
            let valueIndex = filters[searchIndex].options.indexOf(checkedValue);
            filters[searchIndex].options.splice(valueIndex, 1);
            filters[searchIndex].active = filters[searchIndex].options.length > 0;
            setSelectedFilterParticipant(filters);

        }    
        handleApplyFilter();
    }
    // const removeDemographic = (demograpgy,type) => {
    //   if(type==="participant"){       
    //         let filters = [...selectedFilter];
    //         let searchIndex = filters.findIndex(item => item.name === demograpgy); 
    //         let arrayLength=filters[searchIndex].options.length;
    //         filters[searchIndex].options.splice(0, arrayLength);
    //         filters[searchIndex].active = filters[searchIndex].options.length > 0;
    //         setSelectedFilter(filters);   
    //     } 
    //     if(type==="survey"){                     
    //         let filters = [...selectedFilterParticipant];
    //         let searchIndex = filters.findIndex(item => item.name === demograpgy);
    //         let arrayLength=filters[searchIndex].options.length;
    //         filters[searchIndex].options.splice(0, arrayLength);
    //         filters[searchIndex].active = filters[searchIndex].options.length > 0;
    //         setSelectedFilterParticipant(filters);

    //     }    
    //     handleApplyFilter();
    // }
    
  
    useEffect(surveyDemographics, []);
    useEffect(participantDemographics, []);
    
    useEffect(() => {
        handleVisibilty();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedFilter ,selectedFilterParticipant])

    return (
        <Fragment>
            <Modal show={showModel} onHide={() => {setShowModel(false)}}  className="ezi-right-animated-modal theme-mapping-right-modal filter-selection-modal" backdropClassName="ezi-right-modal-backdrop" >
                <Modal.Header className="ezi-modal-header">
                    <Modal.Title className="theme-modal-title ezi-modal-header-title" >
                        <span className="theme-modal-title-text">Filters</span>
                        <span className="ezi-modal-close" onClick={() => {setShowModel(false)}}></span>
                    </Modal.Title>
                </Modal.Header>           
                <Modal.Body>
                    <div className="filter-selection-content">                    
                                {     
                                        selectedFilter  &&
                                        <div className="participant">
                                            <div className="applied-filter-badge" > 
                                                {
                                                    selectedFilter && selectedFilter && selectedFilter.map((key,index)=>                                            
                                                    <div key={index} className="applied-filter-inner">{key.options.length>0 && <span className="filter-modal-heading">{key.name} {/* <button type="button" className='modal-badge-cross' key={index} onClick={(e) => removeDemographic(key.name,'participant')}>X</button> */} :</span>}<div className='modal-badges-wrapper'>{key.options.map((item,val)=><span key={val} className="filter-modal-badge">{item} <button type="button" key={val} onClick={(e) => removeDemographicOption(key.name,item,'participant')}> X</button></span>)}</div></div>
                                                    )
                                                }
                                            </div>
                                        </div>
                                }
                                { 
                                    selectedFilterParticipant &&
                                    <div className="survey">
                                        <div className="applied-filter-badge" > 
                                            {
                                                selectedFilterParticipant && selectedFilterParticipant.map((key,index)=>
                                                <div key={index} className="applied-filter-inner">{key.options.length>0 &&<span className="filter-modal-heading">{key.label} {/* <button type="button" key={index}  className='modal-badge-cross' onClick={(e) => removeDemographic(key.name,'survey')}>X</button> */} :</span>}<div className='modal-badges-wrapper'>{key.options.map((item,val)=><span key={val} className="filter-modal-badge">{item} <button type="button" key={val} onClick={() => removeDemographicOption(key.name,item,'survey')}> X</button></span>)}</div></div>
                                                )
                                            }
                                         </div>
                                    </div>
                                }                           
                            </div>
                </Modal.Body>
            </Modal>
            <div ref={filterRef} className={`custom-demography-select-dropdown ${props.position} ${props.show ? "active" : ""}`}>
                <div className="demograpgy-filter-wrap">
                    <div className="demograpgy-filter-header">
                        {/* <p className="demograpgy-filter-heading">Filters</p> */}
                        {surveyFlag === true ? <button type="button" className={`filter-tab-button ${(filterTabKey === 'survey') && 'active'}`} onClick={() => setFilterTabKey('survey')}>Participant</button> : <span></span>}
                        {participantFlag === true ? <button type="button" className={`filter-tab-button ${(filterTabKey === 'participant') && 'active'}`} onClick={() => setFilterTabKey('participant')}>Survey</button> :
                            <span></span>
                        }

                        <div className="filter-btn-wraps">
                            <button type="button" className="btn-ripple ezi-pink-btn filter-apply-btn" onClick={handleApplyFilter}>Apply</button>
                            <button type="button" className="btn-ripple ezi-pink-btn filter-clear-btn" onClick={handleClearFilter}>Clear</button>
                            <button type="button" className="filter-close-btn" onClick={props.hide}>X</button>
                        </div>
                    </div>
                    <div className={`demography-filter-inner ${filterData || filterDataParticipant ? '' : 'demography-not-found-bg'}`}>

                        {(filterData && filterData.length > 0) || (filterDataParticipant && filterDataParticipant.length > 0 )

                            ? (filterTabKey === 'participant' && participantFlag === true) ?

                                <Tab.Container activeKey={filterActiveTabParticipant} onSelect={k => setFilterActiveTabParticipant(k)}>
                                    <div className="filter-tab-header">
                                        <Nav variant="pills" >
                                            {
                                                filterDataParticipant.map((item, index) =>
                                                    <Nav.Item key={index}>
                                                        <Nav.Link eventKey={item.label}>
                                                            {item.label}
                                                            {selectedFilter.length > 0 && getSelectedFiltersParticipant(item.name) > 0 && <span className="checkbox-count">{getSelectedFiltersParticipant(item.name)}</span>}
                                                        </Nav.Link>
                                                    </Nav.Item>
                                                )
                                            }
                                          
                                        </Nav>
                                    </div>
                                    <Tab.Content className="demography-filter-tab-content">
                                        {
                                            filterDataParticipant.map((item, i) =>
                                                <Tab.Pane eventKey={item.label} key={i}>
                                                    <div className="demography-filter-search-wrap">
                                                        <div className="check_all_wrapper">
                                                            <input className="check_all_checkbox" type="checkbox" onChange={(e) => hanleCheckFilterAllParticipant(e, item.name)} />
                                                            <span className={`check_all_txt ${selectAllParticipant.includes(item.name) ? "checkbox-active" : ""}`}>All</span>
                                                        </div>
                                                    </div>
                                                    <ul>

                                                        {
                                                            (item.options && item.options.length > 0) ? item.options.map((el, index) =>
                                                                <li key={index}>
                                                                    <div className="demography-checkbox-wrap">
                                                                        <label className="ezi-checkbox">
                                                                            <input type="checkbox" value={el.value} onChange={(e) => handleCheckParticipantOption(e, item.name)} checked={isValueCheckedParticipant(item.name, el.value)} />
                                                                            <span className="ezi-checkbox-mark"></span>
                                                                        </label>
                                                                        <span>{(typeof el.text === "object") ? el.text.default : el.text}</span>
                                                                    </div>
                                                                </li>
                                                            ) : <span className="demographic_not_found">data not found</span>
                                                        }
                                                    </ul>
                                                </Tab.Pane>
                                            )
                                        }
                                      

                                    </Tab.Content>
                                </Tab.Container>

                                :

                                <Tab.Container activeKey={filterActiveTab} onSelect={k => setFilterActiveTab(k)}>
                                    <div className="filter-tab-header">
                                        <Nav variant="pills" >
                                            {
                                                filterData.map((item, index) =>
                                                    <React.Fragment>
                                                        {(() => {
                                                            switch (item.question_type) {  
                                                                case "datetime": return (
                                                                    <Nav.Item>
                                                                        <Nav.Link eventKey={item.label}>{item.label}</Nav.Link>
                                                                    </Nav.Item>
                                                                );
                                                                case "numeric": return (
                                                                    <Nav.Item>
                                                                        <Nav.Link eventKey={item.label}>{item.label}</Nav.Link>
                                                                    </Nav.Item>
                                                                );                                                             
                                                                default:
                                                                    return (
                                                                        <Nav.Item key={index}>
                                                                            <Nav.Link eventKey={item.label}>
                                                                                {item.label}
                                                                                {selectedFilter.length > 0 && getSelectedFilters(item.name) > 0 && <span className="checkbox-count">{getSelectedFilters(item.name)}</span>}
                                                                            </Nav.Link>
                                                                        </Nav.Item>
                                                                    )
                                                            }
                                                        })()}
                                                    </React.Fragment>
                                                    
                                                )
                                            }
                                        </Nav>
                                    </div>
                                    <Tab.Content className="demography-filter-tab-content">
                                        {
                                            filterData.map((item, i) =>
                                            <React.Fragment>
                                                    {(() => {
                                                        switch (item.question_type) {                                                        
                                                            case "datetime": return (
                                                                <Tab.Pane eventKey={item.label}>
                                                                    <div className="filter-date-range-wrapper">
                                                                        <span className="select-date">Select Date Range</span>
                                                                        <div className="filter-date-range-picker">
                                                                            <DateRangePicker className="sweet-date-picker" onChange={(data) => setAdditionalFilters({ ...additionalFilters, [item.name]: {question_type:item.question_type, options: data}})} value={(additionalFilters[item.name]) ? additionalFilters[item.name].options : null } clearIcon={null} calendarIcon={null} format="d/MM/y" />
                                                                        </div>
                                                                    </div>
                                                                </Tab.Pane>
                                                            );
                                                            case "numeric": return (
                                                                <Tab.Pane eventKey={item.label}>
                                                                    <div className="filter-range-wrap">
                                                                        <span className="response_range">Set {item.label} Range</span>
                                                                        <InputRange
                                                                            maxValue={100}
                                                                            minValue={0}
                                                                            formatLabel={value => `${value}`}
                                                                            value={(additionalFilters[item.name]) ? additionalFilters[item.name].options : {min: 0,max: 0}}
                                                                            onChange={value => setAdditionalFilters({ ...additionalFilters, [item.name]: {options: value, question_type: item.question_type}})}
                                                                        />
                                                                    </div>
                                                                </Tab.Pane>
                                                            );
                                                            default:
                                                                return (
                                                                    <Tab.Pane eventKey={item.label} key={i}>
                                                                    <div className="demography-filter-search-wrap">
                                                                        <div className="check_all_wrapper">
                                                                            <input className="check_all_checkbox" type="checkbox" onChange={(e) => hanleCheckFilterAll(e, item.name)} />
                                                                            <span className={`check_all_txt ${selectAll.includes(item.name) ? "checkbox-active" : ""}`}>All</span>
                                                                        </div>
                                                                    </div>
                                                                    <ul>                
                                                                        {
                                                                            (item.options && item.options.length > 0) ? item.options.map((el, index) =>
                                                                                <li key={index}>
                                                                                    <div className="demography-checkbox-wrap">
                                                                                        <label className="ezi-checkbox">
                                                                                            <input type="checkbox" value={el} onChange={(e) => handleCheckOption(e, item.name)} checked={isValueChecked(item.name, el)} />
                                                                                            <span className="ezi-checkbox-mark"></span>
                                                                                        </label>
                                                                                        <span>{el}</span>
                                                                                    </div>
                                                                                </li>
                                                                            ) : <span className="demographic_not_found">data not found</span>
                                                                        }
                                                                    </ul>
                                                                </Tab.Pane>
                                                                )
                                                        }
                                                    })()}
                                            </React.Fragment>
                                              
                                            )
                                        }
                                    </Tab.Content>
                                </Tab.Container>

                            :
                            <span className="report-dem-not-found">No Demographics available on Survey</span>
                        }

                    </div>
                </div>
            </div>
            <div className="applied-filter-wrap">    
                <div className="applied-filter-inner">
                    {
                        selectedFilter  &&
                        <div className="participant">
                            <div className="applied-filter-badge" > 
                                {
                                    selectedFilter && selectedFilter && selectedFilter.map((key,index)=>                                            
                                        <div  key={index}>{key.options.length>0}{key.options.map((item,val)=><span key={val} className="applied-badge">{item} <button type="button" key={val} onClick={(e) => removeDemographicOption(key.name,item,'participant')}> X</button></span>)}</div>
                                        )
                                }
                            </div>

                        </div>
                    }                
                    { 
                        selectedFilterParticipant &&
                        <div className="survey">
                                <div className="applied-filter-badge" > 
                                {
                                    selectedFilterParticipant && selectedFilterParticipant.map((key,index)=>
                                    <div key={index} > {key.options.length>0 }{key.options.map((item,val)=><span key={val} className="applied-badge">{item} <button type="button" key={val} onClick={() => removeDemographicOption(key.name,item,'survey')}> X</button></span>)}</div>
                                    )
                                }
                            </div>
                        </div>
                    }
                </div>              
                        {
                           ( visibleFlag===true) && <button onClick={handleShow} type="button" className="filter-show-more">Show More</button>
                        }    
                           
             </div>  

        </Fragment>
    )

}

export default React.memo(ReportCommentFilter);