import React from 'react';
import { CONTINUOUS_TYPE } from 'pages/Report/ReportConstant';
import { connect } from "react-redux";

const ReportHeaderCard = (props) => {

    return (
        <React.Fragment>

            <div className="insight-count-card-wrap">
                <div className="insight-count-card score_index" ref={el => props.screenShotRefVal.current[0] = el}>
                    <div className="insight-count-card-text">
                        <span className="title">{props.title}</span>
                        <span className="subtitle">{props.titleScore}</span>
                        {CONTINUOUS_TYPE.includes(props.version) &&
                            <React.Fragment>
                                {
                                    (props.dayActive !== 'manual') &&
                                    <div className="stock-wise-data">
                                        <span className={`${(props.diffScore >= 0) ? 'arrow-up' : (props.diffScore < 0) && 'arrow-down'}`}></span>
                                        <span className={`since_number ${(props.diffScore >= 0) ? 'gr' : 'rg'}`}>{props.diffScorePercent}</span>
                                        <span className="since_text">since last {props.sinceText}</span>
                                    </div>
                                }
                            </React.Fragment>
                        }
                    </div>
                    <span className="insight-count-icon"></span>
                </div>
                <div className="insight-count-card latest_score" ref={el => props.screenShotRefVal.current[1] = el}>
                    <div className="insight-count-card-text">
                        <span className="title"> {props.scoreTitle}</span>
                        {(props.version === 'exit')
                            ?
                            <span className="subtitle">{props.latestScore}<span className="subtitle-month">Months</span></span>
                            :
                            <span className="subtitle">{props.latestScore}</span>
                        }
                        <span className="score-date">on {props.scoreDate}</span>
                    </div>
                    <span className="insight-count-icon"></span>
                </div>
                <div className="insight-count-card score_index" ref={el => props.screenShotRefVal.current[2] = el}>
                    <div className="insight-count-card-text">
                        <span className="title">{props.avgTitle} </span>
                        <span className="subtitle">{props.average}</span>
                        {CONTINUOUS_TYPE.includes(props.version) &&
                            <React.Fragment>
                                {(props.dayActive !== 'manual') &&
                                    <div className="stock-wise-data">
                                        <span className={`${(props.diffAverage >= 0) ? 'arrow-up' : (props.diffAverage < 0) && 'arrow-down'}`}></span>
                                        <span className={`since_number ${(props.diffAverage >= 0) ? 'gr' : 'rg'}`}>{props.diffAveragePercent}</span>
                                        <span className="since_text">since last {props.sinceText}</span>
                                    </div>
                                }
                            </React.Fragment>
                        }
                    </div>
                    <span className="insight-count-icon"></span>
                </div>
                <div className="insight-count-card total_responses" ref={el => props.screenShotRefVal.current[3] = el}>
                    <div className="insight-count-card-text">
                        <span className="title">{props.responsesTitle}</span>
                        <span className="subtitle">{props.totalResponse}</span>
                        {CONTINUOUS_TYPE.includes(props.version) &&
                            <React.Fragment>
                                {(props.dayActive !== 'manual') &&
                                    <div className="stock-wise-data">
                                        <span className={`${(props.diffRespondents >= 0) ? 'arrow-up' : (props.diffRespondents < 0) && 'arrow-down'}`}></span>
                                        <span className={`since_number ${(props.diffRespondents >= 0) ? 'gr' : 'rg'}`}>{props.diffTotalRespondentsPercent}</span>
                                        <span className="since_text">since last {props.sinceText}</span>
                                    </div>
                                }
                            </React.Fragment>
                        }
                    </div>
                    <span className="insight-count-icon"></span>
                </div>

            </div>

        </React.Fragment>
    )
}
const mapStateToProps = (state) => {
    return {
        screenShotRefVal: state.report.screenShotRef,
    };
};
export default connect(mapStateToProps, null)(ReportHeaderCard);