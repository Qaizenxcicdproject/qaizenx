import React from 'react';

const RespondentResponse = ({ reasonList }) => {
    return (
        <div className="response-list">
            {
                Array.isArray(reasonList) && reasonList
                    ? reasonList.map((item, index) =>
                        <div className="ind-reasons-list-wrapper" key={index}>
                            <p className="list-item-que" index={index + 1}>{item.question && item.question}</p><span class="list-item-ans">{item.answer && item.answer}</span>
                        </div>
                    )
                    :
                    <span>No Response Found</span>
            }
        </div>
    )
}
export default RespondentResponse;