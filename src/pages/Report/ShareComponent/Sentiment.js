import React, { useState, useEffect, useRef, useContext } from 'react';
import Axios from "utility/Axios";
import { ProgressBar } from "react-bootstrap";
import configURL from 'config/config';
import { toast } from "react-toastify";
import AppContext from 'store/AppContext';
import Pagination from "react-js-pagination";
import SweetSearch from 'components/SweetSearch';
import ReportContext from "pages/Report/ReportContext";
import SentimentReplyModel from "pages/Report/ShareComponent/SentimentReplyModel";
import ShowMoreText from 'react-show-more-text';
import ActionModal from '../ActionModal';
import SelectSearch from "react-select-search";
import { Fragment } from 'react';
import { MobileView, BrowserView } from "react-device-detect";
import { AiOutlineClose } from 'react-icons/ai';
import MultiCheckbox from '../../../components/MultiCheckbox';

const SentimentComponent = (props) => {

	const { filterData, datefilter, timezone, surveyId, reportVersion, surveyQuestionData: { name: survey_name = null, insight: { id: questionID, question_type: questionType } } } = useContext(ReportContext);
	const { EziLoader } = useContext(AppContext)
	const [currentPage, setCurrentPage] = useState(1);
	const [commentFilter, setCommentFilter] = useState([]);
	const [commentData, setCommentData] = useState([]);
	const [commentPagination, setCommentPagination] = useState({});
	const inputSearch = useRef(null);
	const [searchLoading, setSearchLoading] = useState(false);
	const [searchValue, setSearchValue] = useState("");
	var searchTimer = null;
	const [showReply, setShowReply] = useState(false)
	const [showGroupReply, setShowGroupReply] = useState(false)
	const [commentCount, setCommentCount] = useState({})
	const [respondentData, setRespondentData] = useState({ respondent_id: null, fname: null, lname: null, comment: null, respondent_contact_number: null, respondent_email: null, ques_comment_id: null })
	const { selectedData: { identifier_id = null, identifier_type = null, identifier_name = null, theme_name = null, slider_value = null, theme_id = null, addon_filter = null } = {} } = props
	const [progressClass, setProgressClass] = useState(null);
	const [showMore, setShowMore] = useState(false);
	const [prevTaskData, setPrevTaskData] = useState({ is_completed: false, data: null })
	const [options, setOptions] = useState([]);
	const [selectedQuestion, setSelectedQuestion] = useState(null);
	const [resetSelectedQuestion, setResetSelectedQuestion] = useState(false);
	const [showContactNo, setShowContactNo] = useState(false)
	const [selectedGroupReply, setSelectedGroupReply] = useState([]);
	const [enableGroupReply, setEnableGroupReply] = useState(false);
	const [selectAllGroupReply, setSelectAllGroupReply] = useState(false);
	const [pageSelectionCount, setPageSelectionCount] = useState(0);
	const [validCommentData, setValidCommentData] = useState([]);
	const hideReplyModel = () => {
		setShowReply(false)
		setShowGroupReply(false)
		setRespondentData({ respondent_id: null, fname: null, lname: null, comment: null, ques_comment_id: null })
	}
	const sendHideReplyModel = () => {
		setShowReply(false);
		setShowGroupReply(false);
		setSelectedGroupReply([]);
		setSelectAllGroupReply(false);
		setEnableGroupReply(false);
		setRespondentData({ respondent_id: null, fname: null, lname: null, comment: null, ques_comment_id: null })
	}

	const showReplyModel = ({ comments = "", respondent_fname = "", respondent_lname = "", respondent_id = "", ques_comment_id = "" }) => {
		setShowReply(true)
		setRespondentData({ respondent_id: respondent_id, fname: respondent_fname, lname: respondent_lname, comment: comments, ques_comment_id: ques_comment_id })
	}
	const showGroupReplyModel = () => {

		if (selectedGroupReply && selectedGroupReply.length > 0) {
			setShowGroupReply(true)
		}
		else {
			toast.warn("Please select comments")
		}

	}
	const [show, setShow] = useState(false);

	const handleCloseAction = () => {
		setShow(false)
		setPrevTaskData({ is_completed: false, data: null })
		setRespondentData({ respondent_id: null, fname: null, lname: null, comment: null, respondent_contact_number: null, respondent_email: null, ques_comment_id: null })
	}
	const handleShowAction = ({ comments = "", respondent_fname = "", respondent_lname = "", respondent_id = "", respondent_contact_number = "", respondent_email = "", ques_comment_id = "" }) => {
		setShow(true)
		checkActionStatus(respondent_id);
		setRespondentData({ respondent_id: respondent_id, fname: respondent_fname, lname: respondent_lname, comment: comments, respondent_contact_number: respondent_contact_number, respondent_email: respondent_email, ques_comment_id: ques_comment_id })
	}

	const handlePaginate = (pageNumber) => {
		setCurrentPage(pageNumber);
	}

	/**
	 * Handle comment search
	 */
	const handleCommentSearch = () => {
		clearTimeout(searchTimer);
		searchTimer = setTimeout(() => {
			setSearchLoading(true)
			if (props.searchWordCloud !== "" && props.searchWordCloud !== undefined) {
				props.setWordCloudSearch();
			}
			let searchVal = inputSearch.current.value;
			setSearchValue(searchVal)
		}, 800);
	}

	const onProgressHandle = (e) => {
		if (props.menuTitle !== "analysis") {
			props.callbackSentiment(e.target.title)
		}
		setCommentFilter([{ "label": "sentiment", "options": [e.target.title] }])
		setProgressClass(e.target.title)
	}

	const handleTotalComment = (e) => {
		if (props.menuTitle !== "analysis") {
			props.callbackSentiment('Total')
		}
		setCommentFilter([])
		setProgressClass(null)
		// setSelectedQuestion(null)
	}

	/**
	 * Sentiment comment listing
	 */
	const sentimentListing = () => {
		EziLoader.show();
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("report_type", reportVersion);
		if (props.searchWordCloud !== "" && props.searchWordCloud !== undefined) {
			formData.append("search", props.searchWordCloud);
			//inputSearch.current.value = "";
		}
		else {
			formData.append("search", searchValue);
		}
		formData.append("comment_filters", JSON.stringify(commentFilter));
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		formData.append("current_page", currentPage);
		selectedQuestion && formData.append("comment_question_id", selectedQuestion);
		if (props.menuTitle === "analysis") {
			formData.append("demographic_id", identifier_id);
			formData.append("demographic_type", identifier_type);
			formData.append("sliderValue", slider_value);
			formData.append("theme_id", theme_id);
			formData.append("addon_filter", addon_filter ? JSON.stringify(addon_filter) : JSON.stringify([]));
			formData.append("roundDecimalPoint", "2");
			formData.append("calculation_type", props.avgScore);
		}
		Axios.post((props.menuTitle === "analysis") ? configURL.ReportNegativeComments : configURL.ReportPostComments, formData).then(response => {
			EziLoader.hide();
			setSearchLoading(false);
			if (response.data.success !== undefined && response.data.success) {
				let comments = response.data.result.data;
				let validComments = [];
				comments.forEach(item => {
					if (item.respondent_email && validateEmail(item.respondent_email)) {
						validComments.push(item);
					}
				});
				let count = selectedGroupReply.reduce((val, itm) => itm.page === currentPage ? val + 1 : val + 0, 0)
				if (count === validComments.length) {
					setSelectAllGroupReply(true)
				}
				else {
					setSelectAllGroupReply(false)
				}
				setCommentData(response.data.result.data);
				setValidCommentData(validComments);
				setCommentPagination(response.data.pagination);
			}
			else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			setSearchLoading(false);
			console.log(err);
		})
	}

	/**
	*  Comment Count Listing
	* */
	const commentCountListing = () => {
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("question_type", questionType);
		formData.append("question_id", questionID);
		selectedQuestion && formData.append("comment_question_id", selectedQuestion);
		formData.append("filters", JSON.stringify(filterData));
		formData.append("date", JSON.stringify(datefilter));
		formData.append("timezone", timezone);
		if (props.menuTitle === "analysis") {
			formData.append("demographic_id", identifier_id);
			formData.append("demographic_type", identifier_type);
			formData.append("sliderValue", slider_value);
			formData.append("theme_id", theme_id);
			formData.append("addon_filter", addon_filter ? JSON.stringify(addon_filter) : JSON.stringify([]));
			formData.append("roundDecimalPoint", "2");
			formData.append("calculation_type", props.avgScore);
		}
		Axios.post(configURL.ReportCommentsCount, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setCommentCount(response.data.result)
			}
			else {
				toast.warn(response.data.message || "Something went wrong")
			}
		}).catch(err => {
			EziLoader.hide()
			setSearchLoading(false);
			console.log(err);
		})
	}
	const checkActionStatus = (respondentId) => {
		if (respondentId !== "") {
			EziLoader.show();
			Axios.get(configURL.task_status + respondentId).then(res => {
				setPrevTaskData({ is_completed: res.data.result.is_completed || false, data: res.data.result.data || null })
				EziLoader.hide();
			}).catch(err => {
				console.log(err)
				EziLoader.hide();
				toast.warn("Something went wrong.")
			})
		}
	}

	const onQuestionChange = value => {
		console.log(value);
		setSelectedQuestion(value);
		setResetSelectedQuestion(true);
		props.onQuestionChange(value)
	}
	const onQuestionReset = () => {

		if (props.menuTitle !== "analysis") {
			// props.callbackSentiment('Total')
			props.onQuestionChange("")
		}
		setCommentFilter([])
		setProgressClass(null)
		setSelectedQuestion(null);
		setResetSelectedQuestion(false);

	}
	const questionListing = () => {
		let formData = new FormData();
		formData.append("survey_id", surveyId);
		formData.append("only_comment_questions", true);
		Axios.post(configURL.ReportSurveyQuestion, formData).then(response => {
			EziLoader.hide();
			if (response.data.success !== undefined && response.data.success) {
				setOptions(response.data.result.data.map((question) => {
					return {
						name: question.title, value: question.id
					}
				}));
			}

		}).catch(err => {
			EziLoader.hide()
			console.log(err);
		})
	}
	const handleMultiCheckbox = (responseData) => {

		let groupChecked = selectedGroupReply;
		let removedIndex = [...groupChecked];
		let presentIndex = groupChecked.findIndex(item => item.ques_comment_id === parseInt(responseData.ques_comment_id));
		if (presentIndex !== -1 && groupChecked.length > 0) {
			removedIndex.splice(presentIndex, 1);
			groupChecked = removedIndex;
			setSelectedGroupReply(removedIndex);
			setSelectAllGroupReply(true)
		}
		else {
			if (selectedGroupReply && selectedGroupReply.length >= 20) {
				toast.warn("Only 20 reply allowed at a time.")
				return true;
			}
			else {
				groupChecked = [...selectedGroupReply, { ques_comment_id: parseInt(responseData.ques_comment_id), respondent_id: responseData.respondent_id, comments: responseData.comments, page: currentPage }];
				setSelectedGroupReply(groupChecked)
				setPageSelectionCount(pageSelectionCount + 1);
			}

		}
		let count = groupChecked.reduce((val, itm) => itm.page === currentPage ? val + 1 : val + 0, 0)
		if (count === validCommentData.length) {
			setSelectAllGroupReply(true)
		}
		else {
			setSelectAllGroupReply(false)
		}

	}
	const handleSelectAllReply = (data) => {
		let groupChecked = selectedGroupReply;
		let exceededSelect = false;
		data.forEach(responseData => {
			if(groupChecked.length <= 19){
			let presentIndex = groupChecked.findIndex(item => item.ques_comment_id === parseInt(responseData.ques_comment_id));
			if ((presentIndex !== -1 && groupChecked.length > 0) && selectAllGroupReply === true) {
				let removedIndex = [...groupChecked];
				removedIndex.splice(presentIndex, 1);
				groupChecked = removedIndex;
			} else {
				if ((presentIndex !== -1 && groupChecked.length > 0) && selectAllGroupReply === false) {

				}
				else {
					if (selectedGroupReply && selectedGroupReply.length >= 20) {
						exceededSelect = true;
						return;
					}
					else {
						//setPageSelectionCount(10);
						groupChecked = [...groupChecked, { ques_comment_id: parseInt(responseData.ques_comment_id), respondent_id: responseData.respondent_id, comments: responseData.comments, page: currentPage }];
					}


				}
			}
}
		});
		if (exceededSelect) {
			toast.warn("Only 20 reply allowed at a time.")

		}
		setSelectedGroupReply(groupChecked);
		let count = groupChecked.reduce((val, itm) => itm.page === currentPage ? val + 1 : val + 0, 0)
		if (count === validCommentData.length) {
			setSelectAllGroupReply(true)
		}
		else {
			setSelectAllGroupReply(false)
		}
	}
	const handleGroupReply = () => {
		setEnableGroupReply(!enableGroupReply);
		setSelectedGroupReply([]);
		setSelectAllGroupReply(false);
	}
	const validateEmail = (email) => {
		return email.match(
			// eslint-disable-next-line no-control-regex
			/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
		);
	}

	useEffect(() => {
		EziLoader.show();
		if (props.searchWordCloud !== "" && props.searchWordCloud !== undefined) {
			inputSearch.current.value = props.searchWordCloud;
			sentimentListing();
		}
		else if ((props.menuTitle === "analysis" && identifier_name && theme_name && slider_value && addon_filter)) {
			sentimentListing();
			questionListing();
		}
		else {
			sentimentListing();
			questionListing();
		}

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [filterData, datefilter, currentPage, commentFilter, searchValue, props.avgScore, identifier_name, theme_name, slider_value, addon_filter, selectedQuestion, resetSelectedQuestion, respondentData, props.searchWordCloud]);

	useEffect(() => {
		EziLoader.show();
		if ((props.menuTitle === "analysis" && identifier_name && theme_name && slider_value && addon_filter)) {
			commentCountListing();
		}
		else {
			commentCountListing();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [filterData, datefilter, props.avgScore, identifier_name, theme_name, slider_value, addon_filter, selectedQuestion, resetSelectedQuestion]);
	// useEffect(() => {
	// 	if (props.searchWordCloud !== "") {
	// 		inputSearch.current.value = props.searchWordCloud;
	// 		sentimentListing();
	// 	}
	// 	//	sentimentListing();
	// 	// eslint-disable-next-line react-hooks/exhaustive-deps
	// }, [props.searchWordCloud]);

	return (
		<div className="ce-comment-section">
			<div className="ce-comment-section-header">
				<label className="commnet-label" onClick={() => handleTotalComment()}>
					<span className="commnet-label-text">Total Comments</span>
					<span className="commnet-label-count">{commentCount.total}</span>
				</label>
				<ProgressBar>
					<ProgressBar title="Positive" className={`positive ${("Positive" === progressClass) && "activeProgress"}`} key={1} label={(commentCount.positive === 0) ? 0 : `${commentCount.positive}%`} min={commentCount.positive} max={(commentCount.positive === 0) ? 0 : commentCount.positive} now={commentCount.positive} onClick={(e) => onProgressHandle(e)} style={{ width: `${commentCount.positive}%` }} />
					<ProgressBar title="Neutral" className={`neutral ${("Neutral" === progressClass) && "activeProgress"}`} key={2} label={(commentCount.neutral === 0) ? 0 : `${commentCount.neutral}%`} min={commentCount.neutral} max={(commentCount.neutral === 0) ? 0 : commentCount.neutral} now={commentCount.neutral} onClick={(e) => onProgressHandle(e)} style={{ width: `${commentCount.neutral}%` }} />
					<ProgressBar title="Negative" className={`negative ${("Negative" === progressClass) && "activeProgress"}`} key={3} label={(commentCount.negative === 0) ? 0 : `${commentCount.negative}%`} min={commentCount.negative} max={(commentCount.negative === 0) ? 0 : commentCount.negative} now={commentCount.negative} onClick={(e) => onProgressHandle(e)} style={{ width: `${commentCount.negative}%` }} />
				</ProgressBar>
				<div className="comment-right-header">
					<SweetSearch loading={searchLoading} change={handleCommentSearch} ref={inputSearch} />
					{/* <div className="comment-filter-btn-wrap">
                            <button type="button" className="ezi-filter-btn" onClick={() => setShowFilter(!showFilter)}>Filter</button>
                            <CommentFilterComponent
                                data={props.data.surveyQuestionData.insight}
                                show={showFilter}
                                hide={() => setShowFilter(false)}
                                position="right"
                                applyFilter={(filters) => setCommentFilter(filters)}                               
                            />
                        </div>                     */}
				</div>
			</div>
			<div className="header-filter-sentiments">
				<div className="select-question">
					{
						commentData.length > 0 &&
						<Fragment>
							<SelectSearch
								options={options}
								search
								value={selectedQuestion || null}
								placeholder="Please select question"
								emptyMessage="Not Found"
								onChange={onQuestionChange}
							/>
							{resetSelectedQuestion && <div className="reset-question"><button onClick={onQuestionReset} className="text-danger"><AiOutlineClose /></button></div>}
							<div class="Group-reply-buttons">
								{!enableGroupReply ?
									<button onClick={handleGroupReply} class="comment-reply select-all comment-date">Group Reply</button> : <Fragment><button onClick={() => handleSelectAllReply(validCommentData)} class="comment-reply select-all comment-date">{selectAllGroupReply ? 'Remove all' : 'Select all'}</button> <button class="comment-reply ezi-pink-btn comment-date" onClick={showGroupReplyModel}>Reply<span className='group_reply_count'>{selectedGroupReply.length || 0}</span></button><AiOutlineClose className="close-reply" onClick={handleGroupReply} /></Fragment>
								}

							</div>
						</Fragment>
					}


				</div>


			</div>

			<div className="ce-comment-body">
				{
					commentData.length > 0 ? commentData.map((item, index) =>
						<div key={`clist${item.ques_comment_id}`} className={`comment-box-emoji-wrap ${item.sentiment_score !== null && item.sentiment_score.toLowerCase()}`}>
							<div className="ce-comment-box">
								<div className="comment-box-text-wrap">
									<span className="comment-heading">
										<ShowMoreText
											lines={3}
											more='Show more'
											less='Show less'
											className='content-css'
											anchorClass='show-more-anchor'
											onClick={() => setShowMore(!showMore)}
											expanded={showMore}
										>
											{item.comments}
										</ShowMoreText>
									</span>
									{/* <div className="comment-badge-wrap">
                                        {item.key_phrases && item.key_phrases.map((key) =>
                                            <span className="comment-badge">
                                                {key.Text}
                                            </span>
                                        )}
                                    </div> */}
								</div>
								<div className="comment-box-right-text">
									<div className="comment-box-right-text-section">
										<span className="comment-date-title">{(item.respondent_fname)}</span>
										<span className="comment-date">{item.response_date} </span>
									</div>
									<div className="comment-box-right-text-section">

										{item.respondent_id &&
											<div className='action-sentiment'>
												<div className={`${(enableGroupReply) && 'action-sentiment disabled'}`}>
													<BrowserView>{item.respondent_contact_number &&
														<div>

															<span className="comment-date whatsapp_icon"><a href={`https://wa.me/${item.respondent_contact_number}`} target="_blank" rel="noopener noreferrer"><img alt="" src={require(`../../../assets/images/report/whatsapp.png`)} /></a></span>

															{showContactNo === index ? <span className="comment-date telephoneIcon" onClick={() => setShowContactNo("")}>{item.respondent_contact_number}</span> :
																<span className="comment-date telephoneIcon" onClick={() => setShowContactNo(index)}><img alt="" src={require(`../../../assets/images/report/telephones.png`)} /></span>
															}
														</div>
													} </BrowserView>
													<MobileView>
														{item.respondent_contact_number && <div>
															<span className="comment-date whatsapp_icon"><a href={`https://wa.me/${item.respondent_contact_number}`} target="_blank" rel="noopener noreferrer"><img alt="" src={require(`../../../assets/images/report/whatsapp.png`)} /></a>
															</span> <a href={`tel:${(item.respondent_contact_number)}`}>	<span className="comment-date telephoneIcon"><img alt="" src={require(`../../../assets/images/report/telephones.png`)} /></span></a></div>
														}
													</MobileView>
												</div>
												{(enableGroupReply && (item.respondent_email && validateEmail(item.respondent_email))) &&
													<label class="ezi-checkbox checkbox-label">
														<MultiCheckbox
															type="checkbox"
															name={`group_checkbox_${item.ques_comment_id}`}
															value={item.ques_comment_id}
															onChange={() => handleMultiCheckbox(item)}
															class="group_reply_checkbox"
															checked={selectedGroupReply}
														/>
														<span class="ezi-checkbox-mark"></span>
													</label>
												}
												<div className={`${(enableGroupReply) && 'action-sentiment disabled'}`}>
													{/* <span className="comment-reply comment-date" onClick={() => showReplyModel(item)}>Reply {item.is_replied === true && <img className="checked-complete" alt="" src={require(`../../../assets/images/report/check.png`)} />}</span> */}
													{(item.respondent_email && validateEmail(item.respondent_email)) ? <span className="comment-reply comment-date" onClick={() => showReplyModel(item)}>Reply {item.is_replied === true && <img className="checked-complete" alt="" src={require(`../../../assets/images/report/check.png`)} />}</span> : ""}

													<span className="comment-reply comment-date" onClick={() => handleShowAction(item)}>Action {item.is_task_created === true && <img className="checked-complete" alt="" src={require(`../../../assets/images/report/check.png`)} />}</span>
												</div>
											</div>
										}
									</div>
								</div>
							</div>
						</div>
					) : <span className="comment-not-found">Comments Not Found</span>
				}
				{((commentData.length > 0) && show) &&
					<ActionModal
						show={show}
						onHide={handleCloseAction}
						respondent_data={respondentData}
						prevTaskData={prevTaskData}
						respondantReport={true}
						actionType='COMMENT'
					/>
				}
				{(validCommentData.length > 0) && <SentimentReplyModel show={showGroupReply} onHide={hideReplyModel} onSendHide={sendHideReplyModel} respondent_data={selectedGroupReply} survey_name={survey_name} actionType="GROUPCOMMENT" />}
				{(commentData.length > 0) && <SentimentReplyModel show={showReply} onHide={hideReplyModel} onSendHide={sendHideReplyModel} respondent_data={respondentData} survey_name={survey_name} actionType="COMMENT" />}
			</div>
			{(commentPagination && commentPagination.current_page && commentPagination.total !== 0) &&
				<div className="pagination-plugin-wrap">
					<Pagination
						activePage={commentPagination.current_page}
						itemsCountPerPage={10}
						totalItemsCount={commentPagination.total}
						onChange={handlePaginate}
						hideDisabled={true}
						firstPageText={<span className="prev-page-text-ic"></span>}
						lastPageText={<span className="next-page-text-ic"></span>}
						nextPageText={<span className="next-text-ic"></span>}
						prevPageText={<span className="prev-text-ic"></span>}
					/>
				</div>
			}

		</div>
	);
}

export default SentimentComponent;