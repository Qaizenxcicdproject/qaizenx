import React, { useState } from "react";
import { Modal, Spinner, Tab, Nav } from 'react-bootstrap';
import useForm from 'react-hook-form';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';
import { useParams } from "react-router-dom";
import Pagination from "react-js-pagination";


const SentimentReplyModel = ({ show, onHide, onSendHide, respondent_data, survey_name, actionType }) => {
    const { register, handleSubmit, errors } = useForm();
    const [saving, setSaving] = useState(false)
    const urlParams = useParams();
    const selectionMode = 'email';
    const [tabKey, setTabKey] = useState('reply_form');
    const [replyLogs, setReplyLogs] = useState([]);
    const [replyPagination, setReplyPagination] = useState({});

    const onSubmit = data => {
        setSaving(true)
        let formData = new FormData()
        formData.append("survey_id", urlParams.survey_id)

        formData.append("reply_msg", data.reply_msg)
        formData.append("reply_subject", data.reply_subject)
        formData.append("communication_method", selectionMode)

        if (actionType === "GROUPCOMMENT") {
            formData.append("for_which", 'COMMENT')
            formData.append("respondent_data", JSON.stringify(respondent_data))
        }
        else {
            formData.append("for_which", actionType)
            let responseData = [{ ques_comment_id: parseInt(respondent_data.ques_comment_id), respondent_id: respondent_data.respondent_id, comments: respondent_data.comment }];
            formData.append("respondent_data", JSON.stringify(responseData))
        }

        Axios.post(configURL.reply_survey_comment, formData).then(res => {
            setSaving(false)
            if (res.data.success === true) {
                toast.success(res.data.message || `Reply sent to ${respondent_data.fname || 'User'} ${respondent_data.lname || ''}`)
                onSendHide()
            } else {
                toast.warn(res.data.message || "Something went wrong.")
            }
        }).catch(err => {
            setSaving(false)
            console.log(err)
            toast.warn("Something went wrong.")
        })
    };

    const communicationHistory = (currentPage = null) => {

        let formData = new FormData()
        formData.append("survey_id", urlParams.survey_id)
        formData.append("respondent_id", respondent_data.respondent_id)
        formData.append("communication_method", selectionMode)
        formData.append("page", currentPage);
        Axios.post(configURL.post_communication_log_histories, formData).then(res => {

            if (res.data.success === true) {
                setReplyLogs(res.data.results.data)
                setReplyPagination(res.data.results.pagination_data);
            } else {
                toast.warn(res.data.message || "Something went wrong.")
            }
        }).catch(err => {

            console.log(err)
            toast.warn("Something went wrong.")
        })
    };

    return (
        <Modal show={show} onHide={onHide} size="md" scrollable={true} aria-labelledby="contained-modal-title-vcenter" centered className="theme-modal-wrapper  ezi-right-animated-modal actionReplyModalTop actionModalTop">
            <Tab.Container activeKey={tabKey} onSelect={k => setTabKey(k)}>
                <Modal.Header className="ezi-modal-header">
                    <Modal.Title id="contained-modal-title-vcenter" className="theme-modal-title ezi-modal-header-title" >
                        <div className="tab-left-header">
                            <Nav variant="pills" >
                                <Nav.Item>
                                    <Nav.Link eventKey="reply_form">{actionType === "GROUPCOMMENT" ? 'Group Reply' : 'Reply'}</Nav.Link>
                                </Nav.Item>
                                {(respondent_data?.respondent_id && actionType !== "GROUPCOMMENT") && <Nav.Item>
                                    <Nav.Link eventKey="reply_history" onClick={communicationHistory}>History</Nav.Link>
                                </Nav.Item>}
                            </Nav>
                        </div>
                        <span className="ezi-modal-close" onClick={onHide}></span>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Tab.Content className="reply-inner-tab-content">
                        <Tab.Pane eventKey="reply_form" mountOnEnter>
                            <form onSubmit={handleSubmit(onSubmit)} className="add-theme-form">
                                <span className="theme-modal-title-text my-3 pl-2">{actionType !== "GROUPCOMMENT" && `Reply to ${respondent_data.fname || "User"} ${respondent_data.lname || ''}`}</span>
                                <div className="theme-field-wrapper">
                                    <div className="theme-field">
                                        <div className="theme-field">
                                            <input type="text" className="theme-field-control filled" name="reply_subject" ref={register({ required: true })} defaultValue={`RE: ${survey_name}`} placeholder="Please enter reply subject" />
                                            {errors.reply_subject && <span className="theme-error_cu">* Reply subject is required.</span>}
                                        </div>
                                    </div>
                                    <div className="theme-field">
                                        <div className="theme-field">
                                            <textarea className="theme-field-control" name="reply_msg" ref={register({ required: true })} rows={6} placeholder="Please enter reply message"></textarea>
                                            {errors.reply_msg && <span className="theme-error_cu">* Reply message is required.</span>}
                                        </div>
                                    </div>
                                </div>
                                {/* <div className="theme-field-wrapper">
                                    <div className="mode-section">
                                        <h5>Mode: </h5>
                                        <label className="checkbox-label">
                                            <input type="radio" name="selection_mode" value='email' className="qaizenx-radio-input" checked={(selectionMode === 'email')} onChange={({ target }) => setSelectionMode(target.value)} />
                                            <span>Email</span>
                                        </label>
                                        <label className="checkbox-label">
                                            <input type="radio" name="selection_mode" value='sms' className="qaizenx-radio-input" checked={(selectionMode === 'sms')} onChange={({ target }) => setSelectionMode(target.value)} />
                                            <span>SMS</span>
                                        </label>
                                    </div>
                                </div> */}
                                <div className="theme-modal-footer replySend">
                                    {/* <button type="button" className="close-theme-btn" onClick={onHide}>Close</button> */}
                                    <button type="submit" className="btn-ripple ezi-pink-btn add-theme-btn" disabled={saving}>
                                        Send {saving && <Spinner animation="border" size="sm" />}
                                    </button>
                                </div>
                            </form>
                        </Tab.Pane>
                        <Tab.Pane eventKey="reply_history">
                            {(replyLogs && replyLogs.length > 0) ? replyLogs.map((item, index) =>
                                <div className="replyHistory">
                                    <div className="reply-history-box">
                                        <div>
                                            <p className="reply-subject"><span className="sub_heading">Subject:&nbsp; </span>{item.templates.reply_subject} </p>
                                            <p className="reply-date">{item.created_at_hf} </p>
                                        </div>
                                        <div className="reply-div">
                                            <p className="reply-message"><span className="reply_heading">Reply Message:&nbsp;</span> {item.templates.reply_msg} </p>
                                        </div>
                                    </div>

                                </div>
                            )
                                :
                                <div className="reply-not-found">
                                    <p className="not-found">History Not Found</p>
                                </div>
                            }



                            {(replyPagination && replyPagination.current_page && replyPagination.total !== 0) &&
                                <div className="pagination-plugin-wrap">
                                    <Pagination
                                        activePage={replyPagination.current_page}
                                        itemsCountPerPage={10}
                                        totalItemsCount={replyPagination.total}
                                        onChange={communicationHistory}
                                        hideDisabled={true}
                                        firstPageText={<span className="prev-page-text-ic"></span>}
                                        lastPageText={<span className="next-page-text-ic"></span>}
                                        nextPageText={<span className="next-text-ic"></span>}
                                        prevPageText={<span className="prev-text-ic"></span>}
                                    />
                                </div>
                            }
                        </Tab.Pane>
                    </Tab.Content>
                </Modal.Body>
            </Tab.Container>
        </Modal>
    );
}

export default SentimentReplyModel