import React from 'react';

const TableTag = ({ array }) => {
  return (
    <React.Fragment>
      <tr>
        {array.map((element,index) => (
          (index === 0 ?
           <th key={`th${index}`}>{element}</th>
            :
            <td key={`td${index}`}> {(element===0 || element === null ? "-" : element)}</td>)
        ))}
      </tr>
   </React.Fragment>
  );
};

export default TableTag;
