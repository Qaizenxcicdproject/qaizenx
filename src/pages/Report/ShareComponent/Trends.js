import React, { useState, useEffect, useContext } from 'react';
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";
import Axios from "utility/Axios";
import { Line as LineChart } from 'react-chartjs-2';
import configURL from 'config/config';
import AppContext from 'store/AppContext';

const Trends = (props) => {
    const { EziLoader } = useContext(AppContext)
    const urlParams = useParams();
    const [surveyId] = useState(urlParams.survey_id)
    const [queTrendFilter, setQueTrendFilter] = useState('daily');
    const [trendQuestion] = useState(props.trendQuestion);
    const [trendData, setTrenData] = useState();
    const appStore = JSON.parse(localStorage.getItem("appState"));
    const timezone = appStore.user.timezone;

    const singleLineChartDataTrend = {
        labels: trendData && trendData.labels,
        datasets: [
            {
                label: "Trend",
                data: trendData && trendData.average,
                borderColor: '#F8C63D',
                backgroundColor: 'transparent',
                borderWidth: 2,
                pointBackgroundColor: '#FFF'
            }

        ]
    };

    const singleLineChartOptionsTrend = {
        responsive: true,
        maintainAspectRatio: false,
        // elements: { point: { radius: 0 } },
        layout: { padding: { top: 0, bottom: 0, left: 0, right: 0 } },
        legend: {
            display: false,
            align: 'start',
            position: 'bottom',
            labels: { fontColor: '#5A4E63', boxWidth: 6, fontSize: 12, fontFamily: "NunitoSansSemiBold", padding: 24, usePointStyle: true, }
        },
        scales: {
            xAxes: [{
                ticks: { fontColor: "#C3CBD4", fontFamily: "NunitoSans", fontSize: 12, beginAtZero: true, padding: 15, },
                gridLines: { color: "transparent", zeroLineColor: 'transparent', },
            }],

            yAxes: [{
                ticks: { fontColor: "#C3CBD4", fontFamily: "NunitoSans", fontSize: 12, beginAtZero: true, padding: 15 },
                gridLines: { color: '#EDEDED', zeroLineColor: '#EDEDED', borderColor: "transparent", drawBorder: false, }
            }]
        }
    }
    const questionTrendChart = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", surveyId);
        formData.append("question_type", trendQuestion.question_type);
        formData.append("question_id", trendQuestion.question_id);
        formData.append("filter", queTrendFilter);
        formData.append("filters", JSON.stringify(props.filterData));
        formData.append("date", JSON.stringify(props.dateFilter));
        formData.append("timezone", timezone);
        Axios.post(configURL.ReportTrendDistribution, formData).then(response => {
            EziLoader.hide();
            // console.log(response.data);
            if (response.data.success !== undefined && response.data.success) {
                setTrenData(response.data.result);
            } else {
                toast.warn(response.data.message);
            }
        })

    }

    useEffect(() => {
        // questionTrendChart();
        if (trendQuestion) {
            questionTrendChart();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [trendQuestion, props.filterData, props.dateFilter, queTrendFilter]);


    return (
        <React.Fragment>
            <div className="question-cm-box-tab-body question-cm-single-line">
                <select className="distribution-select" defaultValue={queTrendFilter} onChange={(e) => setQueTrendFilter(e.target.value)} >
                    <option value="hourly">Hourly</option>
                    <option value="daily">Daily</option>
                    <option value="weekly">Weekly</option>
                    <option value="monthly">Monthly</option>
                </select>
                <LineChart data={singleLineChartDataTrend} options={singleLineChartOptionsTrend} height={150} />

            </div>
        </React.Fragment>
    );
}

export default Trends;