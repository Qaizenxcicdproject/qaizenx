import React from 'react';
import HIVersion3 from 'pages/Report/ReportType/Continuous/HI/HI-V3';
import HIVersion4 from 'pages/Report/ReportType/Continuous/HI/HI-V4';
import NPSVersion3 from 'pages/Report/ReportType/Continuous/NPS/NPS-V3';
import CSATVersion3 from 'pages/Report/ReportType/Continuous/CSAT/CSAT-V3';
import PMF from 'pages/Report/ReportType/Continuous/PMF/PMF';
import CSATVersion5 from 'pages/Report/ReportType/Continuous/CSAT/CSAT-V5';
import Pulse from 'pages/Report/ReportType/Continuous/Pulse/Pulse';
import WellnessVersion2 from 'pages/Report/ReportType/Continuous/WellnessV2/Wellness-V2';
import Wellness from 'pages/Report/ReportType/Standard/Wellness/Wellness';
import DefaultType from 'pages/Report/ReportType/Standard/Default/Default';
import Exit from 'pages/Report/ReportType/Continuous/Exit/Exit';
import PersonalityFactor from 'pages/Report/ReportType/Standard/PersonalityFactor/PersonalityFactor';
import Diversity from 'pages/Report/ReportType/Standard/Diversity/Diversity';
import ESATVersion3 from 'pages/Report/ReportType/Continuous/ESAT/ESAT-V3';
import INN from 'pages/Report/ReportType/Standard/InnovativeIndex/INN';
import DMAT from 'pages/Report/ReportType/Standard/CommonExp/CommonExp';
import HIEXP from 'pages/Report/ReportType/Standard/CommonExp/CommonExp';
import WELEXP from 'pages/Report/ReportType/Standard/CommonExp/CommonExp';
import PULEXP from 'pages/Report/ReportType/Standard/CommonExp/CommonExp';
// import CPulse from 'pages/Report/ReportType/Continuous/CPulse/CPulse';
import PageNotFound from 'components/PageNotFound';
import EPA from 'pages/Report/ReportType/Standard/ElectionPollsAnalysis/EPA';

const VersionComponent = (props) => {

	const components = {
		hiv3: HIVersion3,
		hiv4: HIVersion4,
		npsv3: NPSVersion3,
		csatv3: CSATVersion3,
		pmf: PMF,
		csatv5: CSATVersion5,
		default: DefaultType,
		wellness: Wellness,
		wellnessv2: WellnessVersion2,
		pulse: Pulse,
		exit: Exit,
		pf16: PersonalityFactor,
		diversity: Diversity,
		esatv3: ESATVersion3,
		inn: INN,
		dmat: DMAT,
		hiexp: HIEXP,
		welexp: WELEXP,
		pulexp: PULEXP,
		// cpulse: CPulse,
		epa: EPA,
		error: PageNotFound,
	};

	const VersionName = components[props.tag || 'error'];

	return <VersionName screenShotref={props.screenShotref} />
}


export default VersionComponent;
