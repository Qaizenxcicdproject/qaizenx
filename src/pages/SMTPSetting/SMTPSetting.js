import React, { useState, useEffect, useContext } from "react";
import './SMTPSetting.scss';
import { Breadcrumb } from 'react-bootstrap';
import useForm from 'react-hook-form';
import configURL from 'config/config';
import Axios from "utility/Axios";
import { toast } from 'react-toastify';
import AppContext from 'store/AppContext';
import FormData from 'utility/AppFormData';

const SMTPSetting = () => {

    const { EziLoader, appState, accesFeature, languageObj, handleUnAuthWarn } = useContext(AppContext)
    const { register, handleSubmit, reset } = useForm();
    const [companyData, setCompanyData] = useState({});
    const [companyDetails, setCompanyDetails] = useState({});
    const [isSMTP, setIsSMTP] = useState({});
    // const [timeZone, setTimeZone] = useState({ label: "Asia/Kolkata", value: "Asia/Kolkata" });
    const company_id = appState.current_app_detail.id || null

    /**
     * Save Company
     * @param {Object} data 
     */


    const submitFormData = (data) => {
        if (!accesFeature.update_company) {
            handleUnAuthWarn()
            return;
        }
        if (data.smtpauth === "") {
            toast.warn("Please select SMTP Auth");
            return;
        }
        else if (data.smtpauth === "true" && (data.username === "" || data.password === "")) {
            toast.warn("Please enter username and password");
            return;
        }
        EziLoader.show()
        let newData = companyDetails;
        // const smtpField = data
        newData.smtp = data
        const sendData = new FormData();
        sendData.append("insertdata", JSON.stringify({ 'data': newData }));
        sendData.append("account_id", company_id)
        Axios.post(configURL.save_compnay_details, sendData).then(response => {
            EziLoader.hide()
            if (response.data.success === true) {
                toast.success("SMTP Details Saved Successfully");
                // const result = Object.keys(smtpField).map((key) => smtpField[key])
                // result.splice(7,1);
                // if(result.includes('')){
                //     setIsSMTP(true)
                // }else{
                //     setIsSMTP(false)
                // }
                setIsSMTP(false)
            } else {
                toast.warn(response.data.message || "Something went wrong");
            }
        }).catch(err => {
            console.log(err.toString())
            EziLoader.hide()
        })

    }

    const sendTestMail = (data) => {
        EziLoader.show()
        let formData = new FormData()
        formData.append("email", data.smtp_email)
        Axios.post(configURL.test_mail_credential, formData).then(response => {
            EziLoader.hide()
            if (response.data.success === true) {
                toast.success(response.data.message);
                reset({
                    smtp_email: ""
                })
                getCompanyDetails()
            } else {
                toast.warn(response.data.message || "Something went wrong");
            }
        })
    }

    const getCompanyDetails = () => {
        if (company_id) {
            EziLoader.show()
            let formData = new FormData()
            formData.append("account_id", company_id)
            Axios.post(configURL.get_compnay_details, formData).then(response => {
                EziLoader.hide()
                if (response.data.success) {
                    setCompanyData(response.data.result.smtp || '')
                    setCompanyDetails(response.data.result || '')
                    // const smtpField = response.data.result.smtp
                    // const result = Object.keys(smtpField).map((key) => smtpField[key])
                    // result.splice(7,1);
                    // if (result.includes('')) {
                    //     setIsSMTP(true)
                    // } else {
                    //     setIsSMTP(false)
                    // }
                    setIsSMTP(false)
                }
            }).catch(err => {
                EziLoader.hide()
            })
        }
    }

    useEffect(getCompanyDetails, []);

    /**
     * Handle Form Reset
     */
    const clearForm = () => {
        reset({
            from_name: "",
            from_email_address: "",
            host: "",
            port: "",
            username: "",
            password: "",
            encryption: "",
            smtpauth: ""
        })
    }
    return (
        <React.Fragment>
            <section className="Page-SMTPsetting" >
                <form onSubmit={handleSubmit(submitFormData)} className="company-form">
                    <div className="breadcrumb_ezi">
                        <Breadcrumb>
                            <Breadcrumb.Item>{languageObj.translate('Settings.1')}</Breadcrumb.Item>
                            <Breadcrumb.Item>{languageObj.translate('SmtpSetting.1')}</Breadcrumb.Item>
                        </Breadcrumb>
                        <div className="column-header">
                            <h1 className="page-heading">{languageObj.translate('SmtpDetails.1')}</h1>
                            <div className="column-header-btn">
                                <button type="button" className="btn-ripple clear_all" disabled={!accesFeature.update_company} title={!accesFeature.update_company ? 'Unauthorized Operation' : ''} onClick={clearForm} >{languageObj.translate("ClearAll.1")}</button>
                                <button type="submit" className="btn-ripple add-new">Save Settings</button>
                            </div>
                        </div>
                    </div>
                    <div className="add-company-form-wrap">
                        <div className="company-info-header">
                            <div className="conpany-name-id-wrap">
                                <span className="title">SMTP Details</span>
                                <span className="subtitle">Please fill below details to send system mails from entered email id otherwise email will send from "admin@qaizenx.com"</span>
                            </div>
                        </div>
                        <div className="tablist_ezi">
                            <div className="add-company-field-wrapper">
                                <label>
                                    <input type="text"
                                        placeholder={languageObj.translate('Fromname.1')}
                                        name="from_name"
                                        ref={register({ required: false })}
                                        defaultValue={companyData.from_name || ""} />
                                    {/* {errors.from_name && errors.from_name.type === 'required' &&
                                        <span className="error_cu">{languageObj.translate('from_namerequired.1')}</span>} */}
                                </label>
                                <label>
                                    <input type="email"
                                        placeholder={languageObj.translate('FromEmailAddress.1')}
                                        name="from_email_address"
                                        ref={register({ required: false })}
                                        defaultValue={companyData.from_email_address || ""} />
                                    {/* {errors.from_email_address && errors.from_email_address.type === 'required' &&
                                        <span className="error_cu">{languageObj.translate('from_email_addressrequired.1')}</span>} */}
                                </label>
                                <label>
                                    <input type="text"
                                        placeholder={languageObj.translate('host.1')}
                                        name="host"
                                        ref={register({ required: false })}
                                        defaultValue={companyData.host || ""} />
                                    {/* {errors.host && errors.host.type === 'required' &&
                                        <span className="error_cu">{languageObj.translate('hostrequired.1')}</span>} */}
                                </label>
                                <label>
                                    <input type="text"
                                        placeholder={languageObj.translate('Port.1')}
                                        name="port"
                                        ref={register({ required: false })}
                                        defaultValue={companyData.port || ""} />
                                    {/* {errors.port && errors.port.type === 'required' &&
                                        <span className="error_cu">{languageObj.translate('portrequired.1')}</span>} */}
                                </label>
                                <label className="auth">
                                    <select name="smtpauth" value={companyData.smtpauth || ""} ref={register}
                                        onChange={({ target }) => {
                                            setCompanyData({ ...companyData, smtpauth: target.value })
                                        }}>
                                        <option value="">SMTP Auth</option>
                                        <option value="true">True</option>
                                        <option value="false">False</option>
                                    </select>
                                </label>
                                <label className="encryption">
                                    <input type="text"
                                        placeholder={languageObj.translate('Encryption.1')}
                                        name="encryption"
                                        ref={register({ required: false })}
                                        defaultValue={companyData.encryption || ""} />
                                    {/* {errors.encryption && errors.encryption.type === 'required' &&
                                        <span className="error_cu">{languageObj.translate('Encryptionrequired.1')}</span>} */}
                                </label>

                                <label>
                                    <input type="text"
                                        placeholder={languageObj.translate('Username.1')}
                                        name="username"
                                        ref={register({ required: false })}
                                        defaultValue={companyData.username || ""} />
                                    {/* {errors.username && errors.username.type === 'required' &&
                                        <span className="error_cu">{languageObj.translate('Usernamerequired.1')}</span>} */}
                                </label>
                                <label>
                                    <input type="password"
                                        placeholder={languageObj.translate('Password.1')}
                                        name="password"
                                        ref={register({ required: false })}
                                        defaultValue={companyData.password || ""} />
                                    {/* {errors.password && errors.password.type === 'required' &&
                                        <span className="error_cu">{languageObj.translate('Passwordrequired.1')}</span>} */}
                                </label>

                            </div>
                        </div>
                    </div>
                </form>
                <form onSubmit={handleSubmit(sendTestMail)}>
                    <div className={`add-company-form-wrap ${isSMTP === true ? 'disabled-input-email' : ''}`}>
                        <div className={`email-field-wrapper ${isSMTP === true ? 'mb-2' : ''}`}>

                            <h3 className="email-heading">Check Test Mail</h3>

                            <div className="email-field-section">
                                <label className="test-email mb-0">{isSMTP === true ? <input type="email" className="email-inp disabled-input-email" placeholder="Enter Email Address"
                                    name="smtp_email"
                                    disabled={isSMTP} />
                                    : <input type="email" className="email-inp" ref={register({ required: false })} placeholder="Enter Email Address"
                                        name="smtp_email"
                                        disabled={isSMTP} />}
                                </label>
                                <div className={`send-button ${isSMTP === true ? 'disabled-input-email' : ''}`}>
                                    <button className="send-btn" disabled={isSMTP} type="send">Send</button>
                                </div>

                            </div>
                        </div>
                        {isSMTP === true ? <span className="note-msg">To send test mail please fill below SMTP details first.</span> : <span></span>}
                    </div>
                </form>
                <div className="add-company-form-wrap instruction">
                    <label>Here’s a list of all of them and what do they configure:</label>
                    <b>From Email</b> – the email address you want to send emails from – for example, email@yourdomain.com;<br />
                    <b>From Name</b> – the name that your emails will be sent from;<br />
                    <b>SMTP Host</b> – the hostname for your SMTP server;<br />
                    <b>SMTP Port</b> – the port your server works on;<br />
                    <b>Encryption</b> – if you have SSL/TLS encryption available for that hostname, enter it here;<br />
                    <b>Username</b> – the username for your SMTP server;<br />
                    <b>Password</b> – the password for your SMTP server;<br /><br />
                    <b>Once you configure those parameters, click Save Settings at the top of the page.
                    </b>
                </div>
            </section>
        </React.Fragment>
    )
}

export default SMTPSetting