import React, { useState, Fragment, useEffect } from "react";
import { Modal, Spinner } from 'react-bootstrap';
import useForm from 'react-hook-form';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';
import FormData from 'utility/AppFormData';

function AddNewParticipantModal({ show, onHide, survey_id }) {
    const { register, handleSubmit, errors, setError, clearError, setValue } = useForm();
    const [participantIdentifier, setParticipantIdentifier] = useState([])
    const [participantData, setParticipantData] = useState({})
    const [loadingData, setLoadingData] = useState(false)
    const [errorMsg, setErrorMsg] = useState(null)
    const [searchTxt, setSearchTxt] = useState("")
    const [saving, setSaving] = useState(false)
    const [disableAddNew, setDisableAddNew] = useState(true)
    const [viewFrom, setViewFrom] = useState(false)
    const [webEmail, setWebEmail] = useState()
    const [webSms, setWebSms] = useState()
    const [whatsApp, setWhatsApp] = useState()

    const onSubmit = data => {
        if (data.email === "" && data.contact_number === "") {
            setError("email", "required");
            setError("contact_number", "required");
            return;
        }
        setSaving(true)
        let formData = new FormData()
        formData.append('survey_id', survey_id)
        formData.append("participant_data", JSON.stringify(data));
        Axios.post(configURL.add_api_participant, formData).then(response => {
            setSaving(false)
            if (response.data.success) {
                onHide()
                toast.success(response.data.message || 'Participant Added.')
            } else {
                setErrorMsg(response.data.message || 'Something went wrong')
                setTimeout(() => setErrorMsg(null), 4000);
            }
        }).catch(err => {
            console.log(err.response);
            setErrorMsg('Something went wrong')
            setSaving(false)
        })
    };

    useEffect(() => {
        if (show) {
            setParticipantIdentifier([])
            setParticipantData({})
            setLoadingData(false)
            setDisableAddNew(true)
            setErrorMsg(null)
            setSearchTxt("")
            setSaving(false)
            setViewFrom(false)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [show])

    const getIdentifiers = async () => {
        try {
            setErrorMsg(null)
            setLoadingData(true)
            let formData = new FormData()
            formData.append('survey_id', survey_id)
            const identifiers = await Axios.post(configURL.dynamicParticipantIdentifier, formData)
            setLoadingData(false)
            if (identifiers && identifiers.data.success === true) {
                setParticipantIdentifier(identifiers.data.results)
                setWebEmail(identifiers.data.channels.webEmail)
                setWebSms(identifiers.data.channels.webSms)
                setWhatsApp(identifiers.data.channels.whatsApp)
                setViewFrom(true)
            } else {
                setErrorMsg(identifiers.data.message || 'Something went wrong')
                setTimeout(() => setErrorMsg(null), 4000);
            }
        } catch (error) {
            setLoadingData(false)
            setErrorMsg(`Coul'd not load identifiers`)
            setTimeout(() => setErrorMsg(null), 4000);
        }
    }

    const validateAndSearch = () => {
        if (searchTxt === "") return;
        if (Number.isInteger(Number(searchTxt)) && (searchTxt.length < 10 || searchTxt.length > 15)) {
            setErrorMsg('Please enter valid contact number')
            return;
        }
        let mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!Number.isInteger(Number(searchTxt)) && !searchTxt.match(mailformat)) {
            setErrorMsg('Please enter valid email id')
            return;
        }
        getExistingParticipant()
    }

    const getExistingParticipant = async () => {
        try {
            setErrorMsg(null)
            setLoadingData(true)
            let searchData = new FormData()
            searchData.append('survey_id', survey_id)
            searchData.append('search', searchTxt)
            const participants = await Axios.post(configURL.search_api_participant, searchData)
            setLoadingData(false)
            if (participants && participants.data.success === true && Object.keys(participants.data.result).length > 0) {
                setParticipantData(participants.data.result)
                getIdentifiers()
            } else {
                setDisableAddNew(false)
                setErrorMsg(participants.data.message || 'Participant not found on survey')
                //setTimeout(() => setErrorMsg(null), 4000);
            }
        } catch (error) {
            setLoadingData(false)
            setErrorMsg(`Something went wrong please try again`)
            setTimeout(() => setErrorMsg(null), 4000);
        }
    }
    
    return (
        <Modal show={show} onHide={onHide} size="md" aria-labelledby="contained-modal-title-vcenter" centered className="theme-modal-wrapper addNewParticipantModal" >
            <Modal.Header className="ezi-modal-header">
                <Modal.Title id="contained-modal-title-vcenter" className="theme-modal-title ezi-modal-header-title" >
                    <span className="theme-modal-title-text">Add New Participant</span>
                    <span className="ezi-modal-close" onClick={onHide}></span>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="theme-field-wrapper upper-search">
                    <div className="res-msg-wrapper">
                        {loadingData && <p className="dynamic_participant_loading">Loading Information...</p>}
                    </div>
                    {!viewFrom && <div className="theme-field-50">
                        <div className="theme-field">
                            <input type="text" className="theme-field-control custom-css-input" placeholder="Email or Mobile Number(91XXXXXXXXXX)" onChange={({ target }) => {
                                setErrorMsg(null)
                                setSearchTxt(target.value)
                            }} value={searchTxt} />
                            {errorMsg && <span className="participant_src_err">{errorMsg} </span>}
                            {(!disableAddNew && !viewFrom) && <span type="button" className="add_participant_btn" onClick={getIdentifiers}>Add New</span>}
                        </div>
                        <button type="button" className="btn-ripple ezi-search-btn" disabled={loadingData} onClick={validateAndSearch}>Search</button>
                    </div>}
                </div>
                {viewFrom && <form onSubmit={handleSubmit(onSubmit)} className="add-theme-form">
                    <div className="add_participant_notes">
                        <p>Either email id or contact number is mandatory</p>
                        {errorMsg && <span className="participant_src_err">{errorMsg} </span>}
                    </div>
                    {participantIdentifier.length > 0 && participantIdentifier.map((item, i) => (
                        <div key={i} className="theme-field-wrapper">
                            <div className="theme-field-50">
                                <div className="theme-field">
                                    {/* <label>{item.label}</label> */}
                                    {(() => {
                                        switch (item.type) {
                                            case "personal":
                                                if (item.name === 'fname') {
                                                    return (
                                                        <Fragment>
                                                            <input type="text" className="theme-field-control" name={item.name} ref={register({ required: true })} placeholder={item.label || ''} defaultValue={participantData['fname'] || ""} />
                                                            {errors[item.name] && <span className="theme-error_cu">{item.label} is required.</span>}
                                                        </Fragment>
                                                    );
                                                }
                                                else if (item.name === 'email') {
                                                    return (
                                                        <Fragment>
                                                            <input
                                                                type="text"
                                                                className="theme-field-control"
                                                                name={item.name}
                                                                ref={register({
                                                                    // eslint-disable-next-line
                                                                    pattern: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/i
                                                                })}
                                                                placeholder={item.label || ''}
                                                                defaultValue={(participantData[item.name]) ? participantData[item.name] : !Number.isInteger(Number(searchTxt)) ? searchTxt : '' || ""}
                                                                onChange={({ target }) => {
                                                                    setValue("email", target.value.trim(), { shouldValidate: true })
                                                                    if (target.value.trim() !== "") {
                                                                        clearError('contact_number')
                                                                    }
                                                                }
                                                                } />
                                                            {(errors[item.name] && errors[item.name].type === "required") && <span className="theme-error_cu">please enter either email or contact number.</span>}
                                                            {(errors[item.name] && errors[item.name].type === "pattern") && <span className="theme-error_cu">{item.label} is not valid.</span>}
                                                        </Fragment>
                                                    );
                                                }
                                                else if (item.name === 'contact_number') {
                                                    return (
                                                        <Fragment>
                                                            <input type="text" className="theme-field-control" name="contact_number" ref={register({ pattern: /^[0-9]*$/, minLength: 10, maxLength: 15 })} placeholder={item.label || ''} defaultValue={(participantData[item.name]) ? participantData[item.name] : Number.isInteger(Number(searchTxt)) ? searchTxt : '' || ""} onChange={({ target }) => {
                                                                setValue("contact_number", target.value.trim(), { shouldValidate: true })
                                                                if (target.value.trim() !== "") {
                                                                    clearError('email')
                                                                }
                                                            }} />
                                                            {(errors[item.name] && errors[item.name].type === "required") && <span className="theme-error_cu">please enter either email or contact number.</span>}
                                                            {(errors.contact_number && (errors.contact_number.type === "pattern" || errors.contact_number.type === "minLength" || errors.contact_number.type === "maxLength")) && <span className="theme-error_cu">{item.label} is not valid.</span>}
                                                        </Fragment>
                                                    );
                                                }
                                                else {
                                                    return (
                                                        <Fragment>
                                                            <input type="text" className="theme-field-control" name={item.name} ref={register} placeholder={item.label || ''} defaultValue={participantData[item.name] || ""} />
                                                        </Fragment>
                                                    );
                                                }
                                            case "identifier":
                                                return (
                                                    <Fragment>
                                                        <input className="theme-field-control" name={item.name} ref={register} placeholder={`Enter ${item.label || ''} or select by dropdown`} defaultValue={participantData[item.name] || ""} list={item.name} onChange={({ target }) => setValue(item.name, target.value.trim())} />
                                                        <datalist id={item.name}>
                                                            {item.options.length > 0 && item.options.map(el => <option key={el} value={el} />)}
                                                        </datalist>
                                                    </Fragment>
                                                );
                                            default:
                                                return null
                                        }
                                    })()}
                                </div>
                            </div>
                        </div>
                    ))}
                    <div className="theme-field-wrapper">
                        {console.log(webSms,webEmail,whatsApp)}
                        <div className="theme-field-50">
                            <div className="theme-field communication-checkboxes">
                                <label className="qaizenx-checkbox-input">
                                    Email Invite
                                    {webEmail !== undefined ? 
                                    <div><input type="checkbox" name="communication_channel" value="webEmail" ref={register({ required: true })}/><span className="checkmark"></span></div> : 
                                    <div><input type="checkbox" name="communication_channel" ref={register({ required: true })} value="" disabled />
                                    <span className="checkmark disabled_checkmark"></span></div>}
                                </label>
                                <label className="qaizenx-checkbox-input" >
                                    SMS Invite
                                    { webSms !== undefined ? 
                                    <div><input type="checkbox" name="communication_channel" value="webSms" ref={register({ required: true })} /><span className="checkmark"></span></div> :
                                    <div><input type="checkbox" name="communication_channel" ref={register({ required: true })} value="" disabled /> 
                                    <span className="checkmark disabled_checkmark"></span></div>}
                                </label>
                                <label className="qaizenx-checkbox-input">
                                    WhatsApp Invite
                                    {whatsApp !== undefined ?
                                     <div><input type="checkbox" name="communication_channel" value="whatsApp" ref={register({ required: true })}/><span className="checkmark"></span></div> : 
                                     <div><input type="checkbox" name="communication_channel" ref={register({ required: true })} value="" disabled />
                                    <span className="checkmark disabled_checkmark"></span></div>}
                                </label>
                            </div>
                            {errors.communication_channel && <span className="theme-error_cu">please choose communication channel.</span>}
                        </div>
                    </div>
                    <div className="theme-modal-footer custom-">
                        <button type="button" className="close-theme-btn" onClick={onHide}>Close</button>
                        <button type="submit" className="btn-ripple ezi-pink-btn add-theme-btn" disabled={saving}>
                            Invite {saving && <Spinner animation="border" size="sm" className="custom-spinner" />}
                        </button>
                    </div>
                </form>}
            </Modal.Body>
        </Modal>
    );
}

export default AddNewParticipantModal