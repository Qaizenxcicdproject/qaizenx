import React, { useState, useEffect, useRef, useContext } from "react";
import { Breadcrumb, Modal } from 'react-bootstrap';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { dateConvert } from 'utility/helper';
import 'utility/i18next.js'
import './CategorySurveyDashboard.scss';
import EziAlert from 'components/Alert';
import { toast } from "react-toastify";
import SweetSearch from "components/SweetSearch";
import Pagination from "react-js-pagination";
import { confirmAlert } from 'react-confirm-alert';
import AppContext from 'store/AppContext';
import DateTimePicker from 'react-datetime-picker';
import ActiveSurvey from "components/ActiveSurvey";
import AddNewParticipantModal from "./AddNewParticipantModal";
import FormData from 'utility/AppFormData';

const SurveyActionPlanDashboard = (props) => {
	const { languageObj = {}, accesFeature = {}, EziLoader } = useContext(AppContext)
	let [defaultCategory] = useState({
		category_id: "",
		category_name: "",
		description: ""
	});

	const [categorySurveys, setCategorySurveys] = useState([]);
	const [pagination, setPagination] = useState({});
	const [searchLoading, setSearchLoading] = useState(false);
	let filterType = "total";
	const [listingFilter] = useState({});
	const [addParticipantMeta, setAddParticipantMeta] = useState({
		survey_id: null,
		visible: false
	});
	const [surveyUpdatedata, setSurveyUpdatedata] = useState({
		show: false,
		id: "",
		name: "",
		closeDate: "",
		startDate: ""
	});
	const [activateSurveyData, setActivateSurveyData] = useState({
		show: false,
		closeDate: "",
		survey_id: null
	});
	const perPage = 10;
	const [loading] = useState(false);
	const [modal, setModel] = useState({
		show: false,
		text: "",
		seletedId: "",
		newStatus: ""
	});
	var searchTimer = null;
	const inputSearch = useRef(null);
	/**
	 * Get Selected/Default category MetaData.
	 */
	const getCategoryMetaData = async (pageno = 1) => {
		let formData = new FormData();
		formData.append("category_id", defaultCategory.category_id);
		let response = await Axios.post(configURL.survey_dashboard_details, formData);
		if (response.data.success !== undefined && response.data.success === true) {
			//setCategoryMetaData(response.data.results);
			getCategorySurveys(pageno)
		} else {
			toast.warn(response.data.message);
		}
	}

	/**
	 * Change status
	 * 
	 * @param {number} id data id
	 */
	const handleStatusChange = () => {
		let selectedId = modal.seletedId;
		let newStatus = modal.newStatus;
		setModel({ show: false, newStatus: "", text: "", seletedId: "" })
		let formData = new FormData();
		formData.append("survey_id", selectedId);
		formData.append("status", newStatus);
		Axios.post(configURL.survey_status_update, formData).then(response => {
			if (response.data.success !== undefined && response.data.success === true) {
				toast.success(response.data.message);
				getCategoryMetaData(pagination.current_page || 1)
			} else {
				toast.warn(response.data.message)
			}
		})
	}

	/**
	 * Get Searched category.
	 * 
	 * @param {string} search Search string.
	 */
	const getCategoriesListing = (search = "") => {
		let formData = new FormData();
		formData.append("search", search);
		formData.append("type", "survey");
		Axios.post(configURL.categoryDs, formData).then(response => {
			if (response.data.success !== undefined && response.data.success === true) {
				let result = response.data.results;
				const categoryData = [];
				result.forEach(item => {
					categoryData.push({ id: item.id, name: item.category_name, description: item.description });
				});
				// setCategories(categoryData);
			} else {
				toast.warn(response.data.message);
			}
		})
	}

	/**
	 * Get Perticular template types.
	 * 
	 * @param {string} type template types
	 * @param {number} page Page number
	 */
	const getCategorySurveys = (page = 1, type = filterType, filters = listingFilter) => {
		let searchVal = inputSearch.current.value;
		let formData = new FormData();
		formData.append("search", searchVal);
		formData.append("per_page", perPage);
		formData.append("page", page);
		Axios.post(configURL.action_plan_survey_list, formData).then(response => {
			EziLoader.hide()
			setSearchLoading(false)
			if (response.data.success !== undefined && response.data.success === true) {
				setCategorySurveys(response.data.results);
				setPagination(response.data.pagination);
			} else {
				toast.warn(response.data.message);
			}
		}).catch(err => {
			console.log(err)
			EziLoader.hide()
		})
	}

	/**
	 * Filter Data based on search.
	 * 
	 * @param {string} type Input value
	 */
	const handleFilterSearch = () => {
		clearTimeout(searchTimer);
		searchTimer = setTimeout(() => {
			setSearchLoading(true)
			getCategorySurveys(1, filterType);
		}, 800);
	}

	/**
	 * Handle Pagination
	 * 
	 * @param {string} type Filter Type
	 */
	const handlePagination = (page = 1) => {
		EziLoader.show()
		getCategorySurveys(page, filterType);
	}


	useEffect(() => {
		EziLoader.show()
		getCategoryMetaData()
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [defaultCategory]);
	useEffect(getCategoriesListing, []);

	/**
	 * Change Survey Date
	 */
	const handleUpdateSurveyDate = () => {
		let start_date = (surveyUpdatedata.startDate !== null) ? dateConvert(new Date(surveyUpdatedata.startDate)) : null
		let end_date = (surveyUpdatedata.closeDate !== null) ? dateConvert(new Date(surveyUpdatedata.closeDate)) : null
		let formData = new FormData();
		formData.append("survey_id", surveyUpdatedata.id);
		formData.append("start_date", start_date);
		formData.append("end_date", end_date);
		Axios.post(configURL.update_survey_date, formData).then(response => {
			if (response.data.success !== undefined && response.data.success === true) {
				setSurveyUpdatedata({ show: false, name: "", id: "", closeDate: null, startDate: null })
			} else {
				toast.warn(response.data.message);
			}
		})
	}

	/**
	 * Activate Survey
	 * @param {Date} date 
	 * @param {String} survey_id 
	 */
	const handleActivateSurvey = (date, survey_id) => {
		let formData = new FormData()
		formData.append('survey_id', survey_id)
		if (date) {
			formData.append('enddate', dateConvert(date))
		}
		formData.append('status', 'active')
		Axios.post(configURL.survey_status_update, formData).then(response => {
			if (response.data.success !== undefined && response.data.success === true) {
				toast.success(response.data.message);
				setActivateSurveyData({ show: false, closeDate: null, survey_id: null })
				getCategoryMetaData(pagination.current_page || 1)
			} else {
				toast.warn(response.data.message)
			}
		})
	}

	/**
	* To check Action Planning Ticket Available or not
	*/
	const checkActionPlanListing = (surveyId, surveyName) => {
		let notificationId = toast.info('Loading Action Plan information..', { position: "bottom-right" })
		let formData = new FormData()
		surveyId && formData.append("survey_id", surveyId)
		Axios.post(configURL.action_plan_task_list, formData).then(res => {
			toast.dismiss(notificationId)
			if (res?.data?.results.length > 0) {
				props.history.push(`/action-plan/${surveyId}`, {
					surveyName: surveyName,
				})
			}
			else {
				confirmAlert({
					title: 'Action Planning',
					message: 'Tickets not present for this Survey.',
					buttons: [{ label: 'I Understood' }, { label: 'Close' }]
				});
				return;
			}
		})
	}

	return (
		<React.Fragment>
			<section className="Page-CategorySurveyDashboard Action-plan">
				<AddNewParticipantModal
					show={addParticipantMeta.visible}
					onHide={() => setAddParticipantMeta({ survey_id: null, visible: false })}
					survey_id={addParticipantMeta.survey_id}
				/>
				<EziAlert
					show={modal.show}
					alerttext={modal.text}
					showcancel
					showClose
					confirm={handleStatusChange}
					onhide={() => setModel({ show: false, text: "", seletedId: "", curStatus: "" })}
				/>
				<ActiveSurvey
					show={activateSurveyData.show}
					closeDate={activateSurveyData.closeDate}
					confirm={handleActivateSurvey}
					survey_id={activateSurveyData.survey_id}
					onhide={() => setActivateSurveyData({ show: false, closeDate: null, survey_id: null })}
				/>
				<div className="breadcrumb_ezi">
					<Breadcrumb>
						<Breadcrumb.Item >{languageObj.translate('ActionPlan.1')}</Breadcrumb.Item>
					</Breadcrumb>
					<div class="st_heading">{languageObj.translate('ActionPlan.1')} </div>
				</div>

				<div className="filter-search-wrap">
					<SweetSearch loading={searchLoading} change={handleFilterSearch} ref={inputSearch} />
				</div>

				<div className="category-dashboard-table">

					<div className="category-dashboard-table-row category-table-heading">
						<div className="category-dashboard-table-cell">  {languageObj.translate('Survey.1')}</div>
						<div className="category-dashboard-table-cell">No. of Tickets</div>
						<div className="category-dashboard-table-cell">  {languageObj.translate('Action.1')}</div>
					</div>
					{
						categorySurveys.length > 0 ? categorySurveys.map((item, index) => {
							return (
								<div key={index} className="category-dashboard-table-row">
									<div className="category-dashboard-table-cell" data-title="Survey">
										<div className={`category-table-survey-wrap ${accesFeature.edit_survey || "access_lock"}`} >
											<div className="category-table-survey-text-wrap">
												<span className="category-table-survey-name">{item.display_name}</span>
											</div>
										</div>
									</div>
									<div className="category-dashboard-table-cell" data-title="Owner"><span className="owner-name">{item.count || 0}</span></div>
									<div className={`category-dashboard-table-cell`} data-title="Action">
										<div className="action-wrap">
											<button className="dt-analyze-formatter" onClick={() => checkActionPlanListing(item.id, item.display_name)}>View Action Plan</button>
										</div>
									</div>
								</div>
							)
						}) : null
					}
				</div>
				{categorySurveys.length <= 0 && <div className="category-table-no-result">No result Found</div>}
				<div className="pagination-plugin-wrap category-pagination-formatter">
					<Pagination
						activePage={pagination.current_page}
						itemsCountPerPage="10"
						totalItemsCount={pagination.total}
						onChange={handlePagination}
						hideDisabled={true}
						firstPageText={<span class="prev-page-text-ic"></span>}
						lastPageText={<span class="next-page-text-ic"></span>}
						nextPageText={<span class="next-text-ic"></span>}
						prevPageText={<span class="prev-text-ic"></span>}
					/>
				</div>
			</section>
			<Modal show={surveyUpdatedata.show} onHide={() => setSurveyUpdatedata({ show: false, name: "", id: "", closeDate: null, startDate: null })} size="md" aria-labelledby="ezi-modal-for" centered className="ezi-modal-wrapper" >
				<Modal.Header className="ezi-modal-header">
					<Modal.Title id="ezi-modal-for" className="ezi-modal-title-wrap" >
						<span className="ezi-modal-header-title">Update {surveyUpdatedata.name}</span>
						<span className="ezi-modal-close" onClick={() => setSurveyUpdatedata({ show: false, name: "", id: "", closeDate: null, startDate: null })}></span>
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="survey-date-change-wrapper">
						<div className="survey-date-change-field">
							<label>Survey Started At :</label>
							<DateTimePicker
								onChange={(date) => setSurveyUpdatedata({
									...surveyUpdatedata,
									startDate: date
								})}
								className="ezi-datepicker-custom"
								value={new Date(surveyUpdatedata.startDate)}
								clearIcon={null}
								calendarIcon={null}
								disabled={(surveyUpdatedata.startDate !== null && new Date(surveyUpdatedata.startDate).getTime() < new Date().getTime()) ? true : false}
							/>
						</div>
						<div className="survey-date-change-field">
							<label>Survey Ending At :</label>
							<DateTimePicker
								onChange={(date) => setSurveyUpdatedata({
									...surveyUpdatedata,
									closeDate: date
								})}
								className="ezi-datepicker-custom"
								value={new Date(surveyUpdatedata.closeDate)}
								clearIcon={null}
								calendarIcon={null}
							/>
						</div>
					</div>
				</Modal.Body>
				<div className="ezi-modal-footer">
					<button type="button" className="ezi-modal-close-btn" onClick={() => setSurveyUpdatedata({ show: false, name: "", id: "", closeDate: null, startDate: null })}>Close</button>
					<button type="button" className="btn-ripple ezi-pink-btn ezi-modal-save-btn" onClick={handleUpdateSurveyDate}>Save</button>
				</div>
			</Modal>
			{loading && <EziLoader />}
		</React.Fragment>
	)
}

export default SurveyActionPlanDashboard;