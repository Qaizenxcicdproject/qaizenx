import React, { useEffect, useState, useRef } from "react";
import EziLoader from "components/EziLoader";
import Axios from "utility/Axios";
import configURL from "config/config";
import { toast } from "react-toastify";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { useParams } from 'react-router-dom';
import { confirmAlert } from 'react-confirm-alert';
import FormData from 'utility/AppFormData';

const Schedules = () => {
    const [loading, setLoading] = useState(false)
    const [schedulesData, setSchedulesData] = useState([])
    const [dataColumn, setDataColumn] = useState([{}]);
    const [metaData, setMetaData] = useState({
        total: 0,
        page: 1,
        per_page: 10
    });
    const urlParams = useParams()
    let tableRef = useRef(null);

    const buttonFormatter = (cell, row, rowIndex) => (
        <button type="button" className="table_delete_ic" disabled={(row.status.toLowerCase() === "complete")} title={(row.status.toLowerCase() === "complete") ? 'Communication Already Completed' : 'Remove Communication'} onClick={() => handleDeleteCommunication(row)}>Delete</button>
    )

    const statusFormatter = (cell, row) => {
        return (
            <span className={`history_status ${cell}_c`}>{cell}</span>
        );
    }

    const handleDeleteCommunication = (row) => {
        confirmAlert({
            title: 'Delete Communication',
            message: 'Are you sure you want to delete ?',
            buttons: [
                {
                    label: 'Confirm',
                    onClick: () => {
                        let formData = new FormData();
                        formData.append("communication_id", row.id);
                        formData.append("survey_id", urlParams.survey_id);
                        Axios.post(configURL.deleteCommunication, formData).then(res => {
                            if (res.data && res.data.success) {
                                toast.success(res.data.message || 'Data Deleted');
                                let current_page = tableRef.current.paginationContext.currPage
                                if ((tableRef.current.table.props.data.length - 1) === 0) {
                                    current_page = tableRef.current.paginationContext.currPage - 1
                                }
                                current_page = (current_page > 0) ? current_page : 1
                                getCommunicationSchedules(current_page)
                            } else {
                                toast.warn(res.data.message || 'Something went wrong');
                            }
                        })
                    }
                },
                {
                    label: 'Cancel'
                }
            ]
        });
    }

    /**
     * Handle Table Changes
     * @param {String} type 
     * @param {Object} props 
     */
    const handleTableChange = (type, props) => {
        setLoading(true)
        switch (type) {
            case "pagination":
                getCommunicationSchedules(props.page);
                break;
            default:
                break
        }

    }

    /**
     * Get Communication History
     * @param {Number} page 
     */
    const getCommunicationSchedules = (page = 1) => {
        let formData = new FormData()
        formData.append("page", page)
        formData.append("survey_id", urlParams.survey_id)
        formData.append("per_page", metaData.per_page)
        Axios.post(configURL.communication_schedules, formData).then(response => {
            if (response.data.success === true) {
                setMetaData({ ...metaData, total: response.data.results.pagination_data.total, page: page })
                const columnData = response.data.results.column.map(item => {
                    if (item.dataField === "id") {
                        return {
                            ...item,
                            hidden: true,
                        }
                    }
                    else if (item.dataField === "status") {
                        return {
                            ...item,
                            formatter: statusFormatter,
                        }
                    }
                    else {
                        return item;
                    }
                });
                columnData.push({
                    headerFormatter: () => <div><span>Action</span></div>,
                    formatter: buttonFormatter
                });
                setDataColumn(columnData)
                setSchedulesData(response.data.results.data)
                setLoading(false)
            } else {
                toast.warn(response.data.message);
            }
        })
    }

    useEffect(() => {
        setLoading(true)
        getCommunicationSchedules();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    /**
     * Build Data Table
     * @param {Object} param0 
     */
    const RemotePagination = ({ data, page = 1, sizePerPage, onTableChange, totalSize }) => (
        <div>
            <BootstrapTable
                ref={tableRef}
                remote
                keyField={'id'}
                data={data}
                noDataIndication="No Data Available."
                columns={dataColumn}
                pagination={paginationFactory({ page, sizePerPage, totalSize, hideSizePerPage: true })}
                onTableChange={onTableChange}
            />
        </div>
    )

    return (
        <React.Fragment>
            <div className="users-table-content">
                <RemotePagination
                    data={schedulesData}
                    page={metaData.page || 0}
                    sizePerPage={metaData.per_page}
                    totalSize={metaData.total || 0}
                    onTableChange={handleTableChange}
                />
            </div>
            {loading && <EziLoader />}
        </React.Fragment>
    )
}

export default Schedules;