import React, { useState, useEffect, useRef, useContext } from "react";
import { Tab, Nav, OverlayTrigger, Tooltip, Spinner } from 'react-bootstrap';
import Axios from "utility/Axios"
import configURL from 'config/config';
import { dateConvert } from 'utility/helper';
import DateTimePicker from 'react-datetime-picker';
import { EditorState, convertToRaw } from 'draft-js';
import { stateFromHTML } from 'draft-js-import-html';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import DropdownPlaceholder from './DropdownPlaceholder';
import { outlineRemove } from 'utility/helper';
/** Editor Imports ends */
import { CHANNEL_PLACEHOLDERS_SUBJECT, PUBLISHING_CHANNELS_TXT, CHANNEL_PLACEHOLDERS_SMS, COMMUNICATION_TOOLBAR_CONFIG } from "constants/constants";
import { connect } from 'react-redux';
import * as AppAction from "store/actions";
import AppContext from "store/AppContext";
import { toast } from "react-toastify";
import { useParams } from "react-router-dom";
import InputPrompt from "components/InputPrompt";
import Select from 'react-select'
import JsxParser from "html-react-parser"
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { confirmAlert } from 'react-confirm-alert';
import { cloneDeep } from "lodash"
import { Fragment } from "react";

const CommunicationChannel = ({ surveyAction, dispatchSelectedCount, selectedCount, dispatchChannels, surveyStatus, channelTypes, dispatchSelectedOnEdit, dispatchCommunicateByStatus, participant_status, surveyType, mappedIdentifier = [], saveSetting, thankEmail }) => {
    let channelDispatcher = null;
    // let imageSelectRef = null;
    const { EziLoader } = useContext(AppContext)
    const [lastFocusInput, setLastFocusInput] = useState('message');
    const [editMeta, setEditMeta] = useState(false);
    const [startDateEmail, setStartDateEmail] = useState(new Date());
    const [startDateSMS, setStartDateSMS] = useState(new Date());
    const [startDateWhatsApp, setStartDateWhatsApp] = useState(new Date());
    const [selectedChannel, setSelectedChannel] = useState([]);
    const [selectedTemplate, setSelectedTemplate] = useState({ 'webEmail': '', 'webSms': '', 'whatsApp': '' });
    const [activeChannel, setActiveChannel] = useState('');
    const [showPrompt, setShowPrompt] = useState(false);
    const inputSubjectRef = useRef(null);
    const inputMessageRef = useRef(null);
    const inputWhatsAppRef = useRef(null);
    const [channelTemplates, setChannelTemplates] = useState({})
    const [editorState, setEditorState] = useState(EditorState.createEmpty())
    const urlParams = useParams();
    const [toggleEmail, setToggleEmail] = useState(false);
    const [toggleSms, setToggleSms] = useState(false);
    const [toggleWhatsApp, setToggleWhatsApp] = useState(false);
    // const [qrCodeImages, setQrCodeImages] = useState({});
    const [qrCodeHtml, setQrCodeHtml] = useState(null);
    const [qrCodeText, setQrCodeText] = useState("");
    const [linkCopied, setLinkCopied] = useState(false);
    const [showTemplateLoader, setTemplateLoader] = useState(false);
    const participantsStatusData = [
        { value: 'all', label: 'All' },
        { value: 'completed', label: 'Completed' },
        { value: 'in_progress', label: 'In Progress' },
        { value: 'not_started', label: 'Not Started' },
        { value: 'non_appeared', label: 'Non Appeared' },
    ]
    const [qrCodeDownloadLink, setQrCodeDownloadLink] = useState();

    /**
     * Change Editor Data
     * @param {Object} editorState 
     */
    const onEditorStateChange = (editorState) => {
        handleInputChange({
            target: {
                name: "message",
                value: draftToHtml(convertToRaw(editorState.getCurrentContent()))
            }
        }, 'webEmail')
        setEditorState(editorState)
    }
    /**
     * Handle Variable insert on input
     */
    const insertVariable = (e) => {
        let inputRef = null;
        switch (lastFocusInput) {
            case 'email_subject':
                inputRef = inputSubjectRef
                break;
            case 'sms':
                inputRef = inputMessageRef
                break;
            case 'whats_app':
                inputRef = inputWhatsAppRef
                break;

            default:
                inputRef = null;
                break;
        }
        if (!inputRef) return;

        let textToInsert = e.target.value;
        if (inputRef?.current?.selectionStart) {
            let cursorPosition = inputRef.current.selectionStart;
            let textBeforeCursorPosition = inputRef.current.value.substring(0, cursorPosition);
            let textAfterCursorPosition = inputRef.current.value.substring(cursorPosition, inputRef.current.value.length);
            let finalText = textBeforeCursorPosition + textToInsert + textAfterCursorPosition;
            let customEvent = { ...inputRef };
            customEvent.target = { 'value': finalText, 'name': inputRef.current.name };
            handleInputChange(customEvent, inputRef.current.getAttribute("data-channel"));
        }
    }

    /**
     * Get All Channel Templates
     */
    const getChannelTemplates = () => {
        EziLoader.show()
        let formData = new FormData();
        formData.append("survey_id", urlParams.survey_id);
        formData.append("isedit", (surveyAction === "edit") ? true : false);
        Axios.post(configURL.publishingSettingListing, formData).then(res => {
            EziLoader.hide()
            if (res.data.success !== undefined && res.data.success === true) {
                setChannelTemplates(res.data.data)
                if (res.data.channels) {
                    setSelectedChannel(Object.keys(res.data.channels))
                }
                if (res.data.data.qrCode) {
                    setQrCodeHtml(res.data.data.qrCode.html.replace("[upper_text]", "") || null)
                    // setQrCodeImages(res.data.data.qrCode.image_url || {})
                    setQrCodeText(res.data.data.qrCode.upper_text || "")
                    setQrCodeDownloadLink(res.data.data.qrCode.download_qrcode || "")

                }
            } else {
                toast.warn(res.data.message || "Something went wrong.");
            }
        }).catch(err => {
            EziLoader.hide()
            toast.warn("Something went wrong.");
            console.log(err);
        })
    }

    /**
     * handle selected channel list
     */
    const handleCheckedChannel = (e) => {
        if (e.target.checked) {
            setActiveChannel(e.target.value);
            setSelectedChannel([...selectedChannel, e.target.value]);
        } else {
            let selectedChannelSet = new Set(selectedChannel);
            selectedChannelSet.delete(e.target.value);
            setSelectedChannel([...selectedChannelSet]);
        }
    }

    /**
     * Handle Template change
     * @param {Synthetic Event} e Input event
     * @param {String} tab selected tab
     */
    const changeTemplate = (e, tab) => {
        setSelectedTemplate({ ...selectedTemplate, [tab]: e.target.value });
        if (tab === 'webEmail' && channelTemplates.webEmail[e.target.value]) {
            setEditorState(EditorState.createWithContent(stateFromHTML(channelTemplates.webEmail[e.target.value]['message'])))
        }
    }

    /**
     * Handle Template content change
     * @param {Event} e 
     * @param {String} tab 
     */
    const handleInputChange = (e, tab) => {
        setChannelTemplates({
            ...channelTemplates,
            [tab]: {
                ...channelTemplates[tab],
                [selectedTemplate[tab]]: {
                    ...channelTemplates[tab][selectedTemplate[tab]],
                    [e.target.name]: e.target.value
                }
            }
        });
    }

    /**
     * Handle BookMark Template
     */
    const handleBookMarkTemplate = async (type = 'webEmail') => {
        try {
            setTemplateLoader(true)
            const apiTemplate = {
                ...channelTemplates[type][selectedTemplate[type]],
                is_api_template: !channelTemplates[type][selectedTemplate[type]]['is_api_template'],
                template_id: selectedTemplate[type]
            }
            let formData = new FormData()
            formData.append('api_template', JSON.stringify(apiTemplate))
            formData.append('survey_id', urlParams.survey_id)
            formData.append('type', type)
            const apiTemplateRes = await Axios.post(configURL.bookmark_template, formData)
            setTemplateLoader(false)
            if (apiTemplateRes.data.success === true) {
                setChannelTemplates(channelTemplates => {
                    let updatedTemplates = {}
                    Object.keys(channelTemplates[type]).forEach(el => {
                        if (selectedTemplate[type] === el) {
                            updatedTemplates[el] = {
                                ...channelTemplates[type][el],
                                is_api_template: !channelTemplates[type][el]['is_api_template']
                            }
                        } else {
                            updatedTemplates[el] = {
                                ...channelTemplates[type][el],
                                is_api_template: false
                            }
                        }
                    })
                    return {
                        ...channelTemplates,
                        [type]: updatedTemplates
                    }
                });
            } else {
                toast.warn(apiTemplateRes.data.message || 'Something is wrong here. Please try again')
            }
        } catch (error) {
            setTemplateLoader(false)
            toast.warn('Something is wrong here. Please try again')
            console.log(error);
        }
    }

    /**
     * Handle BookMark Template For Thank You Email
     */
    const handleAddThankTemplate = async (type = 'webEmail', thankMailStatus = true) => {
        if (!thankEmail && thankMailStatus) {
            toast.warn('Please enable Thank you email (Survey Settings > Customize)  before setting up the template')
            return;
        }
        try {
            setTemplateLoader(true)
            let formData = new FormData()
            formData.append('active', Number(!channelTemplates[type][selectedTemplate[type]]['active']))
            formData.append('survey_id', urlParams.survey_id)
            formData.append('type', type)
            formData.append('template_id', selectedTemplate[type])

            const thankYouTempRes = await Axios.post(configURL.bookmark_template_active, formData)
            setTemplateLoader(false)
            if (thankYouTempRes.data.success === true) {
                setChannelTemplates(channelTemplates => {
                    let updatedTemplates = {}
                    Object.keys(channelTemplates[type]).forEach(el => {
                        if (selectedTemplate[type] === el) {
                            updatedTemplates[el] = {
                                ...channelTemplates[type][el],
                                is_thank_email: !channelTemplates[type][el]['is_thank_email'],
                                active: Number(!channelTemplates[type][selectedTemplate[type]]['active'])
                            }
                        } else {
                            updatedTemplates[el] = {
                                ...channelTemplates[type][el],
                                is_thank_email: false,
                                active: 0
                            }
                        }
                    })
                    return {
                        ...channelTemplates,
                        [type]: updatedTemplates
                    }
                });
            } else {
                toast.warn(thankYouTempRes.data.message || 'Something is wrong here. Please try again')
            }
        } catch (error) {
            setTemplateLoader(false)
            toast.warn('Something is wrong here. Please try again')
            console.log(error);
        }
    }

    /**
     * Channel data dispatch
     */
    const handleDispatchTemplates = () => {
        clearTimeout(channelDispatcher)
        setTimeout(() => {
            let channelData = getTemplatesObj()
            dispatchChannels(channelData)
        }, 200);
    }

    /**
     * Get Selected templates and changed data
     */
    const getTemplatesObj = () => {
        let tempSurveySetting = JSON.parse(JSON.stringify(channelTemplates));
        let communicationTemp = {}
        if (tempSurveySetting.webEmail && selectedChannel.includes("webEmail") && selectedTemplate.webEmail && selectedTemplate.webEmail !== "") {
            let templateNameEmail = (tempSurveySetting.webEmail[selectedTemplate.webEmail] && tempSurveySetting.webEmail[selectedTemplate.webEmail].name) ? tempSurveySetting.webEmail[selectedTemplate.webEmail].name : "";
            communicationTemp.webEmail = {
                'templateId': selectedTemplate.webEmail,
                'schedule_datetime': (toggleEmail === false) ? 'now' : dateConvert(startDateEmail),
                'name': templateNameEmail,
                ...channelTemplates.webEmail[selectedTemplate.webEmail]
            }

        }
        if (tempSurveySetting.webSms && selectedChannel.includes("webSms") && selectedTemplate.webSms && selectedTemplate.webSms !== "") {
            let templateNameSms = (tempSurveySetting.webSms[selectedTemplate.webSms] && tempSurveySetting.webSms[selectedTemplate.webSms].name) ? tempSurveySetting.webSms[selectedTemplate.webSms].name : "";
            communicationTemp.webSms = {
                'templateId': selectedTemplate.webSms,
                'schedule_datetime': (toggleSms === false) ? 'now' : dateConvert(startDateSMS),
                'name': templateNameSms,
                ...channelTemplates.webSms[selectedTemplate.webSms]
            }
        }
        if (tempSurveySetting.whatsApp && selectedChannel.includes("whatsApp") && selectedTemplate.whatsApp && selectedTemplate.whatsApp !== "") {
            let templateNamewhatsApp = (tempSurveySetting.whatsApp[selectedTemplate.whatsApp] && tempSurveySetting.whatsApp[selectedTemplate.whatsApp].name) ? tempSurveySetting.whatsApp[selectedTemplate.whatsApp].name : "";
            communicationTemp.whatsApp = {
                'templateId': selectedTemplate.whatsApp,
                'schedule_datetime': (toggleWhatsApp === false) ? 'now' : dateConvert(startDateWhatsApp),
                'name': templateNamewhatsApp,
                ...channelTemplates.whatsApp[selectedTemplate.whatsApp]
            }
        }
        if (tempSurveySetting.webEmbed && selectedChannel.includes("webEmbed")) {
            communicationTemp.webEmbed = "Survey Embeded Code Generated"
        }
        if (tempSurveySetting.qrCode && selectedChannel.includes("qrCode")) {
            communicationTemp.qrCode = {
                upper_text: qrCodeText
            }
        }
        if (tempSurveySetting.webLink && selectedChannel.includes("webLink")) {
            communicationTemp.webLink = "Survey Web Link Generated"
        }
        const seleted = new Set(selectedChannel);
        [...new Set([...Object.keys(communicationTemp)].filter(x => !seleted.has(x)))].forEach(el => delete communicationTemp[el])
        return communicationTemp;
    }

    /**
     * Handle Save Template As new
     */
    const handleSaveNewTemplateEmail = (templateName) => {
        if (selectedTemplate.webEmail && channelTemplates.webEmail[selectedTemplate.webEmail]) {
            let data = channelTemplates.webEmail[selectedTemplate.webEmail];
            let formData = new FormData();
            formData.append("survey_id", urlParams.survey_id);
            formData.append("name", templateName);
            formData.append("body", data.message);
            formData.append("language", localStorage.getItem("i18nextLng"));
            formData.append("type", 'webEmail');
            formData.append("subject", data.subject);
            formData.append("from", data.from);
            formData.append("from_name", data.from_name);
            formData.append("name_key", data.name_key);
            Axios.post(configURL.saveTemplate, formData).then(res => {
                if (res.data.success !== undefined && res.data.success) {
                    toast.success(res.data.message || "Communication template saved");
                    getChannelTemplates()
                } else {
                    toast.warn(res.data.message || "Something went wrong.");
                }
            }).catch(err => {
                toast.warn("Something went wrong.");
                console.log(err);
            })
        }
    }

    /**
     * Handle Save SMS Template As new
     */
    const handleSaveNewTemplate = (templateName, channel) => {
        if (selectedTemplate[channel] && channelTemplates[channel][selectedTemplate[channel]]) {
            let data = channelTemplates[channel][selectedTemplate[channel]];
            let formData = new FormData();
            formData.append("survey_id", urlParams.survey_id);
            formData.append("name", templateName);
            formData.append("body", data.message);
            formData.append("language", localStorage.getItem("i18nextLng"));
            formData.append("type", channel);

            Axios.post(configURL.saveTemplate, formData).then(res => {
                if (res.data.success !== undefined && res.data.success) {
                    toast.success(res.data.message || "Communication template saved");
                    getChannelTemplates()
                } else {
                    toast.warn(res.data.message || "Something went wrong.");
                }
            }).catch(err => {
                toast.warn("Something went wrong.");
                console.log(err);
            })
        }
    }

    /**
     * Handle Save Template As new
     */
    const handleUpdateEmailTemplate = () => {
        if (selectedTemplate.webEmail && channelTemplates.webEmail[selectedTemplate.webEmail]) {
            let data = channelTemplates.webEmail[selectedTemplate.webEmail];
            let formData = new FormData();
            formData.append("template_id", selectedTemplate.webEmail);
            formData.append("survey_id", urlParams.survey_id);
            formData.append("body", data.message);
            formData.append("type", 'webEmail');
            formData.append("language", localStorage.getItem("i18nextLng"));
            formData.append("subject", data.subject);
            formData.append("from", data.from);
            formData.append("from_name", data.from_name);
            Axios.post(configURL.updateTemplate, formData).then(res => {
                if (res.data.success !== undefined && res.data.success) {
                    toast.success(res.data.message || "Communication template updated");
                    getChannelTemplates()
                } else {
                    toast.warn(res.data.message || "Something went wrong.");
                }
            }).catch(err => {
                toast.warn("Something went wrong.");
                console.log(err);
            })
        }
    }

    /**
     * Handle Save Template As new
     */
    // const handleUpdateTemplate = (type = null) => {
    //     if (selectedTemplate[type] && channelTemplates[type][selectedTemplate[type]]) {
    //         let data = channelTemplates[type][selectedTemplate[type]]
    //         let formData = new FormData();
    //         formData.append("template_id", selectedTemplate[type]);
    //         formData.append("body", data.message);
    //         formData.append("type", type);
    //         formData.append("survey_id", urlParams.survey_id);
    //         formData.append("language", localStorage.getItem("i18nextLng"));
    //         Axios.post(configURL.updateTemplate, formData).then(res => {
    //             if (res.data.success !== undefined && res.data.success) {
    //                 toast.success(res.data.message || "Communication template updated");
    //                 getChannelTemplates()
    //             } else {
    //                 toast.warn(res.data.message || "Something went wrong.");
    //             }
    //         }).catch(err => {
    //             toast.warn("Something went wrong.");
    //             console.log(err);
    //         })
    //     }
    // }

    /**
     * Handle Delete Template
     */
    const handleDeleteTemplate = (tab) => {
        confirmAlert({
            title: 'Delete Template',
            message: 'Are you want to delete this communication template ?',
            buttons: [{
                label: 'Confirm',
                onClick: () => {
                    let formData = new FormData();
                    formData.append("template_id", selectedTemplate[tab]);
                    formData.append("type", tab);
                    Axios.post(configURL.deleteCommunicationTemplate, formData).then(res => {
                        if (res.data.success !== undefined && res.data.success === true) {
                            toast.success(res.data.message || "Communication template deleted");
                            let channelData = cloneDeep(channelTemplates)
                            delete channelData[tab][selectedTemplate[tab]]
                            setSelectedTemplate({ ...selectedTemplate, [tab]: "" })
                            setChannelTemplates(channelData)
                        } else {
                            toast.warn(res.data.message || "Something went wrong.");
                        }
                    }).catch(err => {
                        toast.warn("Something went wrong.");
                        console.log(err);
                    })
                }
            }, { label: 'Cancel' }]
        });
    }

    const handleTemplateNameInput = (val) => {
        if (activeChannel === 'webEmail') {
            handleSaveNewTemplateEmail(val)
        } else {
            handleSaveNewTemplate(val, activeChannel)
        }
        setShowPrompt(false)
    }

    const handleToggle = () => {
        if (activeChannel === 'webEmail') {
            setToggleEmail(!toggleEmail);
        }
        if (activeChannel === 'webSms') {
            setToggleSms(!toggleSms);
        }
        if (activeChannel === 'whatsApp') {
            setToggleWhatsApp(!toggleWhatsApp);
        }
    }

    const handleChangeStatus = (type, data) => {
        dispatchCommunicateByStatus({ ...participant_status, [type]: data })
        dispatchSelectedCount(0)
        dispatchSelectedOnEdit([])
    }

    const handleQrTextChange = ({ target }) => {
        let { value } = target
        setQrCodeText(value)
    }
    // const handleDownloadQr = async () => {
    //     if (imageSelectRef.value !== "" && qrCodeImages[imageSelectRef.value]) {
    //         window.open(qrCodeImages[imageSelectRef.value], "_blank")
    //     }
    // }
    const getWebLinks = () => {
        let webLink = channelTemplates.webLink ? channelTemplates.webLink : ''
        if (webLink !== "" && mappedIdentifier.length > 0) {
            let jsonObj = {}
            webLink = `${webLink}?identifiers=`
            mappedIdentifier.forEach(el => {
                if (el?.mapped_question_name) {
                    jsonObj[el.mapped_question_name] = `{${el.identifier_name}}`
                }
            })
            webLink = `${webLink}${JSON.stringify(jsonObj)}`
        }
        return webLink
    }

    /**
     * Updated template subscription
     */
    useEffect(handleDispatchTemplates, [channelTemplates, selectedTemplate, selectedChannel, startDateEmail, startDateSMS, toggleEmail, toggleSms, qrCodeText, startDateWhatsApp, toggleWhatsApp])

    /**
     * Active first link on first render
     */
    useEffect(() => {
        if (surveyType === 'openSurvey') {
            setActiveChannel('webLink');
        }
        if (surveyType === 'oneTimeSurvey' || surveyType === 'continuous') {
            setActiveChannel('webEmail');
        }
        // eslint-disable-next-line
    }, [])

    /**
     * Updated template subscription
     */
    useEffect(getChannelTemplates, [channelTypes])

    return (
        <div className="channel-main-wrap">
            <InputPrompt
                show={showPrompt}
                alerttext="Please Enter Template Name"
                confirm={handleTemplateNameInput}
                onhide={() => setShowPrompt(false)}
            />
            <div className="channel-heading">Channels</div>
            <div className="channel-content">
                {
                    Object.keys(channelTemplates).map((item, index) =>
                        <div className="channel-checkbox-wrap" key={index}>
                            <label className="ezi-checkbox">
                                <input type="checkbox" value={item} checked={selectedChannel.includes(item)} onChange={(e) => handleCheckedChannel(e)} />
                                <span className="ezi-checkbox-mark"></span>
                            </label>
                            <p className="channel-name">{PUBLISHING_CHANNELS_TXT[item]}</p>
                        </div>
                    )
                }
                <div className={`main-channel-tab-wrap ${selectedChannel.length > 0 ? "" : "border-none"}`}>
                    <Tab.Container activeKey={activeChannel}>
                        <div className="survey-channel-tab-header">
                            <div className="tab-left-header">
                                <Nav variant="pills">
                                    {
                                        selectedChannel.includes('webEmail') &&
                                        <Nav.Item onClick={() => setActiveChannel('webEmail')}>
                                            <Nav.Link eventKey="webEmail">Web email</Nav.Link>
                                        </Nav.Item>
                                    }
                                    {
                                        selectedChannel.includes('webSms') &&
                                        <Nav.Item onClick={() => setActiveChannel('webSms')}>
                                            <Nav.Link eventKey="webSms">Web SMS</Nav.Link>
                                        </Nav.Item>
                                    }
                                    {
                                        selectedChannel.includes('webEmbed') &&
                                        <Nav.Item onClick={() => setActiveChannel('webEmbed')}>
                                            <Nav.Link eventKey="webEmbed">Web Embed</Nav.Link>
                                        </Nav.Item>
                                    }
                                    {
                                        selectedChannel.includes('qrCode') &&
                                        <Nav.Item onClick={() => setActiveChannel('qrCode')}>
                                            <Nav.Link eventKey="qrCode">QR Code</Nav.Link>
                                        </Nav.Item>
                                    }
                                    {
                                        selectedChannel.includes('webLink') &&
                                        <Nav.Item onClick={() => setActiveChannel('webLink')}>
                                            <Nav.Link eventKey="webLink">Web Link</Nav.Link>
                                        </Nav.Item>
                                    }
                                    {
                                        selectedChannel.includes('whatsApp') &&
                                        <Nav.Item onClick={() => setActiveChannel('whatsApp')}>
                                            <Nav.Link eventKey="whatsApp">WhatsApp</Nav.Link>
                                        </Nav.Item>
                                    }
                                </Nav>
                            </div>
                        </div>
                        {
                            (selectedChannel.length > 0) &&
                            <Tab.Content className="survey-channel-tab-content channel-tab-content">
                                <Tab.Pane eventKey="webEmail">
                                    <div>
                                        <select className="channel-select-mail" onChange={(e) => changeTemplate(e, 'webEmail')} value={selectedTemplate.webEmail}>
                                            <option value="" key="0">Select Email Template </option>
                                            {
                                                channelTemplates.webEmail && Object.keys(channelTemplates.webEmail).map(key =>
                                                    <option value={key} key={key} data-name={channelTemplates.webEmail[key]['name']}>{channelTemplates.webEmail[key]['name']}</option>
                                                )
                                            }
                                        </select>
                                    </div>
                                    {
                                        (selectedTemplate.webEmail !== '') &&
                                        <React.Fragment>
                                            <div className="email-preview-wrap">
                                                <div className="email-preview-header">
                                                    <span className="email-preview-heading">
                                                        Email Preview
                                                    </span>
                                                    <div className="email-preview-right-header">
                                                        <button type="button" className="email-preview-save" onClick={() => setShowPrompt(true)}>Save As New</button>
                                                        {
                                                            (!channelTemplates?.webEmail[selectedTemplate.webEmail]?.default_template) &&
                                                            <Fragment>
                                                                <button type="button" className="email-preview-save" onClick={handleUpdateEmailTemplate}>Update Selected</button>
                                                                <button type="button" className="email-preview-edit" onClick={() => setEditMeta('webEmail')} > Edit</button>
                                                            </Fragment>
                                                        }

                                                        {channelTemplates.webEmail[selectedTemplate.webEmail]?.name_key === "invitation-email" &&
                                                            (!showTemplateLoader && surveyType !== 'openSurvey') && <OverlayTrigger overlay={<Tooltip>{
                                                                (channelTemplates.webEmail[selectedTemplate.webEmail]?.is_api_template) ? 'Bookmarked for API Communication Template' : 'Bookmark this template for API Communications'
                                                            }</Tooltip>}>
                                                                <button type="button" className={`email-bookmark ${(channelTemplates.webEmail[selectedTemplate.webEmail]?.is_api_template) ? 'bookmarked' : ''} `} onClick={handleBookMarkTemplate.bind(null, 'webEmail')} disabled={showTemplateLoader}>Edit</button>
                                                            </OverlayTrigger>
                                                        }
                                                        {channelTemplates.webEmail[selectedTemplate.webEmail]?.name_key === "thank-you-email" &&
                                                            (!showTemplateLoader && !channelTemplates.webEmail[selectedTemplate.webEmail]?.default_template) &&
                                                            <OverlayTrigger overlay={<Tooltip>{
                                                                (channelTemplates.webEmail[selectedTemplate.webEmail]?.active === 1) ? 'Bookmarked for Thank You Email communication' : 'Bookmark this template for Thank You Email'
                                                            }</Tooltip>}>
                                                                <button type="button" className={`email-bookmark ${(channelTemplates.webEmail[selectedTemplate.webEmail]?.active === 1) ? 'bookmarked' : ''} `} onClick={handleAddThankTemplate.bind(null, 'webEmail', true)}>Edit</button>
                                                            </OverlayTrigger>
                                                        }

                                                        {(channelTemplates.webEmail[selectedTemplate.webEmail]?.name_key !== "thank-you-email" && channelTemplates.webEmail[selectedTemplate.webEmail]?.name_key !== "invitation-email") &&
                                                            (!showTemplateLoader && !channelTemplates.webEmail[selectedTemplate.webEmail]?.default_template) &&
                                                            <OverlayTrigger overlay={<Tooltip>{
                                                                (channelTemplates.webEmail[selectedTemplate.webEmail]?.active === 1) ? 'Bookmarked Email template' : 'Bookmark this template'
                                                            }</Tooltip>}>
                                                                <button type="button" className={`email-bookmark ${(channelTemplates.webEmail[selectedTemplate.webEmail]?.active === 1) ? 'bookmarked' : ''} `} onClick={handleAddThankTemplate.bind(null, 'webEmail', false)}>Edit</button>
                                                            </OverlayTrigger>
                                                        }

                                                        {(!showTemplateLoader && (!channelTemplates.webEmail[selectedTemplate.webEmail]?.default_template)) && <OverlayTrigger overlay={<Tooltip>Delete Template</Tooltip>}>
                                                            <button type="button" className={`delete-data-icon`} onClick={handleDeleteTemplate.bind(null, 'webEmail')} disabled={showTemplateLoader}></button>
                                                        </OverlayTrigger>}

                                                        {showTemplateLoader && <Spinner animation="border" size="sm" className="burgundy-spinner" />}
                                                    </div>
                                                </div>
                                                <div className="email-preview-content">
                                                    {
                                                        (editMeta && editMeta === 'webEmail') &&
                                                        <div className="placeholder-btn-wrap">
                                                            {
                                                                CHANNEL_PLACEHOLDERS_SUBJECT.map((item) =>
                                                                    <div key={item.id} className="placeholder-btn-inner">
                                                                        <button type="button" className="insert-placeholder-btn" value={item.value} onClick={insertVariable}>{item.label}</button>
                                                                    </div>
                                                                )
                                                            }
                                                        </div>
                                                    }
                                                    <div className="channel-form-wrap">
                                                        <div className="channel-form-field">
                                                            <label>From Name:</label>
                                                            <input type="text" className={`email-preview-input ${editMeta === "webEmail" ? 'email-preview-input-edit' : ''}`} name="from_name" value={channelTemplates.webEmail[selectedTemplate.webEmail]?.from_name || 'QaizenX'} onChange={(e) => handleInputChange(e, 'webEmail')} />
                                                        </div>
                                                        <div className="channel-form-field">
                                                            <label>From Email:</label>
                                                            <input readOnly type="text" className={`email-preview-input ${editMeta === "webEmail" ? '' : ''}`} name="from" value={channelTemplates.webEmail[selectedTemplate.webEmail]?.from || ""} onChange={(e) => handleInputChange(e, 'webEmail')} />
                                                        </div>
                                                        {selectedCount > 0 && <div className="channel-form-field">
                                                            <label>To:</label>
                                                            <input type="text" className={`email-preview-input ${editMeta === "webEmail" ? '' : ''}`} readOnly name="to" value={`selected(${selectedCount})`} />
                                                        </div>}
                                                        <div className="channel-form-field">
                                                            <label>Select by status:</label>
                                                            <Select
                                                                isDisabled={surveyStatus === 'draft'}
                                                                className={`ezi-select-plugin ${(surveyStatus === 'draft') ? 'disabled-cls' : ''}`}
                                                                value={participant_status.email}
                                                                placeholder="Please Select"
                                                                onChange={(selected) => handleChangeStatus('email', selected)}
                                                                options={participantsStatusData}
                                                                styles={outlineRemove}
                                                            />
                                                        </div>
                                                        <div className="channel-form-field">
                                                            <label>Subject:</label>
                                                            <input type="text" ref={inputSubjectRef} data-channel="webEmail" className={`email-preview-input ${editMeta === "webEmail" ? 'email-preview-input-edit' : ''}`} name="subject" value={channelTemplates.webEmail[selectedTemplate.webEmail]?.subject} onFocus={() => setLastFocusInput('email_subject')} onChange={(e) => handleInputChange(e, 'webEmail')} />
                                                        </div>
                                                        <div className="channel-form-field">
                                                            <label>Message:</label>
                                                            <Editor
                                                                wrapperClassName="ezi-editor-wrapper"
                                                                editorClassName="ezi-editor-inner"
                                                                toolbarClassName="ezi-editor-toolbar"
                                                                editorState={editorState}
                                                                toolbar={COMMUNICATION_TOOLBAR_CONFIG}
                                                                onEditorStateChange={onEditorStateChange}
                                                                toolbarCustomButtons={[<DropdownPlaceholder />]}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="schedule-main-wrap">
                                                <div className="schedule-toggle-date">
                                                    <label className="schedule-label">Email Schedule  :</label>
                                                    <div className="schedule-toggle-text">Now<div className={`ezi-switch-toggle ${toggleEmail ? "on" : "off"}`} onClick={handleToggle} />Later
                                                        {toggleEmail && <div className="schedule-date-wrap">
                                                            <DateTimePicker
                                                                onChange={(date) => setStartDateEmail(date)}
                                                                value={startDateEmail}
                                                                clearIcon={null}
                                                                calendarIcon={null}
                                                                minDate={new Date()}
                                                            />
                                                        </div>}
                                                    </div>
                                                    {surveyStatus !== 'draft' &&
                                                        <button onClick={saveSetting} type="button" className="btn-ripple ezi-pink-btn communicate_btn">Send</button>
                                                    }
                                                </div>
                                            </div>

                                        </React.Fragment>
                                    }
                                </Tab.Pane>
                                <Tab.Pane eventKey="webSms">
                                    <div>
                                        <select className="channel-select-mail" onChange={(e) => changeTemplate(e, 'webSms')} value={selectedTemplate.webSms}>
                                            <option value="" key="0">Select SMS Template </option>
                                            {
                                                channelTemplates.webSms && Object.keys(channelTemplates.webSms).map(key =>
                                                    <option value={key} key={key} data-name={channelTemplates.webSms[key]['name']}>{channelTemplates.webSms[key]['name']}</option>
                                                )
                                            }
                                        </select>
                                    </div>
                                    {
                                        (selectedTemplate.webSms !== '') &&
                                        <React.Fragment>
                                            <div className="email-preview-wrap">
                                                <div className="email-preview-header">
                                                    <span className="email-preview-heading">
                                                        SMS Preview
                                                    </span>
                                                    {/* <div className="email-preview-right-header">
                                                        <button type="button" className="email-preview-save" onClick={() => setShowPrompt(true)}>Save As New</button>
                                                        <button type="button" className="email-preview-save" onClick={handleUpdateTemplate.bind(null, 'webSms')} disabled={(channelTemplates?.webSms[selectedTemplate.webSms]?.default_template) ? true : false}>Update Selected</button>
                                                        <button type="button" className="email-preview-edit" onClick={() => setEditMeta('webSms')} disabled={(channelTemplates?.webSms[selectedTemplate.webSms]?.default_template) ? true : false}>Edit</button>
                                                        {(!showTemplateLoader && surveyType !== 'openSurvey') && <OverlayTrigger overlay={<Tooltip>{
                                                            (channelTemplates.webSms[selectedTemplate.webSms]?.is_api_template) ? 'Bookmarked for API Communication Template' : 'Bookmark this template for API Communications'
                                                        }</Tooltip>}>
                                                            <button type="button" className={`email-bookmark ${(channelTemplates.webSms[selectedTemplate.webSms]?.is_api_template) ? 'bookmarked' : ''} `} onClick={handleBookMarkTemplate.bind(null, 'webSms')} disabled={showTemplateLoader}>Edit</button>
                                                        </OverlayTrigger>}

                                                        {(!showTemplateLoader && !channelTemplates.webSms[selectedTemplate.webSms]?.default_template) && <OverlayTrigger overlay={<Tooltip>Delete Template</Tooltip>}>
                                                            <button type="button" className={`delete-data-icon`} onClick={handleDeleteTemplate.bind(null, 'webSms')} disabled={showTemplateLoader}></button>
                                                        </OverlayTrigger>}

                                                        {showTemplateLoader && <Spinner animation="border" size="sm" className="burgundy-spinner" />}
                                                    </div> */}
                                                </div>
                                                <div className="email-preview-content">
                                                    {
                                                        (editMeta && editMeta === 'webSms') &&
                                                        <div className="placeholder-btn-wrap">
                                                            {
                                                                CHANNEL_PLACEHOLDERS_SMS.map((item) =>
                                                                    <div key={item.id} className="placeholder-btn-inner">
                                                                        <button type="button" className="insert-placeholder-btn" value={item.value} onClick={insertVariable}>{item.label}</button>
                                                                    </div>
                                                                )
                                                            }
                                                        </div>
                                                    }
                                                    <div className="channel-form-wrap">
                                                        {selectedCount > 0 && <div className="channel-form-field">
                                                            <label>To:</label>
                                                            <input type="text" className={`email-preview-input`} readOnly name="to" value={`selected(${selectedCount})`} />
                                                        </div>}

                                                        <div className="channel-form-field">
                                                            <label>Select by status:</label>
                                                            <Select
                                                                isDisabled={surveyStatus === 'draft'}
                                                                placeholder="Please Select"
                                                                className="ezi-select-plugin"
                                                                value={participant_status.sms}
                                                                onChange={(selected) => handleChangeStatus('sms', selected)}
                                                                options={participantsStatusData}
                                                                styles={outlineRemove}
                                                            />
                                                        </div>

                                                        <div className="channel-form-field">
                                                            <label>Message:</label>
                                                            <textarea maxLength="1500" className={`email-preview-textarea  ${(editMeta === "webSms" && !channelTemplates?.webSms[selectedTemplate.webSms]?.default_template) ? 'email-preview-input-edit' : ''}`} name="message" rows="4" cols="50" onChange={(e) => handleInputChange(e, 'webSms')} value={channelTemplates.webSms[selectedTemplate.webSms]?.message || ""} ref={inputMessageRef} data-channel="webSms" onFocus={() => setLastFocusInput('sms')} />
                                                        </div>
                                                    </div>
                                                    <span className="email-preview-length">{(channelTemplates.webSms[selectedTemplate.webSms]?.message) ? channelTemplates.webSms[selectedTemplate.webSms]?.message.length : 0}/1500</span>
                                                </div>
                                            </div>
                                            <div className="schedule-main-wrap">

                                                <div className="schedule-toggle-date">
                                                    <label className="schedule-label">SMS Schedule :</label>
                                                    <div className="schedule-toggle-text">Now<div className={`ezi-switch-toggle ${toggleSms ? "on" : "off"}`} onClick={handleToggle} />Later
                                                        {toggleSms && <div className="schedule-date-wrap">
                                                            <DateTimePicker
                                                                onChange={(date) => setStartDateSMS(date)}
                                                                value={startDateSMS}
                                                                clearIcon={null}
                                                                calendarIcon={null}
                                                                minDate={new Date()}
                                                            />
                                                        </div>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </React.Fragment>
                                    }
                                </Tab.Pane>
                                <Tab.Pane eventKey="whatsApp">
                                    <div>
                                        <select className="channel-select-mail" onChange={(e) => changeTemplate(e, 'whatsApp')} value={selectedTemplate.whatsApp}>
                                            <option value="" key="0">Select Message </option>
                                            {
                                                channelTemplates.whatsApp && Object.keys(channelTemplates.whatsApp).map(key =>
                                                    <option value={key} key={key} data-name={channelTemplates.whatsApp[key]['name']}>{channelTemplates.whatsApp[key]['name']}</option>
                                                )
                                            }
                                        </select>
                                    </div>
                                    {
                                        (selectedTemplate.whatsApp !== '') &&
                                        <React.Fragment>
                                            <div className="email-preview-wrap">
                                                <div className="email-preview-header">
                                                    <span className="email-preview-heading">
                                                        Message Preview
                                                    </span>
                                                    {/* <div className="email-preview-right-header">
                                                        <button type="button" className="email-preview-save" onClick={() => setShowPrompt(true)}>Save As New</button>
                                                        <button type="button" className="email-preview-save" onClick={handleUpdateTemplate.bind(null, 'whatsApp')} disabled={(channelTemplates?.whatsApp[selectedTemplate.whatsApp]?.default_template) ? true : false}>Update Selected</button>
                                                        <button type="button" className="email-preview-edit" onClick={() => setEditMeta('whatsApp')} disabled={(channelTemplates?.whatsApp[selectedTemplate.whatsApp]?.default_template) ? true : false}>Edit</button>
                                                        {(!showTemplateLoader && surveyType !== 'openSurvey') && <OverlayTrigger overlay={<Tooltip>{
                                                            (channelTemplates.whatsApp[selectedTemplate.whatsApp]?.is_api_template) ? 'Bookmarked for API Communication Template' : 'Bookmark this template for API Communications'
                                                        }</Tooltip>}>
                                                            <button type="button" className={`email-bookmark ${(channelTemplates.whatsApp[selectedTemplate.whatsApp]?.is_api_template) ? 'bookmarked' : ''} `} onClick={handleBookMarkTemplate.bind(null, 'whatsApp')} disabled={showTemplateLoader}>Edit</button>
                                                        </OverlayTrigger>}

                                                        {(!showTemplateLoader && !channelTemplates.whatsApp[selectedTemplate.whatsApp]?.default_template) && <OverlayTrigger overlay={<Tooltip>Delete Template</Tooltip>}>
                                                            <button type="button" className={`delete-data-icon`} onClick={handleDeleteTemplate.bind(null, 'whatsApp')} disabled={showTemplateLoader}></button>
                                                        </OverlayTrigger>}

                                                        {showTemplateLoader && <Spinner animation="border" size="sm" className="burgundy-spinner" />}
                                                    </div> */}
                                                </div>
                                                <div className="email-preview-content">
                                                    {
                                                        (editMeta && editMeta === 'whatsApp') &&
                                                        <div className="placeholder-btn-wrap">
                                                            {
                                                                CHANNEL_PLACEHOLDERS_SMS.map((item) =>
                                                                    <div key={item.id} className="placeholder-btn-inner">
                                                                        <button type="button" className="insert-placeholder-btn" value={item.value} onClick={insertVariable}>{item.label}</button>
                                                                    </div>
                                                                )
                                                            }
                                                        </div>
                                                    }
                                                    <div className="channel-form-wrap">
                                                        {selectedCount > 0 && <div className="channel-form-field">
                                                            <label>To:</label>
                                                            <input type="text" className={`email-preview-input`} readOnly name="to" value={`selected(${selectedCount})`} />
                                                        </div>}

                                                        <div className="channel-form-field">
                                                            <label>Select by status:</label>
                                                            <Select
                                                                isDisabled={surveyStatus === 'draft'}
                                                                placeholder="Please Select"
                                                                className="ezi-select-plugin"
                                                                value={participant_status.whats_app}
                                                                onChange={(selected) => handleChangeStatus('whats_app', selected)}
                                                                options={participantsStatusData}
                                                                styles={outlineRemove}
                                                            />
                                                        </div>

                                                        <div className="channel-form-field">
                                                            <label>Message:</label>
                                                            <textarea maxLength="1500" className={`email-preview-textarea  ${(editMeta === "whatsApp" && !channelTemplates?.whatsApp[selectedTemplate.whatsApp]?.default_template) ? 'email-preview-input-edit' : ''}`} name="message" rows="4" cols="50" onChange={(e) => handleInputChange(e, 'whatsApp')} value={channelTemplates.whatsApp[selectedTemplate.whatsApp]?.message || ""} ref={inputWhatsAppRef} data-channel="whatsApp" onFocus={() => setLastFocusInput('whats_app')} />
                                                        </div>
                                                    </div>
                                                    <span className="email-preview-length">{(channelTemplates.whatsApp[selectedTemplate.whatsApp]?.message) ? channelTemplates.whatsApp[selectedTemplate.whatsApp]?.message.length : 0}/1500</span>
                                                </div>
                                            </div>
                                            <div className="schedule-main-wrap">

                                                <div className="schedule-toggle-date">
                                                    <label className="schedule-label">Message Schedule :</label>
                                                    <div className="schedule-toggle-text">Now<div className={`ezi-switch-toggle ${toggleWhatsApp ? "on" : "off"}`} onClick={handleToggle} />Later
                                                        {toggleWhatsApp && <div className="schedule-date-wrap">
                                                            <DateTimePicker
                                                                onChange={(date) => setStartDateWhatsApp(date)}
                                                                value={startDateWhatsApp}
                                                                clearIcon={null}
                                                                calendarIcon={null}
                                                                minDate={new Date()}
                                                            />
                                                        </div>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </React.Fragment>
                                    }
                                </Tab.Pane>
                                <Tab.Pane eventKey="webEmbed">
                                    <div className="email-preview-wrap">
                                        <div className="email-preview-content">
                                            <div className="channel-form-wrap">
                                                <p>Please Keep copy of below highlighted code and paste it on target website to embed survey</p>
                                                <div className="channel-form-field">
                                                    <code>
                                                        {(channelTemplates.webEmbed && channelTemplates.webEmbed['genrated_link']) ? `<script type='text/javascript'>window.onload = function() {var iframe = document.createElement('iframe');iframe.style.display = "block";iframe.style.width = "500";
                                                        iframe.style.height = "400";iframe.src = "${channelTemplates.webEmbed['genrated_link']}";document.body.appendChild(iframe);}</script>` : 'Web Embed Code Not Exists'}
                                                    </code>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Tab.Pane>
                                <Tab.Pane eventKey="webLink">
                                    <div className="copy_survey_link" >
                                        {surveyType === 'openSurvey' &&
                                            <>
                                                <p>Copy Survey Link</p>
                                                <CopyToClipboard text={getWebLinks()} onCopy={() => {
                                                    setLinkCopied(true);
                                                    setTimeout(() => setLinkCopied(false), 2000);
                                                }}>
                                                    <button className="copy_clipboard" title="Copy Survey Link">Copy</button>
                                                </CopyToClipboard>
                                            </>
                                        }
                                        {linkCopied && <span className="copy-alert-msg">Survey Link Copied.</span>}
                                    </div>
                                </Tab.Pane>
                                <Tab.Pane eventKey="qrCode">
                                    <div className="qr-preview-wrap">
                                        <div className="qr-preview-content">
                                            <div className="qr-box">
                                                {(surveyStatus === 'draft') && <input type="text" name="upperText" placeholder="Enter Upper Text" className="qr-input" onChange={handleQrTextChange} value={qrCodeText} />}
                                                <div className="qr-inner-box">
                                                    {qrCodeHtml && JsxParser(qrCodeHtml)}
                                                </div>
                                                {(surveyStatus !== 'draft' && qrCodeDownloadLink) &&
                                                    <div className="qr-code-download-button">
                                                        <a className="btn-ripple ezi-pink-btn inner_save_btn" href={qrCodeDownloadLink} download="qrcode.png">Download QR code</a>
                                                    </div>
                                                }
                                                {/* {surveyStatus !== 'draft' &&
                                                    <div className="qr_download_section">
                                                        <select name="size" ref={select => imageSelectRef = select}>
                                                            <option value="">Please select size</option>
                                                            <option value="4.5X6">4.5 inch X 6 inch-Table tent stand</option>
                                                            <option value="5.8X8.3">5.8 inch X 8.3 inch-Hanging Flyer</option>
                                                            <option value="8.5X11"> 8 inch X 8 inch-Hanging Flyer</option>
                                                            <option value="8X8">8.5 inch X 11 inch-Hanging Flyer</option>
                                                        </select>
                                                        <button type="button" onClick={handleDownloadQr}>View Qr Code</button>
                                                    </div>
                                                } */}
                                            </div>
                                        </div>
                                    </div>
                                </Tab.Pane>
                            </Tab.Content>
                        }
                    </Tab.Container>
                </div>
            </div>
        </div>
    )
}
const mapStateToProps = state => {

    return {
        surveyAction: state.survey.action,
        surveyStatus: state.survey.status,
        selectedCount: state.participant.selectedCount,
        channelTypes: state.participant.channelTypes,
        surveyType: state.survey.surveyType,
        mappedIdentifier: state.participant.mappedIdentifier,
        participant_status: state.participant.participant_status,
        thankEmail: state.survey.thankEmail,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        dispatchSelectedCount: (count = 0) => dispatch(AppAction.updateSelectedCount({ count })),
        dispatchSelectedOnEdit: (data = []) => dispatch(AppAction.setSelectedOnEdit({ participants: data })),
        dispatchCommunicateByStatus: (data = []) => dispatch(AppAction.setSelectByStatus({ participant_status: data })),
        dispatchChannels: (channels = {}) => dispatch(AppAction.updateChannelData({ channels })),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CommunicationChannel);