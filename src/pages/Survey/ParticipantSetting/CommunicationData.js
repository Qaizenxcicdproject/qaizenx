import React from "react";
import { Tab, Nav } from 'react-bootstrap';
import History from "./Communication/History"
import Schedules from "./Communication/Schedules"

const CommunicationData = () => (
    <div className="participant-list-container" style={{ width: "100%" }}>
        <Tab.Container defaultActiveKey="schedules">
            <div className="survey-button-tab-header">
                <div className="tab-left-header">
                    <Nav variant="pills" >
                        <Nav.Item>
                            <Nav.Link eventKey="schedules">Schedules</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link eventKey="history">History</Nav.Link>
                        </Nav.Item>
                    </Nav>
                </div>
            </div>
            <Tab.Content className="survey-button-tab-content">
                <Tab.Pane eventKey="schedules" mountOnEnter unmountOnExit>
                    <Schedules />
                </Tab.Pane>
                <Tab.Pane eventKey="history" mountOnEnter unmountOnExit>
                    <History />
                </Tab.Pane>
            </Tab.Content>
        </Tab.Container>
    </div>
)

export default CommunicationData;