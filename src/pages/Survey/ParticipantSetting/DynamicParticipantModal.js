import React, { useEffect, useState, Fragment } from "react";
import { Modal, Spinner } from 'react-bootstrap';
import useForm from 'react-hook-form';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';
import { useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import * as AppAction from "store/actions";
import FormData from 'utility/AppFormData';

function DynamicParticipantModal({ dispatchParticipantSourceType, surveyAction, ...modelProps }) {
    const { register, handleSubmit, errors, setError, clearError, setValue } = useForm();
    const [participantIdentifier, setParticipantIdentifier] = useState([])
    const [loading, setLoading] = useState(false)
    const [savingMsg, setSavingMsg] = useState(null)
    const [saving, setSaving] = useState(false)
    const urlParams = useParams()
    const onSubmit = data => {
        if (data.email === "" && data.contact_number === "") {
            setError("email", "required");
            setError("contact_number", "required");
            return;
        }

        setSaving(true)
        let formData = new FormData()
        formData.append('survey_id', urlParams.survey_id)
        formData.append("isedit", (surveyAction === "edit") ? true : false);
        formData.append("participant_data", JSON.stringify(data));

        Axios.post(configURL.savedynamicParticipant, formData).then(response => {
            setSaving(false)
            if (response.data.success) {
                dispatchParticipantSourceType('single')
                modelProps.onHide()
                toast.success(response.data.message || 'Participant data saved.')
            } else {
                setSavingMsg(response.data.message || 'Something went wrong')
                setTimeout(() => setSavingMsg(null), 8000);
            }
        }).catch(err => {
            console.log(err.response);
            setSavingMsg('Something went wrong')
            setSaving(false)
        })
    };
    useEffect(() => {
        if (modelProps.show) {
            setSavingMsg(null)
            setLoading(true)
            let formData = new FormData()
            formData.append('survey_id', urlParams.survey_id)
            Axios.post(configURL.dynamicParticipantIdentifier, formData).then(response => {
                setLoading(false)
                if (response.data.success) {
                    setParticipantIdentifier(response.data.results)
                }
            }).catch(err => {
                console.log(err.response);
                setLoading(false)
            })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [modelProps.show])
    return (

        <Modal {...modelProps} size="md" aria-labelledby="contained-modal-title-vcenter" centered className="theme-modal-wrapper" >
            <Modal.Header className="ezi-modal-header">
                <Modal.Title id="contained-modal-title-vcenter" className="theme-modal-title ezi-modal-header-title" >
                    <span className="theme-modal-title-text">Add New Dynamic Participant</span>
                    <span className="ezi-modal-close" onClick={modelProps.onHide}></span>
                </Modal.Title>
            </Modal.Header>

            <Modal.Body>
                {!loading ? <form onSubmit={handleSubmit(onSubmit)} className="add-theme-form">
                    <div className="add_participant_notes">
                        <p>Either email id or contact number is mandatory</p>
                    </div>
                    {participantIdentifier.length > 0 && participantIdentifier.map((item, i) => (
                        <div key={i} className="theme-field-wrapper">
                            <div className="theme-field-50">
                                <div className="theme-field">
                                    {(() => {
                                        switch (item.type) {
                                            case "personal":
                                                if (item.name === 'fname') {
                                                    return (
                                                        <Fragment>
                                                            <input type="text" className="theme-field-control" name={item.name} ref={register({ required: true })} placeholder={item.label || ''} />
                                                            {errors[item.name] && <span className="theme-error_cu">{item.label} is required.</span>}
                                                        </Fragment>
                                                    );
                                                }
                                                else if (item.name === 'email') {
                                                    return (
                                                        <Fragment>
                                                            <input
                                                                type="text"
                                                                className="theme-field-control"
                                                                name={item.name}
                                                                ref={register({
                                                                    // eslint-disable-next-line
                                                                    pattern: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/i
                                                                })}
                                                                placeholder={item.label || ''}
                                                                onChange={({ target }) => {
                                                                    setValue("email", target.value.trim(), { shouldValidate: true })
                                                                    if (target.value.trim() !== "") {
                                                                        clearError('contact_number')
                                                                    }
                                                                }}
                                                            />
                                                            {(errors[item.name] && errors[item.name].type === "required") && <span className="theme-error_cu">please enter either email or contact number.</span>}
                                                            {(errors[item.name] && errors[item.name].type === "pattern") && <span className="theme-error_cu">{item.label} is not valid.</span>}
                                                        </Fragment>
                                                    );
                                                }
                                                else if (item.name === 'contact_number') {
                                                    return (
                                                        <Fragment>
                                                            <input
                                                                type="text"
                                                                className="theme-field-control"
                                                                name="contact_number"
                                                                ref={register({
                                                                    // eslint-disable-next-line
                                                                    pattern: /^[0-9]*$/,
                                                                    minLength: 7,
                                                                    maxLength: 14
                                                                })}
                                                                placeholder={item.label || ''}
                                                                onChange={({ target }) => {
                                                                    setValue("contact_number", target.value.trim(), { shouldValidate: true })
                                                                    if (target.value.trim() !== "") {
                                                                        clearError('email')
                                                                    }
                                                                }}
                                                            />
                                                            {(errors[item.name] && errors[item.name].type === "required") && <span className="theme-error_cu">please enter either email or contact number.</span>}
                                                            {(errors.contact_number && (errors.contact_number.type === "pattern" || errors.contact_number.type === "minLength" || errors.contact_number.type === "maxLength")) && <span className="theme-error_cu">{item.label} is not valid.</span>}
                                                        </Fragment>
                                                    );
                                                }
                                                else {
                                                    return (
                                                        <Fragment>
                                                            <input type="text" className="theme-field-control" name={item.name} ref={register} placeholder={item.label || ''} />
                                                        </Fragment>
                                                    );
                                                }
                                            case "identifier":
                                                return (
                                                    <Fragment>
                                                        <input className="theme-field-control" name={item.name} ref={register} placeholder={`Enter ${item.label || ''} or select by dropdown`} list={item.name} onChange={({ target }) => setValue(item.name, target.value.trim())} />
                                                        <datalist id={item.name}>
                                                            {item.options.length > 0 && item.options.map(el => <option key={el} value={el} />)}
                                                        </datalist>
                                                    </Fragment>
                                                );
                                            default:
                                                return null
                                        }
                                    })()}
                                </div>
                            </div>
                        </div>
                    ))}
                    <div className="theme-modal-footer">
                        {savingMsg && <p className="dynamic_participant_msg">{savingMsg}</p>}
                        <button type="button" className="close-theme-btn" onClick={modelProps.onHide}>Close</button>
                        <button type="submit" className="btn-ripple ezi-pink-btn add-theme-btn" disabled={saving}>
                            Save Participant {saving && <Spinner animation="border" size="sm" />}
                        </button>
                    </div>
                </form> : <p className="dynamic_participant_loading">Loading Information...</p>
                }
            </Modal.Body >
        </Modal >
    );
}

const mapStateToProps = (state) => {
    return {
        surveyAction: state.survey.action
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatchParticipantSourceType: (type) => dispatch(AppAction.setDynamicParticipantSourceType({ type }))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(DynamicParticipantModal)