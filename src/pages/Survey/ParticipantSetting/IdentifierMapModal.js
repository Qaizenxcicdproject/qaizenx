import React, { useEffect, useState, Fragment } from "react";
import { Modal } from 'react-bootstrap';
import configURL from "config/config";
import { useParams } from "react-router-dom";
import { toast } from 'react-toastify';
import Axios from "utility/Axios";
import FormData from 'utility/AppFormData';

const IdentifierMapModal = ({ isShown, hideModel, submitMapping, data }) => {
    const urlParam = useParams();
    const [questions, setQuestions] = useState([]);
    const [dataLoading, setDataLoading] = useState(true);

    useEffect(() => {
        if (isShown) {
            setDataLoading(true)
            let formData = new FormData();
            formData.append("survey_id", urlParam.survey_id);
            formData.append("type", data.identifier_type);
            Axios.post(configURL.identifier_questions, formData).then(res => {
                setDataLoading(false)
                if (res.data.success !== undefined && res.data.success) {
                    setQuestions(res.data.questions)
                } else {
                    toast.warn(res.data.message);
                }
            }).catch(err => {
                console.log(err);
                setDataLoading(false)
            })
        }
        return () => toast.dismiss()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isShown])

    return (
        <Modal show={isShown} onHide={hideModel} size="md" centered className="theme-modal-wrapper checklist-modal" >
            <Modal.Header className="ezi-modal-header">
                <Modal.Title className="theme-modal-title ezi-modal-header-title" >
                    <span className="theme-modal-title-text">Survey Questions Mapping</span>
                    <span className="ezi-modal-close" onClick={hideModel}></span>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {!dataLoading && <Fragment>
                    <div className="survey-inner-tab-content">
                        {questions.map((item, index) => (
                            <div className="checklist-label-wrap" key={item.id}>
                                <label className="checklist-label">{(index + 1)}. {item.title}</label>
                                <button className="map_identifier_question" onClick={() => submitMapping(item)}>Map</button>
                            </div>
                        ))}
                        {questions.length === 0 && <p>questions not available</p>}
                    </div>
                </Fragment>}
                {dataLoading && <div style={{ textAlign: "center" }}><h4 >Loading...</h4></div>}
            </Modal.Body>
        </Modal>
    );
}

export default IdentifierMapModal