import React, { useState, useContext, useEffect } from "react";
import { Tab, Nav } from "react-bootstrap";
import Axios from "utility/Axios";
import { useParams } from "react-router-dom";
import ParticipantSourceTab from "./ParticipantSourceTab";
import configURL from "config/config";
import { toast } from "react-toastify";
import SavedParticipants from "./SavedParticipants";
import CommunicationData from "./CommunicationData";
import AppContext from "store/AppContext";
import CommunicationChannel from "./CommunicationChannel";
import { connect } from "react-redux";
import ParticipantTabHeader from "./ParticipantTabHeader";
import * as AppAction from "store/actions";
import SurveyIdentifiers from "./SurveyIdentifiers";
import FormData from "utility/AppFormData";

const ParticipantSetting = (props) => {
    const {
        surveyAction,
        surveyStatus,
        selectedParticipant,
        participantSource,
        selectedOnEdit,
        channelTemplate,
        dispatchSelectedOnEdit,
        dispatchSelectedCount,
        dynamicSourceFile,
        dynamicParticipantSourceType,
        surveyType,
        mappedIdentifier,
        participant_status,
        dispatchReloadChannel,
        dispatchParticipantSourceType,
        dispatchDynamicSourceFile,
        participantActiveTab,
    } = props;
    const { EziLoader } = useContext(AppContext);
    const [tabKey, setTabKey] = useState("survey_identifiers");
    const urlParams = useParams();
    const [surveySetting] = useState({});

    /**
     * Save Communication channel data
     */
    const saveChannelData = () => {
        let formData = new FormData();
        if(Object.keys(channelTemplate).length === 0){
            EziLoader.hide();
            toast.warn("Please select any communication channel.");
            return;
        }
        formData.append("survey_id", urlParams.survey_id);
        formData.append("template_json", JSON.stringify(channelTemplate));
        formData.append("isedit", surveyAction === "edit" ? true : false);
        Axios.post(configURL.saveChannelTemplates, formData)
            .then((res) => {
                if (res.data.success !== undefined && res.data.success) {
                    toast.info(res.data.message || "Survey Setting added.");
                    dispatchReloadChannel(participantSource);
                } else {
                    toast.warn(res.data.message || "Something went wrong.");
                }
            })
            .catch((err) => {
                console.log(err);
            });
    };

    /**
     * Save Participant Data.
     */
    const saveParticipentData = () => {
        if (!participantSource || participantSource === "") {
            toast.warn("Please choose participant source");
            return;
        }
        if (
            participantSource === "upload_participant" &&
            dynamicParticipantSourceType === "upload" &&
            !dynamicSourceFile
        ) {
            toast.warn("Please upload dynamic source data.");
            return;
        }
        if (
            participantSource === "upload_participant" &&
            dynamicParticipantSourceType === ""
        ) {
            toast.warn(
                "Please upload or add single dynamic participants data."
            );
            return;
        }
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", urlParams.survey_id);
        formData.append("people_ids", JSON.stringify(selectedParticipant));
        formData.append("survey_setting", JSON.stringify(surveySetting));
        formData.append("isedit", surveyAction === "edit" ? true : false);
        formData.append("participant_source", participantSource);

        if (participantSource === "upload_participant") {
            formData.append(
                "upload_participant_source_type",
                dynamicParticipantSourceType
            );
        }
        if (
            participantSource === "upload_participant" &&
            dynamicParticipantSourceType === "upload"
        ) {
            formData.append("upload_participant_data", dynamicSourceFile);
        }

        Axios.post(configURL.addParticipantsToSurvey, formData)
            .then((res) => {
                EziLoader.hide();
                if (res.data.success !== undefined && res.data.success) {
                    toast.info(res.data.message || "Setting Saved");
                    if (
                        participantSource === "upload_participant" &&
                        dynamicParticipantSourceType === "upload"
                    ) {
                        dispatchParticipantSourceType("");
                        dispatchDynamicSourceFile(null);
                        setTabKey("survey_participants");
                    }
                    if (res.data.warnings && res.data.warnings.length > 0) {
                        res.data.warnings.forEach((element) => {
                            toast.warn(element);
                        });
                    }
                } else {
                    toast.warn(res.data.message || "Something went wrong.");
                }
            })
            .catch((err) => {
                EziLoader.hide();
                console.log(err);
            });
    };

    /**
     * Save Participant data on Edit survey
     */
    const handleCommunicate = () => {
        EziLoader.show();
        let formData = new FormData();
        let participantByStatus = {};
        if (participant_status.email && participant_status.email.value !== "") {
            participantByStatus["email"] = participant_status.email.value;
        }
        if (participant_status.sms && participant_status.sms.value !== "") {
            participantByStatus["sms"] = participant_status.sms.value;
        }
        if (
            participant_status.whats_app &&
            participant_status.whats_app.value !== ""
        ) {
            participantByStatus["whats_app"] =
                participant_status.whats_app.value;
        }
        if(Object.keys(channelTemplate).length === 0){
            EziLoader.hide();
            toast.warn("Please select any communication channel.");
            return;
        }
        formData.append("survey_id", urlParams.survey_id);
        if (selectedOnEdit.length > 0) {
            formData.append("participants_id", JSON.stringify(selectedOnEdit));
        }
        if (Object.keys(participantByStatus).length > 0) {
            formData.append(
                "participant_status",
                JSON.stringify(participantByStatus)
            );
        }
        formData.append("channel_template", JSON.stringify(channelTemplate));
        formData.append("isedit", surveyAction === "edit" ? true : false);

        Axios.post(configURL.save_communicate_api, formData)
            .then((res) => {
                EziLoader.hide();
                if (res.data.success !== undefined && res.data.success) {
                    dispatchSelectedOnEdit([]);
                    dispatchSelectedCount(0);
                    toast.info(res.data.message || "Participant saved !!");
                    /** Move tab cursor to communication history */
                    setTabKey("communication_history");
                } else {
                    toast.warn(res.data.message);
                }
            })
            .catch((err) => {
                EziLoader.hide();
            });
    };

    /**
     * Save Participant data on Edit survey
     */
    const handleSaveIdentifierMap = () => {
        EziLoader.show();
        let formData = new FormData();
        formData.append("survey_id", urlParams.survey_id);
        formData.append("mapping", JSON.stringify(mappedIdentifier));
        Axios.post(configURL.save_identifier_mapping, formData)
            .then((res) => {
                EziLoader.hide();
                if (res.data.success !== undefined && res.data.success) {
                    toast.info(
                        res.data.message || "Identifier mappings saved !!"
                    );
                } else {
                    toast.warn(res.data.message);
                }
            })
            .catch((err) => {
                EziLoader.hide();
            });
    };

    /**
     * Save Participant data
     */
    const handleSaveData = () => {
        if (
            surveyAction === "edit" &&
            surveyStatus !== "draft" &&
            tabKey === "communication_channels"
        ) {
            handleCommunicate();
        } else {
            switch (tabKey) {
                case "communication_channels":
                    saveChannelData();
                    break;
                case "participant_source":
                    saveParticipentData();
                    break;
                case "survey_identifiers":
                    handleSaveIdentifierMap();
                    break;
                default:
                    break;
            }
        }
    };

    useEffect(() => toast.dismiss(), []);
    useEffect(() => {
        if (participantActiveTab === 'ACTIVE') {
            setTabKey("participant_source");
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="tablist_ezi survey-inner-tab-container">
            <Tab.Container activeKey={tabKey} onSelect={(k) => setTabKey(k)}>
                <div className="survey-inner-tab-header-wrap">
                    <div className="tab-left-header">
                        <Nav variant="pills">
                            <Nav.Item>
                                <Nav.Link eventKey="survey_identifiers">
                                    Survey Identifiers
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="participant_source">
                                    Add Participant
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="survey_participants">
                                    Survey Participants
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="communication_channels">
                                    Communication Channels
                                </Nav.Link>
                            </Nav.Item>
                            {surveyType !== "openSurvey" && (
                                <Nav.Item>
                                    <Nav.Link eventKey="communication_history">
                                        Communication Data
                                    </Nav.Link>
                                </Nav.Item>
                            )}
                        </Nav>
                    </div>
                    <div className="tab-right-header">
                        <ParticipantTabHeader
                            saveData={handleSaveData}
                            communicate={() =>
                                setTabKey("communication_channels")
                            }
                            activeTab={tabKey}
                        />
                    </div>
                </div>
                <Tab.Content className="survey-inner-tab-content">
                    <Tab.Pane eventKey="participant_source" mountOnEnter>
                        <ParticipantSourceTab saveSetting={handleSaveData} participantActiveTab={participantActiveTab} />
                    </Tab.Pane>
                    <Tab.Pane
                        eventKey="survey_identifiers"
                        mountOnEnter
                        unmountOnExit
                    >
                        <SurveyIdentifiers />
                    </Tab.Pane>
                    <Tab.Pane
                        eventKey="survey_participants"
                        mountOnEnter
                        unmountOnExit
                    >
                        <SavedParticipants saveSetting={handleSaveData} />
                    </Tab.Pane>
                    <Tab.Pane eventKey="communication_channels" mountOnEnter>
                        <CommunicationChannel saveSetting={handleSaveData} />
                    </Tab.Pane>
                    <Tab.Pane
                        eventKey="communication_history"
                        mountOnEnter
                        unmountOnExit
                    >
                        {surveyType !== "openSurvey" && <CommunicationData />}
                    </Tab.Pane>
                </Tab.Content>
            </Tab.Container>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        surveyAction: state.survey.action,
        surveyStatus: state.survey.status,
        surveyType: state.survey.surveyType,
        selectedParticipant: state.participant.addedParticipant,
        participantSource: state.participant.participantSource,
        selectedOnEdit: state.participant.selectedOnEdit,
        channelTemplate: state.participant.channelTemplate,
        dynamicSourceFile: state.participant.dynamicSourceFile,
        mappedIdentifier: state.participant.mappedIdentifier,
        dynamicParticipantSourceType:
            state.participant.dynamicParticipantSourceType,
        participant_status: state.participant.participant_status
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        dispatchSelectedOnEdit: (data = []) => dispatch(AppAction.setSelectedOnEdit({ participants: data })),
        dispatchSelectedCount: (count = 0) =>
            dispatch(AppAction.updateSelectedCount({ count })),
        dispatchReloadChannel: (type) =>
            dispatch(AppAction.reloadChannels({ type })),
        dispatchParticipantSourceType: (type) =>
            dispatch(AppAction.setDynamicParticipantSourceType({ type })),
        dispatchDynamicSourceFile: (file) =>
            dispatch(AppAction.setDynamicSourceFile({ file })),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ParticipantSetting);
