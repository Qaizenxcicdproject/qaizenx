import React, { Fragment } from 'react';
import { connect } from 'react-redux';

const ParticipantTabHeader = ({ saveData, activeTab, communicate }) => {
    const btnDetails = {
        survey_identifiers: { title: 'Save', showAction: true, showReset: true, handler: saveData },
        participant_source: { title: 'Save', showAction: true, showReset: true, handler: saveData },
        survey_participants: { title: 'Communicate', showAction: true, showReset: false, handler: communicate },
        communication_channels: { title: 'Save & Send', showAction: true, showReset: true, handler: saveData },
        communication_history: { title: '', showAction: false, showReset: false, handler: () => { } }
    }
    const { showAction = false, showReset = false, handler, title } = btnDetails[activeTab] || {}
    return (
        <Fragment>
            {showAction && <button type="button" onClick={handler} className="btn-ripple ezi-pink-btn inner_save_btn">{title}</button>}
            {showReset && <button type="button" className="btn-ripple inner_reset_btn">Reset</button>}
        </Fragment>
    )
}
const mapStateToProps = state => {
    return {
        surveyAction: state.survey.action,
        surveyStatus: state.survey.status
    }
}

export default connect(mapStateToProps)(ParticipantTabHeader);
