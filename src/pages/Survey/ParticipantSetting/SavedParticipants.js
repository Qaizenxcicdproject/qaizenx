import React, { useState, useEffect, useRef, useContext } from 'react';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { useParams } from "react-router-dom";
import { toast } from 'react-toastify';
import SweetSearch from 'components/SweetSearch';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import DemographicFilter from 'components/DemographicFilter';
import { confirmAlert } from 'react-confirm-alert';
import { connect } from 'react-redux';
import AppContext from 'store/AppContext';
import * as AppAction from "store/actions";
import { getTablePagesSizes } from 'utility/helper';
import FormData from 'utility/AppFormData';
import cellEditFactory from 'react-bootstrap-table2-editor';

const SavedParticipants = ({ surveyStatus, surveyAction, selectedOnEdit, dispatchSelectedOnEdit, dispatchSelectedCount, dispatchCommunicateByStatus }) => {
    const [participantFilter, setParticipantFilter] = useState(false);
    const { EziLoader } = useContext(AppContext)
    const [searchLoading, setSearchLoading] = useState(false);
    const [dataLoaded, setDataLoaded] = useState(false);
    const [participants, setParticipants] = useState([]);
    const [dataColumn, setDataColumn] = useState([{}]);
    const [pagination, setPagination] = useState({});
    const [uniqueKeyField, setUniqueKeyField] = useState("");
    const i18Lang = localStorage.getItem("i18nextLng");
    const [filters, setFilters] = useState([]);
    let inputSearchTimer = null;
    let tableRef = useRef(null);
    const inputSerch = useRef(null);
    let pageRef = useRef(25)
    const urlParams = useParams();

    const buttonFormatter = (cell, row, rowIndex) => (
        <div className="participant_table_ic-wrap">
            <button type="button" className="participant_table_ic tab_delete_ic" onClick={() => handleDeleteParticipant(row.id)}>Delete</button>
            <button type="button" className="participant_table_ic tab_reset_ic" disabled={(row.status && row.status.toLowerCase() !== 'complete') ? true : false} onClick={() => handleResetParticipantData(row)} title={((row.status && row.status.toLowerCase() !== 'complete') ? 'Response not available' : '')}>Reset</button>
        </div>
    );

    const handleDeleteParticipant = (rowId) => {
        confirmAlert({
            title: 'Delete Participant',
            message: 'Are you sure you want to delete ?',
            buttons: [
                {
                    label: 'Confirm',
                    onClick: () => {
                        dispatchSelectedOnEdit([]);
                        dispatchSelectedCount(0)
                        let formData = new FormData();
                        formData.append("participant_id", rowId);
                        formData.append("survey_id", urlParams.survey_id);
                        formData.append("isedit", (surveyAction === "edit") ? true : false);
                        Axios.post(configURL.delete_survey_participant, formData).then(res => {
                            if (res.data && res.data.success) {
                                toast.success(res.data.message || 'Data Deleted');
                                let current_page = tableRef.current.paginationContext.currPage
                                if ((tableRef.current.table.props.data.length - 1) === 0) {
                                    current_page = tableRef.current.paginationContext.currPage - 1
                                }
                                current_page = (current_page > 0) ? current_page : 1
                                getParticipantData(current_page, "")
                            } else {
                                toast.warn(res.data.message || 'Something went wrong');
                            }
                        }).catch(err => {
                            toast.error(err.toString());
                        })
                    }
                },
                {
                    label: 'Cancel'
                }
            ]
        });
    }

    const handleMultiDelete = () => {
        confirmAlert({
            title: 'Delete Participants',
            message: 'Are you sure you want to delete?',
            buttons: [
                {
                    label: 'Confirm',
                    onClick: () => {
                        dispatchSelectedOnEdit([]);
                        dispatchSelectedCount(0)
                        EziLoader.show()
                        let formData = new FormData();
                        formData.append("participants_array", JSON.stringify(selectedOnEdit));
                        formData.append("survey_id", urlParams.survey_id);
                        formData.append("isedit", (surveyAction === "edit") ? true : false);
                        Axios.post(configURL.deleteSelectedParticipants, formData).then(res => {
                            EziLoader.hide()
                            if (res.data && res.data.success) {
                                toast.success(res.data.message || 'Data Deleted');
                                let current_page = tableRef.current.paginationContext.currPage
                                if ((tableRef.current.table.props.data.length - 1) === 0) {
                                    current_page = tableRef.current.paginationContext.currPage - 1
                                }
                                current_page = (current_page > 0) ? current_page : 1
                                getParticipantData(current_page, "")
                            } else {
                                toast.warn(res.data.message || 'Something went wrong');
                            }
                        }).catch(err => {
                            EziLoader.hide()
                            toast.error(err.toString());
                        })
                    }
                },
                {
                    label: 'Cancel'
                }
            ]
        });
    }

    /**
     * Reset Participant Data
     * @param {Object} row 
     */
    const handleResetParticipantData = (row) => {
        let participant_id = row.id
        confirmAlert({
            title: 'Reset Participant Response',
            message: 'Are you sure you want to reset this response ?',
            buttons: [
                {
                    label: 'Confirm',
                    onClick: () => {
                        let formData = new FormData();
                        formData.append("participant_id", participant_id);
                        formData.append("survey_id", urlParams.survey_id);
                        Axios.post(configURL.resetParticipantResponse, formData).then(res => {
                            if (res.data && res.data.success) {
                                toast.success(res.data.message || 'Data Deleted');
                                let current_page = tableRef.current.paginationContext.currPage
                                getParticipantData(current_page, "")
                            } else {
                                toast.warn(res.data.message || 'Something went wrong');
                            }
                        }).catch(err => {
                            toast.error(err.toString());
                        })
                    }
                },
                {
                    label: 'Cancel'
                }
            ]
        });
    }

    /**
     * Load data.
     */
    const getParticipantData = (pageNum = 1, search = "") => {
        let formData = new FormData();
        formData.append("page", pageNum);
        formData.append("search", search);
        formData.append("survey_id", urlParams.survey_id);
        formData.append("language", i18Lang);
        formData.append("isedit", (surveyAction === "edit") ? true : false);
        formData.append("filters", JSON.stringify(filters));
        formData.append("per_page", pageRef.current);
        Axios.post(configURL.selected_participants, formData).then(response => {
            EziLoader.hide()
            if (response.status === 200 && response.data.success === true) {
                const columnData = response.data.column.map(item => {
                    if (item.dataField === "status") {
                        return {
                            ...item,
                            formatter: statusFormatter,
                            editable: false
                        }
                    }
                    else if (item.dataField === "participant_source") {
                        return {
                            ...item,
                            editable: false
                        }
                    } else {
                        return item;
                    }
                });
                columnData.push({
                    headerFormatter: () => {
                        return <span className="participant_action">Action</span>
                    },
                    formatter: buttonFormatter,
                    editable: false
                });
                setParticipants(response.data.data);
                setDataColumn(columnData);
                setPagination(response.data.pagination);
                setUniqueKeyField('id')
                setSearchLoading(false);
                setDataLoaded(true);
                if (surveyAction === "create" || surveyStatus === 'draft') {
                    let selectedIds = response.data.data.length > 0 ? response.data.data.reduce((arr, cur) => {
                        return arr.concat(cur.id)
                    }, []) : []
                    dispatchSelectedOnEdit(selectedIds);
                    dispatchSelectedCount(response?.data?.pagination?.total || 0)
                }
            } else {
                toast.warn("Something went wrong!!")
            }
        });
    }

    const statusFormatter = (cell, row) => {
        return (
            <span className={`themes-status_c`}>{cell}</span>
        );
    }

    const handleTableChange = (type, { page, sizePerPage }) => {
        switch (type) {
            case "pagination":
                EziLoader.show()
                pageRef.current = sizePerPage
                getParticipantData(page);
                break;
            case "sort":
                getParticipantData();
                break;
            default:
                break;
        }

    }
    const handleCheckData = (row, isSelect) => {
        if (surveyStatus === 'draft') {
            return false;
        }
        const selected = [...selectedOnEdit];
        if (isSelect) {
            selected.push(row[uniqueKeyField]);
        }
        if (!isSelect) {
            selected.indexOf(row[uniqueKeyField]) !== -1 && selected.splice(selected.indexOf(row[uniqueKeyField]), 1)
        }
        if (selected.length > 200) {
            toast.warn('At Max, 200 Participant can be selected')
            return false;
        }
        dispatchSelectedOnEdit(selected)
        dispatchSelectedCount(selected.length)
        dispatchCommunicateByStatus({ email: [], sms: [] })
    }

    const handleTableSearch = () => {
        let inputValue = inputSerch.current.value;
        clearTimeout(inputSearchTimer);
        inputSearchTimer = setTimeout(() => {
            setSearchLoading(true);
            getParticipantData(1, inputValue)
        }, 1000);
    }

    const handleApplyFilter = (filters) => {
        dispatchSelectedOnEdit([]);
        dispatchSelectedCount(0)
        setFilters(filters)
        setParticipantFilter(false);
    }
    const handleClearFilter = () => {
        dispatchSelectedOnEdit([]);
        dispatchSelectedCount(0)
        setFilters([])
    }

    const handleOnSelectAll = (isSelected, rows) => {
        let newValues = []
        if (isSelected) {
            let selectedData = rows.map(item => item[uniqueKeyField])
            newValues = [...new Set([...selectedOnEdit, ...selectedData])]
        }
        if (!isSelected) {
            let unSelectedData = rows.map(item => item[uniqueKeyField])
            let oldValues = [...selectedOnEdit];
            newValues = oldValues.filter(item => !unSelectedData.includes(item));
        }
        if (newValues.length > 200) {
            toast.warn('At Max, 200 Participant can be selected')
            return [];
        }
        dispatchSelectedOnEdit(newValues)
        dispatchSelectedCount(newValues.length)
        dispatchCommunicateByStatus({ email: [], sms: [] })
    }

    /**
     * Update Row Data Locally
     * @param {*} oldValue 
     * @param {*} newValue 
     * @param {*} row 
     * @param {*} column 
     */
    const updateRowData = (oldValue, newValue, row, column) => {
        if (oldValue !== newValue) {
            row[column.dataField] = newValue;
            let objIndex = participants.map((o) => o[uniqueKeyField]).indexOf(row[uniqueKeyField]);
            participants[objIndex] = row;
            setParticipants(participants);
        }
    }

    /**
     * Build Data Table
     * @param {Object} param0 
     */
    const RemotePagination = ({ data, page = 1, sizePerPage, onTableChange, totalSize }) => (
        <div>
            <BootstrapTable
                ref={tableRef}
                remote
                keyField={uniqueKeyField}
                data={data}
                noDataIndication="No Data Available."
                columns={dataColumn}
                cellEdit={(surveyStatus !== 'draft') ? cellEditFactory({
                    mode: 'click',
                    blurToSave: true,
                    beforeSaveCell: (oldValue, newValue, row, column, done) => { updateRowData(oldValue, newValue, row, column); done(false); },
                }) : false}
                pagination={paginationFactory({ page, sizePerPage, totalSize, sizePerPageList: getTablePagesSizes(totalSize) })}
                onTableChange={onTableChange}
                selectRow={{ mode: 'checkbox', onSelect: handleCheckData, selected: selectedOnEdit, onSelectAll: handleOnSelectAll, hideSelectAll: (surveyStatus === 'draft') }}
            />
        </div>
    )

    /**
     * Download Survey Response Sheet
     */
    const downLoadResponseSheet = () => {
        let formData = new FormData();
        formData.append("survey_id", urlParams.survey_id);
        formData.append("isedit", (surveyAction === "edit") ? true : false);

        Axios.post(configURL.download_response_sheet, formData).then(response => {
            if (response.data.success === true) {
                window.open(response.data.download_link, '_blank').focus();
            } else {
                toast.warn("Something went wrong!!")
            }
        });
    }

    /**
     * Handle bulk update
     */
    const handleBulkUpdate = () => {
        EziLoader.show()
        let updatedRows = participants.filter(item => selectedOnEdit.includes(item[uniqueKeyField]))
        let formData = new FormData();
        formData.append("update_data", JSON.stringify(updatedRows));
        formData.append("survey_id", urlParams.survey_id);
        Axios.post(configURL.update_survey_participants, formData).then(res => {
            EziLoader.hide()
            if (res.data.success === true) {
                toast.success(res.data.message || 'Participants updated');
            } else {
                toast.warn(res.data.message || 'Could not update records Please try again');
            }
        }).catch(err => {
            console.log(err);
            EziLoader.hide()
        })
    }

    useEffect(() => {
        EziLoader.show()
        getParticipantData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [filters])

    return (
        <React.Fragment >
            <div className="saved-demographic-filter-wrap">
                <div className="participant-fiter-wrap">
                    <SweetSearch ref={inputSerch} change={handleTableSearch} loading={searchLoading} />
                    <button type="button" className="ezi-filter-btn " onClick={() => setParticipantFilter(!participantFilter)} >Filter</button>
                    <DemographicFilter
                        applyFilter={handleApplyFilter}
                        clearFilter={handleClearFilter}
                        searchable={true}
                        show={participantFilter}
                        dataSource={{
                            filters: configURL.participantFilters,
                            filterOptions: configURL.participantFiltersOptions
                        }}
                        additional={{ survey_id: urlParams.survey_id }}
                        hide={() => setParticipantFilter(false)}
                    />
                    <button type="button" className="invitees-length-btn " disabled={(selectedOnEdit.length === 0)} onClick={handleMultiDelete}>Delete
                        <span className="invitees-length-badge">
                            {selectedOnEdit.length || 0}
                        </span>
                    </button>
                    {(surveyAction === 'edit' && surveyStatus !== 'draft') &&
                        <button className="ezi-pink-btn bulk-update" onClick={handleBulkUpdate} disabled={(selectedOnEdit.length === 0)}>
                            Bulk Update <span className="invitees-length-badge"> {selectedOnEdit.length || 0}</span>
                        </button>
                    }
                    {(surveyAction === 'edit' && surveyStatus !== 'draft') &&
                        <button className="saved-participant-download" onClick={downLoadResponseSheet} title="Download Response Sheet"></button>}

                </div>

            </div>
            {dataLoaded &&
                <div className="saved_participants_table">
                    <RemotePagination
                        data={participants}
                        page={pagination.current_page || 0}
                        sizePerPage={pageRef.current}
                        totalSize={pagination.total || 0}
                        onTableChange={handleTableChange}
                    />
                </div>
            }
        </React.Fragment>
    )

}
const mapStateToProps = state => {
    return {
        surveyAction: state.survey.action,
        surveyStatus: state.survey.status,
        selectedOnEdit: state.participant.selectedOnEdit
    }
}
const mapDispatchToProps = dispatch => {
    return {
        dispatchSelectedOnEdit: (data = []) => dispatch(AppAction.setSelectedOnEdit({ participants: data })),
        dispatchCommunicateByStatus: (data = []) => dispatch(AppAction.setSelectByStatus({ participant_status: data })),
        dispatchSelectedCount: (count = 0) => dispatch(AppAction.updateSelectedCount({ count })),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SavedParticipants)