import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';
import DateTimePicker from 'react-datetime-picker';
import { Spinner } from "react-bootstrap"
import { useParams } from "react-router-dom";
import Axios from "utility/Axios";
import config from "config/config";
import { toast } from "react-toastify";
import FormData from 'utility/AppFormData';

const SendReminderModal = ({ peoples, ...rest }) => {
    const [selectedDate, setSelectedDate] = useState(null);
    const [selectedDateNow, setSelectedDateNow] = useState(null);
    const [loading, setLoading] = useState(false);
    const urlParams = useParams();

    const handleSendReminder = () => {
        setLoading(true);
        let formData = new FormData();
        let dataJson = {
            participants: peoples,
            survey_id: urlParams.survey_id,
            send_date: selectedDate,
            send_date_now: selectedDateNow
        }
        formData.append("reminder_json", JSON.stringify(dataJson));
        Axios.post(config.send_reminder_url, formData).then(response => {
            if (response.status === 200 && response.data.success === true) {
                toast.success("Reminder sent successfully!")
            } else {
                toast.warn(response.data.message);
            }
            rest.onHide();
            setLoading(false);
        })
    }
    return (
        <Modal {...rest} size="md" aria-labelledby="contained-modal-title-vcenter" centered className="theme-modal-wrapper" >
            <Modal.Header className="ezi-modal-header">
                <Modal.Title id="contained-modal-title-vcenter" className="theme-modal-title ezi-modal-header-title" >
                    <span className="theme-modal-title-text">Send Reminder To {peoples.length} Peoples</span>
                    <span className="ezi-modal-close" onClick={rest.onHide}></span>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="startend-date-wrap">
                    <div className="start-date-wrap">
                        <DateTimePicker value={selectedDate} onChange={(value) => {
                            setSelectedDate(value);
                            setSelectedDateNow(null)
                        }} /> OR
                        <button type="button" className="ezi-pink-btn" onClick={() => {
                            setSelectedDate(null);
                            setSelectedDateNow(new Date())
                        }}>Now</button>
                        <button disabled={loading || peoples.length === 0} title={peoples.length > 0 ? "Send Invitation" : "Please select peoples"} onClick={handleSendReminder} className="ezi-pink-btn">
                            Send
                            {loading && <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true" />}
                        </button>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    )
}

export default SendReminderModal;