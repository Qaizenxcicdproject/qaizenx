import React, { useState, useEffect, useRef, useContext } from 'react';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { useParams } from "react-router-dom";
import { toast } from 'react-toastify';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { connect, useStore } from 'react-redux';
import AppContext from 'store/AppContext';
import * as AppAction from "store/actions";
import InputPrompt from "components/InputPrompt";
import { getTablePagesSizes } from 'utility/helper';
import IdentifierMapModal from './IdentifierMapModal';
import FormData from 'utility/AppFormData';
import { lowerCase } from 'lodash';

const SurveyIdentifiers = ({ dispatchMapping, mappedIdentifier, reloadData }) => {
    const { EziLoader } = useContext(AppContext)
    const [identifiers, setIdentifiers] = useState([]);
    const [dataColumn, setDataColumn] = useState([{}]);
    const [uniqueKeyField, setUniqueKeyField] = useState("id");
    const [pagination, setPagination] = useState({});
    const i18Lang = localStorage.getItem("i18nextLng");
    const [showPrompt, setShowPrompt] = useState(false);
    const [selectedData, setSelectedData] = useState({});
    const [showMapModel, setShowMapModel] = useState(false);
    const [selectedIdentifier, setSelectedIdentifier] = useState([]);
    const urlParams = useParams();
    let tableRef = useRef(null);
    let pageRef = useRef(25)
    let rStore = useStore()

    const buttonFormatter = (_, row, __) => {
        let disableMap = (row.source_type && row.source_type.toLowerCase() === "people") ? true : false
        return (
            <div className="map-identifier-btn-wrap">
                {disableMap && '-'}
                {!disableMap && <>
                    <button type="button" className="map_identifier_btn" title="Map Identifier" onClick={handleMapAction.bind(this, row)}>Map</button>
                    <button type="button" className="participant_table_ic tab_reset_ic" title="Reset Mapping" onClick={handleResetMapping.bind(null, row)} >Reset</button>
                </>}

            </div>
        );
    }

    const urlFormatter = (_, row, __) => {
        let disableMap = (row.source_type && row.source_type.toLowerCase() === "people") ? true : false
        return (
            <div className="map-identifier-btn-wrap">
                {disableMap && '-'}
                {!disableMap &&
                    <label className="switch-toggle" onClick={handleMapToUrl.bind(null, row)}>
                        <span className={`toggle-slider round ${(row.map_to_url === true) ? 'active' : ''}`}></span>
                    </label>
                }

            </div>
        );
    }

    const handleMapAction = (data) => {
        setSelectedData(data)
        setShowMapModel(true)
    }

    /**
     * Set Question for url
     * @param {Object} data 
     */
    const handleMapToUrl = (data) => {

        if (!data.mapped_question_id) {
            toast.warn('Please map question before select.')
            return
        }
        let { data: tableData = [] } = tableRef.current.props
        let { participant: { mappedIdentifier = {} } } = rStore.getState()
        let mappedData = JSON.parse(JSON.stringify(mappedIdentifier))
        let identifiersData = JSON.parse(JSON.stringify(tableData))
        let mapIndx = mappedData.findIndex(el => el.id === data.id);
        let identifiersIndx = identifiersData.findIndex(el => el.id === data.id)
        if (identifiersIndx > -1 && mapIndx > -1) {
            identifiersData[identifiersIndx].map_to_url = !data.map_to_url
            mappedData[mapIndx].map_to_url = !data.map_to_url
            dispatchMapping(mappedData)
            setIdentifiers(identifiersData)
        }
    }

    /**
     * Load data.
     */
    const getInitialData = (page = 1) => {
        let formData = new FormData();
        formData.append("survey_id", urlParams.survey_id);
        formData.append("language", i18Lang);
        formData.append("page", page);
        formData.append("per_page", pageRef.current);
        Axios.post(configURL.identifiers_listing, formData).then(response => {
            EziLoader.hide()
            if (response.status === 200 && response.data.success === true) {
                const mapped_identifiers = response.data.selected_identifier || []
                const selectedIds = mapped_identifiers.map(el => el.id)
                if (selectedIds && selectedIds.length > 0) {
                    setSelectedIdentifier(selectedIds)
                    dispatchMapping(mapped_identifiers)
                } else {
                    setSelectedIdentifier([])
                    dispatchMapping([])
                }
                const columnData = response.data.columns
                columnData.push({
                    headerFormatter: () => {
                        return <span className="participant_action">Map to url</span>
                    },
                    formatter: urlFormatter
                });
                columnData.push({
                    headerFormatter: () => {
                        return <span className="participant_action">Action</span>
                    },
                    formatter: buttonFormatter
                });
                setIdentifiers(response.data.results);
                setDataColumn(columnData);
                setPagination(response.data.pagination)
                setUniqueKeyField('id')
            } else {
                toast.info(response.data.message || "Something went wrong!!")
            }
        }).catch(err => {
            console.log(err);
            EziLoader.hide()
        })
    }

    /**
     * Load initial data.
     */
    const loadInitialData = () => {
        EziLoader.show()
        getInitialData();
    }

    const handleCheckData = (row, isSelect) => {
        let mappedData = [...mappedIdentifier]
        const selected = [...selectedIdentifier];
        let mapIndx = mappedData.findIndex(el => el.id === row[uniqueKeyField]);
        if (isSelect) {
            selected.push(row[uniqueKeyField]);
            if (mapIndx === -1) {
                mappedData.push(row)
            }
        } else {
            selected.indexOf(row[uniqueKeyField]) !== -1 && selected.splice(selected.indexOf(row[uniqueKeyField]), 1)
            mapIndx !== -1 && mappedData.splice(mapIndx, 1)

        }
        dispatchMapping(mappedData)
        setSelectedIdentifier(selected)
    }

    /**
     * Create New Identifier
     * @param {String} name 
     */
    const handleCreateIdentifier = (name) => {
        console.log(name);
        const reserver_name = lowerCase(name)
        const reservername = reserver_name.replace(/ /g, '')
        if (name === "") {
            return;
        }
        if(reservername === "status"){
            toast.warn(`${name} keyword is reserved`)
            return;
        }
        setShowPrompt(false)
        EziLoader.show()
        let formData = new FormData();
        formData.append("survey_id", urlParams.survey_id);
        formData.append("name", name);
        Axios.post(configURL.save_survey_identifier, formData).then(response => {
            EziLoader.hide()
            if (response.status === 200 && response.data.success === true) {
                let current_page = tableRef.current.paginationContext.currPage
                if ((tableRef.current.table.props.data.length - 1) === 0) {
                    current_page = tableRef.current.paginationContext.currPage - 1
                }
                current_page = (current_page > 0) ? current_page : 1
                getInitialData(current_page)
                toast.success(response.data.message || "Identifier Created ")
            } else {
                toast.warn(response.data.message || "Something went wrong!!")
            }
        }).catch(err => {
            console.log(err);
            EziLoader.hide()
        })
    }

    /**
     * Handle Pagination
     * @param {String} type 
     * @param {*} Obj 
     */
    const handleTableChange = (type, { page, sizePerPage }) => {
        switch (type) {
            case "pagination":
                EziLoader.show()
                pageRef.current = sizePerPage
                getInitialData(page);
                break;
            default:
                break
        }

    }

    /**
     * Build Data Table
     * @param {Object} param 
     */
    const RemotePagination = ({ data, page = 1, sizePerPage, onTableChange, totalSize }) => (
        <div>
            <BootstrapTable
                ref={tableRef}
                remote
                keyField={uniqueKeyField}
                data={data}
                noDataIndication="No Data Available."
                columns={dataColumn}
                pagination={paginationFactory({ page, sizePerPage, totalSize, sizePerPageList: getTablePagesSizes(totalSize) })}
                onTableChange={onTableChange}
                selectRow={{ mode: 'checkbox', onSelect: handleCheckData, selected: selectedIdentifier, hideSelectAll: true }}
            />
        </div>
    )

    /**
     * Set Question Mapping
     * @param {Object} data 
     */
    const setQuestionMapping = (data) => {
        let selectedMap = selectedData.id || null
        if (!selectedMap || selectedIdentifier.indexOf(selectedMap) === -1) {
            toast.warn('Please Select Row Before Mapping.')
            return
        }
        setShowMapModel(false)
        let mappedData = JSON.parse(JSON.stringify(mappedIdentifier))
        let identifiersData = JSON.parse(JSON.stringify(identifiers))
        let mapIndx = mappedData.findIndex(el => el.id === selectedMap);
        let identifiersIndx = identifiersData.findIndex(el => el.id === selectedMap)
        identifiersData[identifiersIndx].mapped_question_title = data.title
        identifiersData[identifiersIndx].mapped_question_id = data.id
        identifiersData[identifiersIndx].mapped_question_name = data.question_name || null
        mappedData[mapIndx].mapped_question_id = data.id
        mappedData[mapIndx].mapped_question_title = data.title
        mappedData[mapIndx].mapped_question_name = data.question_name || null
        dispatchMapping(mappedData)
        setIdentifiers(identifiersData)

    }

    /**
     * Reset Question Mapping
     * @param {Object} data 
     */
    const handleResetMapping = (data) => {
        let { data: tableData = [] } = tableRef.current.props
        let { participant: { mappedIdentifier = {} } } = rStore.getState()
        let mappedData = JSON.parse(JSON.stringify(mappedIdentifier))
        let identifiersData = JSON.parse(JSON.stringify(tableData))
        let mapIndx = mappedData.findIndex(el => el.id === data.id);
        let identifiersIndx = identifiersData.findIndex(el => el.id === data.id)
        if (identifiersIndx > -1 && mapIndx > -1) {
            delete identifiersData[identifiersIndx].mapped_question_title
            delete identifiersData[identifiersIndx].mapped_question_id
            delete mappedData[mapIndx].mapped_question_id
            delete mappedData[mapIndx].mapped_question_title
            dispatchMapping(mappedData)
            setIdentifiers(identifiersData)
        }
    }

    useEffect(loadInitialData, [reloadData])

    return (
        <React.Fragment >
            <div className="saved-demographic-filter-wrap">
                <div className="participant-fiter-wrap">
                    <button type="button" className="btn-ripple ezi-pink-btn inner_save_btn" onClick={() => setShowPrompt(true)}>Add New</button>
                    <button type="button" className="invitees-length-btn no-hover" >Selected <span className="invitees-length-badge"> {selectedIdentifier.length || 0}</span> </button>
                </div>
            </div>
            <IdentifierMapModal isShown={showMapModel} data={selectedData} hideModel={() => {
                setShowMapModel(false)
                setSelectedData({})
            }} submitMapping={setQuestionMapping} />
            <InputPrompt
                show={showPrompt}
                alerttext="Please Enter Identifier Name"
                confirm={handleCreateIdentifier}
                onhide={() => setShowPrompt(false)}
            />
            <div className="saved_participants_table">
                <RemotePagination
                    data={identifiers}
                    page={pagination.current_page || 0}
                    sizePerPage={pageRef.current}
                    totalSize={pagination.total || 0}
                    onTableChange={handleTableChange}
                />
            </div>
        </React.Fragment>
    )

}
const mapStateToProps = state => {
    return {
        mappedIdentifier: state.participant.mappedIdentifier,
        reloadData: state.participant.channelTypes,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        dispatchMapping: (mappings = []) => dispatch(AppAction.setIdentifiersMapping({ mappings }))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SurveyIdentifiers)