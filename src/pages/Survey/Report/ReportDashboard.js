import React, { useState, useEffect, useRef, useContext } from "react";
import { Breadcrumb } from 'react-bootstrap';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { getFirstWord } from 'utility/helper';
import 'utility/i18next.js'
import './ReportDashboard.scss';
import { toast } from "react-toastify";
import CategoryFilter from "components/CategoryFilter";
import SweetSearch from "components/SweetSearch";
import Pagination from "react-js-pagination";
import { confirmAlert } from 'react-confirm-alert';
import AppContext from 'store/AppContext';
import SurveyFilter from '../../Survey/CategorySurveyDashboard/SurveyFilter';
import FormData from 'utility/AppFormData';

const ReportDashboard = (props) => {
    const { languageObj = {}, EziLoader } = useContext(AppContext)
    const [defaultCategory, setDefaultCategory] = useState({ category_id: "", description: "" });
    const [categories, setCategories] = useState([]);
    const [categorySurveys, setCategorySurveys] = useState([]);
    const [pagination, setPagination] = useState({});
    const [searchLoading, setSearchLoading] = useState(false);
    const [listingFilter, setListingFilter] = useState({});
    const perPage = 10;
    var searchTimer = null;
    const inputSearch = useRef(null);
    const [showFilter, setShowFilter] = useState(false);

    /**
     * Get Searched category.
     * @param {string} search Search string.
     */
    const getCategoriesListing = (search = "") => {
        let formData = new FormData();
        formData.append("search", search);
        formData.append("type", "survey");
        Axios.post(configURL.categoryDs, formData).then(response => {
            if (response.data.success !== undefined && response.data.success === true) {
                let result = response.data.results;
                const categoryData = [];
                result.forEach(item => {
                    categoryData.push({ id: item.id, name: item.category_name, description: item.description });
                });
                setCategories(categoryData);
            } else {
                toast.warn(response.data.message);
            }
        })
    }

    /**
     * Get Category templates.
     * 
     * @param {object} obj category object
     */
    const changeSurveyCategory = (obj = {}) => {
        setDefaultCategory({
            category_id: obj.id || null,
            category_name: obj.name || null,
            description: obj.description || ""
        });
        props.history.replace("/reports", {
            category_id: obj.id,
            category_name: obj.name
        })
    }

    /**
     * Get Perticular template types.
     * 
     * @param {string} type template types
     * @param {number} page Page number
     */
    const getCategorySurveys = (page = 1, filters = listingFilter) => {
        if (!filters.hasOwnProperty('survey_status')) {
            filters["survey_status"] = ["close", "active"]
        }
        let searchVal = inputSearch.current.value;
        let formData = new FormData();
        formData.append("category_id", defaultCategory.category_id);
        formData.append("per_page", perPage);
        formData.append("page", page);
        formData.append("type", "total");
        formData.append("listing_type", "report");
        formData.append("search", searchVal);
        formData.append("filters", JSON.stringify(filters));
        Axios.post(configURL.category_wise_surveys, formData).then(response => {
            EziLoader.hide()
            setSearchLoading(false)
            if (response.data.success !== undefined && response.data.success === true) {
                setCategorySurveys(response.data.results);
                setPagination(response.data.pagination);
            } else {
                toast.warn(response.data.message);
            }
        }).catch(err => {
            console.log(err)
            EziLoader.hide()
        })
    }

    /**
     * Filter Data based on search.
     * 
     * @param {string} type Input value
     */
    const handleFilterSearch = () => {
        clearTimeout(searchTimer);
        searchTimer = setTimeout(() => {
            setSearchLoading(true)
            getCategorySurveys(1);
        }, 800);
    }

    /**
     * Handle Pagination
     * 
     * @param {string} type Filter Type
     */
    const handlePagination = (page = 1) => {
        EziLoader.show()
        getCategorySurveys(page);
    }

    /**
     * Handle Apply filter
     * @param {Object} filter 
     */
    const handleApplyFilter = (filter = []) => {
        EziLoader.show()
        setListingFilter(filter)
        setShowFilter(false)
        getCategorySurveys(1, filter);
    }

    /**
     * Handle Filter Clear
     */
    const handleClearFilter = () => {
        EziLoader.show()
        setListingFilter({})
        setShowFilter(false)
        getCategorySurveys(1, {});
    }

    const handleSurveyAnalyze = (surveyId) => {
        let notificationId = toast.info('Loading Survey information..', { position: "bottom-right" })
        let formData = new FormData()
        formData.append('survey_id', surveyId)
        Axios.post(configURL.CheckSurveyData, formData).then(response => {
            toast.dismiss(notificationId)
            if (response.data.success !== undefined && response.data.success === true) {
                props.history.push(`/customer-experience/${surveyId}`, { survey_id: surveyId });
            } else {
                confirmAlert({
                    title: 'Analyse Survey',
                    message: 'Data is not pulled for reporting, please try again after some time',
                    buttons: [{ label: 'I Understood' }, { label: 'Close' }]
                });
                return;
            }
        })
    }

    useEffect(getCategoriesListing, []);

    useEffect(() => {
        EziLoader.show()
        getCategorySurveys()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [defaultCategory]);

    return (
        <React.Fragment>
            <section className="Page-ReportDashboard">
                <div className="breadcrumb_ezi">
                    <Breadcrumb>
                        <Breadcrumb.Item>{defaultCategory.category_name || 'All Category'}</Breadcrumb.Item>
                        <Breadcrumb.Item>{languageObj.translate('Surveys.1')}</Breadcrumb.Item>
                    </Breadcrumb>
                    <CategoryFilter handleSelect={changeSurveyCategory} data={categories} defaultSelected={defaultCategory.category_name || 'All Category'} />
                    <span className="template-category-subtitle"> {(defaultCategory.description !== undefined) ? defaultCategory.description : ''} </span>
                </div>
                <div className="filter-search-wrap">
                    <div className="custom-demography-button-wrap">
                        <button type="button" className="ezi-filter-btn" onClick={() => setShowFilter(!showFilter)}>Filter</button>
                        <SurveyFilter
                            show={showFilter}
                            hide={() => setShowFilter(false)}
                            position="right"
                            fixedNode={[{
                                name: "survey_status",
                                label: "Survey Status",
                                options: [
                                    { id: "close", name: "close" },
                                    { id: "active", name: "active" }
                                ]
                            }]}
                            applyFilter={handleApplyFilter}
                            clearFilter={handleClearFilter}
                        />
                    </div>
                    <SweetSearch loading={searchLoading} change={handleFilterSearch} ref={inputSearch} />
                </div>

                <div className="category-dashboard-table">

                    <div className="category-dashboard-table-row category-table-heading">
                        <div className="category-dashboard-table-cell">  {languageObj.translate('Survey.1')}</div>
                        <div className="category-dashboard-table-cell">  Created By</div>
                        <div className="category-dashboard-table-cell">  Report Type</div>
                        <div className="category-dashboard-table-cell"> {languageObj.translate('Status.1')} </div>
                        <div className="category-dashboard-table-cell">
                            <span className="appeared-heading">Appeared / </span>
                            <span className="appeared-heading">Total / </span>
                            <span className="appeared-heading">Response Rate</span>
                        </div>
                        <div className="category-dashboard-table-cell"> {languageObj.translate('Analysis.1')} </div>
                    </div>
                    {
                        categorySurveys.length > 0 ? categorySurveys.map((item, index) => {
                            return (
                                <div key={index} className="category-dashboard-table-row">
                                    <div className="category-dashboard-table-cell" data-title="Survey">
                                        <div className={`category-table-survey-wrap`} >
                                            <div className="category-table-survey-text-wrap">
                                                <span className="category-table-survey-name">{item.name}</span>
                                                <span className="category-table-create">
                                                    Last Modified : {getFirstWord(item.updated_at)}   |   Created on : {getFirstWord(item.created_at)}
                                                </span>
                                                <span className="category-table-create">
                                                    Started on : {getFirstWord(item.starttime) || "---"}   |  Closed on : {getFirstWord(item.endtime) || "---"}
                                                </span>
                                                <span className="category-table-create">Description : {item.description}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="category-dashboard-table-cell" data-title="Owner"><span className="owner-name">{item.owner || "---"}</span></div>
                                    <div className="category-dashboard-table-cell" data-title="Type"><span className="survey-type">{item.report_type_name || '---'}</span></div>
                                    <div className={`category-dashboard-table-cell table-status`} data-title="Status"> <span className={`dt-status-formatter ${item.status || 'draft'}`}>{item.status || 'draft'}</span></div>
                                    <div className="category-dashboard-table-cell " data-title="Responses/Dropout">
                                        <span className="dt-dropouts-formatter">{item.appeared || 0}</span>
                                        <span className="response-devider">/</span>
                                        <span className="dt-dropouts-formatter">{item.total}</span>
                                        <span className="response-devider">/</span>
                                        <span className="dt-dropouts-formatter">{item.response_rate} {!isNaN(item.response_rate) && '%'}</span>
                                    </div>

                                    <div className="category-dashboard-table-cell " data-title="Analysis">
                                        <button type="button" className="dt-analyze-formatter" disabled={(item.status.toLowerCase() === 'active' || item.status.toLowerCase() === 'close') ? false : true} onClick={() => handleSurveyAnalyze(item.id)}>
                                            {languageObj.translate('Analyze.1')}
                                        </button>
                                    </div>
                                </div>
                            )
                        }) : null
                    }
                </div>
                {categorySurveys.length <= 0 && <div className="category-table-no-result">No result Found</div>}
                <div className="pagination-plugin-wrap category-pagination-formatter">
                    <Pagination
                        activePage={pagination.current_page}
                        itemsCountPerPage="10"
                        totalItemsCount={pagination.total}
                        onChange={handlePagination}
                        hideDisabled={true}
                        firstPageText={<span class="prev-page-text-ic"></span>}
                        lastPageText={<span class="next-page-text-ic"></span>}
                        nextPageText={<span class="next-text-ic"></span>}
                        prevPageText={<span class="prev-text-ic"></span>}
                    />
                </div>
            </section>
        </React.Fragment>
    )
}

export default ReportDashboard;