import React, { useState, useContext, useEffect } from "react";
import { Tab, Nav, Spinner } from "react-bootstrap";
import ParticipantSetting from "./ParticipantSetting/ParticipantSetting";
import SurveySetting from "./SurveySetting/SurveySetting";
import { useHistory, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import SurveyPreview from "./SurveyPreview/SurveyPreview";
import Axios from "utility/Axios";
import configURL from "config/config";
import { confirmAlert } from "react-confirm-alert";
import AppContext from "store/AppContext";
import { connect } from "react-redux";
import * as AppActions from "store/actions";
import FormData from "utility/AppFormData";
import { CopyToClipboard } from "react-copy-to-clipboard";

const SurveyCreatorTab = ({
  dispatchShowChecklist,
  dispatchSurveyName,
  surveyState,
  previewSurvey,
  dispatchResetSurveyState,
  changeLocale,
  dispatchSurveyStatus,
  handleSave,
  participantActiveTab,
}) => {
  let { surveyName, categoryName, status, canLaunch, surveyLink, surveyType } =
    surveyState;
  let surveyStatus = status;
  const { languageObj = {} } = useContext(AppContext);
  const [key, setKey] = useState("survey-builder");
  const [loadingBtn, setLoadingBtn] = useState(false);
  const [copied, setCopied] = useState(false);
  let nameChangeTimeOut = null;
  let history = useHistory();
  let urlParams = useParams();

  const handleBackSurvey = () => {
    confirmAlert({
      title: "Back To Dashboard",
      message: "Are you sure you want to go back ?",
      buttons: [
        {
          label: "Confirm",
          onClick: () => {
            dispatchResetSurveyState();
            history.push("/survey-dashboard");
          },
        },
        {
          label: "Cancel",
        },
      ],
    });
  };

  const handleTabSwitch = (tab) => {
    if (!canLaunch) {
      toast.info("Please add some questions before proceeding further!");
    } else {
      setKey(tab);
    }
  };

  /**
   * Handle Status Toggle
   * @param {String} status
   */
  const handleStatusUpdate = (status) => {
    let invertedStatus = status === "inactive" ? "active" : "inactive";
    let formData = new FormData();
    formData.append("survey_id", urlParams.survey_id);
    formData.append("status", invertedStatus);
    setLoadingBtn(true);
    Axios.post(configURL.survey_status_update, formData)
      .then((response) => {
        setLoadingBtn(false);
        if (
          response.data.success !== undefined &&
          response.data.success === true
        ) {
          dispatchSurveyStatus(invertedStatus);
          history.push(
            `/survey-dashboard/edit-survey/${urlParams.category_id}/${urlParams.survey_id}`,
            {
              ...history.location.state,
              status: invertedStatus,
            }
          );
        } else {
          toast.warn(response.data.message);
        }
      })
      .catch((err) => {
        console.log(err);
        setLoadingBtn(false);
      });
  };

  const handleSurveyNameChange = (event) => {
    let inputVal = event.target.value;
    clearTimeout(nameChangeTimeOut);
    nameChangeTimeOut = setTimeout(() => {
      let formData = new FormData();
      formData.append("display_name", inputVal);
      formData.append("survey_id", urlParams.survey_id);
      Axios.post(configURL.survey_name_update, formData).then((response) => {
        if (
          response.data.success !== undefined &&
          response.data.success === true
        ) {
          //toast.success(response.data.message);
          dispatchSurveyName({ surveyName: inputVal });
        } else {
          toast.warn(response.data.message);
        }
      });
    }, 500);
  };

  /**
   * Change status
   *
   * @param {number} id data id
   */
  const handleCloseSurvey = () => {
    confirmAlert({
      title: "Close Survey",
      message: "Are you sure want to close this survey",
      buttons: [
        {
          label: "Confirm",
          onClick: () => {
            let formData = new FormData();
            formData.append("survey_id", urlParams.survey_id);
            formData.append("status", "close");
            Axios.post(configURL.survey_status_update, formData).then(
              (response) => {
                if (
                  response.data.success !== undefined &&
                  response.data.success === true
                ) {
                  toast.success(response.data.message);
                  history.push("/survey-dashboard");
                } else {
                  toast.warn(response.data.message);
                }
              }
            );
          },
        },
        {
          label: "Cancel",
        },
      ],
    });
  };

  const SurveyButton = () => {
    if (surveyStatus && surveyStatus === "draft") {
      return (
        <button
          disabled={!canLaunch}
          title={canLaunch ? "Launch Survey" : "Add Some questions into survey"}
          type="button"
          className="test btn-ripple survey-preview-btn"
          onClick={dispatchShowChecklist}
        >
          Launch
        </button>
      );
    } else if (surveyStatus && surveyStatus === "inactive") {
      return (
        <button
          disabled
          title="Survey Is Launched"
          type="button"
          className="test btn-ripple survey-preview-btn"
        >
          Launched
        </button>
      );
    } else if (surveyStatus && surveyStatus === "active") {
      return (
        <button
          type="button"
          className={`test btn-ripple survey-preview-btn`}
          onClick={handleCloseSurvey}
          title="Close Survey"
        >
          Close Survey
        </button>
      );
    } else if (surveyStatus && surveyStatus === "close") {
      return (
        <button
          disabled
          type="button"
          className={`test btn-ripple survey-preview-btn`}
          title={`Survey is ${surveyStatus}`}
        >
          Survey Closed
        </button>
      );
    }
  };

  const copyHandler = () => {
    setCopied(true);
    setTimeout(() => setCopied(false), 2000);
  };

  useEffect(() => {
    if (participantActiveTab === "ACTIVE") {
      setKey("participant-setting");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <React.Fragment>
      {/* survey heaxder  */}
      <div className="survey-creator-header">
        <div className="survey-creator-header-left">
          <label className="survey-theme-name">{categoryName}</label>
          <div className="survey-template-wrap">
            <div className="template-name-wrap">
              <input
                type="text"
                className={`survey-template-name ${surveyStatus &&
                  surveyStatus &&
                  (surveyStatus === "inactive" || surveyStatus === "draft")
                  ? "editable"
                  : ""
                  }`}
                defaultValue={surveyName}
                Value={surveyName}
                onChange={handleSurveyNameChange}
              />
              {surveyStatus === "active" && surveyType === "openSurvey" && (
                <CopyToClipboard
                  text={surveyLink || "Dummy"}
                  onCopy={copyHandler}
                >
                  <button className="copy_clipboard" title="Copy Survey Link">
                    Copy
                  </button>
                </CopyToClipboard>
              )}
              {copied && (
                <span className="copy-alert-msg">Survey Link Copied.</span>
              )}
            </div>
          </div>
        </div>
        <div className="status-wrap">
          <button
            type="button"
            className="survey_rollback"
            onClick={handleBackSurvey}
          >
            {" "}
            {languageObj.translate("BacktoDashboard.1")}{" "}
          </button>
          <span
            className={`survey-status ${surveyStatus && surveyStatus} ${loadingBtn ? "disabled" : ""
              }`}
            onClick={() =>
              surveyStatus === "active" || surveyStatus === "inactive"
                ? handleStatusUpdate(surveyStatus)
                : ""
            }
          >
            {surveyStatus && surveyStatus}{" "}
            {loadingBtn && (
              <Spinner
                animation="border"
                size="sm"
                style={{ color: "#ce2e6c", borderWidth: ".1em" }}
              />
            )}
          </span>
        </div>
      </div>

      <div className="tablist_ezi survey-tab-container">
        <Tab.Container activeKey={key} onSelect={(k) => handleTabSwitch(k)}>
          <div className="survey-tab-header-wrap">
            <div className="tab-left-header">
              <Nav variant="pills">
                <Nav.Item>
                  <Nav.Link eventKey="survey-builder">
                    {languageObj.translate("SurveyBuilder.1")}
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="survey-setting">
                    {languageObj.translate("SurveySettings.1")}
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="participant-setting">
                    {languageObj.translate("ParticipantSettings.1")}
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="preview-survey" onClick={previewSurvey}>
                    {languageObj.translate("Preview.1")}
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item className="survey-preview-btn-wrap">
                  <Nav.Link>
                    <SurveyButton />
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            </div>
          </div>

          <Tab.Content className="survey-tab-content">
            <Tab.Pane
              eventKey="survey-builder"
              mountOnEnter
              itemID="survey-builder-wrapper"
            >
              <button
                className="ezi-btn custom-survey-save"
                onClick={handleSave}
              >
                Save Settings
              </button>
              <div id="surveyCreatorContainer" />
            </Tab.Pane>
            <Tab.Pane eventKey="participant-setting" mountOnEnter unmountOnExit>
              <ParticipantSetting surveyStatus={surveyStatus} participantActiveTab={participantActiveTab} />
            </Tab.Pane>
            <Tab.Pane eventKey="survey-setting">
              <SurveySetting surveyStatus={surveyStatus} />
            </Tab.Pane>
            <Tab.Pane eventKey="preview-survey">
              <SurveyPreview changeLocale={changeLocale} />
            </Tab.Pane>
          </Tab.Content>
        </Tab.Container>
      </div>
    </React.Fragment>
  );
};
const mapStateToProps = (state) => {
  return {
    surveyState: state.survey,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    dispatchShowChecklist: () =>
      dispatch(AppActions.viewChecklist({ view: true })),
    dispatchSurveyStatus: (status) =>
      dispatch(AppActions.setSurveyStatus({ status })),
    dispatchSurveyName: (data) => dispatch(AppActions.setSurveyName(data)),
    dispatchResetSurveyState: () => dispatch(AppActions.resetSurveyState()),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SurveyCreatorTab);
