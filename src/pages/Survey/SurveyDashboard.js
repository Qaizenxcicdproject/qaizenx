import React, { useState, useEffect, useContext } from 'react';
import './Survey.scss';
import 'react-circular-progressbar/dist/styles.css';
import EziLoader from 'components/EziLoader';
import Axios from 'utility/Axios';
import configURL from 'config/config';
import { toast } from 'react-toastify';
import AppContext from 'store/AppContext';
import RecentActivity from 'components/RecentActivity';
import { SURVEY_DASHBOARD_CARDS } from "constants/constants";
import AddNewParticipantModal from './CategorySurveyDashboard/AddNewParticipantModal';
import RecentSurveyCard from 'components/widgets/RecentSurveyCard';

const SurveyDashboard = (props) => {
    const { languageObj = {}, handleUnAuthWarn = {}, accesFeature = {} } = useContext(AppContext)
    const [loading, setLoading] = useState(false);
    const [countData, setCountData] = useState({
        total_surveys: 0,
        active_surveys: 0,
        inactive_surveys: 0,
        closed_surveys: 0
    });
    const [addParticipantMeta, setAddParticipantMeta] = useState({
        survey_id: null,
        visible: false
    });

    const handleCardClick = (status) => {
        props.history.push('survey-dashboard/category-survey-dashboard', {
            category_id: "",
            category_name: "All Categories",
            survey_status: status,
        });
    }

    const getDashboardInitials = () => {
        setLoading(true);
        Axios.post(configURL.survey_dashboard_details, {}).then(response => {
            setLoading(false)
            if (response.data.success !== undefined && response.data.success) {
                setCountData(response.data.results);
            } else {
                toast.warn(response.data.message);
            }
        })

    }

    useEffect(() => {
        getDashboardInitials();
    }, [])

    return (
        <React.Fragment>
            <AddNewParticipantModal
                show={addParticipantMeta.visible}
                onHide={() => setAddParticipantMeta({ survey_id: null, visible: false })}
                survey_id={addParticipantMeta.survey_id}
            />
            {loading && <EziLoader />}
            <section className="Page-SurveyDashboard">
                <div className="st_heading">{languageObj.translate('SurveyDashboard.1')}</div>
                <div className="st_card-wrap">
                    {
                        SURVEY_DASHBOARD_CARDS.map((item, index) =>
                            <div key={item.id} className="st_card" onClick={() => handleCardClick(item.status)}>
                                <div className="st_card-icon-wrap">
                                    <span className={`st_card-icon ${item.card_icon}`}></span>
                                </div>
                                <div className="st_card-text-wrap">
                                    <span className="st_card-text">{item.card_name}</span>
                                    <span className="st_count">{countData[item.data_label]}</span>
                                </div>
                            </div>
                        )
                    }
                </div>
                <div className="action-hightlight-card-wrap">
                    <div className="st_create-exsting-wrap">
                        <div className="st_action-card-wrap">
                            <div className={`st_action-card create-card ${accesFeature.create_survey || "access_lock"}`} onClick={() => {
                                if (accesFeature.create_survey) {
                                    props.history.push('/survey-dashboard/categories', { action: "add-new" });
                                } else {
                                    handleUnAuthWarn()
                                }

                            }}>
                                <div className="st_action-icon-wrap">
                                    <span className="st_action-icon"></span>
                                </div>
                                <label>Create New Survey</label>
                            </div>
                            <div className="st_action-card existing-card" onClick={() => {
                                props.history.push('survey-dashboard/category-survey-dashboard', {
                                    category_id: "",
                                    category_name: "All Categories",
                                });
                            }}>
                                <div className="st_action-icon-wrap">
                                    <span className="st_action-icon"></span>
                                </div>
                                <label>View Existing Survey</label>
                            </div>
                        </div>
                        <RecentSurveyCard />
                    </div>
                    <RecentActivity type="survey" />
                </div>
            </section>
        </React.Fragment>

    )
}

export default SurveyDashboard;