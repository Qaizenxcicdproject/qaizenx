import React from 'react';
import { Modal } from 'react-bootstrap';
import useForm from 'react-hook-form';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { useParams } from "react-router-dom";
import { toast } from 'react-toastify';
import FormData from 'utility/AppFormData';

const AddEmailTemplate = (props) => {
    const { register, handleSubmit, errors } = useForm();
    let urlParams = useParams();

    const onSubmit = data => {
        var formData = new FormData();
        formData.append("channel_type", 'webEmail');
        formData.append("template_name", data.template_name);
        formData.append("subject", data.subject);
        formData.append("from", data.from);
        formData.append("survey_id", urlParams.survey_id);
        formData.append("message", data.message);

        Axios.post(configURL.addtemplates, formData).then(res => {
            if (res.status === 201 && res.data.success !== undefined && res.data.success) {
                toast.success("Email Template added successfully!");
            } else {
                toast.warn("Unable to add template");
            }
            props.onHide();
            props.refreshpublishingsetting();
        })
    };

    return (
        <Modal {...props} size="md" aria-labelledby="contained-modal-title-vcenter" centered className="theme-modal-wrapper" >
            <Modal.Header className="ezi-modal-header">
                <Modal.Title id="contained-modal-title-vcenter" className="theme-modal-title ezi-modal-header-title" >
                    <span className="theme-modal-title-text">Add New Email Template</span>
                    <span className="ezi-modal-close" onClick={props.onHide}></span>
                </Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <form onSubmit={handleSubmit(onSubmit)} className="add-theme-form">
                    <div className="theme-field-wrapper">
                        <div className="theme-field-50">
                            <div className="theme-field">
                                <label>Template Name</label>
                                <input type="text" className="theme-field-control" name="template_name" ref={register({ required: true })} placeholder="Enter Template Name" />
                                {errors.name && <span className="theme-error_cu">*  is required.</span>}
                            </div>
                            <div className="theme-field">
                                <label>From</label>
                                <input type="text" className="theme-field-control" name="from" ref={register({ required: true })} placeholder="Enter From" />
                                {errors.name && <span className="theme-error_cu">*  is required.</span>}
                            </div>
                            <div className="theme-field">
                                <label>Subject Name</label>
                                <input type="text" className="theme-field-control" name="subject" ref={register({ required: true })} placeholder="Enter Subject Name" />
                                {errors.name && <span className="theme-error_cu">*  is required.</span>}
                            </div>
                        </div>
                        <div className="theme-field">
                            <label>Message</label>
                            <textarea rows="4" cols="50" className="theme-field-control" name="message" ref={register} placeholder="Enter Message"></textarea>
                        </div>
                    </div>
                    <div className="theme-modal-footer">
                        <button type="button" className="close-theme-btn" onClick={props.onHide}>Close</button>
                        <input type="submit" value="Save" className="btn-ripple ezi-pink-btn add-theme-btn" />
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    )
}

export default AddEmailTemplate;