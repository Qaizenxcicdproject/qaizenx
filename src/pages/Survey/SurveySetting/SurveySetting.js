import React, { useState, useEffect, useRef, useContext, useCallback } from "react";
import { Tab, Nav, OverlayTrigger, Tooltip } from 'react-bootstrap';
import DatePicker from 'react-date-picker';
import configURL from 'config/config';
import { useParams } from "react-router-dom";
import Axios from "utility/Axios";
import { toast } from 'react-toastify';
import { confirmAlert } from 'react-confirm-alert';
import AppContext from 'store/AppContext';
import { connect } from 'react-redux';
import * as AppAction from "store/actions";
import FormData from 'utility/AppFormData';
import AsyncSelect from 'react-select/async';
import { outlineRemove } from 'utility/helper';
import ReportTreeView from "pages/Survey/SurveySetting/TreeView/ReportTreeView";
import { SketchPicker } from 'react-color';
import useOutsideClick from 'hooks/useOutsideClick';

const SurveySetting = ({ surveyStatus = "draft", dispatchSurveyLogo, vendorInfo, dispatchReloadChannel, dispatchSurveyType, dispatchSurveyLink, dispatchThankEmail }) => {

    const { languageObj = {}, EziLoader, appState } = useContext(AppContext)
    const [settingTab, setSettingTab] = useState('general');
    const [surveyType, setSurveyType] = useState("");
    const [isLoading] = useState(false);
    const [imageSource, setImageSource] = useState({ 'partner_logo': null, 'company_logo': null });
    const [imageFiles, setImageFiles] = useState({ 'partner_logo': '', 'company_logo': '' });
    const [surveyUsers, setSurveyUsers] = useState([]);
    const [viewPartnerLogo, setViewPartnerLogo] = useState(false);
    const [enableCaptcha, setEnableCaptcha] = useState(false);
    const [enableThankEmail, setEnableThankEmail] = useState(false);
    const [viewColorPicker, setViewColorPicker] = useState(false);
    const [hasSurveyChangeAccess, setHasSurveyChangeAccess] = useState(false);
    const [selectedAccess, setSelectedAccess] = useState({});
    const [colorCode, setColorCode] = useState({ hex: '#5A4E63' });
    const [finalColorCode, setfinalColorCode] = useState('#5A4E63');
    const urlParams = useParams()
    const partnerLogoRef = useRef(null)
    const companyLogoRef = useRef(null)
    const isSelectedLoaded = useRef(false)
    var searchTimer = null;
    let colorpickerRef = useRef();
    const [themeColorCode, setThemeColorCode] = useState({ hex: '#5A4E63' });
    const [finalThemeColorCode, setFinalThemeColorCode] = useState('#5A4E63');
    let themecolorpickerRef = useRef(null);
    const [viewThemeColorPicker, setviewThemeColorPicker] = useState(false);
    const [enableGeolocation, setEnableGeolocation] = useState(false);

    useOutsideClick(colorpickerRef, () => {
        viewColorPicker && setViewColorPicker(false);
    });

    useOutsideClick(themecolorpickerRef, () => {
        viewThemeColorPicker && setviewThemeColorPicker(false);
    });

    const [surveySetting, setSurveySetting] = useState({
        survey_type: "",
        start_date: new Date(),
        end_date: new Date(),
        continues_fq: 'always'
    });

    const handleImageUpload = (e) => {
        var name = e.target.name;
        var file = e.target.files[0];
        var reader = new FileReader();
        reader.onload = () => {
            // console.log({ ...imageSource, [name]: reader.result });
            dispatchSurveyLogo({ ...imageSource, [name]: reader.result })
            setImageSource({ ...imageSource, [name]: reader.result });
        }
        reader.readAsDataURL(e.target.files[0]);
        setImageFiles({ ...imageFiles, [name]: file });
    }

    const handleSaveSurveyAccess = () => {
        let usersData;
        if (surveyUsers && surveyUsers.findIndex(el => el.value === 'ALL') >= 0) {
            usersData = 'ALL'
        } else {
            let newArr = surveyUsers.map(el => el.value)
            newArr.push(appState.user.id)
            usersData = JSON.stringify(newArr)
        }
        EziLoader.show()
        let formData = new FormData()
        formData.append("survey_id", urlParams.survey_id);
        formData.append("survey_access_array", usersData);
        Axios.post(configURL.save_survey_users_access, formData).then(res => {
            EziLoader.hide()
            if (res.data.success === true) {
                toast.success(res.data.message || 'Survey access saved.')
            } else {
                toast.warn(res.data.message || 'Something is not right here. Please try again')
            }
        }).catch(err => {
            console.log(err);
            EziLoader.hide()
        })
    }

    const handleSaveReportAccess = () => {
        EziLoader.show()
        let formData = new FormData()
        formData.append("survey_id", urlParams.survey_id);
        formData.append("report_access", JSON.stringify(selectedAccess));
        Axios.post(configURL.save_report_access_users, formData).then(res => {
            EziLoader.hide()
            if (res.data.success === true) {
                toast.success(res.data.message || 'Report access saved.')
            } else {
                toast.warn(res.data.message || 'Something is not right here. Please try again')
            }
        }).catch(err => {
            console.log(err);
            EziLoader.hide()
        })
    }

    const handleSaveSetting = () => {
        switch (settingTab) {
            case 'survey_access':
                handleSaveSurveyAccess()
                break;
            case 'report_access':
                handleSaveReportAccess()
                break;
            default:
                handleStoreSurveySetting()
                break;
        }
    }

    /**
     * Send Survey Setting data to server.
     */
    const handleStoreSurveySetting = () => {
        if (surveySetting.start_date.getTime() > surveySetting.end_date.getTime()) {
            confirmAlert({
                title: 'Date Error!',
                message: 'Start date could not be greater than End Date..',
                buttons: [{ label: 'I Understood' }]
            });
            return;
        }
        if (surveySetting.end_date.getTime() < new Date().getTime() && new Date(new Date(surveySetting.end_date).toDateString()).getTime() !== new Date(new Date().toDateString()).getTime() ) {
            confirmAlert({
                title: 'Date Error!',
                message: "End date could not be less than Today's Date..",
                buttons: [{ label: 'I Understood' }]
            });
            return;
        }
        EziLoader.show()
        let formData = new FormData();
        formData.append("survey_id", urlParams.survey_id);
        if (imageFiles.partner_logo && imageFiles.partner_logo !== '') {
            formData.append("partner_logo", imageFiles['partner_logo']);
        }
        if (imageFiles.company_logo && imageFiles.company_logo !== '') {
            formData.append("company_logo", imageFiles['company_logo']);
        }
        let setting_json = {
            surveyType: surveyType,
            trigger_profile: null,
            startDate: new Date(surveySetting.start_date.getTime() - (surveySetting.start_date.getTimezoneOffset() * 60000)).toJSON(),
            endDate: new Date(surveySetting.end_date.getTime() - (surveySetting.end_date.getTimezoneOffset() * 60000)).toJSON(),
            schedule: { "repeat type": null, "occurence": null, "start on": null },
            number_of_submissions: null,
            reporting: null,
            editable: null,
            SelectionProfile: { "questions": null, "population": null, "SameQuestion": null }
        };
        if (surveyType === 'continuous') {
            setting_json.frequency = surveySetting.continues_fq
        }
        dispatchThankEmail(enableThankEmail)
        formData.append("setting_json", JSON.stringify(setting_json));
        formData.append("view_partner_logo", viewPartnerLogo);
        formData.append("captcha_enabled", enableCaptcha);
        formData.append("survey_thanks_email", enableThankEmail);
        formData.append("header_background_color", finalColorCode);
        formData.append("survey_theme_color", finalThemeColorCode);
        formData.append("enable_survey_geolocation", enableGeolocation);
        Axios.post(configURL.storeSurveySetting, formData).then(res => {
            EziLoader.hide()
            if (res.data.success !== undefined && res.data.success) {
                dispatchReloadChannel(surveyType)
                dispatchSurveyType(surveyType)
                toast.info(res.data.message || "Survey Setting added");
            } else {
                toast.warn(res.data.message || "Something went wrong");
            }
        }).catch(err => {
            EziLoader.hide()
        })
    }

    /**
     * Handle Survey Reset setting
     */
    const handleSurveyResetSetting = () => {
        setSurveyType("");
        setColorCode({ hex: '#5A4E63' });
        setSurveySetting({
            start_date: new Date(),
            end_date: new Date(),
            continues_fq: 'always'
        });
        setImageSource({
            company_logo: null,
            partner_logo: null,
        })
        dispatchSurveyType("")
    }
    

    const isDateDisabled = (date = new Date()) => {
        if (surveyStatus === 'active' || surveyStatus === 'close') {
            return true
        }
        if (surveyStatus === "inactive") {
            return (date !== null && new Date(new Date(date).toDateString()).getTime() < new Date(new Date().toDateString()).getTime()) ? true : false
        }
        if (surveyStatus === "draft") {
            return false
        }
    }
    useEffect(() => {
        EziLoader.show()
        let formData = new FormData()
        formData.append("survey_id", urlParams.survey_id);
        Axios.post(configURL.getSurveySetting, formData).then(res => {
            EziLoader.hide()
            if (res.data.success !== undefined && res.data.success) {
                let start_date = (res.data.result.start_date) ? new Date(res.data.result.start_date) : new Date()
                let end_date = (res.data.result.end_date) ? new Date(res.data.result.end_date) : new Date()
                let hasChangeAccess = (res.data.result.survey_access && res.data.result.survey_access.includes(appState.user.id))
                setHasSurveyChangeAccess(hasChangeAccess)
                setSurveySetting({
                    start_date: start_date,
                    end_date: end_date,
                    continues_fq: res.data.result.frequency || 'always'
                });
                setViewPartnerLogo(res.data.result.view_partner_logo || false)
                setEnableCaptcha(res.data.result.captcha_enabled || false)
                setEnableThankEmail(res.data.result.survey_thanks_email || false)
                dispatchThankEmail(res.data.result.survey_thanks_email || false)
                setColorCode({ hex: res.data.result.header_background_color || '#5A4E63' })
                setfinalColorCode(res.data.result.header_background_color || '#5A4E63')
                setThemeColorCode({ hex: res.data.result.survey_theme_color || '#5A4E63' })
                setFinalThemeColorCode(res.data.result.survey_theme_color || '#5A4E63')
                setEnableGeolocation(res.data.result.enable_survey_geolocation || false)

                setImageSource({
                    company_logo: res.data.result.company_logo_path || null,
                    partner_logo: res.data.result.partner_logo_path || null,
                })
                dispatchSurveyLogo({
                    company_logo: res.data.result.company_logo_path || null,
                    partner_logo: res.data.result.partner_logo_path || null,
                })
                if (res.data.result.survey_type) {
                    dispatchSurveyType(res.data.result.survey_type)
                    setSurveyType(res.data.result.survey_type)
                }
                if (res.data.result.survey_link) {
                    dispatchSurveyLink(res.data.result.survey_link)
                }
            } else {
                toast.warn(res.data.message || "Something went wrong!!");
            }
        }).catch(err => {
            EziLoader.hide()
        })
        return () => toast.dismiss()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const handleDeleteLogo = type => {
        let formData = new FormData()
        formData.append("survey_id", urlParams.survey_id);
        formData.append("type", type);
        Axios.post(configURL.surveyLogoDelete, formData).then(response => {
            if (response.data.success !== undefined && response.data.success === true) {
                toast.success(response.data.message);
                let updatedData;
                if (type === 'partner') {
                    updatedData = { ...imageSource, partner_logo: null }
                    partnerLogoRef.current.value = null
                    setImageSource(updatedData)
                    setImageFiles({ ...imageFiles, partner_logo: null })
                    dispatchSurveyLogo(updatedData)
                }
                if (type === 'company') {
                    companyLogoRef.current.value = null
                    updatedData = { ...imageSource, company_logo: null }
                    setImageSource(updatedData)
                    setImageFiles({ ...imageFiles, company_logo: null })
                    dispatchSurveyLogo(updatedData)
                }
            } else {
                toast.warn(response.data.message);
            }
        })
    }

    const loadCompanyUsers = (inputValue, callback) => {
        clearTimeout(searchTimer);
        searchTimer = setTimeout(() => {
            let formData = new FormData()
            if (inputValue !== "") {
                formData.append("search", inputValue);
            }
            formData.append("survey_id", urlParams.survey_id);
            Axios.post(configURL.survey_users_list, formData).then(res => {
                if (res.data.success === true) {
                    let users = res.data.results.user || []
                    let selected = (res.data.results.selected && Array.isArray(res.data.results.selected)) ? res.data.results.selected : []
                    let modifiedData = users.map(el => {
                        let roles = el.roles.map(elR => elR.name)
                        return { value: el.id, label: `${el.name} ${roles.length > 0 ? `(${roles.toString()})` : ''} ` }
                    })
                    if (!isSelectedLoaded.current) {
                        modifiedData.push({ value: 'ALL', label: 'All Users' })
                        let selectedUsers = selected.map(el => {
                            let roles = el.roles.map(elR => elR.name)
                            return { value: el.id, label: `${el.name} ${roles.length > 0 ? `(${roles.toString()})` : ''} ` }
                        })
                        setSurveyUsers(selectedUsers)
                        isSelectedLoaded.current = true
                    }
                    callback(modifiedData)
                }
            }).catch(err => {
                console.log("Error occured at survey access users load", err.toString());
            })
        }, 300)
    }

    const handleSetSurveyUsers = (data, action) => {
        if (data && data.findIndex(el => el.value === 'ALL') >= 0) {
            setSurveyUsers([{ value: 'ALL', label: 'All Users' }])
        } else {
            setSurveyUsers(data || [])
        }
    }

    const handleSelectedReportAccess = useCallback((data) => {
        setSelectedAccess(data)
    }, [])

    return (
        <div className="tablist_ezi survey-inner-tab-container">
            <Tab.Container activeKey={settingTab} onSelect={k => setSettingTab(k)}>
                <div className="survey-inner-tab-header-wrap">
                    <div className="tab-left-header">
                        <Nav variant="pills">
                            <Nav.Item>
                                <Nav.Link eventKey="general">{languageObj.translate('General.1')}</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="customize">{languageObj.translate('Customize.1')}</Nav.Link>
                            </Nav.Item>
                            {hasSurveyChangeAccess && <Nav.Item>
                                <Nav.Link eventKey="survey_access">Survey Accesses</Nav.Link>
                            </Nav.Item>}
                            {(hasSurveyChangeAccess && surveyStatus !== 'draft') && <Nav.Item>
                                <Nav.Link eventKey="report_access">Report Accesses</Nav.Link>
                            </Nav.Item>}
                        </Nav>
                    </div>
                    <div className="tab-right-header">
                        {isLoading && <div>loading....</div>}
                        <Nav variant="pills" >
                            <Nav.Item>
                                <Nav.Link>
                                    <button
                                        type="button"
                                        className="btn-ripple ezi-pink-btn inner_save_btn"
                                        onClick={handleSaveSetting}
                                    >
                                        {languageObj.translate('Save.1')}
                                    </button>
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link><button type="button" disabled={(surveyStatus === 'active' || surveyStatus === 'close')} className="btn-ripple inner_reset_btn" title={(surveyStatus === 'active' || surveyStatus === 'close') ? 'Survey is ' + surveyStatus : ''} onClick={handleSurveyResetSetting}>{languageObj.translate('Reset.1')}</button></Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </div>
                </div>
                <Tab.Content className="survey-inner-tab-content">
                    <Tab.Pane eventKey="general">
                        <div className="survey-type-wrap">
                            <h1 className="survey-type-heading">{languageObj.translate('SurveyType.1')}</h1>
                            <div className="survey-type-container">
                                <Tab.Container activeKey={surveyType} onSelect={k => setSurveyType(k)}>
                                    <div className="survey-button-tab-header">
                                        <div className="tab-left-header">
                                            <Nav variant="pills" >
                                                <Nav.Item>
                                                    <Nav.Link disabled={(surveyStatus !== 'draft')} eventKey="oneTimeSurvey" className={(surveyStatus === 'draft') ? 'active-link' : ''}>
                                                        {languageObj.translate('OneTime.1')}
                                                        <OverlayTrigger overlay={<Tooltip>Participants can give/submit survey only once.</Tooltip>}>
                                                            <span className="tabpill-info-icon"></span>
                                                        </OverlayTrigger>
                                                    </Nav.Link>
                                                </Nav.Item>
                                                <Nav.Item >
                                                    <Nav.Link disabled={(surveyStatus !== 'draft')} eventKey="continuous" className={(surveyStatus === 'draft') ? 'active-link' : ''}>
                                                        {languageObj.translate('Continuous.1')}
                                                        <OverlayTrigger overlay={<Tooltip>Participant can give/submit survey multiple times depend on the frequency set by survey administrator.</Tooltip>}>
                                                            <span className="tabpill-info-icon"></span>
                                                        </OverlayTrigger>
                                                    </Nav.Link>
                                                </Nav.Item>
                                                <Nav.Item >
                                                    <Nav.Link disabled={(surveyStatus !== 'draft')} eventKey="openSurvey" className={(surveyStatus === 'draft') ? 'active-link' : ''}>
                                                        Open Survey
                                                        <OverlayTrigger overlay={<Tooltip>Survey link is open and any body can respond to the survey using the same link.</Tooltip>}>
                                                            <span className="tabpill-info-icon"></span>
                                                        </OverlayTrigger>
                                                    </Nav.Link>
                                                </Nav.Item>
                                                <Nav.Item >
                                                    <Nav.Link disabled={(surveyStatus !== 'draft')} eventKey="recurring" className={(surveyStatus === 'draft') ? 'active-link' : ''}>
                                                        Recurring  Survey

                                                        <OverlayTrigger overlay={<Tooltip>Survey will recurred base on the settings of recurrence, user can set the recurrence of the survey from Settings</Tooltip>}>
                                                            <span className="tabpill-info-icon"></span>
                                                        </OverlayTrigger>
                                                    </Nav.Link>
                                                </Nav.Item>
                                            </Nav>
                                        </div>
                                    </div>
                                    <div className="startend-date-wrap">
                                        <div className="start-date-wrap">
                                            <label className="startend-label">{languageObj.translate('Starts.1')}</label>
                                            <DatePicker
                                                format={"dd/MM/yyyy"}
                                                selected={surveySetting.start_date}
                                                value={surveySetting.start_date}
                                                minDate={isDateDisabled(surveySetting.start_date) === true ? false : new Date() }
                                                disabled={isDateDisabled(surveySetting.start_date)}
                                                onChange={(date) => setSurveySetting({ ...surveySetting, start_date: date })}
                                                className="sweet-datepicker-custom"
                                            />
                                        </div>
                                        <span className="date-wrapper-icon"></span>
                                        <div className="end-date-wrap">
                                            <label className="startend-label">{languageObj.translate('Ends.1')}</label>
                                            <DatePicker
                                                selected={surveySetting.end_date}
                                                format={"dd/MM/yyyy"}
                                                value={surveySetting.end_date}
                                                minDate={isDateDisabled(surveySetting.start_date) === true ? false : new Date() }
                                                onChange={(date) => setSurveySetting({ ...surveySetting, end_date: date })}
                                                //disabled={isDateDisabled(surveySetting.end_date)}
                                                className="sweet-datepicker-custom"
                                            />
                                        </div>
                                        {surveyType === 'continuous' &&
                                            <div className="frequency-select-wrap">
                                                <label className="frequency-label">Response Frequency</label>
                                                <select className="frequency-select" value={surveySetting.continues_fq} onChange={({ target }) => setSurveySetting({ ...surveySetting, continues_fq: target.value })} >
                                                    <option value="always">Always On</option>
                                                    <option value="daily">Daily once</option>
                                                    <option value="weekly">Once a week</option>
                                                    <option value="monthly">Once a month</option>
                                                    <option value="quarterly">Once a quarter</option>
                                                </select>
                                            </div>
                                        }
                                    </div>
                                </Tab.Container>
                            </div>
                        </div>
                    </Tab.Pane>
                    <Tab.Pane eventKey="customize">
                        <div className="company-logo-main-wrap">
                            {/* <h1 className="company-logo-heading tab-additional-info ">
                                User can set/customize partner and company logo from this section.
                            </h1> */}
                        </div>
                        {surveyType === 'openSurvey' &&
                            <div className="company-logo-main-wrap">
                                <h1 className="company-logo-heading">Enable Captcha</h1>
                                <label className="ezi-checkbox">
                                    <input type="checkbox"
                                        value={enableCaptcha}
                                        onChange={({ target }) => setEnableCaptcha(target.checked)}
                                        checked={enableCaptcha}
                                    />
                                    <span className="ezi-checkbox-mark"></span>
                                </label>
                            </div>
                        }
                        <div className="company-logo-main-wrap">
                            <h1 className="company-logo-heading">Enable Thank You Email</h1>
                            <label className="ezi-checkbox">
                                <input type="checkbox"
                                    value={enableThankEmail}
                                    onChange={({ target }) => setEnableThankEmail(target.checked)}
                                    checked={enableThankEmail}
                                />
                                <span className="ezi-checkbox-mark"></span>
                            </label>
                        </div>
                        <div className="company-logo-main-wrap">
                            <h1 className="company-logo-heading">Enable Geolocation</h1>
                            <label className="ezi-checkbox">
                                <input type="checkbox"
                                    value={enableGeolocation}
                                    onChange={({ target }) => setEnableGeolocation(target.checked)}
                                    checked={enableGeolocation}
                                />
                                <span className="ezi-checkbox-mark"></span>
                            </label>
                        </div>
                        <div className="company-logo-main-wrap">
                            <h1 className="company-logo-heading">View Partner Logo</h1>
                            <label className="ezi-checkbox">
                                <input type="checkbox"
                                    value={viewPartnerLogo}
                                    onChange={({ target }) => setViewPartnerLogo(target.checked)}
                                    checked={viewPartnerLogo}
                                />
                                <span className="ezi-checkbox-mark"></span>
                            </label>
                        </div>

                        <div className="company-logo-main-wrap">
                            <h1 className="company-logo-heading">Header Background Color</h1>
                            <div className='color_picker_container'>
                                <div className="preview_color" style={{ backgroundColor: finalColorCode }}></div>
                                <button styles="background:red" onClick={() => setViewColorPicker(!viewColorPicker)} className={viewColorPicker ? `ezi-pink-btn` : 'btn_gray'}>
                                    Select Color
                                </button>

                                {viewColorPicker && <div ref={colorpickerRef} className='color_picker_content'>
                                    <SketchPicker color={colorCode} onChangeComplete={setColorCode} />
                                    <button onClick={() => {
                                        setfinalColorCode(colorCode.hex || '#5A4E63')
                                        setViewColorPicker(false)
                                    }} className='btn_gray confirm_color_pick'>
                                        Confirm
                                    </button>
                                </div>}

                            </div>
                        </div>
                        <div className="company-logo-main-wrap">
                            <h1 className="company-logo-heading">Survey Theme Color</h1>
                            <div className='color_picker_container'>
                                <div className="preview_color" style={{ backgroundColor: finalThemeColorCode }}></div>
                                <button styles="background:red" onClick={() => setviewThemeColorPicker(!viewThemeColorPicker)} className={viewThemeColorPicker ? `ezi-pink-btn` : 'btn_gray'}>
                                    Select Color
                                </button>
                                {viewThemeColorPicker && <div ref={themecolorpickerRef} className='color_picker_content'>
                                    <SketchPicker color={themeColorCode} onChangeComplete={setThemeColorCode} />
                                    <button onClick={() => {
                                        setFinalThemeColorCode(themeColorCode.hex || '#5A4E63')
                                        setviewThemeColorPicker(false)
                                    }} className='btn_gray confirm_color_pick'>
                                        Confirm
                                    </button>
                                </div>}

                            </div>
                        </div>
                        {viewPartnerLogo && <div className="company-logo-main-wrap">
                            <h1 className="company-logo-heading">{languageObj.translate('PartnerLogo.1')}</h1>
                            <div className="company-logo-container">
                                <div className="company-logo-content">
                                    <div className="default-registered-image">
                                        {imageSource.partner_logo &&
                                            <button type="button" className="customize-logo" onClick={() => handleDeleteLogo('partner')}>X</button>}
                                        <img alt="partner logo" src={imageSource.partner_logo || require(`../../../assets/images/logo.png`)} className="default-registered-logo" />
                                    </div>
                                    <span className="company-logo-devider">{languageObj.translate('or.1')}</span>
                                    <div className="common-img-upload-wrapper">
                                        <span className="common-upload-btn-formatter">{languageObj.translate('Upload.1')}</span>
                                        <input type="file" className="common-upload-input" name="partner_logo" onChange={handleImageUpload} ref={partnerLogoRef} />
                                    </div>
                                </div>
                            </div>
                        </div>}
                        <div className="company-logo-main-wrap">
                            <h1 className="company-logo-heading">{languageObj.translate('CompanyLogo.1')}</h1>
                            <div className="company-logo-container">
                                <div className="company-logo-content">
                                    <div className="default-registered-image">
                                        {imageSource.company_logo &&
                                            <button type="button" className="customize-logo" onClick={() => handleDeleteLogo('company')}>X</button>}
                                        <img alt="" src={imageSource.company_logo || vendorInfo.logo || require(`../../../assets/images/company-logo/company-logo.png`)} className="default-registered-logo" />
                                    </div>
                                    <span className="company-logo-devider">{languageObj.translate('or.1')}</span>
                                    <div className="common-img-upload-wrapper">
                                        <span className="common-upload-btn-formatter">{languageObj.translate('Upload.1')}</span>
                                        <input type="file" className="common-upload-input" name="company_logo" onChange={handleImageUpload} ref={companyLogoRef} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Tab.Pane>
                    {hasSurveyChangeAccess && <Tab.Pane eventKey="survey_access">
                        <div className="survey_access-wrap">
                            <h1 className="survey_access-heading">Please choose users from dropdown list who will have access to this survey</h1>
                            <div className="user-search-container">
                                <AsyncSelect
                                    className="ezi-select-plugin"
                                    styles={outlineRemove}
                                    value={surveyUsers || []}
                                    loadOptions={loadCompanyUsers}
                                    onInputChange={val => val.replace(/\W/g, '')}
                                    isMulti
                                    isSearchable
                                    defaultOptions
                                    onChange={handleSetSurveyUsers}
                                />
                            </div>
                        </div>
                    </Tab.Pane>}
                    {(hasSurveyChangeAccess && surveyStatus !== 'draft') && <Tab.Pane eventKey="report_access" mountOnEnter unmountOnExit>
                        <ReportTreeView handleChange={handleSelectedReportAccess} />
                    </Tab.Pane>}
                </Tab.Content>
            </Tab.Container>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        surveyStatus: state.survey.status,
        vendorInfo: state.app.vendorInfo,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatchSurveyLogo: (surveyLogos = {}) => dispatch(AppAction.setSurveyLogo({ surveyLogos })),
        dispatchReloadChannel: (type) => dispatch(AppAction.reloadChannels({ type })),
        dispatchSurveyType: (type) => dispatch(AppAction.setSurveyType({ type })),
        dispatchSurveyLink: (surveyLink) => dispatch(AppAction.setSurveyLink({ surveyLink })),
        dispatchThankEmail: (thankEmail) => dispatch(AppAction.setThankEmail({ thankEmail })),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SurveySetting);