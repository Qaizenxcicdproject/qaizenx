import React, { useEffect, useState, useContext, useRef } from 'react'
import "./ReportTreeView.scss"
import { cloneDeep } from "lodash"
import AppContext from 'store/AppContext';
import Axios from "utility/Axios";
import { useParams } from "react-router-dom";
import configURL from 'config/config';
import { toast } from 'react-toastify';
import { Spinner } from 'react-bootstrap';

const ReportTreeView = ({ handleChange }) => {
    const [usersIdentifier, setUsersIdentifier] = useState([])
    const [selectedIdentifiers, setSelectedIdentifiers] = useState({})
    const [searching, setSearching] = useState(false)
    const urlParams = useParams()
    const { EziLoader } = useContext(AppContext)
    const searchTimer = useRef(null)
    const selectionLoaded = useRef(false)

    const getReportAccessUsers = (search = "") => {
        let formData = new FormData()
        if (search !== "") {
            formData.append("search", search);
        }
        formData.append("survey_id", urlParams.survey_id);
        Axios.post(configURL.get_report_access_users, formData).then(res => {
            EziLoader.hide()
            setSearching(false)
            if (res.data.success === true) {
                setUsersIdentifier(res.data.results.users_identifier)
                if (!selectionLoaded.current) {
                    selectionLoaded.current = true
                    setSelectedIdentifiers(res.data.results.selected)
                }
            } else {
                toast.warn(res.data.message || 'Something is not right here.')
            }
        }).catch(err => {
            console.log(err);
            setSearching(false)
            EziLoader.hide()
        })
    }

    /**
     * Handle Collapse parent node
     * @param {Mixed} id
     * @param {String} node
     */
    const handleParentSelect = (id) => {
        let surveyData = cloneDeep(usersIdentifier)
        let parentIndex = surveyData.findIndex(el => el.id === id)
        if (parentIndex >= 0) {
            surveyData[parentIndex].isActive = surveyData[parentIndex].isActive ? !surveyData[parentIndex].isActive : true
            setUsersIdentifier(surveyData)
        }
    }

    /**
     * Handle Collapse child nodes
     * @param {Mixed} id
     * @param {String} node
     */
    const handleChildSelect = (id, node) => {
        let surveyData = cloneDeep(usersIdentifier)
        let parentIndex = surveyData.findIndex(el => el.id === id)
        if (parentIndex >= 0) {
            if (surveyData[parentIndex].identifiers && surveyData[parentIndex].identifiers.length > 0) {
                let childIndex = surveyData[parentIndex].identifiers.findIndex(elc => elc.name === node)
                surveyData[parentIndex].identifiers[childIndex].isActive = surveyData[parentIndex].identifiers[childIndex].isActive ? !surveyData[parentIndex].identifiers[childIndex].isActive : true
                setUsersIdentifier(surveyData)
            }
        }
    }

    /**
     * Handle Select All checkbox
     * @param {Mixed} id
     * @param {String} node
     * @param {Event} event
     */
    const handleSelect = (id, node, { target }) => {
        let selectedData = cloneDeep(selectedIdentifiers)
        if (!selectedData[id]) {
            selectedData[id] = {}
        }
        if (!selectedData[id][node]) {
            selectedData[id][node] = []
        }
        let oldOptions = selectedData[id][node] || []
        if (target.checked) {
            oldOptions.push(target.value)
            selectedData[id][node] = [...new Set(oldOptions)]
        }
        if (!target.checked) {
            let index = oldOptions.indexOf(target.value)
            if (index > -1) oldOptions.splice(index, 1)
            selectedData[id][node] = [...new Set(oldOptions)]
        }
        setSelectedIdentifiers(selectedData)
        /** Pass updated data into parent component */
        handleChange(selectedData)
    }

    /**
     * Handle Select All options
     * @param {Mixed} id
     * @param {String} node
     * @param {Event} event
     */
    const handleSelectAll = (id, node, { target }) => {
        let selectedData = cloneDeep(selectedIdentifiers)
        if (!selectedData[id]) {
            selectedData[id] = {}
        }
        if (!selectedData[id][node]) {
            selectedData[id][node] = []
        }
        if (target.checked) {
            let dataNode = usersIdentifier.find(el => el.id === id)
            if (dataNode && dataNode.identifiers) {
                let dataIndex = dataNode.identifiers.findIndex(el => el.name === node)
                if (dataIndex >= 0) {
                    selectedData[id][node] = [...new Set(dataNode.identifiers[dataIndex].options)]
                }
            }
        }
        if (!target.checked) {
            selectedData[id][node] = []
        }
        setSelectedIdentifiers(selectedData)
        /** Pass updated data into parent component */
        handleChange(selectedData)
    }

    /**
     * Determine whether checkbox are checked or not
     * @param {Mixed} id
     * @param {String} node
     * @param {String} value
     * @returns {Boolean} true/false
     */
    const isChecked = (id, node, value) => {
        return (selectedIdentifiers[id] && selectedIdentifiers[id][node] && selectedIdentifiers[id][node].includes(value)) ? true : false
    }

    /**
     * Determine whether all options are checked or not
     * @param {Mixed} id 
     * @param {String} node 
     * @returns {Boolean} true/false
     */
    const isCheckedAll = (id, node) => {
        let dataNode = usersIdentifier.find(el => el.id === id)
        if (dataNode && dataNode.identifiers && selectedIdentifiers[id] && selectedIdentifiers[id][node]) {
            let dataIndex = dataNode.identifiers.findIndex(el => el.name === node)
            if (dataIndex >= 0) {
                return (selectedIdentifiers[id][node].length === dataNode.identifiers[dataIndex].options.length) ? true : false
            }
        }
        return false
    }

    /**
     * Handle Search list
     * @param {Event} param0 
     */
    const handleListSearch = ({ target }) => {
        let inputVal = target.value
        clearTimeout(searchTimer.current)
        setSearching(true)
        searchTimer.current = setTimeout(() => {
            getReportAccessUsers(inputVal)
        }, 500)
    }

    useEffect(() => {
        EziLoader.show()
        getReportAccessUsers()
        return () => toast.dismiss()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    return (
        <div className="TreeView">
            <div className="search_list">
                <input type="text" className="search_user_list" onChange={handleListSearch} placeholder="Search users by name" />
                {searching && <Spinner className="burgundy-spinner" animation="border" size="sm" />}
            </div>
            <ul id="parent-list">
                {
                    usersIdentifier.map((el, i) => (
                        <li key={i}><span className={`caret ${(el.isActive && el.isActive === true) ? 'caret-down' : ''}`} onClick={handleParentSelect.bind(null, el.id)}>{el.label || null}</span>
                            <ul className={`nested ${(el.isActive && el.isActive === true) ? 'active' : ''}`}>
                                {
                                    (el.identifiers && el.identifiers.length > 0) && el.identifiers.map((elC, index) => (
                                        <li key={i + index}><span className={`caret ${(elC.isActive && elC.isActive === true) ? 'caret-down' : ''}`} onClick={handleChildSelect.bind(null, el.id, elC.name)}>{elC.label}</span>
                                            <ul className={`nested ${(elC.isActive && elC.isActive === true) ? 'active' : ''}`}>
                                                <li>
                                                    <label>
                                                        <input type="checkbox" onChange={handleSelectAll.bind(null, el.id, elC.name)} checked={isCheckedAll(el.id, elC.name)} />
                                                        <span>Select All</span>
                                                    </label>
                                                </li>
                                                {(elC.options && elC.options.length > 0) && elC.options.map((elCO, indexo) => (
                                                    <li key={i + index + indexo}>
                                                        <label>
                                                            <input type="checkbox" value={elCO} onChange={handleSelect.bind(null, el.id, elC.name)} checked={isChecked(el.id, elC.name, elCO)} />
                                                            <span>{elCO}</span>
                                                        </label>
                                                    </li>
                                                ))}
                                            </ul>
                                        </li>
                                    ))
                                }
                            </ul>
                        </li>
                    ))
                }
            </ul>
            {usersIdentifier.length === 0 && <p className="empty_list">No data found...</p>}
        </div>
    )
}
export default React.memo(ReportTreeView)
