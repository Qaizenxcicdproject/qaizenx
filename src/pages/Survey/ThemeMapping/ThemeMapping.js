import React, { useState, useEffect, useContext } from 'react';
import configURL from 'config/config';
import Axios from 'utility/Axios';
import { useParams } from "react-router-dom";
import { toast } from 'react-toastify';
import AppContext from 'store/AppContext';
import FormData from 'utility/AppFormData';

const ThemeMapping = (props) => {
    const { languageObj = {} } = useContext(AppContext)
    let urlParams = useParams();
    const [themeQue, setThemeQue] = useState([]);
    const [surveyThemeDs, setSurveyThemeDs] = useState([]);
    const [surveySubthemeDs, setSurveySubthemeDs] = useState({});
    const [mappedData, setMappedData] = useState();
    const [currentPage, setCurrentPage] = useState(1);
    const [quePerPage] = useState(10);
    const pageNumbers = [];
    const [surveyQuestionListing, setSurveyQuestionListing] = useState([]);

    for (let i = 1; i <= Math.ceil(surveyQuestionListing.length / quePerPage); i++) {
        pageNumbers.push(i);
    }

    const themeDs = () => {
        let formData = new FormData();
        formData.append("category_id", props.category_id);
        Axios.post(configURL.surveythemeDs, formData).then(res => {
            if (res.data.success !== undefined && res.data.success) {
                setSurveyThemeDs(res.data.data);
                setEmptySubthemeObj(res.data.data);
            } else {
                setSurveyThemeDs([]);
            }
        })
    }

    const subthemeDs = (themeId) => {
        let formData = new FormData();
        formData.append("theme_id", themeId);
        Axios.post(configURL.surveySubThemeDs, formData).then(res => {
            if (res.data.success !== undefined && res.data.success) {
                setSurveySubthemeDs({ ...surveySubthemeDs, [themeId]: res.data.data });
            } else {
                setSurveySubthemeDs({ ...surveySubthemeDs, [themeId]: [] });
            }
        })
    }

    const setEmptySubthemeObj = (data) => {
        let tempSubthemeObj = {};
        data.forEach(item => {
            tempSubthemeObj[item.id] = []
        })
        setSurveySubthemeDs(tempSubthemeObj);
    }

    const setEmptyMappingObj = (data) => {
        let tempMappingobj = {};
        Object.keys(data).map(key => {
            tempMappingobj[key] = {
                'theme_id': (data[key]["mapping"] && data[key]["mapping"]["theme_id"]) ? data[key]["mapping"]["theme_id"] : "",
                'subtheme_id': (data[key]["mapping"] && data[key]["mapping"]["subtheme_id"]) ? data[key]["mapping"]["subtheme_id"] : "",
            }
        }
        )
        setMappedData(tempMappingobj);
    }
    const loadInitialSubThemes = (data = []) => {
        let formData = new FormData();
        formData.append("theme_id", JSON.stringify(data));
        Axios.post(configURL.subThemesData, formData).then(res => {
            if (res.status == 201 && res.data.success) {
                let result = res.data.data;
                setSurveySubthemeDs((prevState) => {
                    return { ...prevState, ...result }
                });
            }
        })
    }

    const questionListing = () => {
        let formData = new FormData();
        formData.append("survey_id", urlParams.survey_id);
        Axios.post(configURL.surveyQuestionListing, formData).then(res => {
            if (res.data.success !== undefined && res.data.success) {
                let Response = res.data.data;
                setEmptyMappingObj(Response);
                setSurveyQuestionListing(Response);
                let themes = [];
                Object.keys(Response).forEach((item) => {
                    if (Response[item]["mapping"] && Response[item]["mapping"]["theme_id"]) {
                        themes.push(Response[item]["mapping"]["theme_id"]);
                    }
                });
                loadInitialSubThemes(themes);
            } else {
                setSurveyQuestionListing([]);
            }
        })
    }

    const onThemeChange = ({ target }) => {
        let queId = target[target.selectedIndex].getAttribute('data-queid');
        setMappedData({
            ...mappedData, [queId]: {
                ...mappedData[queId], 'theme_id': target.value, 'subtheme_id': ""
            }
        });
        setSurveyQuestionListing((prevState) => {
            return {
                ...prevState, [queId]: {
                    ...prevState[queId], mapping: {
                        ...prevState[queId]["mapping"], ["theme_id"]: target.value, 'subtheme_id': ""
                    }
                }
            }
        });
        if (target.value !== '') {
            subthemeDs(target.value);
        } else {
            setSurveySubthemeDs({ ...surveySubthemeDs, [target.value]: [] });
        }
    }

    const onSubthemeChange = ({ target }) => {
        let queId = target[target.selectedIndex].getAttribute('data-queid');
        setMappedData((prevState) => {
            return {
                ...prevState, [queId]: {
                    ...prevState[queId], 'subtheme_id': target.value
                }
            }
        });

        setSurveyQuestionListing((prevState) => {
            return {
                ...prevState, [queId]: {
                    ...prevState[queId], mapping: {
                        ...prevState[queId]["mapping"], ["subtheme_id"]: target.value
                    }
                }
            }
        });

    }

    const saveColumnMapping = () => {
        let formData = new FormData();
        formData.append("survey_id", urlParams.survey_id);
        formData.append("mapping", JSON.stringify(mappedData));
        Axios.post(configURL.surveyQuestionMapping, formData).then(res => {
            if (res.status === 201 && res.data.success) {
                toast.success("Theme Mapping updated !");
            } else {
                toast.warn("Questions are not mapped with themes.");
            }
        })
    }

    useEffect(() => {
        questionListing();
        themeDs();
        Axios.get('/card.json').then(res => {
            setThemeQue(res.data);
        })
    }, []);

    useEffect(() => {

    }, [surveyQuestionListing, mappedData, surveySubthemeDs])

    const indexOfLastQuestion = currentPage * quePerPage;
    const indexOfFirstQuestion = indexOfLastQuestion - quePerPage;
    const currentQuestion = themeQue.slice(indexOfFirstQuestion, indexOfLastQuestion);
    const paginate = pageNumber => {
        setCurrentPage(pageNumber);
    }

    return (
        <React.Fragment>
            <section className="Page-ThemeMapping">

                <div className="question-wrapper-section">
                    <div className="questions-header">
                        <span className="question-heading">{languageObj.translate('ThemeMapping.1')}</span>
                        <div className="question-save-wrap">
                            <button type="button" className="btn-ripple ezi-pink-btn question-save" onClick={saveColumnMapping}>{languageObj.translate('Save.1')}</button>
                            <button type="button" className="question-reset">{languageObj.translate('Reset.1')}</button>
                        </div>
                    </div>
                    {
                        Object.keys(surveyQuestionListing).map(key =>
                            <div className="question-wrapper" key={key}>

                                <p className="question-list">{surveyQuestionListing[key]["name"]} </p>
                                <div className="question-theme-select-wrap">
                                    <select name="theme" className="question-theme-select" onChange={(e) => onThemeChange(e)} value={(surveyQuestionListing[key]["mapping"] && surveyQuestionListing[key]["mapping"]["theme_id"]) ? surveyQuestionListing[key]["mapping"]["theme_id"] : ""}>
                                        <option data-queid={key} value="">Select theme</option>
                                        {
                                            surveyThemeDs.map((theme) =>
                                                <option data-queid={key} key={theme.id} value={theme.id}>{theme.name}</option>
                                            )
                                        }
                                    </select>
                                    <select name="subtheme" className="question-subtheme-select" onChange={(e) => onSubthemeChange(e)} value={(surveyQuestionListing[key]["mapping"] && surveyQuestionListing[key]["mapping"]["subtheme_id"]) ? surveyQuestionListing[key]["mapping"]["subtheme_id"] : ""}>
                                        <option value="" data-queid={key}>Select subtheme</option>
                                        {
                                            surveySubthemeDs[mappedData[key]['theme_id']] && surveySubthemeDs[mappedData[key]['theme_id']].map((subtheme) =>
                                                <option data-queid={key} key={subtheme.id} value={subtheme.id}>{subtheme.name}</option>
                                            )
                                        }
                                    </select>
                                </div>
                            </div>
                        )
                    }

                    <div className="question-pagination-wrap">
                        <ul className='question-pagination'>
                            {pageNumbers.map(number => (
                                <li key={number} className={`question-page-item ${(currentPage == number) ? "active" : ""}`}>
                                    <div onClick={() => paginate(number)} className='question-page-link'>
                                        {number}
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>

                </div>

            </section>
        </React.Fragment>

    )

}

export default ThemeMapping;