import React from "react";
import Axios from "utility/Axios";
import configURL from 'config/config';
import { outlineRemove } from 'utility/helper';
import AsyncSelect from 'react-select/async';
import { toast } from "react-toastify";

const IdentifierSelection = ({ saveState, stateData = {}, nextStep, prevStep }) => {
    const { selected_identifier = null } = stateData

    /**
     * Handle Next page navigation
     */
    const handleNext = () => {
        if (!selected_identifier) {
            toast.warn('Please select identifier.')
            return false
        }
        nextStep()
    }

    /**
     * get all identifiers
     */
    const loadSurveyIdentifiers = (_, callback) => {
        let formData = new FormData()
        formData.append("survey_id", stateData?.selected_survey?.value || null);
        Axios.post(configURL.get_survey_identifiers, formData).then(response => {
            if (response.status === 200 && response.data.success === true) {
                const { results: survey_identifiers = [] } = response.data
                const mapped_identifiers = survey_identifiers.map(el => ({ value: el.id, label: el.identifier_name, type: el.type }))
                callback(mapped_identifiers)
            } else {
                toast.info(response.data.message || "Something went wrong!!")
            }
        }).catch(err => {
            console.log(err);
        })
    }

    return (<div className="user-search-container">
        <div className="async-select-container">
            <AsyncSelect
                className="ezi-select-plugin"
                styles={outlineRemove}
                value={selected_identifier || null}
                loadOptions={loadSurveyIdentifiers}
                onInputChange={val => val.replace(/\W/g, '')}
                defaultOptions
                isMulti={false}
                placeholder="Please select identifier"
                isSearchable={false}
                onChange={(data) => {
                    saveState({ selected_identifier: data, trigger_data: null })
                }}
            />
        </div>
        <div className="navigation_btn_wrapper">
            <button className="stepper-btn  prev_btn" onClick={prevStep}>Previous</button>
            <button className="stepper-btn  next_btn" onClick={handleNext}>Save & Next</button>
        </div>
    </div>
    )
}

export default IdentifierSelection