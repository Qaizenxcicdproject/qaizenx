import React, { useState, useEffect, useContext } from "react";
import { useParams } from "react-router-dom";
import Axios from "utility/Axios";
import configURL from 'config/config';
import useForm from 'react-hook-form';
import { Spinner } from 'react-bootstrap';
import { Breadcrumb } from "react-bootstrap";
import { toast } from "react-toastify";
import AppContext from 'store/AppContext';
import DatePicker from 'react-date-picker';
import "./SurveyWorkflow.scss"
import { confirmAlert } from 'react-confirm-alert';

const SaveSurveyWorkflow = ({ history }) => {
    const [loading, setLoading] = useState(false);
    const { workflow_id = "" } = useParams();
    const [workflowData, setWorkflowData] = useState({
        workflow_name: "",
        workflow_description: "",
        start_date: new Date(),
        end_date: new Date()
    });
    const { register, handleSubmit, errors } = useForm();
    const { EziLoader } = useContext(AppContext)

    /**
     * Save Survey workflow initial details
     */
    const saveSurveyWorkflow = (data) => {
        setLoading(true)
        let formData = new FormData()
        if (workflowData.start_date.getTime() > workflowData.end_date.getTime()) {
            confirmAlert({
                title: 'Date Error!',
                message: 'Start date could not be greater than End Date..',
                buttons: [{ label: 'I Understood' }]
            });
            setLoading(false)
            return;
        }
        if (workflowData.end_date.getTime() < new Date().getTime() && new Date(new Date(workflowData.end_date).toDateString()).getTime() !== new Date(new Date().toDateString()).getTime() ) {
            confirmAlert({
                title: 'Date Error!',
                message: "End date could not be less than Today's Date..",
                buttons: [{ label: 'I Understood' }]
            });
            setLoading(false)
            return;
        }
        if (workflow_id && workflow_id !== ""){
            formData.append("workflow_id", workflow_id)
        }
        formData.append("start_date", new Date(workflowData.start_date.getTime() - (workflowData.start_date.getTimezoneOffset() * 60000)).toJSON())
        formData.append("end_date", new Date(workflowData.end_date.getTime() - (workflowData.end_date.getTimezoneOffset() * 60000)).toJSON())
        formData.append("workflow_name", data.workflow_name)
        formData.append("workflow_description", data.workflow_description)

        Axios.post(configURL.save_survey_workflow, formData).then(response => {
            setLoading(false)
            if (response?.data?.success === true) {
                toast.success(response?.data?.message || 'Workflow saved successfully')
                let savedWorkFlowId;
                if (response?.data?.workflow_id || workflow_id)
                    savedWorkFlowId = response?.data?.workflow_id || workflow_id || null
                if (savedWorkFlowId) history.push(`/survey-workflow/save-settings/${savedWorkFlowId}`)
            } else {
                toast.warn(response?.data?.message || 'Something went wrong.')
            }
        }).catch(error => {
            setLoading(false)
            console.log(error);
            toast.warn('Something went wrong.')
        })
    // }
    }

    /**
     * Handle input change
     * @param {Object} target 
     */
    const handleFormChange = ({ target }) => {
        let name = target.name;
        let value = target.value;
        setWorkflowData({
            ...workflowData,
            [name]: value
        })
    }


    /**
     * Get workflow data
     */
    const getSurveyWorkflowData = () => {
        if (workflow_id && workflow_id !== "") {

            EziLoader.show()
            let formData = new FormData()
            formData.append("workflow_id", workflow_id)
            Axios.post(configURL.view_workflow_detail, formData).then(response => {
                EziLoader.hide()
                if (response?.data?.success === true) {
                    let { name = "", description = "", start_date = "", end_date = "" } = response.data.result
                    setWorkflowData({
                        workflow_name: name || "",
                        workflow_description: description || "",
                        start_date: (start_date === "" || !start_date) ? new Date() : new Date(start_date),
                        end_date: (end_date === "" || !end_date) ? new Date() : new Date(end_date)
                    })
                } else {
                    toast.warn(response?.data?.message || 'Something went wrong.')
                }
            }).catch(error => {
                EziLoader.hide()
                console.log(error);
                toast.warn('Something went wrong.')
            })
        }
    }

    useEffect(getSurveyWorkflowData, [])

    return (
        <React.Fragment>
            <section className="Page-SaveSurveyWorkflow">
                <div className="breadcrumb_ezi save-workflow-beadcrumb">
                    <Breadcrumb>
                        <Breadcrumb.Item onClick={() => history.push('/survey-workflow')}>Survey Workflows</Breadcrumb.Item>
                        <Breadcrumb.Item>{workflow_id ? 'Update workflow' : 'Create workflow'}</Breadcrumb.Item>
                    </Breadcrumb>
                </div>

                <form onSubmit={handleSubmit(saveSurveyWorkflow)}>
                    <div className="save-workflow-wrap">
                        <h1 className="workflow-label"> {workflow_id ? 'Update workflow' : 'Create new workflow'} </h1>
                        <div className="workflow-field-control">
                            <input type="text" placeholder='Workflow name' name="workflow_name" ref={register({ required: true })} className="create-workflow-input" onChange={handleFormChange} defaultValue={workflowData.workflow_name || ''} />
                            {errors.workflow_name && <span className="error-message">Please enter workflow name</span>}
                        </div>
                        <div className="workflow-field-control">
                            <div className="startend-date-wrap">
                                <div className="start-date-wrap">
                                    <label className="startend-label">Start</label>
                                    <DatePicker
                                        format={"dd/MM/yyyy"}
                                        value={workflowData.start_date}
                                        minDate={new Date(new Date(workflowData.start_date).toDateString()).getTime() < new Date(new Date().toDateString()).getTime() ? false : new Date()}
                                        onChange={(date) => {
                                            setWorkflowData({ ...workflowData, start_date: date })
                                        }}
                                        disabled={ new Date(new Date(workflowData.start_date).toDateString()).getTime() < new Date(new Date().toDateString()).getTime() ? true : false }
                                        className="sweet-datepicker-custom"
                                    />
                                </div>
                                <span className="date-wrapper-icon"></span>
                                <div className="end-date-wrap">
                                    <label className="startend-label">End</label>
                                    <DatePicker
                                        format={"dd/MM/yyyy"}
                                        value={workflowData.end_date}
                                        minDate={new Date(new Date(workflowData.end_date).toDateString()).getTime() < new Date(new Date().toDateString()).getTime() ? false : new Date()}
                                        onChange={(date) => {
                                            setWorkflowData({ ...workflowData, end_date: date })
                                        }}
                                        className="sweet-datepicker-custom"
                                    />
                                </div>
                            </div>
                            <span className="date-wrapper-icon"></span>
                        </div>
                        <div className="workflow-field-control">
                            <textarea maxLength="250" rows="4" name="workflow_description" ref={register({ required: true })} className="create-workflow-input" placeholder="Workflow description" onChange={handleFormChange} defaultValue={workflowData.workflow_description || ''}></textarea>
                            {errors.workflow_description && <span className="error-message">Please enter workflow description</span>}
                        </div>
                        <div className="workflow_add_btn_wrap">
                            <button type="submit" className="btn-ripple workflow_add_btn" disabled={loading}>
                                Save workflow {loading && <Spinner className="burgundy-spinner" animation="border" size="sm" />}
                            </button>
                        </div>
                    </div>
                </form>
            </section>
        </React.Fragment>
    )
}

export default SaveSurveyWorkflow;