import React, { Fragment, useState } from "react";
import Axios from "utility/Axios";
import configURL from 'config/config';
import { Spinner } from "react-bootstrap"
import { toast } from "react-toastify";
import { useParams, useHistory } from "react-router-dom";
import WorkflowChecklist from "components/WorkflowChecklist";
import TimePicker from 'react-time-picker';

const SetWorkflowDetails = ({ saveState, stateData = {}, prevStep }) => {
    const { selected_identifier = {}, trigger_data = {}, selected_survey = {} } = stateData
    const [saving, setSaving] = useState(false)
    const [showConfirmation, setShowConfirmation] = useState(false)
    const { workflow_id = "" } = useParams();
    const urlRouter = useHistory();

    /**
     * Handle Action Change
     */
    const handleActionChange = ({ target }) => {
        let data
        if (target.value === 'sameday' && target.name === 'action_type') {
            data = { trigger_data: { ...trigger_data, [target.name]: target.value, action_value: null } }
        } else {
            data = { trigger_data: { ...trigger_data, [target.name]: target.value } }
        }
        saveState(data)
    }

    const handleShowConfirmation = () => {
        if (!trigger_data || !trigger_data.action_type || !trigger_data.trigger_time) {
            toast.warn('Please choose trigger action and trigger timing')
            return false
        }
        setShowConfirmation(true)
    }
    /**
     * Save Workflow trigger data
     */
    const handleWorkflowSave = () => {
        setShowConfirmation(false)
        setSaving(true)
        let formData = new FormData()
        formData.append("workflow_setting", JSON.stringify({ selected_survey, selected_identifier, trigger_data }))
        formData.append("workflow_status", 'active')
        if (workflow_id && workflow_id !== "")
            formData.append("workflow_id", workflow_id)

        Axios.post(configURL.save_survey_workflow, formData).then(response => {
            setSaving(false)
            if (response.data.success === true) {
                toast.success(response.data.message || 'Workflow trigger saved successfully.')
                urlRouter.push('/survey-workflow')
            } else {
                toast.warn(response.data.message || 'Something went wrong.')
            }
        }).catch(err => {
            setSaving(false)
            console.log(err);
        })
    }

    return (
        <div className="workflow_setting">
            <h3>Set Identifier Trigger for {(selected_identifier.label) ? selected_identifier.label.toUpperCase() : ''}</h3>
            <div className='trigger_content'>
                <div className="trigger_setting">
                    {
                        (() => {
                            switch (selected_identifier.type) {
                                case 'datetime':
                                    return (
                                        <Fragment>
                                            <div className="workflow_fields_date_action">
                                                <label>Trigger Action</label>
                                                <div className="workflow_fields_datetype">
                                                    <div className="datetype_action">
                                                        <select defaultValue={trigger_data ? trigger_data.action_type : ''} name="action_type" onChange={handleActionChange}>
                                                            <option value="">Please choose</option>
                                                            <option value="after">After</option>
                                                            <option value="sameday">Same Day</option>
                                                            <option value="before">Before</option>
                                                        </select>
                                                    </div>

                                                    {(trigger_data?.action_type !== 'sameday') &&
                                                        <div className="datetype_action_input_value">
                                                            <input type="number" defaultValue={trigger_data ? trigger_data.action_value : 0} name="action_value" onChange={handleActionChange} />
                                                            <span>Days</span>
                                                        </div>
                                                    }

                                                </div>
                                            </div>
                                            <div className="workflow_fields_date_time">
                                                <label>Trigger Time</label>
                                                <TimePicker
                                                    disableClock={true}
                                                    format="h:m: a"
                                                    closeClock={true}
                                                    onChange={(data) => {
                                                        saveState({ trigger_data: { ...trigger_data, trigger_time: data } })
                                                    }}
                                                    value={(trigger_data && trigger_data.trigger_time) ? trigger_data.trigger_time : null}
                                                />
                                            </div>
                                        </Fragment>
                                    )

                                default:
                                    return <span style={{ color: '#ff9966' }}>Option Disabled</span>
                            }
                        })()
                    }
                </div>
            </div>
            <div className="navigation_btn_wrapper_setting">
                <button className="stepper-btn prev_btn" onClick={prevStep}>Previous</button>
                <button className="stepper-btn next_btn" onClick={handleShowConfirmation} disabled={saving}>
                    Save Workflow {saving && <Spinner animation="border" size="sm" />}
                </button>
            </div>
            <WorkflowChecklist show={showConfirmation} onHide={() => setShowConfirmation(false)} onSubmit={handleWorkflowSave} workFlowData={stateData} />
        </div>
    )
}

export default SetWorkflowDetails