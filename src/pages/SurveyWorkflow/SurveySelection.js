import React from "react";
import Axios from "utility/Axios";
import configURL from 'config/config';
import { outlineRemove } from 'utility/helper';
import AsyncSelect from 'react-select/async';
import { toast } from "react-toastify";

const SurveySelection = ({ saveState, stateData = {}, nextStep }) => {
    const { selected_survey = null } = stateData
    let searchTimer = null;

    /**
     * Handle next step navigation
     */
    const handleNext = () => {
        if (!selected_survey) {
            toast.warn('Please select survey first.')
            return false
        }
        nextStep()
    }

    /**
     * Get surveys list
     */
    const loadActiveSurveys = (inputValue, callback) => {
        clearTimeout(searchTimer);
        searchTimer = setTimeout(() => {
            let formData = new FormData()
            if (inputValue !== "") {
                formData.append("search", inputValue);
            }
            Axios.post(configURL.get_active_surveys, formData).then(response => {
                if (response.data.success === true) {
                    let actualData = response.data.results.map(el => ({ value: el.id, label: el.name }))
                    callback(actualData)
                }
            }).catch(err => {
                console.log("Error occured at survey access users load", err.toString());
            })
        }, 300)
    }

    return (
        <div className="user-search-container">
            <div className="async-select-container">
                <AsyncSelect
                    className="ezi-select-plugin"
                    styles={outlineRemove}
                    placeholder="Please select survey"
                    value={selected_survey || null}
                    loadOptions={loadActiveSurveys}
                    onInputChange={val => val.replace(/\W/g, '')}
                    isMulti={false}
                    isSearchable
                    defaultOptions
                    onChange={(data) => {
                        saveState({ selected_identifier: null, selected_survey: data })
                    }}
                />
            </div>
            <div className="navigation_btn_wrapper">
                <button onClick={handleNext} className="stepper-btn next_button_step1">Save & Next</button>
            </div>
        </div>
    )
}

export default SurveySelection