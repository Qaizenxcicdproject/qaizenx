import React, { useState, useEffect, useRef, useContext } from "react";
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { CircularProgressbar } from 'react-circular-progressbar';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { getFirstWord } from 'utility/helper';
import 'utility/i18next.js'
import './SurveyWorkflow.scss';
import { toast } from "react-toastify";
import SweetSearch from "components/SweetSearch";
import Pagination from "react-js-pagination";
import { confirmAlert } from 'react-confirm-alert';
import AppContext from 'store/AppContext';
import FormData from 'utility/AppFormData';

const SurveyWorkflow = ({ history }) => {
    const { EziLoader } = useContext(AppContext)
    const [surveyWorkflows, setSurveyWorkflows] = useState([]);
    const [pagination, setPagination] = useState({});
    const [searchLoading, setSearchLoading] = useState(false);
    const [workflowStatus, setWorkflowStatus] = useState('all');
    const [workflowDashCounts, setWorkflowDashCounts] = useState({
        active_workflow: 0,
        inactive_workflow: 0,
        closed_workflow: 0,
        total_workflow: 0
    });
    const perPage = 10;
    var searchTimer = null;
    const inputSearch = useRef(null);
    const currentPage = useRef(1);

    /**
     * Get Survey Workflows
     */
    const getSurveyWorkflows = (type = workflowStatus) => {
        let searchVal = inputSearch.current.value;
        let formData = new FormData();
        formData.append("per_page", perPage);
        formData.append("page", currentPage.current);
        formData.append("status", type);
        if (searchVal !== "")
            formData.append("search", searchVal);
        Axios.post(configURL.get_survey_workflows, formData).then(response => {
            EziLoader.hide()
            setSearchLoading(false)
            if (response.data.success !== undefined && response.data.success === true) {
                setSurveyWorkflows(response.data.results);
                setPagination(response.data.pagination);
            } else {
                toast.warn(response.data.message);
            }
        }).catch(err => {
            console.log(err)
            EziLoader.hide()
        })
    }

    /**
     * Get Survey Workflows Stats
     */
    const getSurveyWorkflowsStats = () => {
        Axios.post(configURL.survey_workflow_stats, {}).then(response => {
            if (response.data.success !== undefined && response.data.success === true) {
                setWorkflowDashCounts(response.data.results);
            }
        }).catch(err => {
            console.log(err)
        })
    }

    /**
     * Delete Workflow
     * @param {number} workflow_id Unique ID
     */
    const deleteSurveyWorkflow = (workflow_id = "") => {
        confirmAlert({
            title: 'Delete Workflow',
            message: 'Are you sure you want to delete ?',
            buttons: [
                {
                    label: 'Confirm',
                    onClick: () => {
                        let formData = new FormData();
                        formData.append("workflow_id", workflow_id);
                        Axios.post(configURL.delete_survey_workflow, formData).then(response => {
                            if (response.data.success !== undefined && response.data.success === true) {
                                toast.success(response.data.message || 'Workflow deleted successfully.')
                                if (surveyWorkflows.length === 1)
                                    currentPage.current = (currentPage.current > 1) ? currentPage.current - 1 : 1
                                getSurveyWorkflows()
                            } else {
                                toast.warn(response.data.message || 'Something went wrong here.');
                            }
                        }).catch(err => {
                            console.log(err)
                        })
                    }
                },
                {
                    label: 'Cancel'
                }
            ]
        });
    }

    /**
     * Filter Data based on search.
     * 
     * @param {string} type Input value
     */
    const handleFilterSearch = () => {
        clearTimeout(searchTimer);
        searchTimer = setTimeout(() => {
            setSearchLoading(true)
            currentPage.current = 1
            getSurveyWorkflows();
        }, 800);
    }

    /**
     * Filter Data based on status.
     * 
     * @param {string} type Filter value
     */
    const handleFilterByStatus = (type = 'all') => {
        setWorkflowStatus(type)
        EziLoader.show()
        getSurveyWorkflows(type)
    }

    /**
     * Handle Pagination
     * 
     * @param {string} type Filter Type
     */
    const handlePagination = (page = 1) => {
        EziLoader.show()
        currentPage.current = page
        getSurveyWorkflows();
    }

    /**
     * Handle Workflow Status Change
     *
     * @param {string} status Status
     */
    const handleStatusChange = ({ status = 'inactive', id = null }) => {
        if (status === 'closed') {
            toast.info('Workflow is already closed.')
            return false
        }
        let newStatus = (status === 'active') ? 'inactive' : 'active'
        confirmAlert({
            title: 'Workflow Update',
            message: `Are you sure you want to change status from ${status} to ${newStatus}`,
            buttons: [
                {
                    label: 'Confirm',
                    onClick: () => {
                        let formData = new FormData()
                        formData.append("workflow_status", newStatus)
                        formData.append("workflow_id", id)
                        Axios.post(configURL.save_survey_workflow, formData).then(response => {
                            if (response.data.success === true) {
                                getSurveyWorkflows()
                                toast.success(response.data.message || 'Workflow saved successfully.')
                            } else {
                                toast.warn(response.data.message || 'Something went wrong.')
                            }
                        }).catch(err => {
                            console.log(err);
                        })
                    }
                },
                {
                    label: 'Cancel'
                }
            ]
        });
    }

    useEffect(() => {
        EziLoader.show()
        getSurveyWorkflowsStats()
        getSurveyWorkflows()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <React.Fragment>
            <section className="Page-SurveyWorkflow">
                <div className="workflow-dashboard-card-wrap">
                    <div className={`workflow-dashboard-card total_workflow`} onClick={() => handleFilterByStatus('all')} >
                        <div className="workflow-dashboard-card_text_wrap">
                            <p className="workflow-dashboard-card-heading">{workflowDashCounts.total_workflow || 0}</p>
                            <span className="workflow-dashboard-card-subheading">Total Workflows</span>
                        </div>
                        <div className="category_progress_ring_wrap">
                            <div className="category_progress_ring">
                                {
                                    (workflowDashCounts.active_workflow === 0 && workflowDashCounts.total_workflow === 0) ?
                                        <CircularProgressbar className="category_progress_circle" value={0} text={`0%`} /> :
                                        <CircularProgressbar className="category_progress_circle" value={Math.ceil((workflowDashCounts.active_workflow / workflowDashCounts.total_workflow) * 100) || 0} text={`${Math.ceil((workflowDashCounts.active_workflow / workflowDashCounts.total_workflow) * 100)}%`
                                        } />
                                }
                            </div>
                            <span className="circular-survey-text active--text">Active Workflows</span>
                        </div>
                    </div>
                    <div className={`workflow-dashboard-card active_workflow ${(workflowStatus === 'active') ? 'active' : ''}`} onClick={() => handleFilterByStatus('active')}>
                        <div className="workflow-dashboard-card_text_wrap">
                            <p className="workflow-dashboard-card-heading">{workflowDashCounts.active_workflow || 0}</p>
                            <span className="workflow-dashboard-card-subheading">Active Workflows</span>
                        </div>
                    </div>
                    <div className={`workflow-dashboard-card inactive_workflow ${(workflowStatus === 'inactive') ? 'active' : ''}`} onClick={() => handleFilterByStatus('inactive')}>
                        <div className="workflow-dashboard-card_text_wrap">
                            <p className="workflow-dashboard-card-heading">{workflowDashCounts.inactive_workflow || 0}</p>
                            <span className="workflow-dashboard-card-subheading">Inactive Workflows</span>
                        </div>
                    </div>
                    <div className={`workflow-dashboard-card closed_workflow ${(workflowStatus === 'closed') ? 'active' : ''}`} onClick={() => handleFilterByStatus('closed')}>
                        <div className="workflow-dashboard-card_text_wrap">
                            <p className="workflow-dashboard-card-heading">{workflowDashCounts.closed_workflow || 0}</p>
                            <span className="workflow-dashboard-card-subheading">Closed Workflows</span>
                        </div>
                    </div>
                    <div className={`workflow-dashboard-card create-workflow--c`} onClick={() => history.push('/survey-workflow/save')} >
                        <span className="create-workflow--ic"></span>
                        <p className="create-workflow--name">Create new workflow</p>
                    </div>
                </div>
                <div className="filter-search-wrap">
                    <SweetSearch loading={searchLoading} change={handleFilterSearch} ref={inputSearch} />
                </div>
                <div className="workflow-dashboard-table">
                    <div className="workflow-dashboard-table-row workflow-table-heading">
                        <div className="workflow-dashboard-table-cell">Workflow</div>
                        <div className="workflow-dashboard-table-cell">Created By</div>
                        <div className="workflow-dashboard-table-cell">Status</div>
                        <div className="workflow-dashboard-table-cell">Action</div>
                    </div>
                    {
                        surveyWorkflows.length > 0 ? surveyWorkflows.map((item, index) => {
                            return (
                                <div key={index} className="workflow-dashboard-table-row">
                                    <div className="workflow-dashboard-table-cell" data-title="Survey">
                                        <div className={`workflow-table-survey-wrap`} >
                                            <div className="workflow-table-survey-text-wrap">
                                                <span className="workflow-table-survey-name">{item.name || ''}</span>
                                                <span className="workflow-table-create">
                                                    Last Modified : {getFirstWord(item.updated_at)}   |   Created on : {getFirstWord(item.created_at)}
                                                </span>
                                                <span className="workflow-table-create">
                                                    Started at : {getFirstWord(item.start_date) || "---"}   |  Ending on : {getFirstWord(item.end_date) || "---"}
                                                </span>
                                                <span className="workflow-table-create">Description : {item.description}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="workflow-dashboard-table-cell" data-title="Owner"><span className="owner-name">{item.owner || "---"}</span></div>
                                    <div className={`workflow-dashboard-table-cell table-status`} data-title="Status" onClick={() => handleStatusChange(item)}>
                                        <span className={`dt-status-formatter ${item.status || 'inactive'}`}>{item.status || 'inactive'}</span>
                                    </div>

                                    <div className={`workflow-dashboard-table-cell`} data-title="Action">
                                        <div className="action-wrap">
                                            <OverlayTrigger overlay={<Tooltip>Edit Workflow</Tooltip>}>
                                                <button type="button" className="edit_data" onClick={() => {
                                                    history.push(`/survey-workflow/save/${item.id}`)
                                                }}></button>
                                            </OverlayTrigger>
                                            <OverlayTrigger overlay={<Tooltip>Delete Workflow</Tooltip>}>
                                                <button type="button" className="delete_data" onClick={() => deleteSurveyWorkflow(item.id)}></button>
                                            </OverlayTrigger>
                                        </div>
                                    </div>
                                </div>
                            )
                        }) : null
                    }
                </div>
                {surveyWorkflows.length <= 0 && <div className="workflow-table-no-result">No result Found</div>}
                <div className="pagination-plugin-wrap category-pagination-formatter">
                    <Pagination
                        activePage={pagination.current_page}
                        itemsCountPerPage={perPage}
                        totalItemsCount={pagination.total || 0}
                        onChange={handlePagination}
                        hideDisabled={true}
                        firstPageText={<span class="prev-page-text-ic"></span>}
                        lastPageText={<span class="next-page-text-ic"></span>}
                        nextPageText={<span class="next-text-ic"></span>}
                        prevPageText={<span class="prev-text-ic"></span>}
                    />
                </div>
            </section>
        </React.Fragment>
    )
}

export default SurveyWorkflow;