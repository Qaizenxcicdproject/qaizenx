import React, { useState, useEffect, useContext } from "react";
import { useParams } from "react-router-dom";
import { Breadcrumb } from 'react-bootstrap';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from "react-toastify";
import AppContext from 'store/AppContext';
import "./SurveyWorkflow.scss"
import MultiStepContent from '../../components/steps-indicator/MultiStepContent'
import SurveySelection from './SurveySelection'
import IdentifierSelection from './IdentifierSelection'
import SetWorkflowDetails from './SetWorkflowDetails'

const WorkflowSettings = ({ history }) => {
    const { workflow_id = "" } = useParams();
    const [workflowData, setWorkflowData] = useState({
        workflow_name: "",
        workflow_description: "",
        start_date: new Date(),
        end_date: new Date()
    });
    const { EziLoader } = useContext(AppContext)
    const workflowSteps = [
        { name: 'step-1', label: 'Survey Selection', component: SurveySelection },
        { name: 'step-2', label: 'Identifier Selection', component: IdentifierSelection },
        { name: 'step-3', label: 'Workflow Trigger Setting', component: SetWorkflowDetails },
    ]

    /**
     * Get Perticular template types.
     * 
     * @param {string} type template types
     * @param {number} page Page number
     */

    /**
     * Get workflow data
     */
    const getSurveyWorkflowData = () => {
        if (workflow_id && workflow_id !== "") {
            EziLoader.show()
            let formData = new FormData()
            formData.append("workflow_id", workflow_id)
            Axios.post(configURL.view_workflow_detail, formData).then(response => {
                EziLoader.hide()
                if (response?.data?.success === true) {
                    let { name = "", description = "", start_date = "", end_date = "", setting_json = {} } = response.data.result
                    setWorkflowData({
                        workflow_name: name,
                        workflow_description: description,
                        start_date: (start_date === "" || !start_date) ? new Date() : new Date(start_date),
                        end_date: (end_date === "" || !end_date) ? new Date() : new Date(end_date),
                        ...setting_json
                    })
                } else {
                    toast.warn(response?.data?.message || 'Something went wrong.')
                }
            }).catch(error => {
                EziLoader.hide()
                console.log(error);
                toast.warn('Something went wrong.')
            })
        }
    }

    useEffect(getSurveyWorkflowData, [])
    return (
        <React.Fragment>
            <section className="Page-WorkFlowSetting" >
                <div className="breadcrumb_ezi">
                    <Breadcrumb>
                        <Breadcrumb.Item onClick={() => history.push("/survey-workflow")}>Survey Workflows</Breadcrumb.Item>
                        <Breadcrumb.Item onClick={() => history.goBack()}>Save Setting</Breadcrumb.Item>
                    </Breadcrumb>
                </div>
                <header className="workflow-info-header">
                    <div className="workflow-header-col workflow-info-text">
                        <span className="title_c">{workflowData.workflow_name || ''}</span>
                        <span className="subtitle_c">{workflowData.workflow_description || ''}</span>
                    </div>
                    <div className="workflow-header-col workflow-info-desc">
                        <div className="startend-date-wrap">
                            <div className="start-date-wrap">
                                <label className="startend-label">Start</label>
                                <span className="date-text">{workflowData.start_date.toDateString()}</span>
                            </div>
                            <span className="date-wrapper-icon"></span>
                            <div className="end-date-wrap">
                                <label className="startend-label">End</label>
                                <span className="date-text">{workflowData.end_date.toDateString()}</span>
                            </div>
                        </div>
                        <span className="date-wrapper-icon"></span>
                    </div>
                </header>
                <main className="workflow-info-content">
                    <MultiStepContent steps={workflowSteps} defaultData={workflowData} />
                </main>
            </section>
        </React.Fragment>
    )
}

export default WorkflowSettings;