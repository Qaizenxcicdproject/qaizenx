import React, { useState, useEffect, useRef, useContext } from "react";
import { CircularProgressbar } from 'react-circular-progressbar';
import { Breadcrumb, OverlayTrigger, Tooltip } from 'react-bootstrap';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { getFirstWord } from 'utility/helper'
import './CategoryTemplates.scss';
import EziLoader from 'components/EziLoader';
import { toast } from "react-toastify";
import CategoryFilter from "components/CategoryFilter";
import SweetSearch from "components/SweetSearch";
import Pagination from "react-js-pagination";
import { confirmAlert } from 'react-confirm-alert';
import AppContext from 'store/AppContext';
import FormData from 'utility/AppFormData';

const CategoryTemplates = (props) => {

    const { languageObj = {}, handleUnAuthWarn = {}, accesFeature = {} } = useContext(AppContext)
    const [defaultCategory, setDefaultCategory] = useState({ ...props.location.state, description: "" });
    const [categoryMetaData, setCategoryMetaData] = useState({})
    const [categories, setCategories] = useState([]);
    const [categoryTemplates, setCategoryTemplates] = useState([]);
    const [pagination, setPagination] = useState({});
    const [filterType, setFilterType] = useState(props.location.state.template_status || "all");
    const perPage = 10;
    const [loading, setLoading] = useState(false);
    const [searchLoading, setSearchLoading] = useState(false);
    const inputSearch = useRef(null);
    var searchTimer = null;

    /**
     * Get Selected/Default category MetaData.
     */
    const getCategoryMetaData = async () => {
        setLoading(true);
        let formData = new FormData();
        formData.append("category_id", defaultCategory.category_id);
        let response = await Axios.post(configURL.category_templates_metadata_url, formData);
        if (response.data.success !== undefined && response.data.success === true) {
            setCategoryMetaData(response.data.results, getCategoryTemplates());
        } else {
            setLoading(false);
            toast.warn(response.data.message);
        }
    }

    /**
     * Get Searched category.
     * 
     * @param {string} search Search string.
     */
    const getCategoriesListing = (search = "") => {
        let formData = new FormData();
        formData.append("search", search);
        formData.append("type", "template");
        Axios.post(configURL.categoryDs, formData).then(response => {
            if (response.data.success !== undefined && response.data.success === true) {
                let result = response.data.results;
                const categoryData = [];
                result.forEach(item => {
                    categoryData.push({ id: item.id, name: item.category_name, description: item.description });
                });
                setCategories(categoryData);
            } else {
                toast.warn(response.data.message);
            }
        })
    }

    /**
     * Get Category templates.
     * 
     * @param {object} obj category object
     */
    const changeTemplatesCategory = (obj = {}) => {
        setDefaultCategory({
            category_id: obj.id || null,
            category_name: obj.name || null,
            description: obj.description || ""
        })
        props.history.replace("/template-dashboard/category-templates", {
            category_id: obj.id,
            category_name: obj.name
        })
    }

    /**
     * Get Perticular template types.
     * 
     * @param {string} type template types
     * @param {number} page Page number
     */
    const getCategoryTemplates = (page = 1, type = filterType) => {
        let searchVal = inputSearch.current.value;
        let formData = new FormData();
        formData.append("category_id", defaultCategory.category_id);
        formData.append("per_page", perPage);
        formData.append("page", page);
        formData.append("type", type);
        formData.append("search", searchVal);
        Axios.post(configURL.category_templates_url, formData).then(response => {
            setLoading(false);
            setSearchLoading(false)
            if (response.data.success !== undefined && response.data.success === true) {
                setCategoryTemplates(response.data.results);
                setPagination(response.data.pagination);
            } else {
                toast.warn(response.data.message);
            }
        })
    }

    /**
     * Filter Data based on search.
     * 
     * @param {string} type Input value
     */
    const handleFilterSearch = ({ target }) => {
        clearTimeout(searchTimer);
        searchTimer = setTimeout(() => {
            setSearchLoading(true);
            getCategoryTemplates(1, filterType);
        }, 800);
    }

    /**
     * Filter Data based on Click.
     * 
     * @param {string} type Filter Type
     */
    const handleFilterChange = (type = "all") => {
        setLoading(true);
        setFilterType(type);
        getCategoryTemplates(1, type);
    }

    /**
     * Handle Template Delete
     * 
     * @param {number} id Survey Id
     */
    const handleTemplateDelete = (id) => {
        confirmAlert({
            title: 'Delete Template',
            message: 'Are you sure you want to delete ?',
            buttons: [
                {
                    label: 'Confirm',
                    onClick: () => {
                        let filteredData = categoryTemplates.filter(item => item.id !== id);
                        setCategoryTemplates(filteredData);
                        let formData = new FormData();
                        formData.append("id", id);
                        Axios.post(configURL.delete_template, formData).then(response => {
                            if (response.data.success !== undefined && response.data.success === true) {
                                toast.success(response.data.message);
                            } else {
                                toast.warn(response.data.message);
                            }
                        })
                    }
                },
                {
                    label: 'Cancel'
                }
            ]
        });
    }

    /**
     * Handle Pagination
     * 
     * @param {string} type Filter Type
     */
    const handlePagination = (page = 1) => {
        setLoading(true);
        getCategoryTemplates(page, filterType);
    }

    const handleCreateTemplate = () => {
        if (defaultCategory.category_id && defaultCategory.category_id !== "") {
            props.history.push(`/template-dashboard/template-add/${defaultCategory.category_id}`, {
                action: "create-template",
                category_name: defaultCategory.category_name
            })
        } else {
            props.history.push('/template-dashboard/template-category', {
                action: "create-new"
            });
        }
    }

    const handleCreateSurvey = (template_id) => {
        if (accesFeature.create_survey && template_id) {
            let formData = new FormData();
            formData.append("template_id", template_id)
            Axios.post(configURL.create_survey_cat_by_template, formData).then(response => {
                if (response.data.success !== undefined && response.data.success) {
                    let result = response.data.result
                    props.history.push(`/survey-dashboard/add-survey/${result.category_id}`, {
                        template_id: template_id,
                        action: "add-survey",
                        category_name: result.category_name
                    });
                } else {
                    toast.warn(response.data.message)
                }
            })
        } else {
            handleUnAuthWarn()
        }
    }

    /**
     * Edit Template Data
     * @param {Object} item 
     */
    const handleEditTemplate = (item = {}) => {
        if (accesFeature.update_template !== true) {
            handleUnAuthWarn()
            return
        }
        if (item.is_partner_template && item.is_editable === false) {
            props.history.push("/template-dashboard/template-questions/" + item.category_id + "/" + item.id, {
                action: "edit-template",
                canModify: false
            });
        }
        if (item.is_editable === true) {
            props.history.push(`/template-dashboard/edit-template/${item.category_id}/${item.id}`, {
                action: "edit-template",
                category_name: defaultCategory.category_name
            })
        }
    }

    /**
     * Copy Template Data
     * @param {Object} item
     */
    const handleCopyTemplate = (data) => {
        if (accesFeature.create_template !== true) {
            handleUnAuthWarn()
            return
        }
        if (data.is_editable === false) {
            toast.warn(`Template (${data.name}) is not editable`)
            return;
        }
        if (data.is_editable === true) {
            props.history.push(`/template-dashboard/edit-template/${data.category_id}/${data.id}`, {
                action: "clone-template",
                category_name: defaultCategory.category_name
            })
        }
    }

    /* eslint-disable */
    useEffect(() => {
        getCategoryMetaData()
    }, [defaultCategory]);
    useEffect(() => {
        getCategoriesListing()
    }, []);
    /* eslint-enable */

    return (
        <React.Fragment>
            <section className="Page-CompanyCategoryDashboard">

                <div className="breadcrumb_ezi">
                    <Breadcrumb>
                        <Breadcrumb.Item onClick={() => props.history.push("/template-dashboard")}>
                            {languageObj.translate('Template.1')}
                        </Breadcrumb.Item>
                        <Breadcrumb.Item onClick={() => props.history.goBack()}>{defaultCategory.category_name}</Breadcrumb.Item>
                        <Breadcrumb.Item>Templates</Breadcrumb.Item>
                    </Breadcrumb>
                    <CategoryFilter handleSelect={changeTemplatesCategory} data={categories} defaultSelected={defaultCategory.category_name} />
                    <span className="template-category-subtitle"> {(defaultCategory.description !== undefined) ? defaultCategory.description : ''} </span>
                </div>

                <div className="category-dashboard-card-wrap">
                    <div className={`category-dashboard-card total_template ${(filterType === "all") && "active"}`} onClick={() => handleFilterChange("all")}>
                        <div className="category-dashboard-card_text_wrap">
                            <p className="category-dashboard-card-heading">{categoryMetaData.total_templates || 0}</p>
                            <span className="category-dashboard-card-subheading">{languageObj.translate('TotalTemplates.1')}</span>
                        </div>
                        <div className="category_progress_ring_wrap">
                            <div className="category_progress_ring">
                                <CircularProgressbar className="category_progress_circle" value={categoryMetaData.published_template_percentage || 0} text={`${parseInt(categoryMetaData.published_template_percentage) || 0}%`}
                                />
                            </div>
                            <span className="publish-templates-text">{languageObj.translate('PublishedTemplates.1')}</span>
                            <span className="unpublish-templates-text">Unpublished Templates</span>
                        </div>
                    </div>
                    <div className={`category-dashboard-card active_template ${(filterType === "active") && "active"}`} onClick={() => handleFilterChange("active")}>
                        <div className="category-dashboard-card_text_wrap">
                            <p className="category-dashboard-card-heading">{categoryMetaData.active_templates || 0}</p>
                            <span className="category-dashboard-card-subheading">Active Templates</span>
                        </div>
                    </div>
                    <div className={`category-dashboard-card publish_template ${(filterType === "published") && "active"}`} onClick={() => handleFilterChange("published")}>
                        <div className="category-dashboard-card_text_wrap">
                            <p className="category-dashboard-card-heading">{categoryMetaData.published_templates || 0}</p>
                            <span className="category-dashboard-card-subheading">Published</span>
                        </div>
                    </div>
                    <div className={`category-dashboard-card unpublish_template ${(filterType === "unpublished") && "active"}`} onClick={() => handleFilterChange("unpublished")}>
                        <div className="category-dashboard-card_text_wrap">
                            <p className="category-dashboard-card-heading">{categoryMetaData.unpublished_templates || 0}</p>
                            <span className="category-dashboard-card-subheading">Unpublished</span>
                        </div>
                    </div>

                    <div className={`category-dashboard-card create-survey-template ${accesFeature.create_template || "access_lock"}`} onClick={() => {
                        if (accesFeature.create_template) {
                            handleCreateTemplate()
                        } else {
                            handleUnAuthWarn()
                        }
                    }}>
                        <span className="create-survey-template-ic"></span>
                        <p className="create-survey-template-name">Create {(defaultCategory.category_id && defaultCategory.category_id !== "") ? defaultCategory.category_name : ""} Template</p>
                    </div>
                </div>

                <SweetSearch loading={searchLoading} change={handleFilterSearch} ref={inputSearch} />

                <div className="category-dashboard-table">

                    <div className="category-dashboard-table-row category-table-heading">
                        <div className="category-dashboard-table-cell"> Template Name </div>
                        <div className="category-dashboard-table-cell"> Owner </div>
                        <div className="category-dashboard-table-cell"> Status </div>
                        <div className="category-dashboard-table-cell"> Is Active </div>
                        {/* <div className="category-dashboard-table-cell"> Responses </div> */}
                        <div className="category-dashboard-table-cell"> Action </div>
                    </div>
                    {
                        categoryTemplates.length > 0 ? categoryTemplates.map((item, index) => {
                            return (
                                <div key={index} className="category-dashboard-table-row">
                                    <div className="category-dashboard-table-cell" data-title="Template Name">
                                        <div className={`category-table-template-wrap ${accesFeature.update_template || "access_lock"}`} onClick={() => handleEditTemplate(item)}>
                                            {/* <img alt="" src={imageUrl} className="category-template-logo" /> */}
                                            <div className="category-table-template-text-wrap">
                                                <span className="category-table-template-name">{item.name}</span>
                                                <span className="category-table-create">Last Modified: {getFirstWord(item.updated_at)}   |   Created on: {getFirstWord(item.created_at)}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="category-dashboard-table-cell" data-title="Owner"> {item.owner_name || "Dummy"} </div>
                                    <div className="category-dashboard-table-cell table-status" data-title="Status"> {(item.ispublished) ? "Published" : "Unpublished"} </div>
                                    <div className="category-dashboard-table-cell table-status" data-title="Is Active"><span className={`isactive ${item.status}`}>{item.status}</span></div>
                                    {/* <div className="category-dashboard-table-cell " data-title="Responses"> <span className="table-count">{item.response || 0}</span></div> */}
                                    <div className="category-dashboard-table-cell" data-title="Action">
                                        <OverlayTrigger overlay={<Tooltip>{item.ispublished && item.status === 'active' ? 'Create Survey' : `Template is ${(item.ispublished) ? "Published" : "Unpublished"} & ${item.status}`}</Tooltip>}>
                                            <button type="button" className={`create_template ${accesFeature.create_survey || "access_lock"}`} onClick={() => {
                                                if (item.ispublished && item.status === 'active') {
                                                    handleCreateSurvey(item.id)
                                                }
                                            }} ></button>
                                        </OverlayTrigger>
                                        <OverlayTrigger overlay={<Tooltip>Copy Template</Tooltip>}>
                                            <button
                                                type="button"
                                                className={`clone_template ${accesFeature.create_template || "access_lock"}`}
                                                onClick={() => handleCopyTemplate(item)}
                                            >
                                            </button>
                                        </OverlayTrigger>
                                        <button type="button" className={`delete_data ${accesFeature.delete_template || "access_lock"}`} onClick={() => {
                                            if (accesFeature.delete_template) {
                                                handleTemplateDelete(item.id)
                                            } else {
                                                handleUnAuthWarn()
                                            }
                                        }}></button>
                                    </div>
                                </div>
                            )
                        }) : null
                    }
                </div>
                {/* <div className="not-found category-dashboard-table-row"><h4 className="category-dashboard-table-cell" style={{ textAlign: "center" }}>No result Found</h4></div> */}
                {categoryTemplates.length <= 0 && <div className="category-table-no-result">No Result Found</div>}
                <div className="pagination-plugin-wrap category-pagination-formatter">
                    <Pagination
                        activePage={pagination.current_page}
                        itemsCountPerPage="10"
                        totalItemsCount={pagination.total}
                        onChange={handlePagination}
                        hideDisabled={true}
                        firstPageText={<span class="prev-page-text-ic"></span>}
                        lastPageText={<span class="next-page-text-ic"></span>}
                        nextPageText={<span class="next-text-ic"></span>}
                        prevPageText={<span class="prev-text-ic"></span>}
                    />
                </div>
            </section>
            {loading && <EziLoader />}
        </React.Fragment>
    )
}

export default CategoryTemplates;