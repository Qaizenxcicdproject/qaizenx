import React, { useState, useEffect, useContext, useRef } from 'react';
import { Breadcrumb } from 'react-bootstrap';
import Axios from 'utility/Axios';
import configURL from 'config/config';
import Skeleton from 'components/Skeleton';
import Pagination from "react-js-pagination";
import NoResult from 'components/NoResult';
import AppContext from 'store/AppContext';
import { confirmAlert } from "react-confirm-alert";
import EditCategoryModel from 'components/EditCategoryModel';
import { toast } from 'react-toastify';
import InfoModal from 'components/HeaderInfoModal';
import FormData from 'utility/AppFormData';
import './TemplateCategory.scss';

const TemplateCategory = (props) => {
    const { languageObj = {} } = useContext(AppContext)
    const [catCards, setCatCards] = useState([]);
    const [loading, setLoading] = useState(false);
    const [pagination, setPagination] = useState(false);
    const [infoModal, setInfoModal] = useState(false);
    const [categoryEditMeta, setCategoryEditMeta] = useState({
        visible: false,
        data: null,
        type: "template"
    });
    const [perPage] = useState((props.location.state.action === "edit-one") ? 8 : 7);

    const categoryDeleteRef = useRef()

    const deleteCategory = () => {
        if (categoryDeleteRef.current.visible && categoryDeleteRef.current.id !== "") {
            let formData = new FormData();
            formData.append("id", categoryDeleteRef.current.id)
            formData.append("type", "template")
            confirmAlert({
                title: 'Delete Category',
                message: "Are you sure you want to delete this category ?",
                buttons: [
                    {
                        label: 'Confirm',
                        onClick: () => {
                            Axios.post(configURL.delete_category, formData).then(response => {
                                categoryDeleteRef.current = null
                                if (response.data.success === true) {
                                    toast.success(response.data.message || "Category Deleted!")
                                    categoryListing()
                                } else {
                                    toast.warn(response.data.message)
                                }
                            }).catch(err => {
                                categoryDeleteRef.current = null
                            })
                        }
                    },
                    {
                        label: 'Cancel',

                    }
                ]
            });
        }

    }

    const handleDeleteCategory = (e, id) => {
        categoryDeleteRef.current = null
        let formData = new FormData();
        formData.append("id", id)
        formData.append("type", "survey")
        confirmAlert({
            title: 'Delete Category',
            message: "All the templates and themes linked to this category will be deleted ?",
            buttons: [
                {
                    label: 'Confirm',
                    onClick: () => categoryDeleteRef.current = { visible: true, id }
                },
                {
                    label: 'Cancel',
                    onClick: () => categoryDeleteRef.current = null
                }
            ],
            afterClose: () => deleteCategory()
        });
    }

    const categoryListing = (page = 1) => {
        setLoading(true);
        let formData = new FormData();
        formData.append("page", page);
        formData.append("per_page", perPage);
        formData.append("type", "template");
        Axios.post(configURL.categoryListing, formData).then(res => {
            if (res.data.success !== undefined && res.data.success) {
                setCatCards(res.data.data);
                setPagination(res.data.pagination)
                setLoading(false);
            }
        })
    }

    const handlePaginate = (page = 1) => {
        categoryListing(page)
    }
    /* eslint-disable */
    useEffect(() => {
        categoryListing()
    }, []);
    /* eslint-enable */
    return (
        <React.Fragment>
            <section className="Page-TemplateCategory">
                <EditCategoryModel visible={categoryEditMeta.visible} categoryData={categoryEditMeta.data} onHide={() => {
                    setCategoryEditMeta({ ...categoryEditMeta, visible: false, data: null })
                }} type={categoryEditMeta.type} updateList={categoryListing} />
                <div className="breadcrumb_ezi">
                    <Breadcrumb>
                        <Breadcrumb.Item onClick={() => props.history.push("/template-dashboard")}>
                            {languageObj.translate('Template.1')}
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>{languageObj.translate('TemplateCategories.1')}</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="column-header">
                        <h1 className="page-heading">{languageObj.translate('Categories.1')} </h1>
                        <button className="header-info-btn" onClick={() => setInfoModal(true)}></button>
                    </div>
                </div>
                <div className="template_card_c-wrap">
                    {
                        (props.location.state.action === "create-new") && <div className="template_card_c" onClick={() => {
                            props.history.push('/template-dashboard/add-category');
                        }}>
                            <span className="template_card_add_ic"></span>
                            <label className="template_card_add-label">{languageObj.translate('CreateNew.1')}</label>
                            <p className="template_card_c-des">{languageObj.translate('Categories.1')}</p>
                        </div>
                    }
                    {loading
                        ?
                        <Skeleton value={perPage} />
                        :
                        catCards.map((item, index) => (
                            <div className="template_card_c" key={item.id} onClick={() => {
                                if (props.location.state.action === "edit-one") {
                                    props.history.push('/template-dashboard/category-templates', {
                                        category_id: item.id,
                                        category_name: item.category_name
                                    });
                                }
                                else {
                                    props.history.push(`/template-dashboard/template-add/${item.id}`, {
                                        action: "create-template",
                                        category_name: item.category_name
                                    });
                                }
                            }}>
                                <div className="category-icon-wrapper">
                                    <span className={`category-edit_ic`} onClick={(e) => {
                                        e.stopPropagation()
                                        setCategoryEditMeta({ ...categoryEditMeta, visible: true, data: item })
                                    }}></span>
                                    <span className={`category-delte_ic`} onClick={(e) => {
                                        e.stopPropagation()
                                        handleDeleteCategory(e, item.id)
                                    }}></span>
                                </div>
                                <label className="template_card_c-label">{item.category_name}</label>
                                <p className="template_card_c-des">{item.description}</p>
                            </div>
                        ))
                    }
                </div>
                {(catCards.length === 0 && loading === false) && <NoResult />}
                {(pagination && pagination.total > perPage) && <div className="pagination-plugin-wrap">
                    <Pagination
                        activePage={pagination.current_page}
                        itemsCountPerPage={perPage}
                        totalItemsCount={pagination.total}
                        onChange={handlePaginate}
                        hideDisabled={true}
                        firstPageText={<span class="prev-page-text-ic"></span>}
                        lastPageText={<span class="next-page-text-ic"></span>}
                        nextPageText={<span class="next-text-ic"></span>}
                        prevPageText={<span class="prev-text-ic"></span>}
                    />
                </div>}
            </section>
            <InfoModal
                show={infoModal}
                title='Template Categories'
                description='It helps to create and organize the various survey templates by categories. By default few templates under different categories are published for you to use as per purchased package. These templates are developed in association with industry experts and consultants in that particular domain.'
                onHide={() => setInfoModal(false)}
            />
        </React.Fragment>
    )
}

export default TemplateCategory;