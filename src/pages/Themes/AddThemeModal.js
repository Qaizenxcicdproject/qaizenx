import React from "react";
import { Modal } from 'react-bootstrap';
import useForm from 'react-hook-form';
import Axios from "utility/Axios";
import configURL from 'config/config';
import { toast } from 'react-toastify';
import FormData from 'utility/AppFormData';

function AddThemeModal(props) {
    const { register, handleSubmit, errors } = useForm();

    const onSubmit = data => {
        data[props.commonidname] = props.commonid;
        const sendData = new FormData();
        let addData = {
            'category': props.name,
            'data': data,
        }
        sendData.append("type", props.tab);
        sendData.append("insertdata", JSON.stringify(addData));
        Axios.post(configURL.addTheme, sendData).then(res => {
            if (res.data.success !== undefined && res.data.success) {
                toast.success(res.data.message);
                props.onHide();
                props.updatethemelisting();
            } else {
                toast.warn(res.data.message || "Something went wrong.")
            }
        })
    };

    return (

        <Modal {...props} size="md" aria-labelledby="contained-modal-title-vcenter" centered className="theme-modal-wrapper" >
            <Modal.Header className="ezi-modal-header">
                <Modal.Title id="contained-modal-title-vcenter" className="theme-modal-title ezi-modal-header-title" >
                    <span className="theme-modal-title-text">Add New {props.fieldname} </span>
                    <span className="ezi-modal-close" onClick={props.onHide}></span>
                </Modal.Title>
            </Modal.Header>

            <Modal.Body>

                <form onSubmit={handleSubmit(onSubmit)} className="add-theme-form">
                    <div className="theme-field-wrapper">
                        <div className="theme-field-50">
                            <div className="theme-field">
                                <label>{props.fieldname}</label>
                                <input type="text" className="theme-field-control" name="name" ref={register({ required: true })} placeholder="Enter Theme Name" />
                                {errors.name && <span className="theme-error_cu">* {props.fieldname} is required.</span>}
                            </div>
                            <div className="theme-field">
                                <label>Status</label>
                                <select className="theme-field-control" name="status" ref={register({ required: true })} placeholder="Enter Status">
                                    <option value="true">Active</option>
                                    <option value="false">Offline</option>
                                </select>
                            </div>
                        </div>
                        <div className="theme-field">
                            <label>Description</label>
                            <textarea rows="4" cols="50" className="theme-field-control" name="description" ref={register} placeholder="Enter Description"></textarea>
                        </div>
                    </div>
                    <div className="theme-modal-footer">
                        <button type="button" className="close-theme-btn" onClick={props.onHide}>Close</button>
                        <input type="submit" value="Save" className="btn-ripple ezi-pink-btn add-theme-btn" />
                    </div>

                </form>
            </Modal.Body>
        </Modal>


    );
}


export default AddThemeModal;