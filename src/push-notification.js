import firebase from "firebase";

const initializedFirebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyCSiU-BbfKcqlFZZH5gaaMaf42Anw3AjNM",
    authDomain: "qaizenx.firebaseapp.com",
    projectId: "qaizenx",
    storageBucket: "qaizenx.appspot.com",
    messagingSenderId: "1011011993158",
    appId: "1:1011011993158:web:6c16750330304cb1351447",
    measurementId: "G-7F4N4ZNRMZ"
});

const messaging = initializedFirebaseApp.messaging();
export { messaging };