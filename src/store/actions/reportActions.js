import * as ActionType from "./actionTypes"

export const reportScreenShot = (payload = {}) => {
    return {
        type: ActionType.SET_SCREENSHOT_REF,
        payload: payload
    }
}