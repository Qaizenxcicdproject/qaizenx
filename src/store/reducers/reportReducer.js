import * as actionsTypes from "store/actions/actionTypes";

const initalState = {
    screenShotRef: [],
    fullPageScreenShotRef: ""
}

const reportReducer = (state = initalState, action) => {
    switch (action.type) {
        case actionsTypes.SET_SCREENSHOT_REF:
            return {
                ...state,
                screenShotRef: action.payload.screenShotRef,
                fullPageScreenShotRef: action.payload.fullPageScreenShotRef
            }
        default:
            return state
    }
}

export default reportReducer
