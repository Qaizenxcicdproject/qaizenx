
import showdown from "showdown"

export const dateConvert = (date = null) => {
    let currentdate = date || new Date()
    return currentdate.getFullYear() + "-"
        + (currentdate.getMonth() + 1) + "-"
        + currentdate.getDate() + " "
        + currentdate.getHours() + ":"
        + currentdate.getMinutes() + ":"
        + currentdate.getSeconds();
}

export const getDateIST = (date = null) => {
    let currentdate = date || new Date()
    return currentdate.getDate() + "/"
        + (currentdate.getMonth() + 1) + "/"
        + currentdate.getFullYear();
}

export const getFirstWord = (str) => {
    if (str && str.length !== null) {
        let spaceIndex = str.replace(/ .*/, '');
        return spaceIndex;
    }
};

export const toTitleCase = (str) => {
    if (str && str.length !== null) {
        return str.replace(/\w\S*/g, (txt) => {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }
}

export const outlineRemove = {
    control: base => ({
        ...base,
        border: 0,
        boxShadow: "none"
    })
};

export const getNameInitials = string => {
    const names = string.split(' ');
    const initials = names.map(name => name.charAt(0).toUpperCase())
    if (initials.length > 1) {
        return `${initials[0]}${initials[initials.length - 1]}`;
    } else {
        return initials[0];
    }
}

export const compareObject = (obj1, obj2) => {
    for (let p in obj1) {
        if (obj1.hasOwnProperty(p) !== obj2.hasOwnProperty(p)) return false;

        switch (typeof (obj1[p])) {
            case 'object':
                if (!compareObject(obj1[p], obj2[p])) return false;
                break;
            case 'function':
                if (typeof (obj2[p]) === 'undefined' || (p !== 'compareObject' && obj1[p].toString() !== obj2[p].toString())) return false;
                break;
            default:
                if (obj1[p] !== obj2[p]) return false;
        }
    }
    for (let p in obj2) {
        if (typeof (obj1[p]) === 'undefined') return false;
    }
    return true;
};

export const uniqueGenerator = () => {
    let date = Date.now();
    if (date <= uniqueGenerator.previous) {
        date = ++uniqueGenerator.previous;
    } else {
        uniqueGenerator.previous = date;
    }

    return 'unq' + date.toString(20) + Math
        .floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}

export const surveyQuestionModifier = (questions = {}) => {
    let questionsData = questions.pages || [];
    questionsData.forEach((page, index) => {
        if (questionsData[index] && questionsData[index].hasOwnProperty("elements") && questionsData[index]["elements"].length > 0) {
            questionsData[index]["elements"].forEach((item, i) => {
                /** Add Question title if not present */
                if (!questionsData[index]["elements"][i].hasOwnProperty('title')) {
                    questionsData[index]["elements"][i]['title'] = questionsData[index]["elements"][i]['name'] || null
                }
                /** Add Question id in matrix rows if not present */
                if (questionsData[index]["elements"][i]["type"] === 'matrix') {
                    if (questionsData[index]["elements"][i].hasOwnProperty("rows")) {
                        questionsData[index]["elements"][i]["rows"].forEach((mtItem, inum) => {
                            let mtOptions = {}
                            if (typeof mtItem === 'string') {
                                mtOptions['text'] = mtItem
                                mtOptions['value'] = uniqueGenerator()
                            }
                            if (typeof mtItem === 'object') {
                                let rowVal = mtItem['value']
                                mtOptions['text'] = mtItem['text']
                                if (rowVal && !rowVal.startsWith('unq')) {
                                    rowVal = uniqueGenerator()
                                }
                                if (rowVal.length <= 3 || !rowVal) {
                                    rowVal = uniqueGenerator()
                                }
                                mtOptions['value'] = rowVal
                            }
                            questionsData[index]["elements"][i]["rows"][inum] = mtOptions
                        })
                    }
                }
            })
        }
    })
    questions.pages = questionsData
    return questions
}

export const mapParentChild = (questions = {}, mappings = {}) => {
    if (!questions.pages || questions.pages.length === 0) {
        throw new Error('Questions Data is missing.')
    }
    let clonedQuestions = JSON.parse(JSON.stringify(questions))
    let questionsData = clonedQuestions.pages || [];
    questionsData.forEach((page, index) => {
        if (questionsData[index] && questionsData[index].hasOwnProperty("elements") && questionsData[index]["elements"].length > 0) {
            questionsData[index]["elements"].forEach((item, i) => {
                if (questionsData[index]["elements"][i]["hasParent"]) {
                    questionsData[index]["elements"][i]["parent_question_id"] = mappings[questionsData[index]["elements"][i]["id"]] || null
                }
            })
        }
    })
    clonedQuestions.pages = questionsData
    return clonedQuestions
}

export const canSaveBuilderQuestions = (questionsData = null) => {
    if (questionsData && questionsData.hasOwnProperty('pages') && questionsData.pages.length > 0) {
        const Questions = questionsData.pages.reduce((arr, el) => {
            let newEl = (el.hasOwnProperty('elements') && el['elements'].length > 0) ? el['elements'] : [];
            return arr.concat(newEl);
        }, []);

        return Questions.length > 0
    } else {
        return false
    }
}
export const getIdentifierQuestion = (questions = {}) => {
    let questionsData = questions.pages || [];
    let identifierQuestions = []
    questionsData.forEach((page, index) => {
        if (questionsData[index] && questionsData[index].hasOwnProperty("elements") && questionsData[index]["elements"].length > 0) {
            questionsData[index]["elements"].forEach((item, i) => {
                if (item.hasOwnProperty('isIdentifier') && item['isIdentifier']) {
                    identifierQuestions.push(item)
                }
            })
        }
    })
    return identifierQuestions
}
export const getPersonalQuestion = (questions = {}) => {
    let questionsData = questions.pages || [];
    let personalQuestion = []
    questionsData.forEach((page, index) => {
        if (questionsData[index] && questionsData[index].hasOwnProperty("elements") && questionsData[index]["elements"].length > 0) {
            questionsData[index]["elements"].forEach((item, i) => {
                if (item.hasOwnProperty('question_type') && item['question_type'] === 'PERSONALINFO') {
                    personalQuestion.push(item)
                }
            })
        }
    })
    return personalQuestion
}

export const getTablePagesSizes = (total) => {
    return [
        {
            text: "25",
            value: 25
        },
        {
            text: "50",
            value: 50
        },
        {
            text: "100",
            value: 100
        },
        {
            text: "200",
            value: 200
        }
    ]
}

export const doMarkdown = (_, options) => {
    let converter = new showdown.Converter();
    let str = converter.makeHtml(options.text);
    if (str.indexOf("<p>") === 0) {
        str = str.substring(3);
        str = str.substring(0, str.length - 4);
    }
    options.html = str;
}

export const CkEditor_ModalEditor = {
    afterRender: function (modalEditor, htmlElement) {
        var editor = window.CKEDITOR.replace(htmlElement);
        var isUpdating = false;
        editor.on("change", function () {
            isUpdating = true;
            modalEditor.editingValue = editor.getData();
            isUpdating = false;
        });
        editor.setData(modalEditor.editingValue);
        modalEditor.onValueUpdated = function (newValue) {
            if (!isUpdating) {
                editor.setData(newValue);
            }
        };
    },
    destroy: function (modalEditor, htmlElement) {
        var instance = window.CKEDITOR.instances[htmlElement.id];
        if (instance) {
            instance.removeAllListeners();
            instance.destroy(true);
            window.CKEDITOR.remove(instance);
        }
    }
}; 